/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include <array>
#include <string>
#include <stdexcept>
#include <functional>

namespace CMD
{
  inline std::string exec(const std::string &cmd, std::function<void(std::string)> cbStdOut = nullptr) {
    std::array<char, 128> buffer{};
    std::string result{};
    auto cmdStdErr = cmd + " 2>&1";

    #ifdef _WIN32
        auto pipe = _popen(cmd.c_str(), "r");
    #else
        auto pipe = popen(cmdStdErr.c_str(), "r");
    #endif

    if(!pipe) {
      throw std::runtime_error("Failed to open process");
    }

    while(!feof(pipe)) {
      if(fgets(buffer.data(), buffer.size(), pipe)) {
        if(cbStdOut)cbStdOut(buffer.data());
        result += buffer.data();
      }
    }

    #ifdef _WIN32
        auto code = _pclose(pipe);
    #else 
        auto code = pclose(pipe);
    #endif

    if(code)throw std::runtime_error(result);
    return result;
  }
}