/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#pragma once

#include <functional>
#include <string>
#include <utility>
#include <atomic>
#include <vector>
#include <span>
#include "types.h"

typedef std::function<void(u8, const std::string&, std::span<const u8>, std::function<void(std::string)>, std::function<void(std::vector<u8>&)>)> WsCommandCb;

class WsServer
{
  private:
    std::string secretKey;
    std::atomic_bool serverReady{false};
    std::atomic<u32> port{0};

    WsCommandCb cbCommand;
    std::atomic<u32> activeConnections{0};

  public:
    explicit WsServer(std::string secretKey, WsCommandCb cbCommand)
    : secretKey{std::move(secretKey)}, cbCommand{std::move(cbCommand)}
    {}

    void run();
    void stop();

    bool isReady() { return serverReady.load(); }
    u32 getPort() { return port.load(); }

    std::string getUrl() {
      return "ws://127.0.0.1:" + std::to_string(getPort());
    }

    u32 getActiveConnections() {
      return activeConnections.load();
    }
};