/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
#include "wsServer.h"

//#include <boost/asio/ip/address_v4.hpp>
#include <exception>
#include <string>
#include <utility>
#include <vector>
#include <websocketpp/server.hpp>
#include <websocketpp/config/asio_no_tls.hpp>

#include "logger.h"

typedef websocketpp::server<websocketpp::config::asio> server;
typedef server::message_ptr message_ptr;

using websocketpp::lib::bind;

namespace
{
  void onMessage(server &ws, websocketpp::connection_hdl hdl, message_ptr msg, const WsCommandCb& cb)
  {
    const auto &message = msg->get_payload();
    if (msg->get_opcode() != websocketpp::frame::opcode::binary) {
      printf_warn("Received non-binary message, ignore (%s)", message.c_str());
      return;
    }

    if (message.size() < 5) {
      printf_error("Message too small (header): %d", (u32)message.size())
      return;
    }

    const u8 *data = (const u8 *) message.data();

    u32 id = ((u32) data[0] << 8u) | data[1];
    u8 command = data[2];
    u8 argSize = data[3];
    u32 offsetData = 4u + argSize;

    if (message.size() < offsetData) {
      printf_error("Message too small (arg): %d", (u32)message.size());
      return;
    }

    auto arg = message.substr(4, argSize-1);

    std::span<const u8> dataSpan{
      data + offsetData,
      message.size() - offsetData
    };

    try {
      cb(command, arg, dataSpan, [&ws, hdl](std::string stdout){
         ws.send(hdl, stdout, websocketpp::frame::opcode::text);
      }, [&ws, hdl, id](std::vector<u8> &resData){
        resData[0] = (id >> 8u) & 0xFF; // set response header
        resData[1] = id & 0xFF;

        try {
          ws.send(hdl, resData.data(), resData.size(), websocketpp::frame::opcode::binary);
        } catch (websocketpp::exception const & e) {
          printf_error("Failed to send data: %s", e.what())
        }
      });
    }
    catch(const std::exception &e)
    {
      printf_error("Error during message handling: %s", e.what())
      std::vector<u8> resData{
        static_cast<u8>((id >> 8u) & 0xFF),
        static_cast<u8>(id & 0xFF),
        0xFF, 0
      };
      resData.insert(resData.end(), e.what(), e.what() + std::strlen(e.what())); // returns exception to client
      ws.send(hdl, resData.data(), resData.size(), websocketpp::frame::opcode::binary);
    }
  }

  server* wsInstance{nullptr};
}

void WsServer::run()
{
  server ws;
  wsInstance = &ws;

  try {
    ws.set_access_channels(websocketpp::log::alevel::none);
    //ws.set_access_channels(websocketpp::log::alevel::all);
    ws.clear_access_channels(websocketpp::log::alevel::frame_payload);

    ws.init_asio();

    /**
     * Security check for new connections:
     * - Only allow a single active connection at a time.
     * - Only allow when the correct key was provided.
     * - Only allow Origins from "localhost".
     */
    ws.set_validate_handler([this, &ws](websocketpp::connection_hdl hdl) {
      if(activeConnections > 0) {
        printf_error("Too many connections: %d, deny connection", activeConnections.load());
        return false;
      }

      auto conn = ws.get_con_from_hdl(std::move(hdl));
      auto origin = conn->get_origin();
      auto userKey = conn->get_resource();

      if(!origin.starts_with("http://127.0.0.1:")) {
        printf_error("Origin forbidden: %s", origin.c_str());
        return false;
      }

      if(userKey != ("/" + secretKey)) {
        printf_error("Wrong Key: %s, deny connection", userKey.c_str());
        return false;
      }

      printf_info("Connection validated: Origin: %s", origin.c_str());
      return true;
    });

    ws.set_open_handler([this]([[maybe_unused]] const auto& hdl) { ++activeConnections; });
    ws.set_close_handler([this]([[maybe_unused]] const auto& hdl) { --activeConnections; });

    ws.set_message_handler([this, &ws](websocketpp::connection_hdl hdl, message_ptr msg) {
      onMessage(ws, hdl, msg, cbCommand);
    });

    print_info("WS start listening...");

    ws.listen(websocketpp::lib::asio::ip::address_v4::loopback(), 0);

    websocketpp::lib::asio::error_code e;
    auto endpoint = ws.get_local_endpoint(e);
    auto address = endpoint.address().to_string();
    port = endpoint.port();
    serverReady = true;

    if(!endpoint.address().is_loopback()) {
      print_info("Tried to use non-loopback address, abort");
      std::terminate();
    }

    printf_info("WS listening on: %s:%d\n", address.c_str(), port.load());

    ws.start_accept();
    ws.run();

  } catch (websocketpp::exception const & e) {
    printf_error("Websocket-Exception: %s", e.what())
  } catch (std::exception const & e) {
    printf_error("Unhandled Websocket-Exception: %s", e.what())
  } catch (...) {
    print_error("Unknown Websocket-Exception")
  }
}

void WsServer::stop() {
  if(!wsInstance)return;
  wsInstance->stop_listening();
  wsInstance = nullptr;
}
