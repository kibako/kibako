cmake_minimum_required(VERSION 3.25)
project(kibako_native)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_EXTENSIONS OFF) # prevents gnu++ stuff

set(CMAKE_CXX_COMPILER clang++)

add_compile_options(
    -fmacro-prefix-map=${CMAKE_CURRENT_SOURCE_DIR}/=

    "$<$<CONFIG:Debug>:-Og>"
    "$<$<CONFIG:Debug>:-g3>"

    "$<$<CONFIG:Release>:-O3>"
    "$<$<CONFIG:Release>:-flto>"

    # ignore warnings in 3rd-party libs
    --system-header-prefix=websocketpp

     # Warnings
    -Wall -Wextra -Wshadow -Wnon-virtual-dtor
    -Wcast-align -Wunused
    -Woverloaded-virtual -Wpedantic
    -Wconversion -Wsign-conversion
    -Wduplicated-cond -Wduplicated-branches
    -Wlogical-op -Wnull-dereference
    -Wuseless-cast -Wdouble-promotion
    -Wformat=2 -Wlifetime
    -Wreserved-identifier
    -Werror=return-type
    -Wno-unknown-warning-option
)

include_directories(src src/libs src/libs/asio)

add_executable(kibako_native
        src/fs.h
        src/main.cpp
        src/server/wsServer.h src/server/wsServer.cpp src/types.h src/rand.cpp src/rand.h src/logger.h src/cmd.h src/server/httpServer.h src/server/httpServer.cpp src/args.h)

target_link_options(kibako_native PUBLIC
    -fuse-ld=lld
    "$<$<CONFIG:Release>:-flto>"
    "$<$<CONFIG:Release>:-Wl,--strip-all>"
)

target_compile_definitions(kibako_native PUBLIC ASIO_STANDALONE)

set_property(TARGET kibako_native PROPERTY CXX_STANDARD 20)
set_property(TARGET kibako_native PROPERTY CXX_STANDARD_REQUIRED ON)
target_compile_features(kibako_native PUBLIC cxx_std_20)

target_link_libraries(kibako_native)