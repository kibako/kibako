image: node:20.0.0

cache:
  key:
    files:
      - yarn.lock
  paths:
    - .yarn
    - node_modules/

stages:
    - deps
    - test
    - tag
    - build
    - bundle
    - publish

deps:
    stage: deps
    rules:
      - if: $CI_COMMIT_MESSAGE =~ /CI build/
        when: never
      - if: $CI_COMMIT_TAG
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    script:
      - yarn install

test:
    stage: test
    rules:
      - if: $CI_COMMIT_TAG
        when: never
      - if: $CI_COMMIT_MESSAGE =~ /CI build/
        when: never
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    script:
      - yarn test

lint:
    stage: test
    rules:
      - if: $CI_COMMIT_TAG
        when: never
      - if: $CI_COMMIT_MESSAGE =~ /CI build/
        when: never
      - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    script:
      - yarn lint

# Create a new tag when master gets updated (but ignore version bumps)
tag:
    stage: tag
    rules:
      - if: $CI_COMMIT_TAG
        when: never
      - if: $CI_COMMIT_MESSAGE =~ /CI build/
        when: never
      - if: $CI_COMMIT_BRANCH == 'master'
    script:
      # Setup ssh-key to push changes
      - mkdir -p ~/.ssh
      - chmod 700 ~/.ssh
      - eval $(ssh-agent -s)
      - echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add -

      # "trust" gitlab
      - echo "HOST *" > ~/.ssh/config
      - echo "StrictHostKeyChecking no" >> ~/.ssh/config

      # Git-Setup
      - echo "git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git"
      - git remote set-url origin git@$CI_SERVER_HOST:$CI_PROJECT_PATH.git
      - git config --global user.email "${GITLAB_USER_EMAIL}"
      - git config --global user.name "${GITLAB_USER_NAME}"

      # Bump version with NPM
      - NEW_TAG=$(npm version minor --no-git-tag-version)

      # Push master/tag
      - 'git commit -am "tag: CI build"'
      - git push origin HEAD:master

      - git tag -a $NEW_TAG -m "Version ${NEW_TAG}"
      - git push origin "${NEW_TAG}"

# Builds the native app
build_app:
  image: silkeh/clang:16
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - bash scripts/build_cmake_docker.sh
  artifacts:
    paths:
    - app/build/kibako_native

# Builds the web app
build_web:
  stage: build
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - yarn install
    - yarn build
  artifacts:
    paths:
    - build

# Bundle native app and publish as a release + generic-registry artifact
bundle:
  stage: bundle
  rules:
    - if: $CI_COMMIT_TAG
  script:
    # Needed for gitlab releases
    - curl --location --output /usr/local/bin/release-cli "https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/release-cli-linux-amd64"
    - chmod +x /usr/local/bin/release-cli

    - bash scripts/bundle.sh $CI_COMMIT_TAG
    
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "kibako_${CI_COMMIT_TAG}.zip" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/kibako/${CI_COMMIT_TAG}/kibako_${CI_COMMIT_TAG}.zip"'
  artifacts:
    paths:
    - kibako_$CI_COMMIT_TAG.zip
  release:                               # See https://docs.gitlab.com/ee/ci/yaml/#release for available properties
    tag_name: '$CI_COMMIT_TAG'
    description: |
      ---
      <br/>

      # [+ --- Download ---  +]

      Kibako-App: [Download]($CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/kibako/$CI_COMMIT_TAG/kibako_$CI_COMMIT_TAG.zip)

      <br/>


    assets:
      links:
      - name: 'kibako_$CI_COMMIT_TAG.zip'
        url: '$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/kibako/$CI_COMMIT_TAG/kibako_$CI_COMMIT_TAG.zip'

publish:
  stage: publish
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=main "https://gitlab.com/api/v4/projects/46786071/trigger/pipeline"
