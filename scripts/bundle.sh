set -e

apt-get update
apt-get -y install zip

APP_VERSION="$1"

rm -f "kibako_${APP_VERSION}.zip"

# Create bundle directory with Web-assets
rm -rf build_bundle
mkdir build_bundle
cp -r build build_bundle
mv build_bundle/build build_bundle/data

# Copy executable over
cp app/build/kibako_native build_bundle/kibako
cd build_bundle

# remove debug files
rm ./data/wasm/kibako64.debug.wasm
rm ./data/asset-manifest.json
# rm ./data/static/js/*.js.map
rm ./data/static/js/*.js.LICENSE.txt

# Create final zip
zip -r "../kibako_${APP_VERSION}.zip" data kibako
cd ..

# Cleanup
rm -rf build_bundle

