set -e

yarn install

yarn start &
sleep 3

./app/cmake-build-release/kibako_native --url=http://127.0.0.1:3000 --browser

kill $!