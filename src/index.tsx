import React from 'react';
import { createRoot } from 'react-dom/client';

import './index.css';
import './frontend/style/selectbox.css';

import { App } from './frontend/App';
import FileAPI from 'backend/fileapi/fileAPI';
import { KibakoWebsocket } from './backend/native/kibakoWebsocket';

const container = document.getElementById('root');
const root = createRoot(container!);

async function main()
{
  const ws = new KibakoWebsocket();
  try {
    await ws.init();
  } catch(e) {
    console.log("Failed to setup native connection, assume Web-Mode", e);
  }

  await FileAPI.initAPI();

  //root.render(<React.StrictMode><App /></React.StrictMode>);
  root.render(<App />);
}

main().catch(e => console.error(e));