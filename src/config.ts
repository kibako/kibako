/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const Config = {
  EMU_DEBUG_MODE: false,
  F3D_DEBUG_MODE: false,
  DISABLE_ACTORS: false,
  AUTO_OPEN_LAST: false,
  SHOW_KEYS: false,
};

export default Config;