import React, { useCallback, useState } from "react";
import styled from 'styled-components';
import { useEventListener } from "../../helper/useEventListener";

const STAY_TIME = 1500;

// Taken and modifed from: https://www.cssscript.com/show-which-key-is-pressed/

const Container = styled.div`
  position: fixed;
  left: 5px;
  bottom: 5px;
  _width: 300px;
  overflow: hidden;
  user-select: none;

  [data-keys] {
    display: flex;
    background: #2e2e2e;
    border-radius: 10px;

    padding: 10px 10px 12px;
    font-size: 24px;
    font-family: Roboto;
    transform: scale(0.0);
    animation: keys-zoom-in ${STAY_TIME}ms ease;
  }
  [data-keys][data-children="0"] {
    opacity: 0;
  }
  [data-keys] [data-key] + [data-key] {
    margin-left: 10px;
  }
  [data-keys] [data-key] {
    height: 54px;
    min-width: 54px;
    padding: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #2e2e2e;
    background: #EEE;
    border-radius: 5px;
    border-top: 1px solid #f5f5f5;
    box-shadow: inset 0 0 25px #e8e8e8, 0 1px 0 #c3c3c3, 0 4px 0 #c9c9c9;
    text-shadow: 0px 1px 0px #f5f5f5;
  }
  @keyframes keys-zoom-in {
    from {
      transform: scale(0.0);
    }
    7% {
      transform: scale(1.0);
    }
    80% {
      transform: scale(1.0);
    }
    100% {
      transform: scale(0.0);
    }
  }
`;

interface KeyEntry {
  id: number;
  keys: string[];
}

const KeyNameMap: Record<string, string> = {
  ArrowUp: '↑',
  ArrowRight: '→',
  ArrowDown: '↓',
  ArrowLeft: '←',
  Shift: '⇧',
  Meta: '⌘',
  Alt: 'Alt',
  Control: 'Ctrl',
  Escape: 'Esc',
  Backspace: '⌫',
  Enter: '⏎',
  32: 'Space',
  CapsLock: 'caps lock',
  Tab: 'Tab',
};

const MouseNameMap = [
  "Left-Click",
  "Middle-Click",
  "Right-Click",
];

function getModMap(event: KeyboardEvent | MouseEvent)
{
  return {
    Meta: event.metaKey,
    Shift: event.shiftKey,
    Alt: event.altKey,
    Control: event.ctrlKey,
  } as Record<string, boolean>;

}

let nextId = 0;
let currentRemLast = () => {};

export const DebugKeysComponent = () => {
  const [keyStrings, setKeyStrings] = useState<KeyEntry[]>([]);
  
  const removeLastEntry = useCallback(() => {
    const newKeys = [...keyStrings];
    newKeys.splice(0, 1);
    setKeyStrings(newKeys);
  }, [keyStrings, setKeyStrings]);

  currentRemLast = removeLastEntry;

  const render = useCallback((keys: string[]) => {
    setKeyStrings([...keyStrings, {id: nextId++, keys}]);
    setTimeout(() => {currentRemLast()}, STAY_TIME);
  }, [keyStrings, setKeyStrings]);
  
  const eventHandler = useCallback((event: any) => 
  {
    if(event.repeat || ['INPUT', 'TEXTAREA'].includes(event.target.tagName)) {
      return;
    }
  
    const key = KeyNameMap[event.key] || KeyNameMap[event.which] || event.key.toUpperCase();
    const modifiers = getModMap(event);
  
    const newKeys = Object.keys(modifiers)
      .filter((modifier) => modifiers[modifier])
      .map((modifier) => KeyNameMap[modifier]);
  
    if (!Object.keys(modifiers).includes(event.key)) newKeys.push(key);
  
    if((event.ctrlKey || event.shiftKey) && newKeys.length <= 1) {
      return;
    }

    // Ignore WASD movement, but allow ctrl+Key combos (also ignore shift, which is moving fast)
    if(!event.ctrlKey || event.shiftKey) {
      if(['w', 'a', 's', 'd', 'e', 'q'].includes(event.key.toLowerCase()))return;
    }
  
    render(newKeys);
  }, [render]);

  const eventHandlerMouse = useCallback((event: any) => 
  {
    if(!event.ctrlKey && !event.shiftKey) {
      if(event.button === 0)return; // dont print regular left-click
    }
    const modifiers = getModMap(event);
  
    const newKeys = Object.keys(modifiers)
      .filter((modifier) => modifiers[modifier])
      .map((modifier) => KeyNameMap[modifier]);
  
    newKeys.push(MouseNameMap[event.button] || "?");  
    render(newKeys);
  }, [render]);

  useEventListener('keydown', eventHandler);
  useEventListener('mousedown', eventHandlerMouse);

  return <Container>{
    keyStrings.map((keys, i) => <div data-keys key={keys.id}>
      {keys.keys.map((key) => <div data-key key={key}>{key}</div>)}
    </div>)
  }</Container>;
};

export const DebugKeys = React.memo(DebugKeysComponent, (prev, next) => {
  return true;
});