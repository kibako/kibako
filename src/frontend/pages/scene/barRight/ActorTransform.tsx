/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';

import { GenericActor } from 'backend/z64/actors/actorList';
import { disableTransform, enableTransform } from '../../../renderer/ControlsFPS';
import { requestHistoryAdd } from '../../../../backend/editor/history';

interface SelectedActorProps {
  actors: GenericActor[],
  onChange: (actors: GenericActor[]) => void;
};

let lastLength = 0;

const ActorTransformComponent = (props: SelectedActorProps) =>  
{
    const {onChange, actors} = props;

    if(lastLength !== actors.length) {
      lastLength = actors.length;
      disableTransform(true);
    }

    if(actors.length === 0) {
      disableTransform(true);
    } else {
      const objs = actors.map(a => structuredClone(a));
      enableTransform(objs, newObjs => {
        onChange(newObjs as GenericActor[]);
      }, () => requestHistoryAdd());
    }

    return (<></>);
};

export const ActorTransform = React.memo(ActorTransformComponent, (prev, next) => {
  return  prev.onChange === next.onChange
   //&& prev.actors.map(a => a.uuid).join(",") === next.actors.map(a => a.uuid).join(",")
   && prev.actors === next.actors;
   ;
});