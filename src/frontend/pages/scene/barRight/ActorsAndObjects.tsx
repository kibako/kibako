/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useCallback, useState } from 'react';
import { v4 as uuid } from 'uuid';
import { ArrowDown, ArrowRepeat, ArrowUp, Boxes, Hammer, Trash } from 'react-bootstrap-icons';
import { Actor, GenericActor } from '../../../../backend/z64/actors/actorList';
import { RoomEntry } from '../../../../backend/z64/rooms/roomList';
import { ActorType } from '../../../../backend/z64/types';
import { Button } from '../../../elements/Button';
import { ListItem } from '../../../elements/ListElements';
import { SelectBox } from '../../../elements/SelectBox';
import { actorTable } from '../../../../backend/decomp/actorTable';
import { getRoomLayer } from '../../../../backend/z64/actors/actorState';
import { objectTableDecomp } from '../../../../backend/decomp/objectTable';
import { SmallIconButton } from '../../../elements/IconButton';
import styled from 'styled-components';
import { fixObjectDependencies, getMaxObjectCount } from '../../../../backend/z64/actors/actorDeps';
import { SelectBoxSearch } from '../../../elements/SelectBoxSearch';
import { ListAccordion } from '../../../elements/ListAccordion';
import { getGameActorData } from '../../../../backend/z64/actors/actorData';
import { requestHistoryAdd } from '../../../../backend/editor/history';

const ObjName = styled.div`
  width: 190px;
  overflow: hidden;
  color: #BBB;
`;

const IdIcon = styled.div`
  color: #BBB;
  width: 40px;
  height: 28px;
  font-size: 12px;
  margin-right: 8px;
  text-align: center;
  padding-top: 6px;
  border-radius: 5px;
  background-color: rgba(0,0,0, 0.2);
`;

interface Props {
  rooms: RoomEntry[];
  layer: number;
  onAddActors: (newActors: GenericActor[]) => void;
  onRoomChange: (newRoom: RoomEntry) => void;
};

type SelectValType =  Array<[string, string]>;
type SelectSearchType = Array<{name: string, value: string}>;

let selectData: SelectSearchType = [];
let lastSelectedActor = "ACTOR_OBJ_KIBAKO";
let lastSelectedRoom = "0";
let objectSelectEntries: SelectSearchType = [];

const ActorsAndObjectsComponent = (props: Props) => {
  const {onAddActors, onRoomChange} = props;

  const actorData = getGameActorData();

  const [selectedActor, setSelectedActor] = useState(lastSelectedActor);
  const [selectedRoom, setSelectedRoom] = useState(lastSelectedRoom);
  const [selectedObject, setSelectedObject] = useState("OBJECT_AHG");

  // Globals
  lastSelectedActor = selectedActor;
  lastSelectedRoom = selectedRoom;
  const selectedRoomId = parseInt(selectedRoom);

  if(objectSelectEntries.length === 0) {
    objectSelectEntries = objectTableDecomp.list
      .filter(e => e && !e.name.startsWith("OBJECT_GAMEPLAY_"))
      .sort((a,b) => a!.name.localeCompare(b!.name))
      .map(e => ({value: e!.name, name: e!.name.replace("OBJECT_", "")}))
  }

  const roomSelectValues = [
//    ["S", "Scene"],
    ...props.rooms.map(r => [r.roomId+"", r.roomId.toString(10 + 26).toUpperCase()])
  ] as SelectValType;

  if(selectData.length === 0) {
    for(const id in actorData) {
      selectData.push({value: id, name: actorData[id].name});
    }
    selectData = selectData.sort((a,b) => a.name.localeCompare(b.name));
  }

  const addActor = useCallback(() => 
  {
    const actorEntry = actorData[selectedActor];
    if(!actorEntry) {
      console.error("Could not find actor: ", selectedActor);
      return;
    }

    const actorId = actorTable.enum[selectedActor] || 0;
    if(actorId === 0) {
      console.error("Actor not in actor Table!");
      return;
    }

    const newActor: Actor = {
      uuid: uuid(),
      type: ActorType.ROOM,
      id: actorId,
      roomId: parseInt(selectedRoom),
      pos: [0,0,0],
      rot: [0,0,0],
      initValue: 0,
      cutsceneIndex: 127, // MM-only: no cutscene(?)
      spawnTime: 0x3FF, // MM-only: all days
      rotMask: 0, // MM-only: default rotation
    };
    onAddActors([newActor]);
  }, [onAddActors, selectedActor, selectedRoom, actorData]);

  const mainRoom = props.rooms.find(r => r.roomId === selectedRoomId);
  if(!mainRoom && props.rooms.length) {
    setSelectedRoom(props.rooms[0].roomId+"");
  }

  const roomLayer = mainRoom ? getRoomLayer(mainRoom, props.layer) : undefined;

  const moveObject = useCallback((dep: number, idxOld: number, idxNew: number) => 
  {
    if(!roomLayer)return;

    if(idxNew < 0)idxNew = roomLayer.objectDependencies.length - 1;
    if(idxNew >= roomLayer.objectDependencies.length)idxNew = 0;

    const newDeps = [...roomLayer.objectDependencies];
    newDeps.splice(idxOld, 1);
    newDeps.splice(idxNew, 0, dep);
    onRoomChange({...roomLayer, objectDependencies: newDeps});
  }, [roomLayer, onRoomChange]);

  const removeObject = useCallback((dep: number) => 
  {
    if(!roomLayer)return;

    const newRoom = {...roomLayer};
    newRoom.objectDependencies = newRoom.objectDependencies.filter(d => d !== dep);
    onRoomChange(newRoom);
    requestHistoryAdd();
  }, [roomLayer, onRoomChange]);

  const addObject = useCallback(() => 
  {
    if(!roomLayer)return;

    const dep = objectTableDecomp.enum[selectedObject] || 0;
    if(!dep) {
      console.warn("Could not find object!", selectedObject);
      return;
    }

    onRoomChange({
      ...roomLayer, 
      objectDependencies: [...roomLayer.objectDependencies, dep]
    });
    requestHistoryAdd();
  }, [roomLayer, selectedObject, onRoomChange]);

  const fixObjects = useCallback(() => 
  {
    if(!roomLayer)return;
    onRoomChange(fixObjectDependencies({...roomLayer}));
    requestHistoryAdd();
  }, [roomLayer, onRoomChange]);

  const recreateObjects = useCallback(() => 
  {
    if(!roomLayer)return;
    onRoomChange(fixObjectDependencies({...roomLayer}, true));
    requestHistoryAdd();
  }, [roomLayer, onRoomChange]);

  return (
    <ListAccordion text="Actors & Objects" Icon={Boxes}>

      <ListItem style={{paddingRight: 20}}> 
        <SelectBoxSearch
          name="Actor" options={selectData} 
          value={selectedActor} onChange={setSelectedActor} 
        />
      </ListItem>

      <ListItem>
        <SelectBox
          name="Room" options={roomSelectValues} 
          value={selectedRoom} onChange={setSelectedRoom} 
        />
        <Button small minWidth={100} text={"Add Actor"} onClick={addActor}/>
      </ListItem>

      <ListItem>
        <span style={{marginRight: 12}}>Room-Objects</span>

        <SmallIconButton title="Repair / Fix missing" onClick={fixObjects}>
          <Hammer size={18} />
        </SmallIconButton>
        <SmallIconButton title="Recreate" onClick={recreateObjects}>
          <ArrowRepeat size={18} />
        </SmallIconButton>

        <div style={{width: 75, textAlign: 'right'}}>
          {roomLayer?.objectDependencies.length || 0}/{getMaxObjectCount()}
        </div>

      </ListItem>
      
      {roomLayer && roomLayer.objectDependencies.map((dep, i) => 
        <ListItem key={dep + "_" + i} style={{height: 34}}>

          <IdIcon>{dep.toString(16).toUpperCase().padStart(2, "0")}</IdIcon>

          <ObjName>{objectTableDecomp.list[dep]?.name.replace("OBJECT_", "") || (dep+"")}</ObjName>

          <SmallIconButton title="Up" onClick={() => moveObject(dep, i, i-1)}>
            <ArrowUp size={18} />
          </SmallIconButton>
          <SmallIconButton title="Down" onClick={() => moveObject(dep, i, i+1)}>
            <ArrowDown size={18} />
          </SmallIconButton>
          <SmallIconButton title="Delete" onClick={() => removeObject(dep)}>
            <Trash size={18} />
          </SmallIconButton>
        </ListItem>)
      }

      {roomLayer && 
        <ListItem>
          <SelectBoxSearch
            name="Object" options={objectSelectEntries} 
            value={selectedObject} onChange={setSelectedObject} 
          />
          <Button small minWidth={100} text={"Add Object"} onClick={addObject}/>
        </ListItem>
      }
      
    </ListAccordion>
  );
}

export const ActorsAndObjects = React.memo(ActorsAndObjectsComponent, (prev, next) => {
  return prev.rooms === next.rooms
    && prev.onAddActors === next.onAddActors
    && prev.onRoomChange === next.onRoomChange
    && prev.layer === next.layer;
});