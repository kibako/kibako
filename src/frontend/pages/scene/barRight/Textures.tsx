/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { gpuColorFormatNames, GpuTexture } from 'backend/z64/gpu/data/texture';
import { ListContainer, ListItem } from 'frontend/elements/ListElements';
import { ListHeader } from 'frontend/elements/ListHeader';
import React, { useState } from 'react';
import { Images, ChevronDown, ChevronUp } from 'react-bootstrap-icons';
import styled from 'styled-components';
import { ImageCanvas } from './ImageCanvas';

const ImageContainer = styled(ListItem)`
    display: flex;
    padding: 4px 16px;
    width: 100%;
    justify-content: space-between;

    & canvas {
      border: 1px solid black;
    }
`;

interface TexturesComponentProps {
    name?: string;
    textures: GpuTexture[]
};

const TexturesComponent = (props: TexturesComponentProps) => {

  const [open, setOpen] = useState(false);

  return (
    
      <ListContainer>
        <div onClick={() => setOpen(!open)} style={{cursor: 'pointer'}}>
            <ListHeader text={"Textures (" + props.textures.length+")"} 
              Icon={Images} IconRight={open ? ChevronUp : ChevronDown} 
            />
        </div>
        {open && 
            props.textures
                .map((tex, idx) => 
                <ImageContainer key={idx}>
                  <span>
                    {tex.tile.width}x{tex.tile.height} <br/>
                    {gpuColorFormatNames[tex.tile.format]} 
                    <span style={{color: "#999"}}> ({tex.tile.bitSize}bpp)</span>
                  </span>
                  <ImageCanvas width={tex.tile.width} height={tex.tile.height} buffer={tex.rgbaBuffer} />                  
                </ImageContainer>
            )
        }
        </ListContainer>
    
  );
}

export const Textures = React.memo(TexturesComponent, (prev, next) => {
    if(prev.name !== next.name || prev.textures.length !== next.textures.length) {
      return false;
    }

    for(let i=0; i<prev.textures.length; ++i) {
      if(prev.textures[i].rgbaBuffer !== next.textures[i].rgbaBuffer) {
        return false;
      }
    }

    return true;
});