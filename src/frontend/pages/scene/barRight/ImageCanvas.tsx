/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';

const Canvas = styled.canvas`
  width: 128px;
  background-color: black;

  &:hover {
    background-color: white;
  }
`;


interface ImageCanvasComponentProps {
  width: number;
  height: number;
  buffer: Uint8Array;
};

const ImageCanvasComponent = (props: ImageCanvasComponentProps) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    if(canvas && props.buffer.byteLength > 0) {
      const ctx = canvas.getContext('2d');
      const image = new ImageData(new Uint8ClampedArray(props.buffer.buffer), props.width, props.height);
      ctx?.putImageData(image,0,0);
    }
  }, [props]);
  
  return <Canvas ref={canvasRef} width={props.width} height={props.height} ></Canvas>;
};

export const ImageCanvas = React.memo(ImageCanvasComponent, (prev, next) => {
  const res = prev.width === next.width
    && prev.height === next.height
    && prev.buffer.byteLength === next.buffer.byteLength;

  if(res && next.buffer.byteLength > 8 && prev.buffer.byteLength > 8) {
    return res 
      && prev.buffer[0] === next.buffer[0]
      && prev.buffer[2] === next.buffer[2]
      && prev.buffer[7] === next.buffer[7];
  }

  return res;
});
