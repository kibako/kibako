/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useCallback } from 'react';
import { MusicNoteBeamed } from 'react-bootstrap-icons';
import styled from 'styled-components';
import { requestHistoryAdd } from '../../../../backend/editor/history';

// Components

// Backend
import { SceneEntry } from '../../../../backend/z64/scenes/sceneList';
import { getGameSoundDataAmbient, getGameSoundDataBGM } from '../../../../backend/z64/sound/soundData';
import { IntInput } from '../../../elements/IntInput';
import { ListAccordion } from '../../../elements/ListAccordion';
import { ListItem } from '../../../elements/ListElements';
import { SelectBoxSearch } from '../../../elements/SelectBoxSearch';

const WideListItem = styled(ListItem)`
  padding-right: 20px;
   > div {
     width: 100%;
   }
`;

interface Props {
    scene: SceneEntry,
    onChange: (newScene: SceneEntry) => void;
};

const SceneSoundComponent = (props: Props) => {
  const {scene, onChange} = props;

  const selectDataBGM = getGameSoundDataBGM();
  const selectDataAmbient = getGameSoundDataAmbient();

  const setBGM = useCallback((id: string) => {
    onChange({...scene, soundBGM: parseInt(id) || 0});
    requestHistoryAdd();
  }, [scene, onChange]);

  const setAmbience = useCallback((id: string) => {
    onChange({...scene, soundAmbience: parseInt(id) || 0});
    requestHistoryAdd();
  }, [scene, onChange]);

  const setPreset = useCallback((soundPreset: number) => {
    onChange({...scene, soundPreset});
    requestHistoryAdd();
  }, [scene, onChange]);

  return (
    <ListAccordion text="Sound Settings" Icon={MusicNoteBeamed}>

        <ListItem style={{paddingRight: 20}}> 
            <SelectBoxSearch
              name="BGM" options={selectDataBGM} 
              value={scene.soundBGM+""} onChange={setBGM} 
            />
        </ListItem>

        <ListItem style={{paddingRight: 20}}> 
        <SelectBoxSearch
              name="Abience" options={selectDataAmbient} 
              value={scene.soundAmbience+""} onChange={setAmbience} 
            />
        </ListItem>

        <WideListItem> 
          <IntInput name={'Preset'} value={scene.soundPreset} onChange={setPreset} />
        </WideListItem>
    </ListAccordion>
  );
}

export const SceneSound = React.memo(SceneSoundComponent, (prev, next) => {
    return prev.onChange === next.onChange &&
        prev.scene === next.scene;
});