/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
// 3D-Components
import { Actors } from '../../renderer/objects/Actors';
import { Z64MeshRender } from '../../renderer/objects/Z64MeshRender';

// Backend
import { Z64Mesh } from 'backend/z64/mesh/mesh';
import { RoomEntry } from 'backend/z64/rooms/roomList';
import { ObjectSettings } from './CanvasOverlay';
import { Actor } from 'backend/z64/actors/actorList';
import { SceneEntry } from '../../../backend/z64/scenes/sceneList';

interface RoomViewerProps {
    scene: SceneEntry,
    room: RoomEntry,
    altRoomId: number,
    objSettings: ObjectSettings,
    selectedActors: Actor[],
    dayMask: number, // MM only
};

export const RoomViewer = (props: RoomViewerProps) => {
    const {room, objSettings, altRoomId, dayMask} = props;

    let selectedRoom = room;
    if(altRoomId >= 0) {
        // search backwards until we find the first non-null entry
        selectedRoom = room.altRooms.slice(0, altRoomId+1).reverse().find(x => x) || room;
    }

    return <>
        {room.mesh && <Z64MeshRender 
            mesh={room.mesh as Z64Mesh}  
            visible={objSettings.roomMesh}
        />}
        {selectedRoom && <Actors 
            scene={props.scene}
            actors={selectedRoom.actors} 
            room={selectedRoom}
            modelVisible={objSettings.roomActors}
            boxVisible={objSettings.roomActorsBoxes}
            colliderVisible={objSettings.collider}
            selectedActors={props.selectedActors}
            dayMask={dayMask}
        />}
    </>;
}
