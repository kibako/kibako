/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect, useState, useCallback, useRef } from 'react';
import styled from 'styled-components';

// Components
import { Viewport3D } from '../../renderer/Viewport3D';
import { SelectedActor } from './barRight/SelectedActor';
import { CanvasOverlay, defaultObjectSettings, ObjectSettings } from './CanvasOverlay';
import { SceneMenuBar } from './barLeft/SceneMenuBar';
import { useCallbackState } from 'frontend/helper/state';
import { Colors } from 'frontend/style/colors';

// Icons
import { Lightbulb, Diagram2Fill, Grid1x2, CalendarDay, BoxFill, MusicNoteBeamed, BoundingBox } from 'react-bootstrap-icons';

// 3D-Components
import { PointCloud } from '../../renderer/objects/PointCloud';
import { CollMesh } from '../../renderer/objects/CollMesh';
import { WaterBoxes } from '../../renderer/objects/WaterBoxes';
import { Skybox } from '../../renderer/env/Skybox';
import { Actors } from '../../renderer/objects/Actors';

// Backend
import { loadAllScenes, loadScene, SceneEntry, SceneListEntry } from '../../../backend/z64/scenes/sceneList';
import { Actor, GenericActor } from '../../../backend/z64/actors/actorList';
import { RoomEntry, getRoomsFromScene } from '../../../backend/z64/rooms/roomList';
import { SceneLighting } from 'backend/z64/lighting/lighting';
import { Lighting } from './barRight/Lighting';
import { Z64Emulator } from 'backend/z64/emulator/z64Emulator';
import { RoomViewer } from './RoomViewer';
import { GLOBAL_OBJ_SCALE } from 'frontend/renderer/converter/z64Mesh';
import { emulateScene } from 'backend/z64/scenes/sceneEmulator';
import { Textures } from './barRight/Textures';
import { GpuTexture } from 'backend/z64/gpu/data/texture';
import { ListAccordion } from 'frontend/elements/ListAccordion';
import { RoomList } from './barLeft/RoomList';
import { createCounterArray } from 'frontend/helper/js';
import { AltSceneList } from './barLeft/AltSceneList';
import { TopBar } from './barTop/TopBar';
import { getConfig, patchConfig } from '../../../backend/fileapi/configManager';
import { disableTransform } from '../../renderer/ControlsFPS';
import { findActorByUUID, PatchAction, patchActors, patchPos, patchRooms, selectionAdd, selectionAddMulti } from '../../../backend/z64/actors/actorState';
import { preventDefault } from '../../helper/events';
import { vec3Add, vec3Diff } from '../../../backend/utils/math';
import { addCallbackCanvasClick, getMousePosition3D, raycastActor, raycastPathPoints, removeCallbackCanvasClick } from '../../renderer/Raycast';
import { ActorTransform } from './barRight/ActorTransform';
import { ActorsAndObjects } from './barRight/ActorsAndObjects';
import { Context } from '../../../backend/context';
import { DayList } from './barLeft/DayList';
import { historyAdd, historyClear, requestHistoryAdd } from '../../../backend/editor/history';
import { BusName, EventBus } from '../../helper/eventBus';
import { useEventBus } from '../../helper/useEventBus';
import { TabEntry, TabSelector } from '../../elements/TabSelector';
import { SceneSound } from './barRight/SceneSound';
import { Paths } from '../../renderer/objects/Paths';
import { ScenePaths } from './barRight/ScenePaths';
import { ScenePath } from '../../../backend/decomp/parser/parsePaths';
import { PathRaycaseResult } from '../../../backend/z64/path/pathState';
import { useCanvasKeyUp } from '../../helper/useEventListener';
import { getSceneLayer, patchScene } from '../../../backend/z64/scenes/sceneState';

export const RightBarContainer = styled.div/*_css*/`
  background-color: ${Colors.BG_LIGHT};
  height: calc(100% - 48px);
  overflow-x: hidden;
  overflow-y: auto;
  margin-top: 48px;
  padding-bottom: 55px;
  width: 300px;
  resize: horizontal;
`;

export const Content = styled.main/*_css*/`
  height: 100vh;
  display: flex;
  overflow: auto;
  flex-grow: 1;
  flex-direction: column;
`;

export const CanvasWrapper = styled.div/*_css*/`
  width: 100%;
  height: 100%;
  position: relative;
  margin-top: 48px;
`;

export const CanvasOverlayContainer = styled.div/*_css*/`
  top: 0;
  left: 0;
  width: 100%;
  padding: 8px 4px;
  z-index: 100;
  position: absolute;
  pointer-events: none;
`;

export const LeftBarContainer = styled.div/*_css*/`
  background-color: ${Colors.BG_LIGHT};
  height: 100%;
  _height: 400px;
  overflow: hidden;
  overflow-y: scroll;
  padding-top: 42px;
  padding-bottom: 5px;
  width: 300px;
  resize: horizontal;

  _display: flex;
  _flex-direction: column;
`;

const tabEntries: TabEntry[] = [
  {id: "actor", text: "Actors", Icon: BoxFill},
  {id: "sound", text: "Sound", Icon: MusicNoteBeamed},
  {id: "path", text: "Path", Icon: BoundingBox},
  {id: "light", text: "Light", Icon: Lightbulb},
];

let copiedActors: GenericActor[] = [];

export const PageSceneViewer = () => {
    const [scenes, setScenes] = useState([] as SceneListEntry[]);
    const [selectedSceneId, setSelectedSceneId] = useCallbackState(0);
    const [selectedLayer, setSelectedLayer] = useCallbackState(-1);
    const [selectedRoomIds, setSelectedRoomIds] = useState(() => createCounterArray(32));
    const [selectedTab, setSelectedTab] = useState(tabEntries[0].id);

    const [scene, setScene] = useState<SceneEntry>();
    const [rooms, setRooms] = useState([] as RoomEntry[]);

    const [selectedActors, setSelectedActors] = useState<Actor[]>([]);

    const [sceneLightings, setSceneLightings] = useState<SceneLighting[]>([]);
    const [sceneLightingIdx, setSceneLightingIdx] = useState<number>(0);
    const [dayMask, setDayMask] = useState<number>(0x3FF); // MM only (all days by default)
    const [objSettings, setObjSettings] = useState<ObjectSettings>(defaultObjectSettings);

    const sceneLoadUUID = useRef("");
    const sceneLayer = scene ? getSceneLayer(scene, selectedLayer) : scene;
    const selectedRooms = rooms.filter((room, idx) => selectedRoomIds.includes(idx));

    const deselectActors = useCallback(() => {
      setSelectedActors([]);
      disableTransform(true);
    }, [setSelectedActors]);

    const setSceneRooms = useCallback((newScene: SceneEntry, newRooms: RoomEntry[]) => {
      setScene(newScene);
      setRooms(newRooms);

      // try to map the old selected actor to the new rooms/scene (by UUID)
      const newSelection = selectedActors
        .map(a => findActorByUUID(newScene, newRooms, a.uuid))
        .filter(a => a) as Actor[];

      if(newSelection.length) {
        setSelectedActors(newSelection);
      } else {
        deselectActors();
      }
    }, [deselectActors, setScene, setRooms, selectedActors]);

    const setPaths = useCallback((paths: ScenePath[]) => {
      if(scene && sceneLayer) {
        setScene(patchScene(scene, {...sceneLayer, paths}));
      }
    }, [scene, sceneLayer, setScene]);

    const setSceneLayer = useCallback((newScene: SceneEntry) => {
      if(scene && sceneLayer) {
        setScene(patchScene(scene, {...newScene}));
      }
    }, [scene, sceneLayer, setScene]);

    const onOverlaySettings = useCallback((settings: ObjectSettings) => {
        setObjSettings(settings);
    }, [setObjSettings]);

    const onRoomChange = useCallback((newRoom: RoomEntry) => {
      const newRooms = patchRooms(newRoom, rooms);
      setRooms(newRooms);
    }, [rooms]);

    const onActorsChange = useCallback((actors: Actor[]) => {
      if(!scene)return;

      const newActors = patchActors(PatchAction.PATCH,
        scene, rooms, selectedLayer, setScene, setRooms, actors
      );

      if(newActors.length) {
        setSelectedActors(selectionAddMulti(selectedActors, newActors));
      }
    }, [scene, rooms, selectedLayer, selectedActors]);

    const addActors = useCallback((newActors: GenericActor[]) => {
      if(!scene)return;

      const posOffset = vec3Diff(getMousePosition3D(), newActors[0].pos);
      newActors = newActors
        .map(a => structuredClone(a))
        .map(a => patchPos(a, vec3Add(a.pos, posOffset)));

      patchActors(PatchAction.ADD,
        scene, rooms, selectedLayer, 
        setScene, setRooms, newActors
      );

      disableTransform(true);
      setSelectedActors(newActors);
    }, [rooms, scene, selectedLayer]);

    // Actor hotkeys (Clone, Copy, Paste, Delete)
    const onKeypress = useCallback((e: KeyboardEvent) => 
    {
      if(!scene || selectedTab === "path") {
        return preventDefault(e);
      }
      
      if(!e.ctrlKey && e.code === "Delete" && selectedActors.length > 0) {
        patchActors(PatchAction.DELETE,
          scene, rooms, selectedLayer, 
          setScene, setRooms, selectedActors
        );
        deselectActors();
        requestHistoryAdd();
      }

      if(e.ctrlKey && e.code === "KeyD" && selectedActors.length) {
        patchActors(PatchAction.ADD,
          scene, rooms, selectedLayer, 
          setScene, setRooms, selectedActors.map(a => structuredClone(a))
        );
        requestHistoryAdd();
      }

      if(e.ctrlKey && e.code === "KeyC") {
        copiedActors = selectedActors.map(a => structuredClone(a));
      }

      if(e.ctrlKey && e.code === "KeyV" && copiedActors.length > 0) {
        addActors(copiedActors);
        requestHistoryAdd();
      }

      preventDefault(e);
    }, [selectedActors, rooms, scene, selectedLayer, deselectActors, addActors, selectedTab]);

    // Async History-Add handler
    const historyAddCallback = useCallback(() => {
      if(scene)historyAdd(scene, rooms);
    }, [scene, rooms]);

    useEventBus(BusName.HISTOR_ADD, historyAddCallback);
    
    const clickCallback = useCallback((holdingShift: boolean) => {
      if(!sceneLayer)return;

      if(selectedTab === "path") 
      {
        if(selectedActors.length > 0)deselectActors();
        // Paths
        EventBus.send(BusName.PATH_SELECTION, {
          holdingShift, entries: raycastPathPoints(),
        } as PathRaycaseResult);
      } else {
        // Actors
        const hits = raycastActor();
        
        // cycle between actors if clicked on the same actor
        let hit = hits.find(h => !selectedActors.map(a => a.uuid).includes(h.uuid)) || hits[0];
        const actor = findActorByUUID(sceneLayer, selectedRooms, hit?.uuid || "");
        if(!actor) {
          deselectActors();
        } else {
          setSelectedActors(holdingShift ? selectionAdd(selectedActors, actor) : [actor]);      
        }
      }

    }, [selectedRooms, sceneLayer, selectedActors, deselectActors, selectedTab]);
  
    // Hooks for actor selection
    useEffect(() => {
      addCallbackCanvasClick(clickCallback);
      return () => removeCallbackCanvasClick(clickCallback);
    }, [clickCallback]);

    // Hooks for actor hotkeys
    useCanvasKeyUp(onKeypress);

    useEffect(() => {
      let sceneEntry = scenes[selectedSceneId];
      if (!sceneEntry || !sceneEntry.offsetStart) return;

      const loadData = async () => 
      {
        historyClear();
        console.time("Scene-Load"); 
        let newScene = await loadScene(sceneEntry);
        setScene(newScene);
        console.timeEnd("Scene-Load");
      }
      loadData().catch(e => console.error(e));
    }, [scenes, selectedSceneId, setScene]);

    // Load rooms and scene-data on scene-id change
    useEffect(() => {
        const loadData = async () => 
        {
          if(!scene)return;

          const oldSceneLoadUUID = sceneLoadUUID.current;
          sceneLoadUUID.current = scene.sceneId + "_" + selectedLayer + "_" + sceneLightingIdx;
          
          // prevent complete scene updates when only actors change
          if(oldSceneLoadUUID === sceneLoadUUID.current) {
              return;
          }

          if(selectedLayer >= scene.altSetups.length) {
            setSelectedLayer(-1);
            return;
          }

          console.time("Scene-Post-Load");  
          Z64Emulator.getInstance().clearSavestates();

          const sceneLayer = getSceneLayer(scene, selectedLayer);
          const newRooms = await getRoomsFromScene(scene); // @TODO: don't reload if only layer changes
  
          await emulateScene(scene, newRooms, selectedLayer, sceneLayer.lighting[sceneLightingIdx]);

          setRooms(newRooms);
          setSceneLightings(sceneLayer.lighting);
          
          if(sceneLightingIdx >= sceneLayer.lighting.length) {
            setSceneLightingIdx(0);
          }

          historyClear();
          historyAdd(scene, newRooms); // initial history

          console.timeEnd("Scene-Post-Load"); 
        };

        loadData().catch(e => console.error(e));

    }, [scene, sceneLightingIdx, selectedLayer, setSelectedLayer]);

    const changeScene = useCallback((id: number) => {
        if(id === selectedSceneId)return;
        setRooms([]);
        deselectActors();
        setSelectedSceneId(id);
        patchConfig('lastScene', id); // don't wait for promise

    }, [selectedSceneId, setSelectedSceneId, setRooms, deselectActors]);

    const changeLayer = useCallback((id: number) => {
        if(id === selectedLayer)return;
        setRooms([]);
        deselectActors();
        setSelectedLayer(id);
    }, [selectedLayer, setSelectedLayer, setRooms, deselectActors]);

    useEffect(() => 
    {
      (async () => 
      {
        const [scenes, lastScene] = await Promise.all([
          loadAllScenes(), getConfig("lastScene", "0")
        ]);

        let lastSceneId = parseInt(lastScene);
        let sceneId = scenes.findIndex(scene => scene.offsetStart && scene.sceneId === lastSceneId)

        if(sceneId === -1) {
          sceneId = scenes.findIndex(scene => scene.offsetStart);
        }

        setScenes(scenes);
        setSelectedSceneId(sceneId);
      })().catch(e => console.error(e));
    }, [setScenes, setSelectedSceneId]);

    const roomTextures: GpuTexture[] = [];
    for(const room of rooms) {
      if(room.mesh) {
        roomTextures.push(...room.mesh.gpuObjectOpaque.textures, ...room.mesh.gpuObjectTransparent.textures);
      }
    }

    return (
        <React.Fragment>
            {scene && <TopBar scene={scene} rooms={rooms} setSceneRooms={setSceneRooms} />}
            <LeftBarContainer>
                <SceneMenuBar 
                    scenes={scenes}
                    selectedSceneId={selectedSceneId}
                    setSelectedSceneId={changeScene}
                />
                {scene && 
                    <ListAccordion text="Scene Layers" Icon={Diagram2Fill}>
                        <AltSceneList altAltSceneIdx={selectedLayer} 
                        altIds={scene.altSetups.map(a => a ? a.altId : undefined)} 
                        onChange={changeLayer} 
                        />
                    </ListAccordion>
                }
                <ListAccordion text="Rooms" Icon={Grid1x2}>
                    <RoomList selectedRoomIds={selectedRoomIds} onChange={setSelectedRoomIds} roomCount={rooms.length} />
                </ListAccordion>

                {
                  Context.isMM() && 
                  <ListAccordion text="Spawn-Time Filter" Icon={CalendarDay}>
                    <DayList dayMask={dayMask} onChange={setDayMask}  />
                  </ListAccordion>
                }
                
            </LeftBarContainer>

            <Content>
                <CanvasWrapper>
                    <CanvasOverlayContainer>
                        <CanvasOverlay 
                            initialSetting={objSettings}
                            onChange={onOverlaySettings}
                        />
                    </CanvasOverlayContainer>
                    <Viewport3D>
                        <Skybox color={sceneLightings[sceneLightingIdx]?.ambientColor || [0.5, 0.5, 0.5]} />
                        <group scale={[GLOBAL_OBJ_SCALE, GLOBAL_OBJ_SCALE, GLOBAL_OBJ_SCALE]}>
                            {
                                scene && scene.collision && 
                                (<React.Fragment>
                                    {objSettings.collision && <PointCloud collData={scene.collision} />}
                                    <CollMesh collData={scene.collision} visible={objSettings.collision} />
                                </React.Fragment>)  
                            }

                            { objSettings.water && scene && scene.collision && <WaterBoxes waterBoxes={scene.collision.waterBoxes} /> }        
                            { (objSettings.sceneActor && sceneLayer && rooms && rooms[0]) && 
                                <Actors 
                                    actors={sceneLayer.actors} 
                                    scene={sceneLayer}
                                    room={rooms[0]}
                                    modelVisible={objSettings.roomActors}
                                    colliderVisible={objSettings.collider}
                                    boxVisible={true}
                                    selectedActors={selectedActors}
                                    dayMask={0xFFFF}
                                /> 
                            }

                            {(sceneLayer && objSettings.paths) && 
                              <Paths paths={sceneLayer.paths} onChange={setPaths} editMode={selectedTab === "path"} />
                            }

                            {sceneLayer && selectedRooms
                                .map(room => <RoomViewer
                                    key={room.uuid}
                                    scene={sceneLayer}
                                    room={room}
                                    altRoomId={selectedLayer}
                                    objSettings={objSettings}
                                    selectedActors={selectedActors}
                                    dayMask={dayMask}
                                />)
                            }
                        </group>
                    </Viewport3D>
                    
                </CanvasWrapper>
            </Content>

            <RightBarContainer>
              <TabSelector selectedId={selectedTab} entries={tabEntries} onSelect={setSelectedTab} 
              /> 
                {
                  (selectedTab === "actor") && <>
                    {selectedActors.map(selectedActor => 
                      <SelectedActor 
                        key={selectedActor.uuid}
                        actor={selectedActor} 
                        onChange={onActorsChange} 
                      />
                    )}

                    <ActorTransform 
                      actors={selectedActors}
                      onChange={onActorsChange} 
                    />

                    {(selectedRooms.length > 0) && 
                      <ActorsAndObjects
                        rooms={selectedRooms}
                        layer={selectedLayer}
                        onAddActors={addActors}
                        onRoomChange={onRoomChange}
                      ></ActorsAndObjects>
                    }

                    <Textures textures={roomTextures} />
                  </>
                }

                {(selectedTab === "sound" && sceneLayer) && 
                  <SceneSound scene={sceneLayer} onChange={setSceneLayer} />
                }

                {(selectedTab === "path" && sceneLayer) && 
                  <ScenePaths paths={sceneLayer.paths} onChange={setPaths} />
                }

                {((selectedTab === "light") && sceneLightings) && 
                  <ListAccordion text="Lighting" Icon={Lightbulb}>
                        <Lighting 
                            sceneLightings={sceneLightings}
                            lightingIndex={sceneLightingIdx}
                            setLightingIndex={setSceneLightingIdx}
                        />
                    </ListAccordion>}

            </RightBarContainer>
        </React.Fragment>
    );
}