/**
* @copyright 2022 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import styled from "styled-components";
import React, { useCallback } from 'react';
import { Colors } from "frontend/style/colors";
import { MoonFill, SunFill } from "react-bootstrap-icons";

const DayContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  pointer-events: auto;
  padding: 6px 5px 0 10px;
`;

const DayButton = styled.span`
  cursor: pointer;
  user-select: none;

  width: 36px;
  height: 28px;
  margin: 2px;
  display: inline-block;
  text-align: center;
  padding-top: 5px;
  border-radius: 7px;

  color: rgba(255,255,255, 0.6);
  background-color: rgba(0,0,0, 0.25);

  transition: 
    color 70ms linear 0ms,
    opacity 50ms ease 0ms,
    background-color 70ms linear 0ms;

  &:hover {
    _color: white;
    _background-color: rgba(0,0,0, 0.15);
    opacity: 0.8;
  }

  span {
    position: relative;
    left: 0px;
    top: -1px;
    font-size: 14px;
  }

  svg {
    margin-left: 2px;
  }
`;

const DayButtonWide = styled(DayButton)`
  width: 76px;
`;

interface Props {
  dayMask: number,
  onChange: (newDayMask: number) => void;
};

const DAY_IDS = [9,8,7,6,5,4,3,2,1,0];

function getDay(idx: number) {
  return Math.floor((9 - idx) / 2);
}

const DayListComponent = (props: Props) => {

  const {dayMask, onChange} = props;
  
  const toggleDay = useCallback((mask: number) => {
    if(!!(dayMask & mask)) {
      onChange(dayMask & ~mask);
    } else {
      onChange(dayMask | mask);
    }
  }, [dayMask, onChange]);

  const toggleAll = useCallback(() => {
    if(dayMask > 0) {
      onChange(0);
    } else {
      onChange(0x3FF);
    }
  }, [dayMask, onChange]);

  return <div>
  <DayContainer>
  {
    DAY_IDS.map(idx => {
      return (
      <DayButton 
        key={idx}
        title={(idx % 2 === 0 ? "Night " : "Day ") + getDay(idx)}
        style={!!(dayMask & (1 << idx)) ? {
            color: '#111',
            backgroundColor: Colors.PRIMARY_LIGHT,
        } : {}}
        onClick={() => toggleDay(1 << idx)}
      >
        <span>{getDay(idx)}</span>
        {idx % 2 === 0 ? <MoonFill /> : <SunFill/>}
      </DayButton>)
    })
  }
  <DayButtonWide onClick={toggleAll}>Toggle</DayButtonWide>
</DayContainer>
</div>;
};

export const DayList = React.memo(DayListComponent, (prev, next) => {
  return prev.dayMask === next.dayMask &&
    prev.onChange === next.onChange;
});