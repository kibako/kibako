/**
* @copyright 2022 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import styled from "styled-components";
import React, { useCallback } from 'react';
import { createCounterArray } from "frontend/helper/js";
import { Colors } from "frontend/style/colors";

const RoomContainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  pointer-events: auto;
  padding: 6px 5px 0 10px;
`;

const RoomButton = styled.span`
  cursor: pointer;
  user-select: none;

  width: 30px;
  height: 30px;
  margin: 2px;
  display: inline-block;
  text-align: center;
  padding-top: 6px;
  border-radius: 7px;

  color: rgba(255,255,255, 0.6);
  background-color: rgba(0,0,0, 0.25);


  transition: 
    color 70ms linear 0ms,
    opacity 50ms ease 0ms,
    background-color 70ms linear 0ms;

  &:hover {
    _color: white;
    _background-color: rgba(0,0,0, 0.15);
    opacity: 0.8;
  }
`;

const RoomButtonWide = styled(RoomButton)`
  width: 64px;
`;

interface RoomProps {
  roomCount: number;
  selectedRoomIds: number[];
  onChange: (roomIds: number[]) => void;
};

const RoomListComponent = (props: RoomProps) => {

  const {selectedRoomIds, onChange} = props;
  const roomIds = createCounterArray(props.roomCount);

  const toggleAllRooms = useCallback(() => {
    if(selectedRoomIds.length) {
      onChange([]);
    } else {
      onChange(createCounterArray(32));
    }
  }, [onChange, selectedRoomIds]);

  const toggleRoomId = useCallback((id: number) => {
    if(selectedRoomIds.includes(id)) {
      onChange(selectedRoomIds.filter(val => val !== id));
    } else {
      onChange([...selectedRoomIds, id]);
    }
  }, [onChange, selectedRoomIds]);

  return <div>
  <RoomContainer>
  {
    roomIds.map(idx => {
      return (
      <RoomButton 
        key={idx}
        style={selectedRoomIds.includes(idx) ? {
            color: '#111',
            backgroundColor: Colors.PRIMARY_LIGHT,
        } : {}}
        onClick={() => toggleRoomId(idx)}
      >
        {idx.toString(10 + 26).toUpperCase()}
      </RoomButton>)
    })
  }
  <RoomButtonWide onClick={toggleAllRooms}>Toggle</RoomButtonWide>
</RoomContainer>
</div>;
};

export const RoomList = React.memo(RoomListComponent, (prev, next) => {
  return prev.roomCount === next.roomCount &&
    prev.selectedRoomIds.length === next.selectedRoomIds.length &&
    prev.onChange === next.onChange;
});