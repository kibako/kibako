/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';

import { CCircle, Git, TagFill, Youtube } from 'react-bootstrap-icons';

import packageJson from '../../../../package.json';
import { IconButton } from 'frontend/elements/IconButton';
import FileAPI from 'backend/fileapi/fileAPI';
import { readAddressMap } from '../../../backend/decomp/addressMap';
import { parseObjectTable } from '../../../backend/decomp/objectTable';
import { Colors } from '../../style/colors';
import { showNotification } from '../../layout/Notifications';
import { getFeatures } from '../../helper/features';
import { getConfig } from '../../../backend/fileapi/configManager';
import { parseMakefile } from '../../../backend/decomp/makefile';
import Config from '../../../config';

export const LINK_GITLAB = "https://gitlab.com/kibako/kibako";
export const LINK_YOUTUBE = "https://www.youtube.com/channel/UCfPKx8Hfy_YFns-WSCuZfrA";
export const LINK_APP = "https://gitlab.com/kibako/kibako/-/releases";

export const Main = styled.main`
    background-color: ${Colors.BG_LIGHT};
    width: 100%;
    height: 100%;
    border: 0;
    margin: 0;
    padding: '10px';
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    align-items: center;
    justify-content: space-between;
`;

export const TitleContainer = styled.div`
  background-color: #252525;
  background-image: url(img/title_bg.png);
  background-repeat: repeat-x;
  background-position-y: bottom;
  height: 230px;

  padding: 14px;
  
  width: 100%;
  display: flex;
  justify-content: center;

  & img {
    height: 160px;
    margin-right: 15px;
  }
`;

export const Title = styled.h1`
    text-shadow: rgba(0,0,0, 0.3) 6px 6px 2px;

    color: #fedd54;
    font-size: 5.0em;
    font-weight: bold;
    line-height: 1;
    margin-top: 10px;
    text-align: center;
    margin-bottom: 3px;

    text-align: left;

    &:last-child {
      
      font-size: 4em;
      font-weight: bold;
      color: #c08f35;
    }
`;

const FileSelect = styled.div`
  display: flex;

  & img {
    width: 300px;
    margin: 5px;
    cursor: pointer;
  }
`;

const ImgStack = styled.div`
  position: relative;
  width: 320px;
  height: 230px;
  margin: 5px;

  user-select: none;
  -webkit-user-select: none;

  & span {
    font: Roboto;
    width: 50%;
    text-align: center;
    position: absolute;
    left: 25%;
    bottom: 10px;
    font-size: 20px;
    background: #e79;
    color: black;
    border-radius: 10px;

    transition: left 50ms linear 0ms, 
                bottom 50ms linear 0ms;
  }

  &:hover span {
    bottom: -15px;
  }

  & .imgFront:hover {
    filter: brightness(1.15);
    left: -10px;
    top: -10px;
  }

  & .rotLeft:hover {
    transform: rotate(-2deg);
  }

  & .rotRight:hover {
    transform: rotate(2deg);
  }


  & .imgFront:active {
    filter: brightness(1.22);
    left: -5px;
    top: -5px;
  }

  & img {
    transition: left 50ms linear 0ms, 
                top 50ms linear 0ms,
                filter 60ms linear 0ms,
                transform 20ms linear 0ms;

    margin: 10px;
    position: absolute;
    left: 0px;
    top: 0px;

    &.imgBgA {
      filter: brightness(0.15);
      left: 10px;
      top: 10px;
    }

    &.imgBgB {
      filter: brightness(0.1);
    }
  }
`;

const Notice = styled.div`
  font-size: 16px;
  background-color: rgb(143, 62, 62);
  border-radius: 25px;
  padding: 12px 25px;
  height: 140px;
  margin-top: 54px;
  margin-right: 12px;
  box-shadow: rgba(0,0,0,0.3) 6px 6px 5px;

  & p {
    margin-top: 12px;
    margin-bottom: 0px;
  }
`;

export const Credits = styled.div`
    background-color: #252525;
    background-image: url(img/title_bg_bottom.png);
    background-repeat: repeat-x;
    background-position-y: top;

    height: 100px;

    width: 100%;
    display: flex;
    background-color: #252525;
    padding: 5px;
    padding-top: 44px;
    margin-top: 4px;
    align-items: center;
    justify-content: center;
`;

interface PageRomSelectProps {
  onData: (data: Uint8Array) => void;
};

export const PageRomSelect = (props: PageRomSelectProps) => {

  const {onData} = props;
  const features = getFeatures();
  const isSupported = features.isNative || features.hasFileApi;
  const [lastPath, setLastPath] = useState("");

  useEffect(() => {
    getConfig('decompDir').then(path => {
      console.log(path);
      setLastPath(path || "");

      if(Config.AUTO_OPEN_LAST) {
        console.clear();
        openDecomp(true);
      }
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setLastPath]);

  const openDecomp = useCallback(async (tryLoadOld: boolean) => 
  {
    await FileAPI.openDir(tryLoadOld);
    const res = await FileAPI.scanDir();

    if(!res.files.includes("Makefile")) {
      showNotification('error', "No Makefile found!");
      return;
    }

    const makefileData = parseMakefile(await FileAPI.readFileText("Makefile"));
    if(!makefileData.rom) {
      showNotification('error', "Could not detect ROM name in Makefile");
      return;
    }

    const romPath = makefileData.rom;

    if(res.files.includes(romPath)) {
      try {
        console.time("Initial Load");
        const [romBuffer] = await Promise.all([
          FileAPI.readFileBuffer(romPath),
          readAddressMap(),
          parseObjectTable()
        ]);

        const romBufferArray = new Uint8Array(romBuffer);
        console.timeEnd("Initial Load");
        onData(romBufferArray);
      } catch(e: any) {
        showNotification('error', "Failed to load ROM '"+romPath+"'");
        console.error(e);
      }
    } else {
      showNotification('error', "No ROM '"+romPath+"' found");
    }
  }, [onData]);

  return (
    <Main>
      <TitleContainer>
        <img src="./img/box.svg" draggable="false" alt="Logo"/>
        <div>
          <Title>Kibako</Title>
          <Title>きばこ</Title>
        </div>
      </TitleContainer>
      
      <FileSelect>
      {!isSupported &&
        <Notice>
          <p>This browser does not support the <a target={"_blank"} rel="noreferrer" href="https://developer.mozilla.org/en-US/docs/Web/API/window/showDirectoryPicker#browser_compatibility">File System Access API</a>.</p>
          <p>Please use a chromium-based browser, or download the native app.</p>
          <p>The native app works in all browsers, and has additional features.</p>
        </Notice>
      }

        {isSupported &&
          <ImgStack onClick={() => openDecomp(false)} id="menu_open_new">
            <img draggable="false" className='imgBgA' alt="" src="./img/card_bg.svg"/>
            <img draggable="false" className='imgBgB' alt="" src="./img/card_bg.svg"/>
            <span>Open Decomp</span>
            <img draggable="false" className='imgFront rotLeft' src="./img/card.svg" alt="Open New"/>
          </ImgStack>
        }

        {(isSupported && lastPath) &&
          <ImgStack onClick={() => openDecomp(true)} id="menu_open_last">
            <img draggable="false" className='imgBgA' alt="" src="./img/card_bg.svg"/>
            <img draggable="false" className='imgBgB' alt="" src="./img/card_bg.svg"/>
            <span>Open Last</span>
            <img draggable="false" className='imgFront' src="./img/card_last.svg" alt="Open Last"/>
          </ImgStack>
        }

        <ImgStack onClick={() => window.open(LINK_APP, "_blank")} id="menu_download_app">
          <img draggable="false" className='imgBgA' alt="" src="./img/card_bg.svg"/>
          <img draggable="false" className='imgBgB' alt="" src="./img/card_bg.svg"/>
          <span>{features.isNative ? "Update App" : "Download App"}</span>
          <img draggable="false" className='imgFront rotRight' src="./img/card_app.svg" alt="Open New"/>
        </ImgStack>
      </FileSelect>
      
        <Credits>
            <IconButton text={"v" + packageJson.version + (features.isNative ? "-linux" : "")}  Icon={TagFill} 
              onClick={()=> window.open(LINK_GITLAB, "_blank")}
            />

            <IconButton text={"HailToDodongo"}  Icon={CCircle} 
                onClick={()=> window.open(LINK_YOUTUBE, "_blank")} 
            />

            <IconButton text="YouTube" Icon={Youtube} 
                onClick={()=> window.open(LINK_YOUTUBE, "_blank")} 
            />
            <IconButton text="GitLab" Icon={Git} 
                onClick={()=> window.open(LINK_GITLAB, "_blank")}
            />
        </Credits>
    </Main>
  );
};
