/**
 * Prevents most default shortcuts, but allows certain ones (reload, devtools, ...)
 * @param e key event
 */
export function preventDefault(e: KeyboardEvent) 
{
  if(e.ctrlKey && e.code === "KeyS") {
    e.preventDefault();
  }
  
  if(e.code === "Tab")return; // tabbing in inputs

  // keep Input/Selectboxes
  if(e.target instanceof HTMLInputElement)return;
  if(e.target instanceof HTMLSelectElement)return;
  if(e.target instanceof HTMLButtonElement)return;

  if(e.ctrlKey && e.code === "KeyR")return; // Reload page
  if(e.code === "F5")return; // also reload
  if(e.code === "F12")return; // Devtools
  if(e.code === "F11")return; // Fullscreen
  
  e.preventDefault();
}

export function ctrlAndKey(e: KeyboardEvent, key: string) {
  return e.ctrlKey && e.key === key;
}

export function ctrlShiftAndKey(e: KeyboardEvent, key: string) {
  return e.ctrlKey && e.shiftKey && e.key.toLowerCase() === key;
}