/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { useEffect } from "react";
import { BusName, Callback, EventBus } from "./eventBus";

export function useEventBus(busName: BusName, callback: Callback)
{
  useEffect(() => {
    EventBus.subscribe(busName, callback);
    return () => { EventBus.unsubscribe(busName, callback);}
  }, [callback, busName]);
};