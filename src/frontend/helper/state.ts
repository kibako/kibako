/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { useCallback, useState } from "react";

export function useCallbackState<T>(initValue: T): [T, (n: T) => void, (n: T) => void]
{
  const [value, setValue] = useState<T>(initValue);
  const toggleValue = useCallback((flag: T) => setValue(flag), []);
  return [value, toggleValue, setValue];
};