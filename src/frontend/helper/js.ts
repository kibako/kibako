/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export function createCounterArray(count: number): number[] {
  return new Array(count).fill(0).map((_, i) => i);
}