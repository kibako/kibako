/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { KibakoWebsocket } from "../../backend/native/kibakoWebsocket";

export function getFeatures()
{
  return {
    isNative: !!KibakoWebsocket.getInstance(),
    hasFileApi: !!window.showDirectoryPicker
  }
}