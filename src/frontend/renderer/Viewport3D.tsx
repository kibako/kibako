/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { FunctionComponent } from 'react';
import { Canvas } from '@react-three/fiber';
import { ControlsFPS } from './ControlsFPS';
import { Environment } from './env/Environment';
import { Raycast } from './Raycast';

interface Viewport3DProps {
  children?: React.ReactNode
};

const ControlsMemo = React.memo(() => <ControlsFPS/>);

const Viewport3DComponent: FunctionComponent<Viewport3DProps> = (props) => {
  
  return (
    <Canvas id='canvas'>
        <Environment/>
        <ControlsMemo/>
        <Raycast/>
        { props.children }
    </Canvas>
  );
};

export const Viewport3D = React.memo(Viewport3DComponent, (prev, next) => {
  return prev.children === next.children;
});