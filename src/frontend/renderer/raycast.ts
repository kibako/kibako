import { Layers } from "three";

export enum RaycastLayer {
  DEFAULT=0, ACTOR=1
}

export function layerActor() {
  const layer = new Layers();
  layer.set(RaycastLayer.ACTOR);
  return layer;
}
