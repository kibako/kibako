/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect, useCallback } from 'react';
import { useThree, Camera } from '@react-three/fiber';
import { Scene, Raycaster, Vector2 } from 'three';
import { GLOBAL_OBJ_SCALE } from './converter/z64Mesh';
import { layerActor } from './raycast';
import { PathRaycaseEntry } from '../../backend/z64/path/pathState';

type ClickCallback = (holdingShift: boolean) => void;

const state = {
  mousePos: new Vector2(0, 0),
}

const raycaster = new Raycaster();
const raycasterActor = new Raycaster();
raycasterActor.layers = layerActor();

let currentScene: Scene|undefined;
let currentCamera: Camera|undefined;

let clickCallbacks = new Set<ClickCallback>();

// max. amount of move events, before it is no longer considered a click.
// This prevents click-events from being fired during a dragging action.
const MAX_MOVE_BEFORE_CLICK = 6;
let moveClickCounter = 0;

export function getMousePosition3D(): [number, number, number]
{
  if(!currentScene || !currentCamera)return [0,0,0];

  const collMesh = currentScene.getObjectByName("CollMesh");
  if(!collMesh) {
    console.log("No Collision found");
    return [0,0,0];
  }

  raycaster.setFromCamera(state.mousePos, currentCamera);
  const intersects = raycaster.intersectObjects([collMesh]);

  if(intersects.length) {
    const pt = intersects[0].point;
    return [
      pt.x / GLOBAL_OBJ_SCALE, 
      pt.y / GLOBAL_OBJ_SCALE, 
      pt.z / GLOBAL_OBJ_SCALE,
    ];
  }

  return [0,0,0];
}

export function raycastActor()
{
  if(!currentScene || !currentCamera)return [];

  raycasterActor.setFromCamera(state.mousePos, currentCamera);
  const intersects = raycasterActor.intersectObjects(currentScene.children);

  const res = [];
  for(const hit of intersects) {
    const parent = hit.object.parent;
    if(!parent || !parent.userData?.actorUUID)continue;

    res.push({
      uuid: parent.userData.actorUUID,
      distance: hit.distance
    });
  }

  return res;
}

export function raycastPathPoints(): PathRaycaseEntry[]
{
  if(!currentScene || !currentCamera)return [];

  raycasterActor.setFromCamera(state.mousePos, currentCamera);
  const intersects = raycasterActor.intersectObjects(currentScene.children);

  const res = [];
  for(const hit of intersects) {
    const parent = hit.object.parent;
    if(!parent || !parent.userData?.pathUUID)continue;

    res.push({
      uuid: parent.userData.pathUUID,
      pointIdx: parent.userData.pointIdx || 0,
      distance: hit.distance
    });
  }

  return res;
}

export function addCallbackCanvasClick(cb: ClickCallback) {
  clickCallbacks.add(cb);
}

export function removeCallbackCanvasClick(cb: ClickCallback) {
  clickCallbacks.delete(cb);
}

const RaycastComponent = () => {

  const { camera, gl, scene } = useThree();

  //console.log("RaycastComponent");

  currentScene = scene;
  currentCamera = camera;

  const canvasNode = gl.domElement;

  const updateMousePos = (e: MouseEvent, canvasNode: HTMLCanvasElement) => {
    state.mousePos.x = ( e.offsetX / canvasNode.width ) * 2 - 1;
    state.mousePos.y = - ( e.offsetY / canvasNode.height ) * 2 + 1;
  }

  // Update mouse postion in relative coords.
  const handleMouse = useCallback((e: MouseEvent) => {
    updateMousePos(e, canvasNode);
    ++moveClickCounter;
  }, [canvasNode]);

  const handleMouseDown = useCallback(() => {
    moveClickCounter = 0;
  }, []);

  const handleMouseLeave = useCallback(() => {
    // when adding new actors, the mouse is outside the canvas.
    // set to screen-center to avoid weird positions
    state.mousePos.x = 0;
    state.mousePos.y = 0;
  }, []);

  // Capture clicks
  const handleClick = useCallback((e: MouseEvent) => {
    updateMousePos(e, canvasNode);

    if(moveClickCounter < MAX_MOVE_BEFORE_CLICK) {
      for(const cb of clickCallbacks)cb(e.shiftKey);
    }

    moveClickCounter = 0;
  }, [canvasNode]);

  useEffect(() => {
    canvasNode?.addEventListener("mousemove", handleMouse);
    canvasNode?.addEventListener("mousedown", handleMouseDown);
    canvasNode?.addEventListener("mouseleave", handleMouseLeave);
    canvasNode?.addEventListener("click", handleClick);
    return () => {
      canvasNode?.removeEventListener("mousemove", handleMouse);
      canvasNode?.removeEventListener("mousedown", handleMouseDown);
      canvasNode?.removeEventListener("mouseleave", handleMouseLeave);
      canvasNode?.removeEventListener("click", handleClick);
    }
  }, [canvasNode, handleMouse, handleMouseDown, handleMouseLeave, handleClick]);

  return (<></>);
};

export const Raycast = React.memo(RaycastComponent, (prev, next) => {
  return true;
});