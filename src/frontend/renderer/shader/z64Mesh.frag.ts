/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import flags from './faceFlags';

const shader = /*glsl*/`#version 300 es

precision highp float;
precision highp int;
precision mediump sampler3D;

#define isFrontFace gl_FrontFacing

#define USE_CMUX_2_CYCLE 1

${flags}

// Lighting structs
struct LightDirectional 
{
  vec3 dir;
  vec3 color;
};

// Inputs

in vec4 combColorA1;
in vec4 combColorB1;
in vec4 combColorC1;
in vec4 combColorD1;

in vec4 combColorA2;
in vec4 combColorB2;
in vec4 combColorC2;
in vec4 combColorD2;

in vec3 uvA;
in vec3 uvB;

in vec2 uvEndA;
in vec2 uvEndB;

flat in ivec2 faceSetting;
in vec3 worldPos;

uniform sampler3D textureMap;
uniform vec2 lensAndAlpha;

out highp vec4 fragmentColor;

// UV Mapping helper:
vec2 uvMirror(vec2 uvEnd, vec2 uvIn)
{
  vec2 uvMod2 = mod(uvIn, uvEnd * 2.0);
  return mix(uvMod2, (uvEnd * 2.0) - uvMod2, step(uvEnd, uvMod2));
}

vec2 wrapUV(vec2 uvEnd, const vec2 pixelStep, vec2 uv, vec2 clamped, vec2 mirror) 
{
  vec2 uvOut = uv;
  vec2 uvOutMC = uv;

  vec2 minUVMirror = pixelStep;//vec2(0.01, 0.01); // @TODO: fixme!

  vec2 uvClamped = clamp(uv, pixelStep, uvEnd); // CLAMP
  vec2 mirrorClamp = step(uvEnd, uvClamped);

  // this works for UV that either have clamp or mirror enabled, but not both...
    uvOut = mix(uvOut, clamp(uvOut, pixelStep, uvEnd + minUVMirror), clamped); // CLAMP
    uvOut = mix(uvOut, minUVMirror + uvMirror(uvEnd, uvOut), mirror); // MIRROR

  // this works specifically with clamp+mirror (otherwise edges are wrong, or the mirror-phase is off)
    uvOutMC = max(uvOutMC, minUVMirror); // mirror prevents negative repet
    uvOutMC = mix(uvOutMC, uvEnd, mirrorClamp); // clamp with mirror-phase
    uvOutMC = mix(uvOutMC, uvMirror(vec2(1.0, 1.0) - vec2(1.0 / 512.0, 1.0 / 512.0), uvOutMC), mirror); // @TODO: fix 512.0 values

  vec2 mirrorAndClamp = step(vec2(2.0, 2.0), clamped + mirror);
  return mix(uvOut, uvOutMC, mirrorAndClamp);
}

void main() 
{
  ivec3 size = textureSize(textureMap, 0);
  vec2 pixelStep = 1.0 / vec2(size) * 0.5; // @TODO: get actual step from size before scaling

  vec3 uvXYNormA = vec3(wrapUV(uvEndA, pixelStep, uvA.xy, 
    vec2(getFlagToggle(FACE_FLAG_TEX_0_U_CLAMP), getFlagToggle(FACE_FLAG_TEX_0_V_CLAMP)),
    vec2(getFlagToggle(FACE_FLAG_TEX_0_U_MIRROR), getFlagToggle(FACE_FLAG_TEX_0_V_MIRROR))
  ), (uvA.z + 0.5) / float(size.z));

  vec3 uvXYNormB = vec3(wrapUV(uvEndB, pixelStep, uvB.xy, 
    vec2(getFlagToggle(FACE_FLAG_TEX_1_U_CLAMP), getFlagToggle(FACE_FLAG_TEX_1_V_CLAMP)),
    vec2(getFlagToggle(FACE_FLAG_TEX_1_U_MIRROR), getFlagToggle(FACE_FLAG_TEX_1_V_MIRROR))
  ), (uvB.z + 0.5) / float(size.z));

  vec4 texel0 = texture(textureMap, uvXYNormA);
  vec4 texel1 = texture(textureMap, uvXYNormB);

  // Color Combiner - Cycle 1

  // Overwrite with texture if needed...
  vec4 A1 = flagSelectX(FACE_FLAG_CCMUX_A1_FETCH, combColorA1, flagSelectX(FACE_FLAG_CCMUX_A1_TEX, texel0, texel1));
  vec4 B1 = flagSelectX(FACE_FLAG_CCMUX_B1_FETCH, combColorB1, flagSelectX(FACE_FLAG_CCMUX_B1_TEX, texel0, texel1));
  vec4 C1 = flagSelectX(FACE_FLAG_CCMUX_C1_FETCH, combColorC1, flagSelectX(FACE_FLAG_CCMUX_C1_TEX, texel0, texel1));
  vec4 D1 = flagSelectX(FACE_FLAG_CCMUX_D1_FETCH, combColorD1, flagSelectX(FACE_FLAG_CCMUX_D1_TEX, texel0, texel1));

  // ...and decide between color and alpha
  A1.rgb = flagSelectX(FACE_FLAG_CCMUX_A1_ALPHA, A1.rgb, A1.aaa);
  B1.rgb = flagSelectX(FACE_FLAG_CCMUX_B1_ALPHA, B1.rgb, B1.aaa);
  C1.rgb = flagSelectX(FACE_FLAG_CCMUX_C1_ALPHA, C1.rgb, C1.aaa);
  D1.rgb = flagSelectX(FACE_FLAG_CCMUX_D1_ALPHA, D1.rgb, D1.aaa);

  // Overwrite alpha if needed
  A1.a = flagSelectX(FACE_FLAG_ACMUX_A1_FETCH, combColorA1.a, flagSelectX(FACE_FLAG_ACMUX_A1_TEX, texel0.a, texel1.a));
  B1.a = flagSelectX(FACE_FLAG_ACMUX_B1_FETCH, combColorB1.a, flagSelectX(FACE_FLAG_ACMUX_B1_TEX, texel0.a, texel1.a));
  C1.a = flagSelectX(FACE_FLAG_ACMUX_C1_FETCH, combColorC1.a, flagSelectX(FACE_FLAG_ACMUX_C1_TEX, texel0.a, texel1.a));
  D1.a = flagSelectX(FACE_FLAG_ACMUX_D1_FETCH, combColorD1.a, flagSelectX(FACE_FLAG_ACMUX_D1_TEX, texel0.a, texel1.a));

#ifdef USE_CMUX_2_CYCLE
  // Color Combiner - Cycle 2
  vec4 colorCycle1 = (A1 - B1) * C1 + D1;

  // Overwrite with texture if needed...
  A1 = flagSelectY(FACE_FLAG_CCMUX_A1_FETCH, combColorA2, flagSelectY(FACE_FLAG_CCMUX_A1_TEX, texel0, texel1));
  B1 = flagSelectY(FACE_FLAG_CCMUX_B1_FETCH, combColorB2, flagSelectY(FACE_FLAG_CCMUX_B1_TEX, texel0, texel1));
  C1 = flagSelectY(FACE_FLAG_CCMUX_C1_FETCH, combColorC2, flagSelectY(FACE_FLAG_CCMUX_C1_TEX, texel0, texel1));
  D1 = flagSelectY(FACE_FLAG_CCMUX_D1_FETCH, combColorD2, flagSelectY(FACE_FLAG_CCMUX_D1_TEX, texel0, texel1));
  
  // ...and decide between color and alpha...
  A1.rgb = flagSelectY(FACE_FLAG_CCMUX_A1_ALPHA, A1.rgb, A1.aaa);
  B1.rgb = flagSelectY(FACE_FLAG_CCMUX_B1_ALPHA, B1.rgb, B1.aaa);
  C1.rgb = flagSelectY(FACE_FLAG_CCMUX_C1_ALPHA, C1.rgb, C1.aaa);
  D1.rgb = flagSelectY(FACE_FLAG_CCMUX_D1_ALPHA, D1.rgb, D1.aaa);

  // ...and the last result
  A1.rgb = flagSelectY(FACE_FLAG_CCMUX_A2_COMBINED, A1.rgb, colorCycle1.rgb);
  B1.rgb = flagSelectY(FACE_FLAG_CCMUX_B2_COMBINED, B1.rgb, colorCycle1.rgb);
  C1.rgb = flagSelectY(FACE_FLAG_CCMUX_C2_COMBINED, C1.rgb, colorCycle1.rgb);
  D1.rgb = flagSelectY(FACE_FLAG_CCMUX_D2_COMBINED, D1.rgb, colorCycle1.rgb);

  // Overwrite alpha if needed
  A1.a = flagSelectY(FACE_FLAG_ACMUX_A1_FETCH, combColorA2.a, flagSelectY(FACE_FLAG_ACMUX_A1_TEX, texel0.a, texel1.a));
  B1.a = flagSelectY(FACE_FLAG_ACMUX_B1_FETCH, combColorB2.a, flagSelectY(FACE_FLAG_ACMUX_B1_TEX, texel0.a, texel1.a));
  C1.a = flagSelectY(FACE_FLAG_ACMUX_C1_FETCH, combColorC2.a, flagSelectY(FACE_FLAG_ACMUX_C1_TEX, texel0.a, texel1.a));
  D1.a = flagSelectY(FACE_FLAG_ACMUX_D1_FETCH, combColorD2.a, flagSelectY(FACE_FLAG_ACMUX_D1_TEX, texel0.a, texel1.a));

  // ...and the last result
  A1.a = flagSelectY(FACE_FLAG_ACMUX_A2_COMBINED, A1.a, colorCycle1.a);
  B1.a = flagSelectY(FACE_FLAG_ACMUX_B2_COMBINED, B1.a, colorCycle1.a);
  C1.a = flagSelectY(FACE_FLAG_ACMUX_C2_COMBINED, C1.a, colorCycle1.a);
  D1.a = flagSelectY(FACE_FLAG_ACMUX_D2_COMBINED, D1.a, colorCycle1.a);

#endif  

  vec4 color = (A1 - B1) * C1 + D1;

  if(color.a < lensAndAlpha.y)discard;

  color.a = (lensAndAlpha.y > 0.1) ? 1.0 : color.a;

  // add stripes to invisible actors
  float lensValue = step(0.75, sin((worldPos.x + worldPos.y) * 10.0) * (lensAndAlpha.x));
  color.rgb = mix(color.rgb, vec3(1.0, 0.0, 1.0), lensValue);

  //color.rgb = vec3(worldPos.z * 0.01); 
  //color.rgb = A1.rgb;
  //color.rg = uvXYNormA.xy;
  //color.b = 0.0;
  //color.a = 1.0; // TEST

  fragmentColor = color;
}
`;

export default shader;