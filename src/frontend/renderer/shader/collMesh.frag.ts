/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const shader = /*glsl*/`
varying vec3 fragColor;

void main() 
{
  gl_FragColor = vec4(fragColor, 1.0);
}
`;

export default shader;