/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
import { useRef } from "react";
import { useFrame, useThree } from "@react-three/fiber";
import { Color, BackSide, Mesh } from 'three';
import { VectorRGBFloat } from "backend/z64/types";

import skyboxFragShader from '../shader/skybox.frag';
import skyboxVertexShader from '../shader/skybox.vert';

interface SkyboxProps {
  color: VectorRGBFloat,
};

const SkyboxComponent = (props: SkyboxProps) => {

    const uniforms = useRef({
        bottomColor: { value: new Color( 0x202020 ) },
        midColor: { value: new Color( 0x666666 ) },
        topColor: { value: new Color( 0xffffff ) },
    });

    if(uniforms.current) {
      const bgColor = new Color(
        props.color[0] / 0xFF,
        props.color[1] / 0xFF,
        props.color[2] / 0xFF,
      );

      uniforms.current.midColor.value = bgColor;
      uniforms.current.bottomColor.value = bgColor.clone().lerp(new Color( 0x202020 ), 0.8);
      uniforms.current.topColor.value = bgColor.clone().lerp(new Color( 0xffffff ), 0.5);
    }

    const mesh = useRef<Mesh>() as React.RefObject<Mesh>;
    const { camera } = useThree();
    
    useFrame(state => {
        if(mesh.current) {
            const skyMesh = (mesh.current as Mesh);
            skyMesh.position.copy(camera.position);
        }
    });
    

    return (
      <mesh 
        name="Skybox"
        frustumCulled={false} 
        ref={mesh}
      >
        <sphereGeometry  attach="geometry" args={[500, 20, 12]} />
        <shaderMaterial 
          vertexShader={skyboxVertexShader} 
          fragmentShader={skyboxFragShader}  
          uniforms={uniforms.current}
          side={BackSide}
          depthWrite={false}
          attach="material" 
        />
    </mesh>
  );
};

export const Skybox = React.memo(SkyboxComponent, (prev, next) => {
  return prev.color[0] === next.color[0] &&
    prev.color[1] === next.color[1] &&
    prev.color[2] === next.color[2];
});