/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { addEffect, addAfterEffect, useThree } from '@react-three/fiber'
import React, { useEffect, useState } from 'react';
import { CylinderGeometry, SphereGeometry, Vector3 } from 'three';
import { GLOBAL_OBJ_SCALE } from '../converter/z64Mesh';
import { RaycastLayer } from '../raycast';
import Stats from './stats';

const EnvironmentComponent = () => {
  const { camera, gl } = useThree();
  (window as any).gl = gl;
  (window as any).cam = camera;

  const [stats] = useState(new (Stats as any)());
  const [init] = useState(true);

  useEffect(() => {
    stats.showPanel(1); // 0: fps, 1: ms, 2: mb, 3+: custom
    stats.domElement.style.cssText = 'position:absolute;bottom:0px;right:0px;';
    
    document.getElementsByTagName("canvas")[0]?.parentNode?.appendChild(stats.dom as Node);

    addEffect(() => stats.begin());
    addAfterEffect(() => stats.end());
  }, [stats]);

  useEffect(() => {
    //(camera as any).fov = 60;
    //camera.updateProjectionMatrix();
    camera.layers.enable(RaycastLayer.ACTOR);
    
    gl.sortObjects = true;

    gl.setTransparentSort((a, b) => {
      if((a.geometry instanceof SphereGeometry))return 0;
      if((b.geometry instanceof SphereGeometry))return 0;
      if((a.geometry instanceof CylinderGeometry))return 0;
      if((b.geometry instanceof CylinderGeometry))return 0;

      if(!a.geometry.boundingSphere || !a.geometry.boundingSphere.center) {
        return 0;
      }
      if(!b.geometry.boundingSphere || !b.geometry.boundingSphere.center) {
        return 0;
      }

      const posA = a.geometry.boundingSphere.center as Vector3;
      const posB = b.geometry.boundingSphere.center as Vector3;
      const camPosNorm = camera.position.clone().divideScalar(GLOBAL_OBJ_SCALE);

      const distA = camPosNorm.distanceToSquared(posA);
      const distB = camPosNorm.distanceToSquared(posB);

      return distB - distA;
    });
    
  }, [init, camera, gl]);

  return <ambientLight />;
};

export const Environment = React.memo(EnvironmentComponent, (prev, next) => {
  return true;
});