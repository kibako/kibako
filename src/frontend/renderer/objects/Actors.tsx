/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
import { Actor } from '../../../backend/z64/actors/actorList';
import { Actor3D } from './Actor';
import { RoomEntry } from '../../../backend/z64/rooms/roomList';
import { Context } from '../../../backend/context';
import { SceneEntry } from '../../../backend/z64/scenes/sceneList';

interface ActorsProps {
  actors: Actor[];
  scene: SceneEntry,
  room: RoomEntry,
  modelVisible: boolean;
  boxVisible: boolean;
  colliderVisible: boolean;
  selectedActors: Actor[];
  dayMask: number; // MM only
};

const ActorsComponent = (props: ActorsProps) => {
  const dayMask = props.dayMask;

  const selectedUUIDs = props.selectedActors.map(a => a.uuid);
  const actors = Context.isMM()
   ? props.actors.filter(a => (a.spawnTime & dayMask) || !a.spawnTime)
   : props.actors;

  return (
    <group>
      {actors.map(actor => <Actor3D 
        key={actor.uuid}
        actor={actor}
        scene={props.scene}
        room={props.room}
        modelVisible={props.modelVisible}
        boxVisible={props.boxVisible}
        colliderVisible={props.colliderVisible}
        isSelected={selectedUUIDs.includes(actor.uuid)}
      />)}
    </group>
  );
};

export const Actors = React.memo(ActorsComponent, (prev, next) => {
  return prev.actors === next.actors
  
  && prev.scene.sceneId === next.scene.sceneId
  && prev.scene.altId === next.scene.altId
  && prev.scene.actorDependency === next.scene.actorDependency
  && prev.scene.paths.length === next.scene.paths.length

  && prev.room.uuid === next.room.uuid
  && prev.modelVisible === next.modelVisible
  && prev.boxVisible === next.boxVisible
  && prev.colliderVisible === next.colliderVisible
  && prev.selectedActors === next.selectedActors
  && prev.selectedActors.length === next.selectedActors.length
  && prev.dayMask === next.dayMask;
});