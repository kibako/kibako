/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React from 'react';
import { Color, Euler } from 'three';
import { WaterBox } from '../../../backend/z64/collision/collisionTypes';

interface WaterBoxesProps {
  waterBoxes: WaterBox[];
};

export const WaterBoxes = (props: WaterBoxesProps) => {
  return (
    <React.Fragment>
      {props.waterBoxes.map(((box, indx) => 
        (<mesh key={indx + "_" + box.sizeX}
          name="WaterBox"
          position={[
            box.pos[0] + box.sizeX/2,
            box.pos[1],
            box.pos[2] + box.sizeZ/2
          ]}
          rotation={new Euler(Math.PI * -0.5, 0, 0)}
          >
          <planeGeometry attach="geometry" args={[
            box.sizeX, box.sizeZ
          ]} />
          <meshStandardMaterial 
              transparent={true}
              attach="material" 
              color={new Color(0.0, 0.0, 1.0)} 
              opacity={0.5}
          />
      </mesh>)
      ))}
    </React.Fragment>
  );
};