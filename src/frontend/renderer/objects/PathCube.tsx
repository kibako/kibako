/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect, useRef } from 'react';
import { Mesh, BoxGeometry, MeshStandardMaterial } from 'three';
import { Vector3 } from '../../../backend/z64/types';
import { layerActor } from '../raycast';

const ACTOR_BOX_SCALE = 20;

// SUPERGLOBAL: since this component is static, create the buffer/texture only once per page-load
let geometry: BoxGeometry | undefined;
let material: MeshStandardMaterial | undefined;

interface DefaultCubeProps {
  hidden?: boolean;
  selected?: boolean;
  color: Vector3;
};

const PathCubeComponent = (props: DefaultCubeProps) => {
  const meshRef = useRef<Mesh>(null);

  useEffect(() => 
  {
    if(!geometry) {
      // Initial dummy box to avoid popping-in
      geometry = new BoxGeometry(1,1,1, 1,1,1); // size-XYZ, segments-XYZ
      geometry.scale(ACTOR_BOX_SCALE, ACTOR_BOX_SCALE, ACTOR_BOX_SCALE);
      geometry.computeBoundingSphere();
      geometry.computeBoundingBox();
    }

    if(!material) {
      material = new MeshStandardMaterial();
    }
    
    if(meshRef.current) {
      const colorFactor = (props.selected ? 1.0 : 0.25) / 255;
      meshRef.current.geometry = geometry;
      const newMat = material.clone();
      newMat.color.setRGB(
        props.color[0] * colorFactor,
        props.color[1] * colorFactor,
        props.color[2] * colorFactor,
      );
      meshRef.current.material = newMat;
      meshRef.current.visible = !props.hidden;
    }
  }, [props.hidden, props.selected, props.color]);

  return (<mesh ref={meshRef} layers={layerActor()} scale={(props.selected ? 1.1 : 1.0) * 0.7} ></mesh>);
};

export const PathCube = React.memo(PathCubeComponent, (prev, next) => {
  return prev.hidden === next.hidden
    && prev.selected === next.selected
    && prev.color === next.color;
});