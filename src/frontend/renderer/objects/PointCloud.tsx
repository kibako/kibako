/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, {useEffect, useRef } from 'react';
import { Float32BufferAttribute, BufferGeometry, Points } from 'three';
import { CollisionData } from '../../../backend/z64/collision/collisionTypes';

interface PointCloudProps {
  collData: CollisionData;
};

export const PointCloudComponent = (props: PointCloudProps) => {
  const mesh = useRef() as React.RefObject<Points>;
   
  const geomRef = useRef<BufferGeometry>(null);

  useEffect(() => {
    if(geomRef.current) {
      console.log("Update Point cloud");
      geomRef.current.setAttribute( 'position', new Float32BufferAttribute( 
          props.collData.vertices
      , 3 ) );
    }
  }, [props.collData]);

  return (
    <points
        name="PointCloud"
        position={[0, 0, 0]}
        ref={mesh}
        frustumCulled={false}
        scale={[1, 1, 1]}>
        <bufferGeometry  attach="geometry" ref={geomRef} />
        <pointsMaterial attach="material" color={'#333'} size={0.5}  />
    </points>
  );
};

export const PointCloud = React.memo(PointCloudComponent, (prev, next) => {
  return prev.collData === next.collData;
});