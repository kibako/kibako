/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, {useRef } from 'react';

import { Float32BufferAttribute, Uint16BufferAttribute, BufferGeometry, Mesh } from 'three';

import { CollisionData } from '../../../backend/z64/collision/collisionTypes';
import { useEffect } from 'react';

import collMeshFragShader from '../shader/collMesh.frag';
import collMeshVertexShader from '../shader/collMesh.vert';

const { computeBoundsTree, disposeBoundsTree, acceleratedRaycast } = require('three-mesh-bvh');

(BufferGeometry.prototype as any).computeBoundsTree = computeBoundsTree;
(BufferGeometry.prototype as any).disposeBoundsTree = disposeBoundsTree;
Mesh.prototype.raycast = acceleratedRaycast;

interface CollMeshProps {
  collData: CollisionData;
  visible: boolean
};

export const CollMesh = (props: CollMeshProps) => {
  const mesh = useRef<Mesh>(null);
  const geomRef = useRef<BufferGeometry>(null);

  useEffect(() => {
    if(!mesh.current)return;
    
    if(props.collData.vertices.length === 0) {
      console.warn("Collision was loaded but has no data!");
      return;
    }

    const geometry = mesh.current.geometry;
    const vertexCount = props.collData.verticesNoIndex.length;
    const faceCount = Math.floor(vertexCount / 3);

    (geometry as any).disposeBoundsTree();

    geometry.setAttribute('position', new Float32BufferAttribute(props.collData.verticesNoIndex, 3));
    geometry.setAttribute('normal', new Float32BufferAttribute(props.collData.normals, 3));

    // build index array, only needed to handle updating the mesh between scenes
    const indexArray = new Array(faceCount);
    for(let i=0; i<faceCount; ++i)indexArray[i] = i;
    geometry.setIndex(new Uint16BufferAttribute(indexArray, 1));

    // raycasting setup
    (geometry as any).computeBoundsTree();

   }, [props.collData]);

  return (
    <mesh
        ref={mesh}
        name="CollMesh"
        frustumCulled={false}
        visible={props.visible}
      >
        <bufferGeometry  attach="geometry" ref={geomRef} />
        <shaderMaterial 
          vertexShader={collMeshVertexShader} 
          fragmentShader={collMeshFragShader}  
          attach="material" 
          transparent={true}
        />
    </mesh>
  );
};