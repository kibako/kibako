/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, {useEffect, useRef } from 'react';

import { Box3, DoubleSide, Mesh, RawShaderMaterial, Sphere, Vector2, Vector3 } from 'three';

import { Z64Mesh, Z64MeshObjectType } from '../../../backend/z64/mesh/mesh';
import { UniformTexture3D } from '../converter/texture';
import { z64MeshToThreeJS } from '../converter/z64Mesh';

import z64MeshVertexShader from '../shader/z64Mesh.vert';
import z64MeshFragShader from '../shader/z64Mesh.frag';

// SUPERGLOBAL: this shader is cloned for each component instance, so it can be global
let z64BaseMaterial: RawShaderMaterial | undefined;

const ALPHA_CLIP_OPAQUE = 0.2;
const ALPHA_CLIP_TRANSP = 0.05;

interface Z64MeshUniform {
  textureMap: UniformTexture3D,
  lensAndAlpha: {value: Vector2},
}

const emptyZ64MeshUniform = (): Z64MeshUniform => ({
    textureMap: {value: undefined},
    lensAndAlpha: {value: new Vector2(0.0, 0.0)},
});

const loadGeometry = (
  mesh: Mesh, 
  uniform: Z64MeshUniform, 
  z64Mesh: Z64Mesh, 
  type: Z64MeshObjectType
  ) => 
{
  //console.time("z64ToThree");
  const threeJsData = z64MeshToThreeJS(z64Mesh, type, mesh.geometry);
  //console.timeEnd("z64ToThree");
  uniform.textureMap = { value: threeJsData.texture };

  // A Base-material that is cloned later is used.
  // This has the benefit of only compiling/using one shader with multiple uniforms.
  if(!z64BaseMaterial) {
    z64BaseMaterial = new RawShaderMaterial({
      vertexShader: z64MeshVertexShader,
      fragmentShader: z64MeshFragShader,
      transparent: true,
      side: DoubleSide, // side is handled in shader
    });
  }

  const isTransp = type === Z64MeshObjectType.TRANSPARENT;

  const meshMaterial = z64BaseMaterial.clone();
  (meshMaterial.uniforms as any) = uniform;
  meshMaterial.transparent = isTransp;

  // maps should not write to depth-buffer
  if(z64Mesh.isMap && isTransp) {
    meshMaterial.depthWrite = false;
  }

  meshMaterial.forceSinglePass = false;
  meshMaterial.uniformsNeedUpdate = true;
  mesh.material = meshMaterial;
  
  // Calculate bounding box/sphere for three.js culling and sorting
  mesh.geometry.boundingBox = new Box3(
    new Vector3(z64Mesh.aabb.min[0], z64Mesh.aabb.min[1], z64Mesh.aabb.min[2]),
    new Vector3(z64Mesh.aabb.max[0], z64Mesh.aabb.max[1], z64Mesh.aabb.max[2])
  );
  mesh.geometry.boundingSphere = new Sphere(new Vector3(), 0.0);
  mesh.geometry.boundingBox.getBoundingSphere(mesh.geometry.boundingSphere);

  mesh.frustumCulled=true;

  // Update geometry
  mesh.geometry = threeJsData.geometry;
};

interface Z64MeshRenderProps {
  mesh: Z64Mesh,
  // have visibility inside the component to prevent re-creating the mesh over and over again
  visible: boolean,
  lensOnly?: boolean
};

const Z64MeshRenderComponent = (props: Z64MeshRenderProps) => {
  const meshOpaque = useRef<Mesh>() as React.RefObject<Mesh>;
  const meshTransparent = useRef<Mesh>() as React.RefObject<Mesh>;

  // Uniforms
  const uniformsOpaqueRef = useRef(emptyZ64MeshUniform());
  const uniformsOpaque = uniformsOpaqueRef.current;

  const uniformsTransparentRef = useRef(emptyZ64MeshUniform());
  const uniformsTransparent = uniformsTransparentRef.current;

  const hasObjOpaque = props.mesh.gpuObjectOpaque.buffer.byteLength;
  const hasObjTransp = props.mesh.gpuObjectTransparent.buffer.byteLength;

  useEffect(() => 
  {
    if(meshOpaque.current && hasObjOpaque) {
      loadGeometry(meshOpaque.current, uniformsOpaque, props.mesh, Z64MeshObjectType.OPAQUE);
    }
    if(meshTransparent.current && hasObjTransp) {
      loadGeometry(meshTransparent.current, uniformsTransparent, props.mesh, Z64MeshObjectType.TRANSPARENT);
    }
  }, [props.mesh, uniformsOpaque, uniformsTransparent, hasObjOpaque, hasObjTransp]);

  useEffect(() => {
    uniformsOpaque.lensAndAlpha.value = new Vector2(props.lensOnly ? 1.0 : 0.0, ALPHA_CLIP_OPAQUE);
    uniformsTransparent.lensAndAlpha.value = new Vector2(props.lensOnly ? 1.0 : 0.0, ALPHA_CLIP_TRANSP);
  }, [props.lensOnly, uniformsTransparent, uniformsOpaque]);

  return (
    <>
      {hasObjOpaque ? <mesh ref={meshOpaque} visible={props.visible} name="MeshOpaque" /> : undefined}
      {hasObjTransp ? <mesh ref={meshTransparent} visible={props.visible} name="MeshTransparent" /> : undefined}
    </>
  );
};

export const Z64MeshRender = React.memo(Z64MeshRenderComponent, (prev, next) => {
  return prev.mesh.uuid === next.mesh.uuid
   && prev.visible === next.visible
   && prev.lensOnly === next.lensOnly;
});