/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useEffect, useRef } from 'react';
import { BoxGeometry, EdgesGeometry, LineBasicMaterial, LineSegments } from 'three';
import { layerActor } from '../raycast';

const ACTOR_BOX_SCALE = 20;

// SUPERGLOBAL: since this component is static, create the buffer/texture only once per page-load
let geometry: EdgesGeometry | undefined;
let materialOn: LineBasicMaterial | undefined;
let materialOff: LineBasicMaterial | undefined;

interface WireframeCubeProps {
  visible: boolean;
};

const WireframeCubeComponent = (props: WireframeCubeProps) => {
  const meshRef = useRef<LineSegments>() as React.RefObject<LineSegments>;

  useEffect(() => 
  {
    if(!geometry) {
      geometry = new EdgesGeometry( new BoxGeometry( 1,1,1 ) );
      geometry.scale(ACTOR_BOX_SCALE, ACTOR_BOX_SCALE, ACTOR_BOX_SCALE);
    }

    if(!materialOn) {
      materialOn = new LineBasicMaterial({color: 0xFF5555, linewidth: 1});
    }
    if(!materialOff) {
      materialOff = new LineBasicMaterial({color: 0xEEEEEE, linewidth: 1});
    }
    
    if(meshRef.current) {
      meshRef.current.geometry = geometry;
      meshRef.current.material = props.visible ? materialOn : materialOff;
    }
  }, [props.visible]);

  return (<lineSegments ref={meshRef} layers={layerActor()} ></lineSegments>);
};

export const WireframeCube = React.memo(WireframeCubeComponent, (prev, next) => {
  return prev.visible === next.visible;
});