/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, {useCallback, useEffect, useRef, useState } from 'react';
import { Float32BufferAttribute, BufferGeometry, LineSegments, Uint8BufferAttribute } from 'three';
import { ScenePath } from '../../../backend/decomp/parser/parsePaths';
import { requestHistoryAdd } from '../../../backend/editor/history';
import { hslToRGB } from '../../../backend/utils/color';
import { getPathIdx, patchPathDelPoint, PathRaycaseResult, setPathPosById } from '../../../backend/z64/path/pathState';
import { Vector3 } from '../../../backend/z64/types';
import { BusName } from '../../helper/eventBus';
import { preventDefault } from '../../helper/events';
import { useEventBus } from '../../helper/useEventBus';
import { useCanvasKeyUp } from '../../helper/useEventListener';
import { disableTransform, enableTransform, TransformObject } from '../ControlsFPS';
import { PathCube } from './PathCube';

interface SelectionData {
  pathUUID: string;
  pointIdx: number;
}

interface Props {
  paths: ScenePath[];
  editMode: boolean;
  onChange: (p: ScenePath[]) => void;
};

function isSelected(selectionData: SelectionData[], uuid: string, pointIdx: number) {
  return !!selectionData.find(s => s.pathUUID === uuid && s.pointIdx === pointIdx);
}

function selectionToTransform(entry: SelectionData, path?: ScenePath): TransformObject {
  return {
    uuid: entry.pathUUID + "_" + entry.pointIdx,
    pos: path?.points[entry.pointIdx] || [0,0,0],
    rot: [0,0,0] as Vector3,
  };
}

export const PathsComponent = (props: Props) => {
  const mesh = useRef() as React.RefObject<LineSegments>;
  const {paths, onChange, editMode} = props; 
  const pathCount = paths.length;

  const [selectionData, setSelectionData] = useState<SelectionData[]>([]);
   
  const geomRef = useRef<BufferGeometry>(null);
  const pathColors = props.paths.map((_, i) => hslToRGB(i / props.paths.length, 1.0, 0.52));

  useEffect(() => setSelectionData([]), [pathCount]);

  useEffect(() => {
    if(geomRef.current) {
      let pointCount = 0;
      for(const path of paths)pointCount += path.points.length;

      const dataPos = new Float32Array(pointCount*6);
      const dataColor = new Uint8Array(pointCount*6);

      let pathIdx = 0;
      let i=0;
      for(const path of paths) 
      {
        const color = pathColors[pathIdx];
        let lastPt = path.points[0];

        for(const pt of path.points) {
          dataPos[i+0] = lastPt[0];
          dataPos[i+1] = lastPt[1];
          dataPos[i+2] = lastPt[2];

          dataPos[i+3] = pt[0];
          dataPos[i+4] = pt[1];
          dataPos[i+5] = pt[2];

          dataColor[i+0] = color[0] * 0.5;
          dataColor[i+1] = color[1] * 0.5;
          dataColor[i+2] = color[2] * 0.5;

          dataColor[i+3] = color[0];
          dataColor[i+4] = color[1];
          dataColor[i+5] = color[2];

          i += 6;
          lastPt = pt;
        } 
        ++pathIdx;
      }
 
      geomRef.current.setAttribute('position', new Float32BufferAttribute(dataPos, 3));
      geomRef.current.setAttribute('color', new Uint8BufferAttribute(dataColor, 3, true));
    }
  }, [paths, pathColors]);

  const onPathSelect = useCallback((data: PathRaycaseResult) => {
    if(!data || !data.entries.length) {
      setSelectionData([]);
      return;
    }

    const entry = data.entries[0];
    const pointIdx = entry.pointIdx || 0;

    if(data.holdingShift) {
      if(!isSelected(selectionData, entry.uuid, pointIdx)) {
        setSelectionData([...selectionData, {pathUUID: entry.uuid, pointIdx}]);
      }
    } else {
      setSelectionData([{pathUUID: entry.uuid, pointIdx}]);
    }

  }, [selectionData, setSelectionData]);

  useEffect(() => {
    if(selectionData.length === 0 && editMode) {
      disableTransform(true);
      return;
    }

    const pointRef = selectionData.map(entry => 
      selectionToTransform(entry, paths.find(p => p.uuid === entry.pathUUID))
    );

    enableTransform(pointRef, (newPos) => 
    {
      const newPaths = [...paths];
      for(let i in newPos) {
        const selEntry = selectionData[i];
        setPathPosById(paths, selEntry.pathUUID, selEntry.pointIdx, newPos[i].pos);
      }
      onChange(newPaths);
    }, () => requestHistoryAdd());

  }, [paths, selectionData, onChange, editMode]);

  useEffect(() => {
    if(!editMode) {
      setSelectionData([]);
      disableTransform(true);
      return;
    }
  }, [editMode]);

  const onKeypress = useCallback((e: KeyboardEvent) => 
  {
    if(!e.ctrlKey && e.code === "Delete" && selectionData.length > 0) 
    {
      let newPaths = paths;
      const newSelection: SelectionData[] = [];
      for(const sel of selectionData) {
        const pathIdx = getPathIdx(newPaths, sel.pathUUID);
        newPaths = patchPathDelPoint(newPaths, pathIdx, sel.pointIdx);
        if(sel.pointIdx < (newPaths[pathIdx]?.points.length || 0)) {
          newSelection.push(sel);
        }
      }
      onChange(newPaths);
      setSelectionData(newSelection);
      requestHistoryAdd();
    }

    if(e.ctrlKey && e.code === "KeyD" && selectionData.length) 
    {
      let newPaths = [...paths];
      let newSelection: SelectionData[] = [];

      const selectionDataSorted = [...selectionData].sort((a,b) => b.pointIdx - a.pointIdx);
      for(const sel of selectionDataSorted) 
      {
        const pathIdx = getPathIdx(newPaths, sel.pathUUID);
        const path = newPaths[pathIdx];
        path.points = [...path.points];
        path.points.splice(sel.pointIdx, 0, path.points[sel.pointIdx]);
        newSelection.push({pathUUID: sel.pathUUID, pointIdx: sel.pointIdx+1});
      }
      onChange(newPaths);
      setSelectionData(newSelection);

      requestHistoryAdd();
    }
    preventDefault(e);
  }, [selectionData, setSelectionData, paths, onChange]);  

  useCanvasKeyUp(onKeypress);

  useEventBus(BusName.PATH_SELECTION, onPathSelect);

  return (
    <>
      <lineSegments
          name="Paths"
          position={[0, 0.5, 0]}
          ref={mesh}
          frustumCulled={false}
          scale={[1, 1, 1]}>
          <bufferGeometry  attach="geometry" ref={geomRef} />
          <lineBasicMaterial attach="material" vertexColors={true} linewidth={3} />
      </lineSegments>
      {editMode &&
        props.paths.map((path, p) => path.points.map((point, i) => 
          <group position={point} key={path.uuid + "_" + i} userData={{
            pathUUID: path.uuid,
            pointIdx: i
          }}>
            <PathCube color={pathColors[p]} selected={isSelected(selectionData, path.uuid, i)}/>
          </group>
        ))
      }
    </>
  );
};

export const Paths = React.memo(PathsComponent, (prev, next) => {
  return prev.paths === next.paths
    && prev.editMode === next.editMode
    && prev.onChange === next.onChange;
});