/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Euler, Quaternion, Vector3 as TVector3 } from 'three';
import { Actor} from '../../../backend/z64/actors/actorList';
import { s16AngleToRad } from '../../../backend/z64/math/angle';
import { Z64MeshRender } from './Z64MeshRender';
import { DefaultCube } from './DefaultCube';
import { WireframeCube } from './WireframeCube';
import { ActorType } from 'backend/z64/types';
import { aabbGetCenter, aabbGetExtend } from 'backend/utils/aabb';
import { ActorFlags } from 'backend/z64/actors/actorFlags';
import { Vector3 } from "backend/z64/types";
import { isZ64MeshFilled, Z64Mesh } from 'backend/z64/mesh/mesh';
import { ActorEmuData, EmuCommand, EmuCommandType } from 'backend/z64/emulator/actorEmuBridge';
import { ActorEmuEnvData, emulateActor } from 'backend/z64/actors/actorEmulator';
import { SavestateSlot, Z64Emulator } from 'backend/z64/emulator/z64Emulator';
import { useThree } from '@react-three/fiber';
import { GLOBAL_OBJ_SCALE } from '../converter/z64Mesh';
import { BusName, EventBus } from 'frontend/helper/eventBus';
import { RoomEntry } from '../../../backend/z64/rooms/roomList';
import { Collider } from './Collider';
import { useEventBus } from '../../helper/useEventBus';
import { SceneEntry } from '../../../backend/z64/scenes/sceneList';

const EMULATE_ACTOR = true;
const SCALE_SELECTED: Vector3 = [1.2,1.2,1.2];
const INITIAL_FRAMES = 3;

let currentFps = 20;
let currentDay = 1;
let currentTime = (0x1000) * 0; // hours

interface ActorProps {
  actor: Actor;
  scene: SceneEntry;
  room: RoomEntry;
  modelVisible: boolean;
  boxVisible: boolean;
  colliderVisible: boolean;
  isSelected: boolean;
};

const Actor3DComponent = (props: ActorProps) => {
  const {actor, scene, room, isSelected, colliderVisible} = props;
  const actorInitValue = actor.initValue;
  const actorPos = actor.pos;
  const actorRot = actor.rot;
  const actorUUID = actor.uuid;

  const [z64Mesh, setZ64Mesh] = useState<Z64Mesh>();
  const [emuData, setEmuData] = useState<ActorEmuData>();
  const [emuRunning, setEmuRunning] = useState(false);

  const { camera } = useThree();

  const rotEuler = new Euler(
    s16AngleToRad(actor.rot[0]),
    s16AngleToRad(actor.rot[1]),
    s16AngleToRad(actor.rot[2]),
    "YXZ"
  );

  const aabbCenter = z64Mesh
    ? aabbGetCenter(z64Mesh.aabb)
    : [...actor.pos] as Vector3;

  const userData = {actorUUID: actor.uuid};
  const showFallbackBox = !z64Mesh || props.boxVisible;


  // Emulation data
  const frame = useRef(INITIAL_FRAMES);
  const emuNexFrame = useRef(false);
  const emuPaused = useRef(false);
  const lastFrameTime = useRef(-10000);
  
  const emuEventCb = useCallback((data: EmuCommand) => {

    switch(data.type) 
    {
      case EmuCommandType.START:
          if(data.actorUUID === actorUUID) 
          {
            if(emuPaused.current) {
              emuPaused.current = false;
            } else {
              setEmuRunning(true);
            }
          } else if(emuRunning) {
            // if another actor start emulation, stop ours
            frame.current = INITIAL_FRAMES;
            setEmuRunning(false);
          }
        break;

        case EmuCommandType.PAUSE: 
          if(data.actorUUID === actorUUID) {
            emuPaused.current = true;
          }
        break;

        case EmuCommandType.STOP: 
          //if(data.actorUUID === actorUUID) {
            frame.current = INITIAL_FRAMES;
            if(emuRunning)setEmuRunning(false);
            
            if(data.actorUUID === actorUUID) {
              Z64Emulator.getInstance().clearSavestate(SavestateSlot.ACTOR);
            }
          //}
        break;

        case EmuCommandType.FPS_SET: 
          currentFps = data.value || 20;
        break;

        case EmuCommandType.MM_DAY: currentDay = parseInt(data.value) || 0; break;
        case EmuCommandType.MM_TIME: currentTime = parseInt(data.value) || 0; break;
    }
  }, [actorUUID, setEmuRunning, emuRunning]);

  useEventBus(BusName.ACTOR_EMU, emuEventCb);

  const emulateNextFrame = useCallback(async (time: DOMHighResTimeStamp = 0) => {
    let deltaTime = time - lastFrameTime.current;
    const fpsDeltaTime = (1000 / currentFps) - 1; // not accurate, must be smaller to compare to current delta
  
    if(!emuPaused.current && deltaTime > fpsDeltaTime) {
      lastFrameTime.current = time;
      ++frame.current;
      // make time pass, and pass it faster on higher framerates
      const timeOffset = Math.round(frame.current * (20 / currentFps)) * 6;

      let camDir = new TVector3();
      let camRotQuat = new Quaternion();
      camera.getWorldDirection(camDir)
      camera.getWorldQuaternion(camRotQuat);
      let camRot = new Euler().setFromQuaternion(camRotQuat, "YXZ");

      const envData: ActorEmuEnvData = {
        frame: frame.current, 
        camPos: [
          camera.position.x / GLOBAL_OBJ_SCALE,
          camera.position.y / GLOBAL_OBJ_SCALE,
          camera.position.z / GLOBAL_OBJ_SCALE,
        ],
        camDir: [camDir.x, camDir.y, camDir.z],
        camRot: [
          camRot.x, 
          Math.PI + camRot.y, 
          camRot.z
        ],
        playerPos: [
          camera.position.x / GLOBAL_OBJ_SCALE,
          camera.position.y / GLOBAL_OBJ_SCALE - 55,
          camera.position.z / GLOBAL_OBJ_SCALE,
        ] as Vector3,
        day: currentDay,
        time: currentTime + timeOffset,
      };

      const {emuData, z64Mesh} = emulateActor(actor, scene, room, envData, true, true);
      setEmuData(emuData);
      setZ64Mesh(isZ64MeshFilled(z64Mesh) ? z64Mesh : undefined);

      EventBus.send(BusName.ACTOR_EMU_DATA, emuData);

      const textures = [
        ...(z64Mesh?.gpuObjectOpaque.textures || []),
        ...(z64Mesh?.gpuObjectTransparent.textures || [])
      ];
      if(textures.length){
        EventBus.send(BusName.ACTOR_TEXTURES, textures);
      }
    }

    if(emuNexFrame.current) {  
      requestAnimationFrame(emulateNextFrame);
    }
  }, [camera, actor, scene, room]);

  // Start or stop emulation depending on the "emuRunning" state
  useEffect(() => {
    if(emuRunning) {
      emuNexFrame.current = true;
      emuPaused.current = false;
      emulateNextFrame();
    } else {
      emuNexFrame.current = false;
      emuPaused.current = false;
      frame.current = INITIAL_FRAMES;
      Z64Emulator.getInstance().clearSavestate(SavestateSlot.ACTOR);
    }
  }, [emuRunning, emulateNextFrame]);

  // Stop emulation if this Components gets deleted (e.g. scene change)
  useEffect(() => () => {
    emuNexFrame.current = false;
  }, [] );

  useEffect(() => {
    if(!EMULATE_ACTOR)return;

    const {emuData, z64Mesh} = emulateActor(actor, scene, room, {
      frame: INITIAL_FRAMES, playerPos: [
      actor.pos[0] + 40, // set play to actor to force actors to render
      actor.pos[1] + 0,
      actor.pos[2] + 0,
      ], camPos: [0,0,0], camDir: [1,0,0], camRot: [0.0, 3.0, 0.0],
      day: currentDay,
      time: currentTime,
    });
    emuNexFrame.current = false;

    // console.log("Emu Actor", z64Mesh?.uuid);    

    setEmuData(emuData);
    setZ64Mesh(isZ64MeshFilled(z64Mesh) ? z64Mesh : undefined);

  }, [actor, scene, room, actorInitValue, actorPos, actorRot]); // every value here triggers a fresh re-render

  return (
    <group name="Actor">
      {/* Actor 3D Model */}

      {z64Mesh &&
        <group visible={props.modelVisible}>
          <Z64MeshRender 
            visible={true} 
            mesh={z64Mesh} 
            lensOnly={((emuData?.flags || 0) & ActorFlags.INVISIBLE) !== 0}
          /> 
        </group>
      }

      {/* Hidden cube for clicking / selection wireframe */}
      
      <group position={aabbCenter} userData={userData}
        scale={z64Mesh 
          ? aabbGetExtend(z64Mesh.aabb, 0.1 * 0.5)
          : SCALE_SELECTED}
      >
        {(isSelected || emuRunning) && <WireframeCube visible={emuRunning} />}
        <DefaultCube hidden={true} />
      </group>
      
      {/* Fallback cube for clicking / selection wireframe */}

      {showFallbackBox && 
        <group position={actor.pos} rotation={rotEuler} userData={userData}>
          <DefaultCube special={actor.type === ActorType.SCENE} />
          {
            isSelected && 
            <group scale={SCALE_SELECTED}><WireframeCube visible={emuRunning} /></group>
          }
        </group>
      }

      {/* Collider */}
      {colliderVisible && emuData && <Collider collider={emuData.collider} />}
    </group>
  );
};

export const Actor3D = React.memo(Actor3DComponent, (prev, next) => {
  return prev.actor.uuid === next.actor.uuid
  && prev.room.uuid === next.room.uuid

  && prev.scene.sceneId === next.scene.sceneId
  && prev.scene.altId === next.scene.altId
  && prev.scene.actorDependency === next.scene.actorDependency
  && prev.scene.paths.length === next.scene.paths.length

  && prev.actor.initValue === next.actor.initValue
  && prev.actor.rotMask === next.actor.rotMask
  
  && prev.actor.pos[0] === next.actor.pos[0]
  && prev.actor.pos[1] === next.actor.pos[1]
  && prev.actor.pos[2] === next.actor.pos[2]

  && prev.actor.rot[0] === next.actor.rot[0]
  && prev.actor.rot[1] === next.actor.rot[1]
  && prev.actor.rot[2] === next.actor.rot[2]

  && prev.modelVisible === next.modelVisible
  && prev.boxVisible === next.boxVisible
  && prev.colliderVisible === next.colliderVisible
  && prev.isSelected === next.isSelected;
});