/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { ComponentType } from 'react';
import { IconProps } from 'react-bootstrap-icons';
import styled from 'styled-components';

const ListIconRight = styled.span`
  flex-grow: 1;
  text-align: right;
`;

const ListHeaderElem = styled.div`
  user-select: none;
  padding: 12px 18px;
  display: flex;
  align-items: center;
  transition: background-color 120ms cubic-bezier(0.4, 0, 0.2, 1) 10ms;

  &:hover {
    background-color: rgba(255, 255, 255, 0.08);
  }
`;

interface ListHeaderProps {
    text: string;
    Icon: ComponentType<IconProps>;
    iconColor?: string;
    IconRight?: ComponentType<IconProps>;
};

const ListHeaderComponent = (props: ListHeaderProps) =>  
{  
    return <ListHeaderElem >
      <props.Icon size={22} style={{marginRight: 14}} color={props.iconColor} />
      {props.text}
      {props.IconRight && <ListIconRight><props.IconRight/></ListIconRight>}
    </ListHeaderElem>;
};

export const ListHeader = React.memo(ListHeaderComponent, (prev, next) => {
  return prev.text === next.text &&
    prev.Icon === next.Icon &&
    prev.iconColor === next.iconColor &&
    prev.IconRight === next.IconRight;
});