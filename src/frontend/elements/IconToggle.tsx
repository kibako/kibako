/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React, { ComponentType } from 'react';
import { IconProps } from 'react-bootstrap-icons';
import styled from 'styled-components';

const Toggle = styled.button`
  pointer-events: auto;
  box-shadow: black 0px 0px 2px;
  border: 1px solid ${Colors.MONO};
  border-radius: 6px;
  text-align: center;
  padding-top: 4px;
  cursor: pointer;

  height: 32px;
  width: 44px;
  
  background: none;
  color: white;
  margin: 2px;

  transition: border-color 100ms linear 0ms, 
              background-color 70ms linear 0ms;

  &:hover {
    border-color: ${Colors.MONO_DARK};
    background-color: rgba(${Colors.MONO_RGB}, 0.1);
  }

  &:active {
    border-color: ${Colors.MONO_DARK};
    background-color: rgba(${Colors.MONO_RGB}, 0.3);
  }
`;

const ToggleActive = styled(Toggle)`
  border: 1px solid ${Colors.PRIMARY_DARK};
  background-color: ${Colors.PRIMARY_LIGHT};
  border-color: ${Colors.PRIMARY_DARK};

  &:hover {
    border-color: ${Colors.PRIMARY_DARK};
    background-color: rgba(${Colors.PRIMARY_DARK_RGB}, 1.0);
  }

  &:active {
    border-color: ${Colors.PRIMARY_DARK};
    background-color: rgba(${Colors.PRIMARY_DARK_RGB}, 0.3);
  }
`;

interface IconToggleProps {
    Icon: ComponentType<IconProps>;
    onClick: (state: boolean) => void;
    active: boolean;
};

const IconToggleComponent = (props: IconToggleProps) =>  
{ 
  const ToggleElem = props.active ? ToggleActive : Toggle;

  return <ToggleElem onClick={() => props.onClick(!props.active)}>
      <props.Icon size={18} color={props.active ? "black" : "white"}/>
    </ToggleElem>;
};

export const IconToggle = React.memo(IconToggleComponent, (prev, next) => {
  return prev.active === next.active &&
    prev.Icon === next.Icon &&
    prev.onClick === next.onClick;
});