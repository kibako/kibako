/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React, { ComponentType } from 'react';
import { IconProps } from 'react-bootstrap-icons';
import styled from 'styled-components';

const Button = styled.button`
  border: none;
  border-radius: 4px;
  padding: 4px 12px;
  height: 36px;
  min-width: 100px;
  background: ${Colors.BG_LIGHT};
  color: #EEE;
  margin: 4px;
  user-select: none;
  cursor: pointer;

  display: flex;
  align-items: center;
  justify-content: center;
  font-family: Roboto;
  font-weight: 500;
  font-size: 14px;

  transition: border-color 100ms linear 0ms, 
              background-color 70ms linear 0ms;

  &:hover {
    background-color: ${Colors.MONO_DARK};
  }

  &:active {
    opacity: 0.8;
  }
`;

const ButtonDark = styled(Button)`
  background: #282828;

  &:hover {
    background-color: #333;
  }
`;

export const SmallIconButton = styled.div`
  border: 1px solid ${Colors.BG_LIGHT};
  background-color: ${Colors.BG_LIGHT};
  border-radius: 5px;
  margin: 0 2px;
  width: 32px;
  height: 32px;
  text-align: center;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0;

  svg {
    margin-top: 0;
  }

  &:hover {
    background-color: ${Colors.MONO_DARK};
  }

  &:active {
    opacity: 0.8;
  }
`;

export const SmallIconButtonDark = styled(SmallIconButton)`
  background: #282828;

  &:hover {
    background-color: #333;
  }
`;

interface IconButtonProps {
    text: string;
    Icon?: ComponentType<IconProps>;
    icon?: string;
    dark?: boolean;
    onClick?: () => void
};

export const IconButtonComponent = (props: IconButtonProps) =>  
{  
  const Btn = props.dark ? ButtonDark : Button;

  return <Btn onClick={props.onClick}>
    {props.Icon && <props.Icon size={22} style={{marginRight: 8}} />}
    {props.icon && <span style={{marginRight: 8}}>{props.icon}</span>}
    {props.text}
  </Btn>;
};

export const IconButton = React.memo(IconButtonComponent, (prev, next) => {
  return prev.text === next.text &&
    prev.Icon === next.Icon &&
    prev.dark === next.dark &&
    prev.onClick === next.onClick;
});