/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React from 'react';
import styled from 'styled-components';

const ButtonBase = styled.button`
  border: none;
  border-radius: 4px;
  padding: 4px 12px;
  height: 36px;
  min-width: 100px;
  background: ${Colors.INPUT_BG};
  color: #EEE;
  margin: 4px;

  display: flex;
  align-items: center;
  justify-content: center;
  font-family: Roboto;
  font-weight: 500;
  font-size: 14px;

  transition: border-color 100ms linear 0ms, 
              background-color 70ms linear 0ms;

  &:hover {
    background-color: ${Colors.INPUT_BG_HOVER};
  }

  &:active {
    opacity: 0.8;
  }
`;

interface ButtonProps {
    text: string;
    onClick?: () => void;
    monochrome?: boolean;
    minWidth?: number;
    small?: boolean;
};

export const ButtonComponent = (props: ButtonProps) =>  
{  
    const minWidth = props.minWidth || 80;
    return <ButtonBase onClick={props.onClick}
      style={props.small ? {height: 30, minWidth} : {minWidth}}
      >{props.text}</ButtonBase>;
};

export const Button = React.memo(ButtonComponent, (prev, next) => {
  return prev.text === next.text &&
    prev.minWidth === next.minWidth && 
    prev.monochrome === next.monochrome &&
    prev.onClick === next.onClick &&
    prev.small === next.small;
});