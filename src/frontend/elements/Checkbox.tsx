/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Colors } from 'frontend/style/colors';
import React from 'react';
import styled from 'styled-components';

const CHECKBOX_SIZE = 14;

const Label = styled.label`
  width: 100%;
  cursor: pointer;
  display: flex;
  margin: 1px 0px;
  user-select: none;
`;

export const InputCheck = styled.input`
  cursor: pointer;
  margin: 2px 6px 0 0;
  width: ${CHECKBOX_SIZE}px;
  height: ${CHECKBOX_SIZE}px;
  border: 1px solid ${Colors.BORDER_DARK};
  border-radius: 2px;
  background-color: ${Colors.INPUT_BG};
  accent-color: ${Colors.PRIMARY_LIGHT};
`;

interface Props {
  name: string;
  checked: boolean;
  value: number;
  onChange: (state: number) => void;
};

const CheckboxComponent = (props: Props) =>  
{ 
  return <Label>
    <InputCheck 
      type="checkbox"
      checked={props.checked} 
      onChange={e => props.onChange(e.currentTarget.checked ? props.value : 0)} 
    />
    {props.name}
  </Label>;
};

export const Checkbox = React.memo(CheckboxComponent, (prev, next) => {
  return prev.checked === next.checked
    && prev.name === next.name
    && prev.value === next.value
    && prev.onChange === next.onChange;
});