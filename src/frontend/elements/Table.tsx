/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import styled from 'styled-components';

export const Table = styled.table`
  border-spacing: 0;
  border-spacing: 0;
  margin: 10px 0 10px 0;
  padding: 0;

  & tr td {
    border-spacing: 0;
    border-bottom: 1px solid #444;
    padding: 10px;
    height: 50px;
  }
`;

export const Cell = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
`;