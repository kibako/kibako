/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import React, { useState } from 'react';

import styled from 'styled-components';

import { IconButton } from 'frontend/elements/IconButton';

import { KeyboardFill } from 'react-bootstrap-icons';

import { HelpOverlay } from './HelpOverlay';
import { Colors } from 'frontend/style/colors';
import { Context } from '../../backend/context';

export const AppBar = styled.div/*_css*/`
    top: 0;
    left: auto;
    right: 0;
    position: absolute;
    box-sizing: border-box;
    flex-shrink: 0;
    flex-direction: column;
    transition: width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,
                margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;

    margin-left: 300px;
    width: 100%;
    background-color: ${Colors.BG_DARK};
    height: 48px;
    border-bottom: 1px solid ${Colors.BORDER_DARK};
`;

export const Toolbar = styled.div/*_css*/`
    display: flex;
    position: relative;
    align-items: center;
    padding-right: 4px;
    padding-left: 0px;
    padding-top: 2px;
`;

const GameIcon = styled.div`
  user-select: none;
  color: #aaa;
  height: 30px;
  margin: 2px 10px 4px 10px;
  display: inline-block;
  text-align: center;
  padding: 6px 6px 0 6px;
  border-radius: 7px;
  border: 1px solid #444;
  font-size: 12px;
`;

const EditorTitle = styled.p`
  user-select: none;
  flex-grow: 1; 
  font-size: 1.10rem;
  margin: 0;
  color: #eee;
`;

export const HeaderBar = () => {
  const [helpOpen, setHelpOpen] = useState(false);
  const gameName = Context.isMM() ? "MM" : "OOT";

  return (
  <AppBar>
    <Toolbar>
        <GameIcon title='Game'>{gameName}</GameIcon>
        <EditorTitle>Scene Editor</EditorTitle>

        {helpOpen && <HelpOverlay onClose={() => setHelpOpen(false)} />}
        <IconButton text="Controls" Icon={KeyboardFill} 
            onClick={() => setHelpOpen(true)}
        />
    </Toolbar>
</AppBar>
  );
}