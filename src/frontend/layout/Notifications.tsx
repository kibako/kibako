/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BusName, EventBus } from 'frontend/helper/eventBus';
import { Colors } from 'frontend/style/colors';
import React from 'react';
import { useState } from 'react';
import { useCallback } from 'react';
import { Check, ExclamationDiamond, Icon, Info, X } from 'react-bootstrap-icons';
import styled from 'styled-components';
import { useEventBus } from '../helper/useEventBus';

const REMOVE_TIMEOUT_SEC = 3;
const NOTI_WIDTH = 280;

const Container = styled.div`
  position: absolute;
  right: 5px;
  bottom: 5px;
  width: ${NOTI_WIDTH + 2}px;
  overflow: hidden;
`;

const Alert = styled.div`
    animation: animNotification ${REMOVE_TIMEOUT_SEC}s ease 0s 1 normal forwards;

    cursor: pointer;
    min-height: 40px;
    width: ${NOTI_WIDTH}px;
    border-radius: 8px;
    margin-top: 6px;
    padding: 4px 14px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color: ${Colors.BG_LIGHT};

    & p {
      color: #fff;
      font-family: Roboto;
      margin-left: 10px;
      width: 100%;
      text-align: left;
    }

    .closeIcon {
      color: #fff;
      transition: transform 0.1s;
      font-size: 18px;
  
      &:hover {
        transform: scale(1.2);
      }
    }
`;

const IconContainer = styled.div`
  height: 36px;
  width: 48px;
  border-radius: 50%;
  background: rgba(255, 255, 255, 0.253);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const icons: Record<string, Icon> = {
  "info": Info,
  "success": Check,
  "error": ExclamationDiamond,
};

const colors: Record<string, string> = {
  "info": "#4779ae",
  "success": "#4fa62f",
  "error": "#ac3e3e",
};

const createNotification = (data: NotificationData, cbClose: () => void) => {
  const NotiIcon = icons[data.type];
  const color = colors[data.type];

  return <Alert key={data.id} style={{backgroundColor: color}} onClick={cbClose}>
    <IconContainer>
      <NotiIcon size={20}/>
    </IconContainer>
    <p>{data.text}</p>
    
    <X className='closeIcon' size={30}/>
  </Alert>;
};

export interface NotificationData {
  id: number;
  type: 'info' | 'success' | 'error';
  text: string;
};

let lastId = 0;
export function showNotification(type: 'info' | 'success' | 'error', text: string) {
  EventBus.send(BusName.NOTIFICATION, {id: ++lastId, type, text});
}

let currentRemove: (id: number) => void  = ()=>{};

export const NotificationsComponent = () =>  
{
  const [entries, setEntries] = useState<NotificationData[]>([]);

  // Remove notifications
  const removeEntry = useCallback((id: number) => 
  {
    setEntries(entries.filter(e => e.id !== id));
  }, [entries, setEntries]);
  currentRemove = removeEntry;

  // Adding new notifications
  const onNotification = useCallback((data: any) => 
  {
    const entry = data as NotificationData;
    setEntries([...entries, entry]);
    setTimeout(() => currentRemove(data.id), REMOVE_TIMEOUT_SEC*1000 + 100);
  }, [entries, setEntries]);

  // Hookup events to show new notifications
  useEventBus(BusName.NOTIFICATION, onNotification);

  return <Container>
    {entries.map(data => createNotification(data, () => removeEntry(data.id)))}
  </Container>;
};

export const Notifications = React.memo(NotificationsComponent, (prev, next) => {
  return true;
});