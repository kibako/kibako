/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
import { KibakoWebsocket } from '../native/kibakoWebsocket';
import { getConfig, patchConfig } from './configManager';

const utf8Decoder = new TextDecoder();

/**
 * Helper for the JS Filesystem-API:
 * https://developer.mozilla.org/en-US/docs/Web/API/File_System_Access_API
 * 
 * This is used to read/write from the users FS, as well as remembering the state across sessions.
 */
namespace FileAPI
{
  export enum APIType {
    DUMMY, FS_ACCESS, NODE, NATIVE
  }
  /**
   * API independent interface for File access
   */
  export interface APIFunctions {
    type: APIType,
    openDir: (tryLoadOld: boolean) => Promise<void>;
    scanDir: (path: string) => Promise<DirScanResult>;
    readFileBuffer: (path: string) => Promise<ArrayBuffer>;
    readFileText: (path: string) => Promise<string>;
    writeFileText: (path: string, content: string) => Promise<void>;
  }

  export interface DirScanResult {
    files: string[];
    dirs: string[];
  };

  // dummy fallback that does nothing
  const dummyAPI: APIFunctions = {
    type: APIType.DUMMY,
    openDir: async function() {},
    scanDir: async function() {
      return {files: [], dirs: []};
    },
    readFileBuffer: async function (path: string): Promise<ArrayBuffer> {
      return new ArrayBuffer(0);
    },
    readFileText: async function (path: string): Promise<string> {
      return "";
    },
    writeFileText: async function() {}
  };

  let handle: FileSystemDirectoryHandle|undefined;
  let api = dummyAPI;
  let baseDir = "";

  const options = {
    id: 'z64-decomp-fs',
    mode: "readwrite" as FileSystemPermissionMode
  };

  export function setAPI(newAPI: APIFunctions) {
    api = newAPI;
  }

  export async function initAPI()
  {
    const ws = KibakoWebsocket.getInstance();
    if(ws) 
    {
      console.log("Native");

      baseDir = await getConfig("decompDir", "");
      api.type = APIType.NATIVE;

      api.openDir = async (tryLoadOld: boolean) => {
        console.log("Open", tryLoadOld);
        let path: string|undefined;

        if(tryLoadOld) {
          path = baseDir;
          if(path)await ws.cmdSetBaseDir(path);
        }
        if(!path)path = await ws.cmdOpenDir();
        if(!path)throw new Error("no Path selected");

        baseDir = path;
        console.log("Base-Dir: ", baseDir);
        patchConfig("decompDir", baseDir);
      };

      api.scanDir = (path: string) => ws.cmdScanDir(path);
      api.readFileBuffer = async (path: string) => ws.cmdReadFile(path);
      
      api.readFileText = async (path: string) => {
        return utf8Decoder.decode(await ws.cmdReadFile(path));
      };

      api.writeFileText = (path: string, content: string) => ws.cmdWriteFile(path, content);
    } 
    else 
    {
      api.type = APIType.FS_ACCESS;
      api.openDir = async (tryLoadOld: boolean) => {
        if(tryLoadOld) {
          const oldHandle = await getConfig("decompDir");
          if(oldHandle instanceof FileSystemDirectoryHandle) {
            console.log("Old File-Handle found, resume last session");
            //console.log(oldHandle);
            handle = oldHandle;
          }
        } else {
          handle = undefined;
        }
    
        if(!handle) {
          console.log("No File-Handle found, ask user");
          handle = await window.showDirectoryPicker(options);
        }
    
        if(handle) {
          await ensurePermission();
          await patchConfig("decompDir", handle);
        }
      }

      api.scanDir = async (path) =>
      {
        if(!handle || !await ensurePermission()) {
          throw new Error("File-Handle not loaded");
        }
    
        const dirHandle = await resolveDirHandle(handle, path);
        const res: DirScanResult = {
          files: [] as string[],
          dirs: [] as string[]
        };
        for await (const entry of dirHandle.values()) {
          if(entry.kind === 'file') {
            res.files.push(entry.name);
          } else {
            res.dirs.push(entry.name);
          }
        }
        return res;
      };

      api.readFileBuffer = async (path: string) => {
        if(!handle || !await ensurePermission()) {
          throw new Error("File-Handle not loaded");
        }
        
        const fileHandle = await resolveFileHandle(handle, path);
        const file = await fileHandle.getFile();
        return await file.arrayBuffer();
      };
      api.readFileText = async (path: string) => {
        if(!handle || !await ensurePermission()) {
          throw new Error("File-Handle not loaded");
        }

        const fileHandle = await resolveFileHandle(handle, path);
        const file = await fileHandle.getFile();
        return await file.text();
      };
      api.writeFileText = async (path: string, content: string) => {
        if(!handle || !await ensurePermission()) {
          throw new Error("File-Handle not loaded");
        }

        const fileHandle = await resolveFileHandle(handle, path);
        const writer = await fileHandle.createWritable({
          keepExistingData: false
        });
        await writer.write(content);
        await writer.close();
      };
    }
  }

  /**
   * Ensures that the file handle is still valid
   * @returns true if permission is now set
   */
  async function ensurePermission() {
    // only the file-access API needs permissions
    if(api.type !== APIType.FS_ACCESS)return true;

    if(!handle)return false;

    if ((await handle.queryPermission(options)) === 'granted') {
      return true;
    }
    if ((await handle.requestPermission(options)) === 'granted') {
      return true;
    }
    return false;
  }  

  /**
   * Opens a new file-handle, or an existing one if already set in the past.
   * @param tryLoadOld set to true to force new directory-picker
   */
  export async function openDir(tryLoadOld: boolean) 
  {
    return await api.openDir(tryLoadOld);
  }

  /**
   * Get list of all dirs/files in the given path
   * @returns 
   */
  export async function scanDir(path = ""): Promise<DirScanResult>
  {
    return await api.scanDir(path);
  }

  async function resolveDirHandle(dirHandle: FileSystemDirectoryHandle, path: string) 
  {
    if(path.length === 0) {
      return dirHandle;
    }
    
    const pathParts = path.split("/");
    for(const part of pathParts) 
    {
      dirHandle = await dirHandle.getDirectoryHandle(part);
    }
    return dirHandle;
  }

  async function resolveFileHandle(dirHandle: FileSystemDirectoryHandle, path: string) 
  {
    const pathParts = path.split("/");
    const fileName = pathParts.pop() as string;

    for(const part of pathParts) 
    {
      dirHandle = await dirHandle!.getDirectoryHandle(part);
    }
    return dirHandle.getFileHandle(fileName);
  }

  /**
   * Reads a file into memory as a buffer
   * @param path relative to base directory
   * @returns buffer
   */
  export async function readFileBuffer(path: string) {
    //console.time("Load file " + path);
    const res = await api.readFileBuffer(path);
    //console.timeEnd("Load file " + path);
    return res;
  }

  /**
   * Reads a file into memory as a text
   * @param path relative to base directory
   * @returns test
   */
  export async function readFileText(path: string) {
    //console.time("Load file " + path);
    const res = await api.readFileText(path);
    //console.timeEnd("Load file " + path);
    return res;
  }

  export async function writeFileText(path: string, content: string) {
    return api.writeFileText(path, content);
  }
};

export default FileAPI;