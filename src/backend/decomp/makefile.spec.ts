import { parseMakefile } from "./makefile";

describe('Decomp - Makefile', () => 
{
  test("Parse empty", () => {
    const data = parseMakefile("");
    expect(data).toEqual({
      rom: ""
    });
  });

  test("Parse Example", () => {
    const data = parseMakefile(`

#### Files ####

# ROM image
ROM := some_rom_name.z64
ELF := $(ROM:.z64=.elf)
# description of ROM segments
SPEC := spec
ifeq ($(COMPILER),ido)
SRC_DIRS := $(shell find src -type d -not -path src/gcc_fix)
else
SRC_DIRS := $(shell find src -type d)
endif

all: $(ROM)
ifeq ($(COMPARE),1)
	@md5sum $(ROM)
	@md5sum -c checksum.md5
endif
    `);
    expect(data).toEqual({
      rom: "some_rom_name.z64"
    });
  });

  test("Parse mixed case", () => {
    const data = parseMakefile(`
all: $(ROM)
ROM := Some_Rom.Z64
ELF := $(ROM:.z64=.elf)
    `);
    expect(data).toEqual({
      rom: "Some_Rom.Z64"
    });
  });

  test("Parse more space", () => {
    const data = parseMakefile(`
all: $(ROM)
ROM   :=  Some_Rom.Z64
ELF := $(ROM:.z64=.elf)
    `);
    expect(data).toEqual({
      rom: "Some_Rom.Z64"
    });
  });

  test("Parse mixed spacing", () => {
    const TAB = "\t";
    const data = parseMakefile(`
all: $(ROM)
ROM:=${TAB}some_other_rom.n64 # Comment 
ELF := $(ROM:.z64=.elf)
    `);
    expect(data).toEqual({
      rom: "some_other_rom.n64"
    });
  });

  test("Parse MM", () => {
    const TAB = "\t";
    const data = parseMakefile(`
COMPFLAGS := --threads $(N_THREADS)
ifneq ($(NON_MATCHING),1)
  COMPFLAGS += --matching
endif

#### Files ####

# ROM image
ROMC := abc.def.rev1.rom.z64
ROM := $(ROMC:.rom.z64=.rom_uncompressed.z64)
ELF := $(ROM:.z64=.elf)
# description of ROM segments
SPEC := spec

# create asm directories
$(shell mkdir -p asm data)
    `);
    expect(data).toEqual({
      rom: "abc.def.rev1.rom_uncompressed.z64"
    });
  });

});
