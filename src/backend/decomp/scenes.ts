/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { DataObject, parseFileVars } from "./c/parser";
import { ActorTable, parseActorTableCached } from "./actorTable";
import FileAPI from "../fileapi/fileAPI";
import { TransitionActorStruct } from "./actor";
import { SceneLighting } from "../z64/lighting/lighting";
import { SceneActorDependency } from "../z64/scenes/sceneList";
import { CollisionData, emptyCollisionData } from "../z64/collision/collisionTypes";
import { Vector3 } from "../z64/types";
import { DEG_TO_BINANG } from "./c/utils";
import { Game } from "../context";
import { parseCollision } from "./parser/parseCollision";
import { parseLighting } from "./parser/parseLighting";
import { parsePaths, ScenePath } from "./parser/parsePaths";

export type SceneDataHeader = {
  name: string; // varianble name of the header
  map: Record<string, Array<number|string>>; // map of header to arguments
  indices: Record<string, number>; // indices for the header entries by command name
}

interface SceneSoundData {
  preset: number;
  ambience: number;
  bgm: number;
};

export interface SceneData {
  mainHeader: SceneDataHeader,
  altHeaders: Array<SceneDataHeader|undefined>,
  actors: Record<string, TransitionActorStruct[]>;
  lighting: Record<string, SceneLighting[]>;
  dependency: SceneActorDependency,
  textureBank: number, // MM only
  collision: CollisionData,
  sound: Record<string, SceneSoundData>;
  paths: Record<string, ScenePath[]>;
}

export function getSceneCmdIntVal(cmd: Array<number|string>, idx: number) {
  return parseInt((cmd[idx] || "") + "") || 0;
}

export function getSceneCmdVarRef(cmd: Array<number|string>, idx: number) {
  return (cmd[idx] || "").toString().replace("&", "");
}

export function parseHeaderVars(vars: DataObject[]): SceneDataHeader[]
{
  const res: SceneDataHeader[] = [];

  vars = vars.filter(v => v.type === "SceneCmd");
  for(const header of vars) {
    const dataHeader: SceneDataHeader = {
      name: header.name,
      map: {},
      indices: {},
    };

    for(let i=0; i<header.data.length; ++i) {
      const name = header.data[i][0];
      dataHeader.map[name] = header.data[i].slice(1);
      dataHeader.indices[name] = i;
    }

    res.push(dataHeader);
  }

  return res;
}

export function parseAltHeaders(mainHeader: SceneDataHeader, headerVars: SceneDataHeader[], vars: DataObject[]): Array<SceneDataHeader|undefined>
{
  const altHeaders: Array<SceneDataHeader|undefined> = [];

  // Check for alternate headers
  if(mainHeader.map.SCENE_CMD_ALTERNATE_HEADER_LIST) {
    // get name of variable that stores the list
    const altRef = mainHeader.map.SCENE_CMD_ALTERNATE_HEADER_LIST[0] as string;
    const altList = vars.find(v => v.name === altRef); // de-reference
    for(const entry of (altList?.data || []))
    {
      if(entry === 0) {
        altHeaders.push(undefined);
        continue;
      }

      const altHeader = headerVars.find(h => h.name === entry);
      if(!altHeader) {
        console.error("Can't find altrnate-header: ", entry);
        altHeaders.push(mainHeader);
      } else {
        altHeaders.push(altHeader);
      }
    }
  }

  return altHeaders;
}

function parseTransitionActorList(vars: DataObject[], actorTable: ActorTable, game: Game): Record<string, TransitionActorStruct[]>
{
  const res: Record<string, TransitionActorStruct[]> = {};

  const actorVars = vars.filter(v => v.type === "TransitionActorEntry" && Array.isArray(v.data));
  
  for(const actorVar of actorVars) 
  {
    const actors = [] as TransitionActorStruct[];
    for(const entry of actorVar.data as any) 
    {
      const specIsArray = Array.isArray(entry[0]);
      const specData = specIsArray ? entry[0] : entry;
      const offset = specIsArray ? 1 : 4;
      let cutsceneIndex = 0; // MM only

      const pos: Vector3 = Array.isArray(entry[offset+1]) ? [
        parseInt(entry[offset+1][0]),
        parseInt(entry[offset+1][1]),
        parseInt(entry[offset+1][2]),
      ] : [
        parseInt(entry[offset+1]),
        parseInt(entry[offset+2]),
        parseInt(entry[offset+3]),
      ];

      const rotEntry = entry[entry.length-2];
      let rotY: number;

      if(Array.isArray(rotEntry) && rotEntry[0] === "DEG_TO_BINANG") // fast64
      {
        rotY = DEG_TO_BINANG(rotEntry[1]);
      } else if(game === Game.MM) // MM
      {
        rotY = parseInt(rotEntry);
        rotY = DEG_TO_BINANG((rotY >> 7) & 0x1FF);
        cutsceneIndex = rotEntry & 0x7F;
      } else { // Vanilla OOT
        rotY = parseInt(rotEntry);
      }

      actors.push({
        roomFront: parseInt(specData[0]),
        camIdxFront: parseInt(specData[1]),
        roomBack: parseInt(specData[2]),
        camIdxBack: parseInt(specData[3]),
        id: actorTable.enum[entry[offset+0]] || 0,
        pos,
        rotY,
        params: parseInt(entry[entry.length-1]),
        cutsceneIndex,
      });
    }

    res[actorVar.name] = actors;
  }

  return res;
}

export function parseScene(src: string, sceneName: string, actorTable: ActorTable, game: Game): SceneData
{
  src = src.replaceAll("\t", " ");
  const vars = parseFileVars(src, sceneName.replace(/scene$/, ""));
  const headerVars = parseHeaderVars(vars);

  const data: SceneData = {
    mainHeader: headerVars[0], 
    altHeaders: [],
    actors: {},
    lighting: {},
    dependency: SceneActorDependency.NONE,
    textureBank: 0,
    collision: emptyCollisionData(),
    sound: {},
    paths: {},
  };

  // Main Dependency (Dungeon / Field)
  if(data.mainHeader.map.SCENE_CMD_SPECIAL_FILES) {
    const depName = (data.mainHeader.map.SCENE_CMD_SPECIAL_FILES[1] || "").toString();
    if(depName.includes("FIELD"))data.dependency = SceneActorDependency.FIELD;
    if(depName.includes("DANGEON"))data.dependency = SceneActorDependency.DUNGEON;
  }

  // Collision
  if(data.mainHeader.map.SCENE_CMD_COL_HEADER) {
    const collVarName = getSceneCmdVarRef(data.mainHeader.map.SCENE_CMD_COL_HEADER, 0);
    const collVar = vars.find(v => v.name === collVarName);
    if(collVar) {
      parseCollision(vars, collVar, data.collision);
    }
  }

  // Texture Bank
  if(game === Game.MM) {
    if(data.mainHeader.map.SCENE_CMD_SKYBOX_SETTINGS) {
      data.textureBank = (data.mainHeader.map.SCENE_CMD_SKYBOX_SETTINGS[0] as number) || 0;
    }
  }

  data.altHeaders = parseAltHeaders(data.mainHeader, headerVars, vars);

  // Scan data for all layers that only exist in the header
  const layerHeaders = [data.mainHeader, ...data.altHeaders];
  for(const layerHeader of layerHeaders) {
    if(!layerHeader)continue;

    // Sound
    const soundCommand = layerHeader.map.SCENE_CMD_SOUND_SETTINGS;
    if(soundCommand) {
      data.sound[layerHeader.name] = {
        preset: getSceneCmdIntVal(soundCommand, 0),
        ambience: getSceneCmdIntVal(soundCommand, 1),
        bgm: getSceneCmdIntVal(soundCommand, 2),
      };
    }
  }

  // Paths  
  for(const pathVar of vars.filter(v => v.type === "Path")) {
    data.paths[pathVar.name] = parsePaths(vars, pathVar);
  }

  data.actors = parseTransitionActorList(vars, actorTable, game);
  data.lighting = parseLighting(vars);
  return data;
}

export async function parseSceneFile(basePath: string, file: string,  game: Game): Promise<SceneData>
{
  const actorTable = await parseActorTableCached();

  const fullPath = basePath + "/" + file;
  const sceneName = file.substring(0, file.length-2); // remove ".c" at the end
  const src = await FileAPI.readFileText(fullPath);
  return parseScene(src, sceneName, actorTable, game);
}
