import { Game } from "../context";
import { SceneActorDependency } from "../z64/scenes/sceneList";
import { parseScene } from "./scenes";
//import { Decomp } from "./actorTable";

describe('Decomp - Scenes', () => 
{
  test("Example - Single Layer", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_sceneCommands[] = {
    SCENE_CMD_SOUND_SETTINGS(1, 0x02,3),
    SCENE_CMD_TRANSITION_ACTOR_LIST(3, someroom_sceneTransitionActorList_000084),
    SCENE_CMD_SPECIAL_FILES(2, OBJECT_GAMEPLAY_FIELD_KEEP),
    SCENE_CMD_ENV_LIGHT_SETTINGS(2, someroom_sceneLightSettings0x0000BB),
    SCENE_CMD_END(),
};

TransitionActorEntry someroom_sceneTransitionActorList_000084[] = {
  { 0, 255, 1, 255, ACTOR_EN_ABC, 10, 20, 30, 0, 0x0100 },
  { 2, 255, 3, 255, ACTOR_EN_DEF, 10, 20, 30, 5, 0x0101 },
  { 4, 255, 5, 255, ACTOR_EN_ABC, 10, 20, 30, 0, 0x0102 },
};

u8 someroom_scene_possiblePadding_000FFF = 42;

LightSettings someroom_sceneLightSettings0x0000BB[] = {
  { 0x68, 0x5A, 0x50, 0x49, 0x49, 0x33, 0xFF, 0xFF, 0xFA, 0xB7, 0xB7, 0xB7, 0x32, 0x32, 0x5A, 0x64, 0x64, 0x78, 0x07C0, 0x0FA0 },
  { 0x10, 0x5A, 0x50, 0x49, 0x49, 0x33, 0xFF, 0xFF, 0xFA, 0xB7, 0xB7, 0xB7, 0x32, 0x32, 0x5A, 0x64, 0x64, 0x78, 0x07C0, 0x0FA0 },
};

`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_ABC: 0x10,
        ACTOR_EN_DEF: 0x42,
      }
    };

    const roomData = parseScene(src, "someroom", actorTable, Game.OOT);

    expect(roomData).toEqual({
      
        mainHeader: {
          name: "someroom_sceneCommands",
          map: {
            SCENE_CMD_SOUND_SETTINGS: [1, 2, 3],
            SCENE_CMD_TRANSITION_ACTOR_LIST: [3, "someroom_sceneTransitionActorList_000084"],
            SCENE_CMD_SPECIAL_FILES: [2, "OBJECT_GAMEPLAY_FIELD_KEEP"],
            SCENE_CMD_ENV_LIGHT_SETTINGS: [2, "someroom_sceneLightSettings0x0000BB"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_SOUND_SETTINGS: 0,
            SCENE_CMD_TRANSITION_ACTOR_LIST: 1,
            SCENE_CMD_SPECIAL_FILES: 2,
            SCENE_CMD_ENV_LIGHT_SETTINGS: 3,
            SCENE_CMD_END: 4,
          }
        },
        altHeaders: [],
        actors: {
          someroom_sceneTransitionActorList_000084: [
            {
              roomFront: 0,
              camIdxFront: 255,
              roomBack: 1,
              camIdxBack: 255,
              id: 0x10,
              pos: [10, 20, 30],
              rotY: 0,
              params: 0x0100,
              cutsceneIndex: 0,
            },
            {
              roomFront: 2,
              camIdxFront: 255,
              roomBack: 3,
              camIdxBack: 255,
              id: 0x42,
              pos: [10, 20, 30],
              rotY: 5,
              params: 0x0101,
              cutsceneIndex: 0,
            },
            {
              roomFront: 4,
              camIdxFront: 255,
              roomBack: 5,
              camIdxBack: 255,
              id: 0x10,
              pos: [10, 20, 30],
              rotY: 0,
              params: 0x0102,
              cutsceneIndex: 0,
            },
          ]
        },
        lighting: {
          someroom_sceneLightSettings0x0000BB: [
            {
              ambientColor: [104, 90, 80],
              diff0Dir: [73, 73, 51],
              diff0Color: [255, 255, 250],
              diff1Dir: [-73, -73, -73],
              diff1Color: [50, 50, 90],
              fogColor: [100, 100, 120],
              forStart: 1984,
              drawDist: 4000
            },
            {
              ambientColor: [16, 90, 80],
              diff0Dir: [73, 73, 51],
              diff0Color: [255, 255, 250],
              diff1Dir: [-73, -73, -73],
              diff1Color: [50, 50, 90],
              fogColor: [100, 100, 120],
              forStart: 1984,
              drawDist: 4000
            }
          ]
        },
        dependency: SceneActorDependency.FIELD,
        collision: {
          normals: [],
          vertices: [],
          verticesNoIndex: [],
          waterBoxes: [],
        },
        textureBank: 0,
        sound: {
          someroom_sceneCommands: {
            preset: 1,
            ambience: 2,
            bgm: 3,
          }
        },
        paths: {},
    });
  });

  test("Example - Single Layer (Paths)", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_sceneCommands[] = {
    SCENE_CMD_SOUND_SETTINGS(1, 0x02,3),
    SCENE_CMD_SPECIAL_FILES(2, OBJECT_GAMEPLAY_FIELD_KEEP),
    SCENE_CMD_PATH_LIST(someroom_scenePathway_0000A0),
    SCENE_CMD_END(),
};

u8 someroom_scene_possiblePadding_000FFF = 42;

Vec3s someroom_scenePathwayList_0000B0[] = {
  {   10,      20,    -30 },
  {   -40,     50,     60 },
};

Vec3s someroom_scenePathwayList_0000C0[] = {
  {   11,      22,   33 },
};

Path someroom_scenePathway_0000A0[] = {
  { 2, someroom_scenePathwayList_0000B0 },
  { 1, someroom_scenePathwayList_0000C0 },
};

`;
    
    const actorTable = {list: [], enum: {}};
    const roomData = parseScene(src, "someroom", actorTable, Game.OOT);

    roomData.paths.someroom_scenePathway_0000A0[0].uuid = "UUID"; // is random
    roomData.paths.someroom_scenePathway_0000A0[1].uuid = "UUID"; // is random

    expect(roomData).toEqual({
      
        mainHeader: {
          name: "someroom_sceneCommands",
          map: {
            SCENE_CMD_SOUND_SETTINGS: [1, 2, 3],
            SCENE_CMD_SPECIAL_FILES: [2, "OBJECT_GAMEPLAY_FIELD_KEEP"],
            SCENE_CMD_PATH_LIST: ["someroom_scenePathway_0000A0"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_SOUND_SETTINGS: 0,
            SCENE_CMD_SPECIAL_FILES: 1,
            SCENE_CMD_PATH_LIST: 2,
            SCENE_CMD_END: 3,
          }
        },
        altHeaders: [],
        actors: {},
        lighting: {},
        dependency: SceneActorDependency.FIELD,
        collision: {
          normals: [],
          vertices: [],
          verticesNoIndex: [],
          waterBoxes: [],
        },
        textureBank: 0,
        sound: {
          someroom_sceneCommands: {
            preset: 1,
            ambience: 2,
            bgm: 3,
          }
        },
        paths: {
          someroom_scenePathway_0000A0: [
            {
              uuid: "UUID",
              additionalPathIdx: 0, // MM only
              customValue: 0, // MM only
              points: [
                [10,  20,-30],
                [-40, 50, 60],
              ]
            },
            {
              uuid: "UUID",
              additionalPathIdx: 0, // MM only
              customValue: 0, // MM only
              points: [
                [11,  22, 33],
              ]
            }
          ]
        },
    });
  });


  test("Example - Multi Layer", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_sceneCommands[] = {
    SCENE_CMD_ALTERNATE_HEADER_LIST(someroom_sceneAlternateHeaders),
    SCENE_CMD_SOUND_SETTINGS(1,2,3),
    SCENE_CMD_TRANSITION_ACTOR_LIST(3, someroom_sceneTransitionActorList_000084),
    SCENE_CMD_SPECIAL_FILES(0x00, OBJECT_GAMEPLAY_FIELD_KEEP),
    SCENE_CMD_ENV_LIGHT_SETTINGS(2, someroom_sceneLightSettings0x0000BB),
    SCENE_CMD_PATH_LIST(someroom_scenePathway_Main),
    SCENE_CMD_END(),
};

SceneCmd* someroom_sceneAlternateHeaders[] = {
  NULL,
  someroom_sceneSet_0000F0,
  NULL,
};

/////////////////////
//   Main Scene    //
/////////////////////

TransitionActorEntry someroom_sceneTransitionActorList_000084[] = {
  { 0, 255, 1, 255, ACTOR_EN_ABC, 10, 20, 30, 0, 0x0100 },
  { 2, 255, 3, 255, ACTOR_EN_DEF, 10, 20, 30, 5, 0x0101 },
  { 4, 255, 5, 255, ACTOR_EN_ABC, 10, 20, 30, 0, 0x0102 },
};

u8 someroom_scene_possiblePadding_000FFF = 42;

LightSettings someroom_sceneLightSettings0x0000BB[] = {
  { 0x68, 0x5A, 0x50, 0x49, 0x49, 0x33, 0xFF, 0xFF, 0xFA, 0xB7, 0xB7, 0xB7, 0x32, 0x32, 0x5A, 0x64, 0x64, 0x78, 0x07C0, 0x0FA0 },
  { 0x10, 0x5A, 0x50, 0x49, 0x49, 0x33, 0xFF, 0xFF, 0xFA, 0xB7, 0xB7, 0xB7, 0x32, 0x32, 0x5A, 0x64, 0x64, 0x78, 0x07C0, 0x0FA0 },
};

Vec3s someroom_pathPointsMain[] = {
  {   10,      20,    -30 },
  {   -40,     50,     60 },
};

Path someroom_scenePathway_Main[] = {
  { 2, someroom_pathPointsMain },
};

/////////////////////
// Alternate Scene //
/////////////////////

SceneCmd someroom_sceneSet_0000F0[] = {
  SCENE_CMD_TRANSITION_ACTOR_LIST(1, someroom_sceneTransitionActorList_altA),
  SCENE_CMD_SPECIAL_FILES(0x00, OBJECT_GAMEPLAY_FIELD_KEEP),
  SCENE_CMD_ENV_LIGHT_SETTINGS(1, someroom_sceneLightSettings_altA),
  SCENE_CMD_SOUND_SETTINGS(4,5,6),
  SCENE_CMD_PATH_LIST(someroom_scenePathway_Alt),
  SCENE_CMD_END(),
};

TransitionActorEntry someroom_sceneTransitionActorList_altA[] = {
  { 2, 255, 3, 255, ACTOR_EN_DEF, 10, 20, 30, 5, 0x0201 },
};

LightSettings someroom_sceneLightSettings_altA[] = {
  { 0x10, 0x5A, 0x50, 0x49, 0x49, 0x33, 0xFF, 0xFF, 0xFA, 0xB7, 0xB7, 0xB7, 0x32, 0x32, 0x5A, 0x64, 0x64, 0x78, 0x07C0, 0x0FA0 },
};

Vec3s someroom_pathPointsAlt[] = {
  {   111,      222,    -333 },
};

Path someroom_scenePathway_Alt[] = {
  { 2, someroom_pathPointsAlt },
};

`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_ABC: 0x10,
        ACTOR_EN_DEF: 0x42,
      }
    };

    const roomData = parseScene(src, "someroom", actorTable, Game.OOT);

    roomData.paths.someroom_scenePathway_Main[0].uuid = "UUID"; // is random
    roomData.paths.someroom_scenePathway_Alt[0].uuid = "UUID"; // is random

    expect(roomData).toEqual({
        mainHeader: {
          name: "someroom_sceneCommands",
          map: {
            SCENE_CMD_ALTERNATE_HEADER_LIST: ["someroom_sceneAlternateHeaders"],
            SCENE_CMD_SOUND_SETTINGS: [1, 2, 3],
            SCENE_CMD_TRANSITION_ACTOR_LIST: [3, "someroom_sceneTransitionActorList_000084"],
            SCENE_CMD_SPECIAL_FILES: [0, "OBJECT_GAMEPLAY_FIELD_KEEP"],
            SCENE_CMD_ENV_LIGHT_SETTINGS: [2, "someroom_sceneLightSettings0x0000BB"],
            SCENE_CMD_PATH_LIST: ["someroom_scenePathway_Main"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_ALTERNATE_HEADER_LIST: 0,
            SCENE_CMD_SOUND_SETTINGS: 1,
            SCENE_CMD_TRANSITION_ACTOR_LIST: 2,
            SCENE_CMD_SPECIAL_FILES: 3,
            SCENE_CMD_ENV_LIGHT_SETTINGS: 4,
            SCENE_CMD_PATH_LIST: 5,
            SCENE_CMD_END: 6,
          }
        },
        altHeaders: [
          undefined,
          {
            name: "someroom_sceneSet_0000F0",
            map: {
              SCENE_CMD_TRANSITION_ACTOR_LIST: [1, "someroom_sceneTransitionActorList_altA"],
              SCENE_CMD_SPECIAL_FILES: [0, "OBJECT_GAMEPLAY_FIELD_KEEP"],
              SCENE_CMD_ENV_LIGHT_SETTINGS: [1, "someroom_sceneLightSettings_altA"],
              SCENE_CMD_SOUND_SETTINGS: [4, 5, 6],
              SCENE_CMD_PATH_LIST: ["someroom_scenePathway_Alt"],
              SCENE_CMD_END: [],
            },
            indices: {
              SCENE_CMD_TRANSITION_ACTOR_LIST: 0,
              SCENE_CMD_SPECIAL_FILES: 1,
              SCENE_CMD_ENV_LIGHT_SETTINGS: 2,
              SCENE_CMD_SOUND_SETTINGS: 3,
              SCENE_CMD_PATH_LIST: 4,
              SCENE_CMD_END: 5,
            }
          },
          undefined
        ],
        actors: {
          someroom_sceneTransitionActorList_000084: [
            {
              roomFront: 0,
              camIdxFront: 255,
              roomBack: 1,
              camIdxBack: 255,
              id: 0x10,
              pos: [10, 20, 30],
              rotY: 0,
              params: 0x0100,
              cutsceneIndex: 0,
            },
            {
              roomFront: 2,
              camIdxFront: 255,
              roomBack: 3,
              camIdxBack: 255,
              id: 0x42,
              pos: [10, 20, 30],
              rotY: 5,
              params: 0x0101,
              cutsceneIndex: 0,
            },
            {
              roomFront: 4,
              camIdxFront: 255,
              roomBack: 5,
              camIdxBack: 255,
              id: 0x10,
              pos: [10, 20, 30],
              rotY: 0,
              params: 0x0102,
              cutsceneIndex: 0,
            },
          ],
          someroom_sceneTransitionActorList_altA: [
            {
              roomFront: 2,
              camIdxFront: 255,
              roomBack: 3,
              camIdxBack: 255,
              id: 0x42,
              pos: [10, 20, 30],
              rotY: 5,
              params: 0x0201,
              cutsceneIndex: 0,
            }
          ]
        },
        lighting: {
          someroom_sceneLightSettings0x0000BB: [
            {
              ambientColor: [104, 90, 80],
              diff0Dir: [73, 73, 51],
              diff0Color: [255, 255, 250],
              diff1Dir: [-73, -73, -73],
              diff1Color: [50, 50, 90],
              fogColor: [100, 100, 120],
              forStart: 1984,
              drawDist: 4000
            },
            {
              ambientColor: [16, 90, 80],
              diff0Dir: [73, 73, 51],
              diff0Color: [255, 255, 250],
              diff1Dir: [-73, -73, -73],
              diff1Color: [50, 50, 90],
              fogColor: [100, 100, 120],
              forStart: 1984,
              drawDist: 4000
            }
          ],
          someroom_sceneLightSettings_altA: [
            {
              ambientColor: [16, 90, 80],
              diff0Dir: [73, 73, 51],
              diff0Color: [255, 255, 250],
              diff1Dir: [-73, -73, -73],
              diff1Color: [50, 50, 90],
              fogColor: [100, 100, 120],
              forStart: 1984,
              drawDist: 4000
            }
          ]
        },
        dependency: SceneActorDependency.FIELD,
        collision: {
          normals: [],
          vertices: [],
          verticesNoIndex: [],
          waterBoxes: [],
        },
        textureBank: 0,
        sound: {
          someroom_sceneCommands: {
            preset: 1,
            ambience: 2,
            bgm: 3,
          },
          someroom_sceneSet_0000F0: {
            preset: 4,
            ambience: 5,
            bgm: 6,
          }
        },
        paths: {
          someroom_scenePathway_Main: [
            {
              uuid: "UUID",
              additionalPathIdx: 0, // MM only
              customValue: 0, // MM only
              points: [
                [10,  20,-30],
                [-40, 50, 60],
              ]
            }
          ],
          someroom_scenePathway_Alt: [
            {
              uuid: "UUID",
              additionalPathIdx: 0, // MM only
              customValue: 0, // MM only
              points: [
                [111,  222, -333]
              ]
            }
          ]
        },
    });
  });

});
