import { DataObject } from './parser';
import * as C from './writer';

const hex = (data: any[]) => {
  (data as any).hint = C.FormatHint.HEX;
  return data;
};

describe('C - Writer', () => 
{
  describe('Value', () => 
  {
    test('number', () => {
      expect(C.writeValue(42)).toBe("42");
      expect(C.writeValue(-4242)).toBe("-4242");
      expect(C.writeValue(0)).toBe("0");

      expect(C.writeValue(0x10, C.FormatHint.HEX)).toBe("0x10");
      expect(C.writeValue(-0xFF22, C.FormatHint.HEX)).toBe("-0xFF22");
      expect(C.writeValue(0, C.FormatHint.HEX)).toBe("0");

      expect(C.writeValue(0.125)).toBe("0.125");
      expect(C.writeValue(0.125, C.FormatHint.FLOAT)).toBe("0.125f");
      expect(C.writeValue(-1.4, C.FormatHint.FLOAT)).toBe("-1.4f");
      expect(C.writeValue(5, C.FormatHint.FLOAT)).toBe("5.f");

      // should not happen:
      expect(C.writeValue(Infinity, C.FormatHint.FLOAT)).toBe("INFINITY");
      expect(C.writeValue(-Infinity, C.FormatHint.FLOAT)).toBe("-INFINITY");
      expect(C.writeValue(NaN, C.FormatHint.FLOAT)).toBe("NAN");
    });

    test('string', () => {
      expect(C.writeValue(`"Test"`)).toBe(`"Test"`);
      expect(C.writeValue(`""`)).toBe(`""`);

      // ignore hints that do not match the actual data-type
      expect(C.writeValue(`ACTOR_TEST_ABC`, C.FormatHint.HEX)).toBe(`ACTOR_TEST_ABC`);
      expect(C.writeValue(`"Some String"`, C.FormatHint.FLOAT)).toBe(`"Some String"`);
    });

    test('macro', () => {
      expect(C.writeValue(`SCENE_CMD_ECHO_SETTINGS(0)`)).toBe(`SCENE_CMD_ECHO_SETTINGS(0)`);
    });
  });


  describe('Array', () => 
  {
    test('Empty', () => {
      const str = C.writeArray([]);
      expect(str).toBe("{}");
    });

    test('Simple', () => {
      let str = C.writeArray([1,2,3,4]);
      expect(str).toBe("{1, 2, 3, 4}");

      str = C.writeArray([100, 2.5, `"Test"`, "MACRO(1,2,3)"]);
      expect(str).toBe(`{100, 2.5, "Test", MACRO(1,2,3)}`);
    });

    test('Nested', () => {
      let str = C.writeArray([
        1, 2, [42, 43, 44], [3]
      ]);
      expect(str).toBe("{1, 2, {42, 43, 44}, {3}}");

      str = C.writeArray([
        [1,2,3], [4,5,6], [7,8,9]
      ]);
      expect(str).toBe("{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}");
    });

    test('Deeply Nested', () => {
      let str = C.writeArray([[[[[[[7]]]]]]]);
      expect(str).toBe("{{{{{{{7}}}}}}}");

      str = C.writeArray([1,[2,[3,[4,[5,[6,[7]]]]]]]);
      expect(str).toBe("{1, {2, {3, {4, {5, {6, {7}}}}}}}");
    });

    test('Nested Mixed', () => {
      let str = C.writeArray([
        hex(["ACTOR_EN_ABC", [111,  0, -10], hex([0, 0x4000, 0]), 0xABCD]),
        hex(["ACTOR_EN_DEF", [222,  5, -20], hex([0, 0x4234, 0]), 0x1234]),
        hex(["ACTOR_EN_ABC", [333,  0, -30], hex([0, 0xABCD, 0]), 0x5678]),
        hex(["ACTOR_EN_DEF", [444, -5,-100], hex([0, 0xFFFF, 0]), 0xAAAA]),
      ], true);

      expect(str).toBe(
`{
    {ACTOR_EN_ABC, {111, 0, -10}, {0, 0x4000, 0}, 0xABCD},
    {ACTOR_EN_DEF, {222, 5, -20}, {0, 0x4234, 0}, 0x1234},
    {ACTOR_EN_ABC, {333, 0, -30}, {0, 0xABCD, 0}, 0x5678},
    {ACTOR_EN_DEF, {444, -5, -100}, {0, 0xFFFF, 0}, 0xAAAA}
}`);
    });

  });

  describe('File', () => 
  {
    test('Empty', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: [1,2,3],
      };
      const srcOut = C.patchFileVar("", dataObj);
      expect(srcOut).toBe(false);
    });

    test('Non matching', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: 999,
      };
      const srcOut = C.patchFileVar(
`u8 some_test = 42;
u8 some_va = 42;
u8 some_var_ = 42;`
      , dataObj);

      expect(srcOut).toBe(false);
    });

    test('Partial match', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "var",
        data: 999,
      };
      const srcOut = C.patchFileVar(
`u8 non_matching = 42;
u8 matches_end_var = 42;
u8 var_matches_start = 42;`
      , dataObj);

      expect(srcOut).toBe(false);
    });

    test('Double matching (invalid C)', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: 999,
      };
      const srcOut = C.patchFileVar(
`u8 some_var = 42;
u8 some_var = 42;`
      , dataObj);

      // Thisis invalid C, but we still patch and only replace the first one
      expect(srcOut).toBe(
`u8 some_var = 999;
u8 some_var = 42;`);
    });

    test('Empty input', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "",
        data: [],
      };

      // empty input shpuld fail, since it would patch the first variable in the file no matter the name
      const srcOut = C.patchFileVar(`u8 some_var[] = {1,2,3};`, dataObj);
      expect(srcOut).toBe(false);
    });

    test('Simple Value', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: 999,
      };
      const srcOut = C.patchFileVar(
`// Example C Code
u8 some_var = 42;
u8 other_var = 43;`
      , dataObj);

      expect(srcOut).toBe(
`// Example C Code
u8 some_var = 999;
u8 other_var = 43;`);
    });

    test('Simple Value - Same-Line', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: 999,
      };
      const srcOut = C.patchFileVar(
`// Example C Code
u8 some_var = 42;u8 other_var = 43;`
      , dataObj);

      
      expect(srcOut).toBe(
`// Example C Code
u8 some_var = 999;u8 other_var = 43;`);
    });

    test('Array Value', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: [1,2,3, [44, 55]],
      };
      const srcOut = C.patchFileVar(
`// Example C Code
u8 some_var[] = {};
u8 other_var = 43;`
      , dataObj);

      expect(srcOut).toBe(
`// Example C Code
u8 some_var[] = {1, 2, 3, {44, 55}};
u8 other_var = 43;`);
    });

    test('Array Value with size (number)', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: [1,2,3, [44, 55]],
      };
      const srcOut = C.patchFileVar(
`// Example C Code
u8 some_var[5] = {1,2,3,4,5};
u8 other_var = 43;`
      , dataObj);

      expect(srcOut).toBe(
`// Example C Code
u8 some_var[] = {1, 2, 3, {44, 55}};
u8 other_var = 43;`);
    });

    test('Array Value with size (constant)', () => 
    {
      const dataObj: DataObject = {
        type: 'u8',
        name: "some_var",
        data: [1,2,3, [44, 55]],
      };
      const srcOut = C.patchFileVar(
`// Example C Code
u8 some_var[SOME_SIZE] = {1,2,3,4,5};
u8 other_var = 43;`
      , dataObj);

      expect(srcOut).toBe(
`// Example C Code
u8 some_var[] = {1, 2, 3, {44, 55}};
u8 other_var = 43;`);
    });

    test('Real Example - Data', () => 
    {
      const srcIn = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_room_0Commands[] = {
    SCENE_CMD_ECHO_SETTINGS(0),
    SCENE_CMD_END(),
};

s16 someroom_room_0ObjectList_000040[] = {
    OBJECT_ABC,
    OBJECT_DEF,
};

ActorEntry someroom_room_0ActorEntry_00005C[] = {
    { ACTOR_EN_ABC,        {   111,      0,   -10 }, {      0, 0X4000,      0 }, 0xABCD },
    { ACTOR_EN_DEF,        {   222,      5,   -20 }, {      0, 0X4234,      0 }, 0x1234 },
    { ACTOR_EN_ABC,        {   333,      0,   -30 }, {      0, 0XABCD,      0 }, 0x5678 },
};

u8 someroom_room_0_possiblePadding_000FFF = 42;

Gfx someroom_room_0DL_000A50[] = {
  gsDPSomeCoolCmd(),
  gsDPSomeCoolCmd(11),
};

u64 someroom_room_0Tex_000E00[] = {
  #include "assets/scenes/someroom/someroom_room_0Tex_000E00.rgba16.inc.c"
};`;

  const dataObj: DataObject = {
    type: 'ActorEntry',
    name: "someroom_room_0ActorEntry_00005C",
    data: [
      hex(["ACTOR_EN_ABC", [555,  0, -10], hex([0, 0x4000, 0]), 0xABCD]),
      hex(["ACTOR_EN_DEF", [666,  5, -20], hex([0, 0x4234, 0]), 0x1234]),
      hex(["ACTOR_EN_ABC", [777,  0, -30], hex([0, 0xABCD, 0]), 0x4000]),
      hex(["ACTOR_EN_DEF", [888, -5,-100], hex([0, 0xFFFF, 0]), 0xAAAA]),
    ],
  };
  const srcOut = C.patchFileVar(srcIn, dataObj, true);

const srcExpected = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_room_0Commands[] = {
    SCENE_CMD_ECHO_SETTINGS(0),
    SCENE_CMD_END(),
};

s16 someroom_room_0ObjectList_000040[] = {
    OBJECT_ABC,
    OBJECT_DEF,
};

ActorEntry someroom_room_0ActorEntry_00005C[] = {
    {ACTOR_EN_ABC, {555, 0, -10}, {0, 0x4000, 0}, 0xABCD},
    {ACTOR_EN_DEF, {666, 5, -20}, {0, 0x4234, 0}, 0x1234},
    {ACTOR_EN_ABC, {777, 0, -30}, {0, 0xABCD, 0}, 0x4000},
    {ACTOR_EN_DEF, {888, -5, -100}, {0, 0xFFFF, 0}, 0xAAAA}
};

u8 someroom_room_0_possiblePadding_000FFF = 42;

Gfx someroom_room_0DL_000A50[] = {
  gsDPSomeCoolCmd(),
  gsDPSomeCoolCmd(11),
};

u64 someroom_room_0Tex_000E00[] = {
  #include "assets/scenes/someroom/someroom_room_0Tex_000E00.rgba16.inc.c"
};`;

    expect(srcOut).toBe(srcExpected);
      
    });
  });

  describe('Macro', () => 
  {
    test('Empty', () => 
    {
      const macroArgs = [1, "some_cool_var_name"];
      const srcOut = C.patchFileMacro("", "", "TEST_MACRO", macroArgs);
      expect(srcOut).toBe(false);
    });

    test('No Container', () => 
    {
      const src = `
        TEST_MACRO(1,2,3);

        Type var_name[] = {
          TEST_MACRO(1,2,3),
          TEST_MACRO(1,2,3),
        };
      `;
      const macroArgs = [1, "some_cool_var_name"];
      const srcOut = C.patchFileMacro(src, "no_in_the_file", "TEST_MACRO", macroArgs);
      expect(srcOut).toBe(false);
    });

    test('No Macro', () => 
    {
      const src = `
        TEST_MACRO(1,2,3);

        Type var_name[] = {
          TEST_MACRO(1,2,3),
          TEST_MACRO(1,2,3),
        };
      `;
      const macroArgs = [1, "some_cool_var_name"];
      const srcOut = C.patchFileMacro(src, "var_name", "UNKNOWN_MACRO", macroArgs);
      expect(srcOut).toBe(false);
    });

    test('Matching Macro', () => 
    {
      const src = `
        TEST_MACRO(1,2,3);

        Type var_name[] = {
          TEST_MACRO(1,2,3),
          TEST_MACRO_B(1,2,3),
        };
      `;

      const srcExpected = `
        TEST_MACRO(1,2,3);

        Type var_name[] = {
          TEST_MACRO(42, some_cool_var_name),
          TEST_MACRO_B(1,2,3),
        };
      `;
      const macroArgs = [42, "some_cool_var_name"];
      const srcOut = C.patchFileMacro(src, "var_name", "TEST_MACRO", macroArgs);
      expect(srcOut).toBe(srcExpected);
    });

    test('Matching Macro + Partial match', () => 
    {
      const src = `
        TEST_MACRO(1,2,3);

        Garbage imUsingThis = {
          var_name,
          var_name
        };

        Type not_my_var[] = {
          TEST_MACRO(1,2,3),
          TEST_MACRO_B(1,2,3),
        };

        Type var_name[] = {
          TEST_MACRO(1,2,3),
          TEST_MACRO_B(1,2,3),
        };
      `;

      const srcExpected = `
        TEST_MACRO(1,2,3);

        Garbage imUsingThis = {
          var_name,
          var_name
        };

        Type not_my_var[] = {
          TEST_MACRO(1,2,3),
          TEST_MACRO_B(1,2,3),
        };

        Type var_name[] = {
          TEST_MACRO(42, some_cool_var_name),
          TEST_MACRO_B(1,2,3),
        };
      `;
      const macroArgs = [42, "some_cool_var_name"];
      const srcOut = C.patchFileMacro(src, "var_name", "TEST_MACRO", macroArgs);
      expect(srcOut).toBe(srcExpected);
    });
  });

  describe('Remove Var', () => 
  {
    test('Empty', () => {
      expect(C.removeVar("", "")).toBe(false);
      expect(C.removeVar(" ", "  ")).toBe(false);
      expect(C.removeVar(" ", "someVar")).toBe(false);
      expect(C.removeVar("aaaa", "")).toBe(false);
    });

    test('Non Matching', () => {
      const src = 
`
u8 testA = 42;
u8 testB = 43;
u8 testC = 44;
`;
      expect(C.removeVar(src, "testFF")).toBe(false);
    });

    test('Simple var', () => {
      const src = 
`
u8 testA = 42;
u8 testB = 43;
u8 testC = 44;
`;
      expect(C.removeVar(src, "testB")).toBe(
`
u8 testA = 42;
u8 testC = 44;
`
      );
    });

    test('Partial match var', () => {
      const src = 
`
u8 testA = 42;
u8 testB2 = 43;
u8 testB = 44;
`;
      expect(C.removeVar(src, "testB")).toBe(
`
u8 testA = 42;
u8 testB2 = 43;
`
      );
    });

    test('Partial match var (no space)', () => {
      const src = 
`
u8 testA=42;
u8 testB2=43;
u8 testB=44;
`;
      expect(C.removeVar(src, "testB")).toBe(
`
u8 testA=42;
u8 testB2=43;
`
      );
    });

    test('Partial match array', () => {
      const src = 
`
u8 testA[]  = {42};
u8 testB2[] = {43};
u8 testB[]  = {44};
`;
      expect(C.removeVar(src, "testB")).toBe(
`
u8 testA[]  = {42};
u8 testB2[] = {43};
`
      );
    });

    test('Array var', () => {
      const src = 
`
u8 testA0[2] = {11, 21};
u8 testB0[2] = {12, 22};
u8 testC0[2] = {13, 23};
`;
      expect(C.removeVar(src, "testB0")).toBe(
`
u8 testA0[2] = {11, 21};
u8 testC0[2] = {13, 23};
`
      );
    });

    test('Array var (no size)', () => {
      const src = 
`
u8 testA0[] = {11, 21};
u8 testB0[] = {12, 22};
u8 testC0[] = {13, 23};
u8 testD0[] = {14, 24};
`;
      expect(C.removeVar(src, "testC0")).toBe(
`
u8 testA0[] = {11, 21};
u8 testB0[] = {12, 22};
u8 testD0[] = {14, 24};
`
      );
    });

    test('Array var (newline)', () => {
      const src = 
`
u8 testA0[] = {11, 21};

u8 testB0[] = {
  12, 22
};

u8 testC0[] = {
  13, 23
};

u8 testD0[] = {14, 24};
`;
      expect(C.removeVar(src, "testC0")).toBe(
`
u8 testA0[] = {11, 21};

u8 testB0[] = {
  12, 22
};

u8 testD0[] = {14, 24};
`
      );
    });

    test('Header extern', () => {
      const src = 
`
extern Vec3s testscene_pathwayList0_header0[];
extern Path testscene_scene_pathway0[1];
extern SurfaceType testscene_polygonTypes[];
`;
      expect(C.removeVar(src, "testscene_scene_pathway0")).toBe(
`
extern Vec3s testscene_pathwayList0_header0[];
extern SurfaceType testscene_polygonTypes[];
`
      );
    });

    // If the end is missing, assume that the next wrong declaration still belongs to the same var
    test('Invalid C - missing end', () => {
      const src = 
`
u8 testA = 42;
u8 testB = 43
u8 testC = 44;
u8 testF = 55;
`;
      expect(C.removeVar(src, "testB")).toBe(
`
u8 testA = 42;
u8 testF = 55;
`
      );
    });
  });

  describe('Inject Var', () => 
  {
    test('Empty src/ref', () => {
      const src = ``;
      expect(C.injectVar(src, "", "u8 data = 5;")).toBe(
`
u8 data = 5;
`
);
    });

    test('Empty src', () => {
      const src = ``;
      expect(C.injectVar(src, "nonExisting", "u8 data = 5;")).toBe(
`
u8 data = 5;
`
);
    });

    test('Empty ref', () => {
      const src = 
`
u8 oldVarA = 42;
u8 oldVarB[] = {1,2,3};
u8 oldVarC = 42;
`;
      expect(C.injectVar(src, "nonExisting", "u8 data = 5;")).toBe(
`
u8 oldVarA = 42;
u8 oldVarB[] = {1,2,3};
u8 oldVarC = 42;
u8 data = 5;
`
);
    });

    test('Empty ref (no newline)', () => {
      const src = 
`
u8 oldVarA = 42;
u8 oldVarB[] = {1,2,3};
u8 oldVarC = 42;`;
      expect(C.injectVar(src, "nonExisting", "u8 data = 5;")).toBe(
`
u8 oldVarA = 42;
u8 oldVarB[] = {1,2,3};
u8 oldVarC = 42;
u8 data = 5;
`
);
    });

    test('With ref', () => {
      const src = 
`
u8 oldVarA = 42;
u8 oldVarB[] = {1,2,3};
u8 oldVarC = 42;
`;
      expect(C.injectVar(src, "oldVarB", "u8 data = 5;")).toBe(
`
u8 oldVarA = 42;
u8 oldVarB[] = {1,2,3};
u8 data = 5;
u8 oldVarC = 42;
`
);
    });

    test('With ref (newline)', () => {
      const src = 
`
u8 oldVarA = 42;

u8 oldVarB[] = {
  1,2,3
};

u8 oldVarC = 42;
`;
      expect(C.injectVar(src, "oldVarB", "u8 data = 5;")).toBe(
`
u8 oldVarA = 42;

u8 oldVarB[] = {
  1,2,3
};
u8 data = 5;

u8 oldVarC = 42;
`
);
    });

    test('With ref  header', () => {
      const src = 
`
extern EnvLightSettings testscene_scene_header00_lightSettings[4];
extern Vec3s testscene_pathwayList0_header0[];
extern Path testscene_scene_pathway0[1];
extern SurfaceType testscene_polygonTypes[];
`;
      expect(C.injectVar(src, "testscene_pathwayList0_header0", "extern u8 data;")).toBe(
`
extern EnvLightSettings testscene_scene_header00_lightSettings[4];
extern Vec3s testscene_pathwayList0_header0[];
extern u8 data;
extern Path testscene_scene_pathway0[1];
extern SurfaceType testscene_polygonTypes[];
`
);
    });
  });

});