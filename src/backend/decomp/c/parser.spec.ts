import * as C from './parser';

describe('C - Parser', () => 
{
  describe('Value', () => 
  {
    test("numbers", () => {
      // decimal
      expect(C.parseValue("5")).toBe(5);
      expect(C.parseValue("-42")).toBe(-42);
      expect(C.parseValue("50u")).toBe(50);

      // hex & binary
      expect(C.parseValue("0x123")).toBe(0x123);
      expect(C.parseValue("0X456")).toBe(0x456);
      expect(C.parseValue("0x333u")).toBe(0x333);

      expect(C.parseValue("0b101")).toBe(0b101);
      expect(C.parseValue("0B111")).toBe(0b111);
      expect(C.parseValue("0B111u")).toBe(0b111);

      // floats
      expect(C.parseValue("1.25")).toBe(1.25);
      expect(C.parseValue("-1.25")).toBe(-1.25);
      expect(C.parseValue("1.5f")).toBe(1.5);
      expect(C.parseValue("-1.5f")).toBe(-1.5);

      expect(C.parseValue("1.")).toBe(1);
      expect(C.parseValue("1.f")).toBe(1);
    });

    test("string", () => {
      expect(C.parseValue(`"text"`)).toBe(`"text"`);
      expect(C.parseValue(`'char'`)).toBe(`'char'`);
      expect(C.parseValue(`"{42}"`)).toBe(`"{42}"`);
    });

    test("macros", () => {
      expect(C.parseValue(`SOME_DEFINE`)).toBe(`SOME_DEFINE`);
      expect(C.parseValue(`SOME_MACRO(1,2,3)`)).toEqual(["SOME_MACRO", 1, 2, 3]);
      expect(C.parseValue(`NESTED(1, MACRO(2,3), 4)`)).toEqual(
        ["NESTED", 1, ["MACRO", 2, 3], 4]
      );
    });

    test("brackets", () => {
      expect(C.parseValue(`(50)`)).toBe(50);
      expect(C.parseValue(`((50))`)).toBe(50);
      expect(C.parseValue(`("bla")`)).toBe(`"bla"`);
    });

    test("special", () => {
      expect(C.parseValue(`NULL`)).toBe(0);
      expect(C.parseValue(`null`)).toEqual("null");
      expect(C.parseValue(`(NULL)`)).toEqual(0);
    });
  });


  describe('Array', () => 
  {
    test("Trailing Comma", () => {
      expect(C.parseArray(`{1,}`)).toEqual([1]);
      expect(C.parseArray(`{1,2,3,}`)).toEqual([1,2,3]);
      expect(C.parseArray(`{,}`)).toEqual([]); // invalid C-Syntax
    });

    test("Empty", () => {
      expect(C.parseArray(`{}`)).toEqual([]);
      expect(C.parseArray(`{  } `)).toEqual([]);
      expect(C.parseArray(`{ 

       } `)).toEqual([]);
    });

    test("Simple - Integer", () => {
      expect(C.parseArray(`{1, 2, 0x30, 42u  }`)).toEqual([1,2, 0x30, 42]);
      expect(C.parseArray(` {1, 2, 5.4f 
        , 1, 5,
        44
       }`)).toEqual([1,2, 5.4, 1,5, 44]);
    });

    test("Simple - String", () => {
      expect(C.parseArray(`{"aa", "bb"}`)).toEqual([`"aa"`, `"bb"`]);
      // don't parse arrays inside of strings
      expect(C.parseArray(`{"{aa}", "{bb}"}`)).toEqual([`"{aa}"`, `"{bb}"`]);
      expect(C.parseArray(`{"{aa,bb}", "{cc}"}`)).toEqual([`"{aa,bb}"`, `"{cc}"`]);
    });

    test("Nested - Integer", () => {
      expect(C.parseArray(`{5,6, {10, 20, 30}, 7, {40}}`)).toEqual(
        [5,6, [10,20,30], 7, [40]]
      )
      expect(C.parseArray(`{5,6, {10, 
        20, 
        30}, 7}`)).toEqual(
        [5,6, [10,20,30], 7]
      );
    });
    
    test("Deeply Nested - Integer", () => {
      expect(C.parseArray(`{1, {2, {3, {{{{4,5,6,7}}}}}}}`)).toEqual(
        [1, [2, [3, [[[[4,5,6,7]]]]]]]
      )
    });

    test("Simple - Mixed", () => {
      expect(C.parseArray(`{MACRO(  "x", 5), 42, 5.2f, CONST_ABC}`)).toEqual(
        [["MACRO", `"x"`, 5], 42, 5.2, "CONST_ABC"]
      );
    });

    test("Nested - Mixed", () => {
      expect(C.parseArray(`{
        { ACTOR_EN_ABC,        {   111,      0,   -10 }, {      0, 0X4000,      0 }, 0xABCD },
        { ACTOR_EN_DEF,        {   222,      5,   -20 }, {      0, 0X4234,      0 }, 0x1234 },
        { ACTOR_EN_ABC,        {   333,      0,   -30 }, {      0, 0XABCD,      0 }, 0x5678 },
        { ACTOR_EN_DEF,        {   444,     -5,  -100 }, {      0, 0XFFFF,      0 }, 0xAAAA },
      }`)).toEqual([
        ["ACTOR_EN_ABC", [111,  0, -10], [0, 0x4000, 0], 0xABCD],
        ["ACTOR_EN_DEF", [222,  5, -20], [0, 0x4234, 0], 0x1234],
        ["ACTOR_EN_ABC", [333,  0, -30], [0, 0xABCD, 0], 0x5678],
        ["ACTOR_EN_DEF", [444, -5,-100], [0, 0xFFFF, 0], 0xAAAA],
      ]);
    });

    test("Nested - Macros", () => 
    {
      expect(C.parseArray(`{
        {10, MACRO_A(  "{5}", {1,2}), 42},
        {20, MACRO_B(A((44)), D({1,2})), 42}
      `)).toEqual([
        [10, ["MACRO_A", `"{5}"`, [1,2]], 42],
        [20, ["MACRO_B", 
          ["A", 44], 
          ["D", [1,2]]
        ], 42],
      ]);
    });

    test("Nested - Parentheses", () => 
    {
      expect(C.parseArray(`{1,(2),3}`)).toEqual([1,2,3]);
    });


    // Invalid Syntax - but i still want to parse it
    test("Missing End", () => {
      expect(C.parseArray(`{1`)).toEqual([1]);
      expect(C.parseArray(`{1,2,3,`)).toEqual([1,2,3]);
      expect(C.parseArray(`{1, {2,3`)).toEqual([1, [2,3]]);
    });
  });
});
