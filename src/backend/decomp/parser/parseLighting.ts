/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { SceneLighting } from "../../z64/lighting/lighting";
import { DataObject, u8ToS8 } from "../c/parser";

export function parseLighting(vars: DataObject[])
{
  const res: Record<string, SceneLighting[]> = {};
  const lightVars = vars.filter(v => 
    (v.type === "LightSettings" || v.type === "EnvLightSettings")
    && Array.isArray(v.data)
  );

  for(const lightVar of lightVars) {
    res[lightVar.name] = lightVar.data.map((data: any) => {
      data = data.flatMap((x: any) => x) as any[];

      return {
        ambientColor: [data[0], data[1], data[2]],
        
        diff0Dir: [u8ToS8(data[3]), u8ToS8(data[4]), u8ToS8(data[5])],
        diff0Color: [data[6], data[7], data[8]],

        diff1Dir: [u8ToS8(data[9]), u8ToS8(data[10]), u8ToS8(data[11])],
        diff1Color: [data[12], data[13], data[14]],

        fogColor: [data[15], data[16], data[17]],
        forStart: parseInt(data[18]) || 0,
        drawDist: parseInt(data[19]),
      }
    });
  }

  return res;
}
