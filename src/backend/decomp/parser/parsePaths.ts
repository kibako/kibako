/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Vector3 } from "../../z64/types";
import { DataObject } from "../c/parser";
import { v4 as uuid } from 'uuid';

export interface ScenePath {
  uuid: string;
  additionalPathIdx: number; // MM only
  customValue: number; // MM only
  points: Vector3[];
};

export function parsePaths(vars: DataObject[], pathArray: DataObject)
{
  const data: ScenePath[] = [];
  for(const pathEntry of (pathArray.data as Array<string>[])) 
  {
    const path: ScenePath = {
      uuid: uuid(),
      additionalPathIdx: 0,
      customValue: 0,
      points: [],
    };

    // must be MM (don't check the game to keep parser generic)
    if(pathEntry.length > 2) {
      path.additionalPathIdx = parseInt(pathEntry[1]);
      path.customValue = parseInt(pathEntry[2]);
    }

    const pathVarName = pathEntry[pathEntry.length - 1] || "";
    const pathVar = vars.find(v => v.name === pathVarName);
    if(pathVar) {
      path.points = pathVar.data;
    }

    data.push(path);
  }

  return data;
}
