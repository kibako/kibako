/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/
const ROM_OOT_REGEX = /ROM[ \t]*:=[ \t]*([a-z0-9_.-]+\.[zn]64)/i;
const ROM_MM_REGEX = /ROMC[ \t]*:=[ \t]*([a-z0-9_.-]+\.[zn]64)/i;

export function parseMakefile(src: string)
{
  const resOot = ROM_OOT_REGEX.exec(src);
  if(resOot && resOot[1]) {
    return {rom: resOot[1] || ""};
  }

  const resMM = ROM_MM_REGEX.exec(src);
  if(resMM && resMM[1]) {
    const name = resMM[1] || "";
    return {rom: name.replace("rom.z64", "rom_uncompressed.z64")};
  }

  return {rom: ""};
}