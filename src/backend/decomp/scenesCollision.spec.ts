import { Game } from "../context";
import { SceneActorDependency } from "../z64/scenes/sceneList";
import { u16ToS16 } from "./c/parser";
import { parseScene } from "./scenes";
//import { Decomp } from "./actorTable";

describe('Decomp - Scenes', () => 
{
  test("Collision - No Water", async () => {
    const src = `

SceneCmd test_sceneCommands[] = {
    SCENE_CMD_SPECIAL_FILES(2, OBJECT_GAMEPLAY_DANGEON_KEEP),
    SCENE_CMD_COL_HEADER(&test_sceneCollisionHeader),
    SCENE_CMD_END(),
};

Vec3s test_sceneColl_Vertices[] = {
  {1,2,3}, 
  {4,5,6},
  {11, -22, 33}, 
  {-44, 55, -66}, 
};

CollisionPoly test_sceneColl_Polygons[] = {
  // type,       indices 0-2,          normal XYZ         distance
  {0x0000, 0xE000, 0xE002, 0x0001, 0x0000, 0x7FFF, 0x0000, 0xABCD},
  {0x0000, 0xE001, 0x0000, 0x0003, 0x8001, 0x0000, 0x0000, 0xEF12},
  {0x0001, 0xE002, 0x0003, 0x8001, 0x36B2, 0xCF10, 0x68DD, 0x3456},
};

CollisionHeader test_sceneCollisionHeader = {
  { -1000, -1001, -1002 }, // aabb-min
  { 2000, 2001, 2002 },    // aabb-max
  4, test_sceneColl_Vertices,
  3, test_sceneColl_Polygons,
  test_sceneColl_SurfaceType,
  test_sceneColl_CamDataList,
  0, NULL 
};

`;
    
    const actorTable = {list: [], enum: {}};
    const roomData = parseScene(src, "test_", actorTable, Game.OOT);

    // Indices: [0,2,1], [1,0,3], [2,3,1]

    expect(roomData).toEqual({
      
        mainHeader: {
          name: "test_sceneCommands",
          map: {
            SCENE_CMD_SPECIAL_FILES: [2, "OBJECT_GAMEPLAY_DANGEON_KEEP"],
            SCENE_CMD_COL_HEADER: ["&test_sceneCollisionHeader"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_SPECIAL_FILES: 0,
            SCENE_CMD_COL_HEADER: 1,
            SCENE_CMD_END: 2,
          }
        },
        altHeaders: [],
        actors: {},
        lighting: {},
        dependency: SceneActorDependency.DUNGEON,
        collision: {
          normals: [
            0x0000, 0x7FFF, 0x0000,
            0x0000, 0x7FFF, 0x0000,
            0x0000, 0x7FFF, 0x0000,
            
            0x8001, 0x0000, 0x0000,
            0x8001, 0x0000, 0x0000,
            0x8001, 0x0000, 0x0000,

            0x36B2, 0xCF10, 0x68DD,
            0x36B2, 0xCF10, 0x68DD,
            0x36B2, 0xCF10, 0x68DD,
          ].map(n => u16ToS16(n) / 0x7FFF),
          vertices: [
            1,2,3, 
            4,5,6, 
            11,-22,33, 
            -44,55,-66],
          verticesNoIndex: [
            1,2,3,     11,-22,33,     4,5,6,
            4,5,6,       1,2,3,     -44,55,-66,
            11,-22,33, -44,55,-66,    4,5,6,
          ],
          waterBoxes: [],
        },
        textureBank: 0,
        sound: {},
        paths: {},
    });
  });

  test("Collision - With Water", async () => {
    const src = `

SceneCmd test_sceneCommands[] = {
    SCENE_CMD_SPECIAL_FILES(2, OBJECT_GAMEPLAY_DANGEON_KEEP),
    SCENE_CMD_COL_HEADER(&test_sceneCollisionHeader),
    SCENE_CMD_END(),
};

Vec3s test_sceneColl_Vertices[] = {
  {1,2,3}, 
  {4,5,6},
  {11, -22, 33}, 
  {-44, 55, -66}, 
};

CollisionPoly test_sceneColl_Polygons[] = {
  // type,       indices 0-2,          normal XYZ         distance
  {0x0000, 0xE000, 0xE002, 0x0001, 0x0000, 0x7FFF, 0x0000, 0xABCD},
  {0x0000, 0xE001, 0x0000, 0x0003, 0x8001, 0x0000, 0x0000, 0xEF12},
  {0x0001, 0xE002, 0x0003, 0x8001, 0x36B2, 0xCF10, 0x68DD, 0x3456},
};

WaterBox test_sceneCollisionWaterBox[] = {
//  xMin,  y,  zMin, xLen, zLen,  properties
  { -111, 200, -222, 200, 300, 0x0000ABCD },
  { 100,  130,   50,  10, 400, 0x00001234 },
};

CollisionHeader test_sceneCollisionHeader = {
  { -1000, -1001, -1002 }, // aabb-min
  { 2000, 2001, 2002 },    // aabb-max
  4, test_sceneColl_Vertices,
  3, test_sceneColl_Polygons,
  test_sceneColl_SurfaceType,
  test_sceneColl_CamDataList,
  2, test_sceneCollisionWaterBox
};

`;
    
    const actorTable = {list: [], enum: {}};
    const roomData = parseScene(src, "test_", actorTable, Game.OOT);

    // Indices: [0,2,1], [1,0,3], [2,3,1]

    expect(roomData).toEqual({
      
        mainHeader: {
          name: "test_sceneCommands",
          map: {
            SCENE_CMD_SPECIAL_FILES: [2, "OBJECT_GAMEPLAY_DANGEON_KEEP"],
            SCENE_CMD_COL_HEADER: ["&test_sceneCollisionHeader"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_SPECIAL_FILES: 0,
            SCENE_CMD_COL_HEADER: 1,
            SCENE_CMD_END: 2,
          }
        },
        altHeaders: [],
        actors: {},
        lighting: {},
        dependency: SceneActorDependency.DUNGEON,
        collision: {
          normals: [
            0x0000, 0x7FFF, 0x0000,
            0x0000, 0x7FFF, 0x0000,
            0x0000, 0x7FFF, 0x0000,
            
            0x8001, 0x0000, 0x0000,
            0x8001, 0x0000, 0x0000,
            0x8001, 0x0000, 0x0000,

            0x36B2, 0xCF10, 0x68DD,
            0x36B2, 0xCF10, 0x68DD,
            0x36B2, 0xCF10, 0x68DD,
          ].map(n => u16ToS16(n) / 0x7FFF),
          vertices: [
            1,2,3, 
            4,5,6, 
            11,-22,33, 
            -44,55,-66],
          verticesNoIndex: [
            1,2,3,     11,-22,33,     4,5,6,
            4,5,6,       1,2,3,     -44,55,-66,
            11,-22,33, -44,55,-66,    4,5,6,
          ],
          waterBoxes: [
            {
              pos: [-111, 200, -222],
              sizeX: 200,
              sizeZ: 300,
              flags: 0x0000ABCD     
            },
            {
              pos: [100, 130, 50],
              sizeX: 10,
              sizeZ: 400,
              flags: 0x00001234     
            }
          ],
        },
        textureBank: 0,
        sound: {},
        paths: {},
    });
  });
});
