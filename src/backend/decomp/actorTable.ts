/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import FileAPI from "../fileapi/fileAPI";
import { strip } from "./c/parser";

export const actorTable: ActorTable = {
  list: [],
  enum: {}
};

export interface ActorTable {
  list: ({
    name: string; // Enum name
    overlay: string; // Overlay Name
  } | undefined)[];
  enum: Record<string, number>;
}

export const parseActorTable = async (): Promise<ActorTable> =>
{
  actorTable.list = [];
  actorTable.enum = {};

  const tableFile = strip(await FileAPI.readFileText("include/tables/actor_table.h"));

  const lines = tableFile.split("\n");
  for(let line of lines) 
  {
    line = line.trim();
    const macro = line.match(/DEFINE_ACTO[_A-Z]+\((.*)\)/m);
    if(macro && macro[1]) 
    {
      if(macro[0].startsWith("DEFINE_ACTOR_UNSET")) {
        actorTable.list.push(undefined);
        continue;
      }

      const args = macro[1].split(",").map(x => x.trim());
      const name = args[1] || "";

      actorTable.enum[name] = actorTable.list.length;
      actorTable.list.push({
        name,
        overlay: args[0] || "",
      });
    }
  }

  return actorTable;
}

export const parseActorTableCached = async (): Promise<ActorTable> =>
{
  if(actorTable.list.length === 0) {
    await parseActorTable();
  }
  return actorTable;
}