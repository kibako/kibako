import { Vector3 } from "backend/z64/types";

export interface ActorStruct {
  id: number,
  pos: Vector3,
  rot: Vector3,
  params: number,
  spawnTime: number; // MM only
  cutsceneIndex: number; // MM only
  rotMask: number; // MM only
};

export interface TransitionActorStruct {
  roomFront: number,
  camIdxFront: number,
  roomBack: number,
  camIdxBack: number,
  id: number,
  pos: Vector3,
  rotY: number,
  params: number,
  cutsceneIndex: number; // MM only
};