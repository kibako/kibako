import { Game } from "../context";
import { SceneActorDependency } from "../z64/scenes/sceneList";
import { parseScene } from "./scenes";
//import { Decomp } from "./actorTable";

describe('Decomp - Scenes', () => 
{
  test("Example - Fast64", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"
#include "someroom_scene.h"
#include "segment_symbols.h"
#include "command_macros_base.h"
#include "z64cutscene_commands.h"
#include "variables.h"

/**
 * Header Child Day (Default)
*/
SceneCmd someroom_scene_header00[] = {
    SCENE_CMD_SOUND_SETTINGS(0x00, 0x00, 0x02),
    SCENE_CMD_ROOM_LIST(1, someroom_scene_roomList),
    SCENE_CMD_MISC_SETTINGS(0x00, 0x00),
    SCENE_CMD_COL_HEADER(&someroom_collisionHeader),
    SCENE_CMD_SPECIAL_FILES(0x00, OBJECT_GAMEPLAY_DANGEON_KEEP),
    SCENE_CMD_SKYBOX_SETTINGS(0x01, 0x00, LIGHT_MODE_TIME),
    SCENE_CMD_ENTRANCE_LIST(someroom_scene_header00_entranceList),
    SCENE_CMD_SPAWN_LIST(1, someroom_scene_header00_playerEntryList),
    SCENE_CMD_ENV_LIGHT_SETTINGS(4, someroom_scene_header00_lightSettings),
    SCENE_CMD_TRANSITION_ACTOR_LIST(1, someroom_scene_header00_transitionActors),
    SCENE_CMD_PATH_LIST(someroom_scene_pathway0),
    SCENE_CMD_END(),
};

RomFile someroom_scene_roomList[] = {
    { (u32)_someroom_room_0SegmentRomStart, (u32)_someroom_room_0SegmentRomEnd },
};

ActorEntry someroom_scene_header00_playerEntryList[] = {
    {
        /* Actor ID   */ ACTOR_PLAYER,
        /* Position   */ { 0, -120, 0 },
        /* Rotation   */ { DEG_TO_BINANG(0.000), DEG_TO_BINANG(0.000), DEG_TO_BINANG(0.000) },
        /* Parameters */ 0x0FFF
    },
};

TransitionActorEntry someroom_scene_header00_transitionActors[] = {
  // Wooden Door
  {
      /* Room & Cam Index (Front, Back) */ { 0, 0x00, 0, 0x00 },
      /* Actor ID                       */ ACTOR_EN_DOOR,
      /* Position                       */ { 40, -115, 85 },
      /* Rotation Y                     */ DEG_TO_BINANG(0.000),
      /* Parameters                     */ 0x1234
  },
};

Spawn someroom_scene_header00_entranceList[] = {
    // { Spawn Actor List Index, Room Index }
    { 0, 0 },
};

EnvLightSettings someroom_scene_header00_lightSettings[4] = {
    // Dawn Lighting
    {
        {    70,    45,    57 },   // Ambient Color
        {    73,    73,    73 },   // Diffuse0 Direction
        {   180,   154,   138 },   // Diffuse0 Color
        {   -73,   -73,   -73 },   // Diffuse1 Direction
        {    20,    20,    60 },   // Diffuse1 Color
        {   140,   120,   100 },   // Fog Color
        ((1 << 10) | 993),         // Blend Rate & Fog Near
        12800,                     // Fog Far
    },
    // Day Lighting
    {
        {   105,    90,    90 },   // Ambient Color
        {    73,    73,    73 },   // Diffuse0 Direction
        {   255,   255,   240 },   // Diffuse0 Color
        {   -73,   -73,   -73 },   // Diffuse1 Direction
        {    50,    50,    90 },   // Diffuse1 Color
        {   100,   100,   120 },   // Fog Color
        ((1 << 10) | 996),         // Blend Rate & Fog Near
        12800,                     // Fog Far
    },
    // Dusk Lighting
    {
        {   120,    90,     0 },   // Ambient Color
        {    73,    73,    73 },   // Diffuse0 Direction
        {   250,   135,    50 },   // Diffuse0 Color
        {   -73,   -73,   -73 },   // Diffuse1 Direction
        {    30,    30,    60 },   // Diffuse1 Color
        {   120,    70,    50 },   // Fog Color
        ((1 << 10) | 995),         // Blend Rate & Fog Near
        12800,                     // Fog Far
    },
    // Night Lighting
    {
        {    40,    70,   100 },   // Ambient Color
        {    73,    73,    73 },   // Diffuse0 Direction
        {    20,    20,    35 },   // Diffuse0 Color
        {   -73,   -73,   -73 },   // Diffuse1 Direction
        {    50,    50,   100 },   // Diffuse1 Color
        {     0,     0,    30 },   // Fog Color
        ((1 << 10) | 992),         // Blend Rate & Fog Near
        12800,                     // Fog Far
    },
};

Vec3s someroom_pathwayList0_header0[] = {
  {   -80,    30,   -10 },
  {   -70,    30,   -10 },
  {   -60,    30,   -10 },
  {   -50,    30,   -10 },
  {   -40,    30,   -10 },
};

Path someroom_scene_pathway0[1] = {
  { ARRAY_COUNTU(someroom_pathwayList0_header0), someroom_pathwayList0_header0 },
};

SurfaceType someroom_polygonTypes[] = {
  { 0x00000000, 0x00000000 },
};

CollisionPoly someroom_polygons[] = {
  { 0x0000, 0x0000, 0x0001, 0x0002, COLPOLY_SNORMAL(0.0), COLPOLY_SNORMAL(1.0), COLPOLY_SNORMAL(-4.371138828673793e-08), 0x0078 },
  { 0x0000, 0x0000, 0x0002, 0x0003, COLPOLY_SNORMAL(0.0), COLPOLY_SNORMAL(1.0), COLPOLY_SNORMAL(-4.371138828673793e-08), 0x0078 },
  { 0x0000, 0x0004, 0x0005, 0x0006, COLPOLY_SNORMAL(-0.64676433801651), COLPOLY_SNORMAL(0.762690007686615), COLPOLY_SNORMAL(-3.3338242388936123e-08), 0x011e },
  { 0x0000, 0x0004, 0x0006, 0x0007, COLPOLY_SNORMAL(-0.64676433801651), COLPOLY_SNORMAL(0.762690007686615), COLPOLY_SNORMAL(-3.3338242388936123e-08), 0x011e },
  { 0x0000, 0x0002, 0x0001, 0x0005, COLPOLY_SNORMAL(-0.6467645168304443), COLPOLY_SNORMAL(0.7626897692680359), COLPOLY_SNORMAL(-3.333822817808141e-08), 0x011e },
  { 0x0000, 0x0002, 0x0005, 0x0004, COLPOLY_SNORMAL(-0.6467645168304443), COLPOLY_SNORMAL(0.7626897692680359), COLPOLY_SNORMAL(-3.333822817808141e-08), 0x011e },
  { 0x0000, 0x0008, 0x0000, 0x0003, COLPOLY_SNORMAL(0.0), COLPOLY_SNORMAL(1.0), COLPOLY_SNORMAL(-4.371138828673793e-08), 0x0078 },
  { 0x0000, 0x0008, 0x0003, 0x0009, COLPOLY_SNORMAL(0.0), COLPOLY_SNORMAL(1.0), COLPOLY_SNORMAL(-4.371138828673793e-08), 0x0078 },
};

Vec3s someroom_vertices[10] = {
	{ 275, -120, 300 },
	{ 300, -120, 300 },
	{ 300, -120, -300 },
	{ 275, -120, -300 },
	{ 319, -104, -300 },
	{ 319, -104, 300 },
	{ 390, -44, 300 },
	{ 390, -44, -300 },
	{ -300, -120, 300 },
	{ -300, -120, -300 },
};

CollisionHeader someroom_collisionHeader = {
	-300,
	-120,
	-300,
	390,
	-44,
	300,
	10,
  someroom_vertices,
  8,
  someroom_polygons,
  someroom_polygonTypes,
  0,
  0,
  0
};
    
    
`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_DOOR: 0x10,
      }
    };

    const roomData = parseScene(src, "someroom", actorTable, Game.OOT);

    roomData.paths.someroom_scene_pathway0[0].uuid = "UUID";

    expect(roomData).toEqual({
        mainHeader: {
          name: "someroom_scene_header00",
          map: {
            SCENE_CMD_SOUND_SETTINGS: [0x00, 0x00, 0x02],
            SCENE_CMD_ROOM_LIST: [1, "someroom_scene_roomList"],
            SCENE_CMD_MISC_SETTINGS: [0x00, 0x00],
            SCENE_CMD_COL_HEADER: ["&someroom_collisionHeader"],
            SCENE_CMD_SPECIAL_FILES: [0x00, "OBJECT_GAMEPLAY_DANGEON_KEEP"],
            SCENE_CMD_SKYBOX_SETTINGS: [0x01, 0x00, "LIGHT_MODE_TIME"],
            SCENE_CMD_ENTRANCE_LIST: ["someroom_scene_header00_entranceList"],
            SCENE_CMD_SPAWN_LIST: [1, "someroom_scene_header00_playerEntryList"],
            SCENE_CMD_ENV_LIGHT_SETTINGS: [4, "someroom_scene_header00_lightSettings"],
            SCENE_CMD_TRANSITION_ACTOR_LIST: [1, "someroom_scene_header00_transitionActors"],
            SCENE_CMD_PATH_LIST: ["someroom_scene_pathway0"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_SOUND_SETTINGS: 0,
            SCENE_CMD_ROOM_LIST: 1,
            SCENE_CMD_MISC_SETTINGS: 2,
            SCENE_CMD_COL_HEADER: 3,
            SCENE_CMD_SPECIAL_FILES: 4,
            SCENE_CMD_SKYBOX_SETTINGS: 5,
            SCENE_CMD_ENTRANCE_LIST: 6,
            SCENE_CMD_SPAWN_LIST: 7,
            SCENE_CMD_ENV_LIGHT_SETTINGS: 8,
            SCENE_CMD_TRANSITION_ACTOR_LIST: 9,
            SCENE_CMD_PATH_LIST: 10,
            SCENE_CMD_END: 11,
          }
        },
        altHeaders: [],
        dependency: SceneActorDependency.DUNGEON,
        actors: {
          someroom_scene_header00_transitionActors: [
            {
              roomFront: 0,
              camIdxFront: 0,
              roomBack: 0,
              camIdxBack: 0,
              id: 0x10,
              pos: [40, -115, 85],
              rotY: 0,
              params: 0x1234,
              cutsceneIndex: 0,
            },
          ]
        },
        lighting: {
          someroom_scene_header00_lightSettings: [
            {
              ambientColor     : [ 70,    45,    57],
              diff0Dir: [ 73,    73,    73],
              diff0Color    : [ 180,   154,   138],
              diff1Dir: [-73,   -73,   -73],
              diff1Color    : [ 20,    20,    60],
              fogColor         : [140,   120,   100],
              forStart    : 0, // @TODO: parse
              drawDist: 12800
            },
            {
              ambientColor     : [ 105,    90,    90],
              diff0Dir: [  73,    73,    73],
              diff0Color    : [ 255,   255,   240],
              diff1Dir: [ -73,   -73,   -73],
              diff1Color    : [  50,    50,    90],
              fogColor         : [ 100,   100,   120],
              forStart    : 0, // @TODO: parse
              drawDist: 12800
            },
            {
              ambientColor     : [120,    90,     0],
              diff0Dir: [ 73,    73,    73],
              diff0Color    : [250,   135,    50],
              diff1Dir: [-73,   -73,   -73],
              diff1Color    : [ 30,    30,    60],
              fogColor         : [120,    70,    50],
              forStart    : 0, // @TODO: parse
              drawDist: 12800
            },
            {
              ambientColor     : [ 40,    70,   100],
              diff0Dir: [ 73,    73,    73],
              diff0Color    : [ 20,    20,    35],
              diff1Dir: [-73,   -73,   -73],
              diff1Color    : [ 50,    50,   100],
              fogColor         : [  0,     0,    30],
              forStart    : 0, // @TODO: parse
              drawDist: 12800
            },
          ]
        },
        collision: {
          normals:     [
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
-0.6467787713248085, 0.7626880703146458, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719,
            0,                  1, -0.00003051850947599719
          ],
          vertices: [
            275, -120, 300,
            300, -120, 300,
            300, -120, -300,
            275, -120, -300,
            319, -104, -300,
            319, -104, 300,
            390, -44, 300,
            390, -44, -300,
            -300, -120, 300,
            -300, -120, -300,
          ],

          verticesNoIndex: [
            /* 0 */ 275, -120, 300,
            /* 1 */ 300, -120, 300,
            /* 2 */ 300, -120, -300,

            /* 0 */ 275, -120, 300,
            /* 2 */ 300, -120, -300,
            /* 3 */ 275, -120, -300,

            /* 4 */ 319, -104, -300,
            /* 5 */ 319, -104, 300,
            /* 6 */ 390, -44, 300,

            /* 4 */ 319, -104, -300,
            /* 6 */ 390, -44, 300,
            /* 7 */ 390, -44, -300,

            /* 2 */ 300, -120, -300,
            /* 1 */ 300, -120, 300,
            /* 5 */ 319, -104, 300,

            /* 2 */ 300, -120, -300,
            /* 5 */ 319, -104, 300,
            /* 4 */ 319, -104, -300,

            /* 8 */ -300, -120, 300,
            /* 0 */ 275, -120, 300,
            /* 3 */ 275, -120, -300,

            /* 8 */ -300, -120, 300,
            /* 3 */ 275, -120, -300,
            /* 9 */ -300, -120, -300,
          ],
          waterBoxes: [],
        },
        textureBank: 0,
        sound: {
          someroom_scene_header00: {
            preset: 0,
            ambience: 0,
            bgm: 2,
          }
        },
        paths: {
          someroom_scene_pathway0: [
            {
              uuid: "UUID",
              additionalPathIdx: 0, // MM only
              customValue: 0, // MM only
              points: [
                [-80,    30,   -10],
                [-70,    30,   -10],
                [-60,    30,   -10],
                [-50,    30,   -10],
                [-40,    30,   -10],
              ]
            },
          ]
        }
    });
  });
});
