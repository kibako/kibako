import * as Decomp from "./actorTable";
import FileAPI from "../fileapi/fileAPI";

describe('Decomp - ActorTable', () => 
{
  let testFileActor = "";

  FileAPI.setAPI({
    type: FileAPI.APIType.NODE,
    readFileBuffer: async function (path: string): Promise<ArrayBuffer> {
      throw new Error("Tried to read unknown file: " + path);
    },
    readFileText: async function (path: string): Promise<string> {
      switch(path) {
        case 'include/tables/actor_table.h': return testFileActor;
        default: throw new Error("Tried to read unknown file: " + path); break;
      }
    },
    writeFileText: async function(){},
    openDir: async function(){},
    scanDir: async function() {
      return {files: [], dirs: []};
    },
  });

  test("Parse empty", async () => {
    testFileActor = "";

    const table = await Decomp.parseActorTable();
    expect(table).toEqual({
      list: [],
      enum: {},
    });
  });

  test("Parse example", async () => {

testFileActor = `/**
* File Description
*
* DEFINE_ACTOR_UNSET is needed to define empty entries from the original game
*/
/* 0x0000 */ DEFINE_ACTOR_INTERNAL(Player, ACTOR_PLAYER, ACTOROVL_ALLOC_NORMAL, "Player")
/* 0x0001 */ DEFINE_ACTOR_UNSET(ACTOR_UNSET_1)

/* 0x0002 */ DEFINE_ACTOR(En_TestA, ACTOR_EN_TEST_A, ACTOROVL_ALLOC_NORMAL, "En_TestA")

/* 0x0003 */ DEFINE_ACTOR(En_TestB, ACTOR_EN_TEST_B, ACTOROVL_ALLOC_NORMAL, "En_TestB")

    `;
    const table = await Decomp.parseActorTable();

    expect(table).toEqual({
      list: [
        {name: "ACTOR_PLAYER", overlay: "Player"},
        undefined,
        {name: "ACTOR_EN_TEST_A", overlay: "En_TestA"},
        {name: "ACTOR_EN_TEST_B", overlay: "En_TestB"},
      ],
      enum: {
        ACTOR_PLAYER: 0x00,
        ACTOR_EN_TEST_A: 0x02,
        ACTOR_EN_TEST_B: 0x03,
      }
    });
  });
});
