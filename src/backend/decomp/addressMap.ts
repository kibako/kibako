import FileAPI from "../fileapi/fileAPI";

let addressMap: Record<string, number> = {};

export async function readAddressMap(): Promise<Record<string, number>>
{
  addressMap = {};
  const perf = performance.now();

  const buildFiles = await FileAPI.scanDir("build");
  const mapFileName = buildFiles.files.find(n => n.endsWith(".map"));
  const mapFile = await FileAPI.readFileText("build/" + mapFileName);

  const parts = mapFile.matchAll(/^[ ]+?0x00000000([0-9A-F]{8})[ ]+?([a-z_][a-z0-9_]+)/gim);

  for(const entry of parts) {
    addressMap[entry[2]] = parseInt(entry[1], 16);
  }

  addressMap._COUNT = Object.keys(addressMap).length;
  console.log("AddressMap Parser: " + (performance.now() - perf) + "ms");
  return addressMap; 
}

export async function readAddressMapCached(): Promise<Record<string, number>>
{
  if(addressMap._COUNT) {
    return addressMap;
  }
  return await readAddressMap();
}

/**
 * Converts a RAM address from the code-section back into a ROM address
 */
export function codeRamToRom(map: Record<string, number>, addressRam: number) 
{
  // By default, code in ROM: 0xA94000 to 0xBCEF30
  return map._codeSegmentRomStart + (addressRam - map._codeSegmentStart);
};