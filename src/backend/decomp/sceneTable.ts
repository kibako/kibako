/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import FileAPI from "../fileapi/fileAPI";
import { parseFileTypedefEnum, strip } from "./c/parser";

export namespace Decomp 
{
  export interface SceneDef {
    name: string;
    path: string;
    titleName: string;
    enumName: string;
    drawCfgEnum: string;
    drawCfgId: number;
  }

  export const parseSceneTable = async (): Promise<SceneDef[]> =>
  {
    const sceneDefs: SceneDef[] = [];
    const [sceneTable, sceneHeader, specFile] = await Promise.all([
      await FileAPI.readFileText("include/tables/scene_table.h"),
      await FileAPI.readFileText("include/z64scene.h"),
      await FileAPI.readFileText("spec"),
    ]);

    const drawEnum = parseFileTypedefEnum(sceneHeader, "SceneDrawConfig");
    const tableStr = strip(sceneTable);

    const lines = tableStr.split("\n");
    for(const line of lines) 
    {
      const macro = line.match(/DEFINE_SCENE\((.*)\)/m);
      if(macro && macro[1]) 
      {
        const args = macro[1].split(",").map(x => x.trim());
        const name = args[0] || "";

        // try to get the path of that scene
        let path = "";
        const pathReg = specFile.match(new RegExp(`include "build/(.*)/${name}.o`, 'm'));
        if(pathReg && pathReg[1]) {
          path = pathReg[1];
        }

        sceneDefs.push({
          name, path,
          titleName: args[1] || "",
          enumName: args[2] || "",
          drawCfgEnum: args[3] || "",
          drawCfgId: drawEnum.enum[args[3] || ""] || 0
        });
      }
    }

    return sceneDefs;
  }
}