import { Game } from "../context";
import { parseRoom, sortRoomFiles } from "./rooms";
//import { Decomp } from "./actorTable";

describe('Decomp - Rooms', () => 
{
  test("File Sort - normal", () => {
    const res = sortRoomFiles([
      "myroom_room_3.c",
      "myroom_room_0.c",
      "myroom_room_2.c",
      "myroom_room_1.c",
    ]);

    expect(res).toEqual([
      "myroom_room_0.c","myroom_room_1.c",
      "myroom_room_2.c","myroom_room_3.c",
    ]);
  });

  test("File Sort - with numbers", () => {
    const res = sortRoomFiles([
      "myroom_1_room_3.c",
      "myroom_1_room_0.c",
      "myroom_1_room_2.c",
      "myroom_1_room_1.c",
    ]);

    expect(res).toEqual([
      "myroom_1_room_0.c",
      "myroom_1_room_1.c",
      "myroom_1_room_2.c",
      "myroom_1_room_3.c",
    ]);
  });


  test("Example - Single Layer", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_room_0Commands[] = {
    SCENE_CMD_ACTOR_LIST(3, someroom_room_0ActorEntry_00005C),
    SCENE_CMD_OBJECT_LIST(2, someroom_room_0ObjectList_000040),
    SCENE_CMD_ECHO_SETTINGS(0),
    SCENE_CMD_END(),
};

s16 someroom_room_0ObjectList_000040[] = {
    OBJECT_ABC,
    OBJECT_DEF,
};

ActorEntry someroom_room_0ActorEntry_00005C[] = {
    { ACTOR_EN_ABC,        {   111,      0,   -10 }, {      0, 0X4000,      0 }, 0xABCD },
    { ACTOR_EN_DEF,        {   222,      5,   -20 }, {      0, 0X4234,      0 }, 0x1234 },
    { ACTOR_EN_ABC,        {   333,      0,   -30 }, {      0, 0XABCD,      0 }, 0x5678 },
};

PolygonDlist2 someroom_room_0PolygonDlist2_00032C[3] = {
  { {  -1000,    210,  -1230 },   1024, someroom_room_2DL_001B30, NULL },
  { {  -1200,    220,  -1230 },    512, someroom_room_2DL_0035C8, NULL },
  { {  -1300,    230,  -1230 },    128, NULL, someroom_room_2DL_006AA0 },
};

u8 someroom_room_0_possiblePadding_000FFF = 42;

Gfx someroom_room_0DL_000A50[] = {
  gsDPSomeCoolCmd(),
  gsDPSomeCoolCmd(11),
};

u64 someroom_room_0Tex_000E00[] = {
  #include "assets/scenes/someroom/someroom_room_0Tex_000E00.rgba16.inc.c"
};`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_ABC: 0x10,
        ACTOR_EN_DEF: 0x42,
      }
    };

    const objectTable = {
      list: [],
      enum: {
        OBJECT_ABC: 0x20,
        OBJECT_DEF: 0x21,
      }
    };

    const addressMap = {
      someroom_room_2DL_001B30: 0x03000110,
      someroom_room_2DL_0035C8: 0x83000220,
      someroom_room_2DL_006AA0: 0x03000330,
    };

    const roomData = parseRoom(src, "someroom_room_0", actorTable, objectTable, addressMap, Game.OOT);
    expect(roomData).toEqual({
      mainHeader: {
        name: "someroom_room_0Commands",
        map: {
          SCENE_CMD_ACTOR_LIST: [3, "someroom_room_0ActorEntry_00005C"],
          SCENE_CMD_OBJECT_LIST: [2, "someroom_room_0ObjectList_000040"],
          SCENE_CMD_ECHO_SETTINGS: [0],
          SCENE_CMD_END: [],
        },
        indices: {
          SCENE_CMD_ACTOR_LIST: 0,
          SCENE_CMD_OBJECT_LIST: 1,
          SCENE_CMD_ECHO_SETTINGS: 2,
          SCENE_CMD_END: 3,
        }
      },
      altHeaders: [],
      actors: {
        someroom_room_0ActorEntry_00005C: [
          {id: 0x10, pos: [111,  0, -10], rot: [0, 0x4000, 0], params: 0xABCD, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
          {id: 0x42, pos: [222,  5, -20], rot: [0, 0x4234, 0], params: 0x1234, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
          {id: 0x10, pos: [333,  0, -30], rot: [0, 0xABCD, 0], params: 0x5678, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
        ]
      },
      objList: {
        someroom_room_0ObjectList_000040: [0x20, 0x21],
      },
      mesh: {
        opaque: [0x110, 0x220],
        transparent: [0x330]
      }
    });
  });

  test("Example - Multi Layer", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd someroom_room_0Commands[] = {
    SCENE_CMD_ECHO_SETTINGS(0),
    SCENE_CMD_ALTERNATE_HEADER_LIST(someroom_room_0AlternateHeaders0x000050),
    SCENE_CMD_OBJECT_LIST(2, someroom_room_0ObjectList_000040),
    SCENE_CMD_ACTOR_LIST(2, someroom_room_0ActorEntry_0000A0),
    SCENE_CMD_END(),
};

SceneCmd* someroom_room_0AlternateHeaders0x000050[] = {
  NULL,
  someroom_room_0Set_000470,
  NULL,
};

SceneCmd someroom_room_0Set_000470[] = {
  SCENE_CMD_ECHO_SETTINGS(0),
  SCENE_CMD_ACTOR_LIST(3, someroom_room_0ActorEntry_0000F0),
  SCENE_CMD_OBJECT_LIST(1, someroom_room_0ObjectList_000080),
  SCENE_CMD_END(),
};

s16 someroom_room_0ObjectList_000040[] = {
    OBJECT_ABC,
    OBJECT_DEF,
};

ActorEntry someroom_room_0ActorEntry_0000A0[] = {
  { ACTOR_EN_ABC,        {   99,      0,   -10 }, {      0, 0X4000,      0 }, 0xABCD },
  { ACTOR_EN_ABC,        {   55,      0,   -30 }, {      0, 0XABCD,      0 }, 0x5678 },
};

u8 someroom_room_0_possiblePadding_000FFF = 42;

s16 someroom_room_0ObjectList_000080[] = {
  OBJECT_ABC,
};

ActorEntry someroom_room_0ActorEntry_0000F0[] = {
    { ACTOR_EN_ABC,        {   111,      0,   -10 }, {      0, 0X4000,      0 }, 0xABCD },
    { ACTOR_EN_DEF,        {   222,      5,   -20 }, {      0, 0X4234,      0 }, 0x1234 },
    { ACTOR_EN_ABC,        {   333,      0,   -30 }, {      0, 0XABCD,      0 }, 0x5678 },
};

// test old "RoomShapeCullableEntry" here
RoomShapeCullableEntry someroom_room_0PolygonDlist_00032C[2] = {
  { someroom_room_0DL_000B50, someroom_room_0DL_000D50 },
  { NULL, someroom_room_0DL_000C50 },
};

Gfx someroom_room_0DL_000A50[] = {
  gsDPSomeCoolCmd(),
  gsDPSomeCoolCmd(11),
};

u64 someroom_room_0Tex_000E00[] = {
  #include "assets/scenes/someroom/someroom_room_0Tex_000E00.rgba16.inc.c"
};`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_ABC: 0x10,
        ACTOR_EN_DEF: 0x42,
      }
    };

    const objectTable = {
      list: [],
      enum: {
        OBJECT_ABC: 0x20,
        OBJECT_DEF: 0x21,
      }
    };

    const addressMap = {
      someroom_room_0DL_000B50: 0x03000110,
      someroom_room_0DL_000D50: 0x83000220,
      someroom_room_0DL_000C50: 0x03000330,
    };

    const roomData = parseRoom(src, "someroom_room_0", actorTable, objectTable, addressMap, Game.OOT);
    expect(roomData).toEqual({
      mainHeader: {
        name: "someroom_room_0Commands",
        map: {
          SCENE_CMD_ECHO_SETTINGS: [0],
          SCENE_CMD_ALTERNATE_HEADER_LIST: ["someroom_room_0AlternateHeaders0x000050"],
          SCENE_CMD_OBJECT_LIST: [2, "someroom_room_0ObjectList_000040"],
          SCENE_CMD_ACTOR_LIST: [2, "someroom_room_0ActorEntry_0000A0"],
          SCENE_CMD_END: [],
        },
        indices: {
          SCENE_CMD_ECHO_SETTINGS: 0,
          SCENE_CMD_ALTERNATE_HEADER_LIST: 1,
          SCENE_CMD_OBJECT_LIST: 2,
          SCENE_CMD_ACTOR_LIST: 3,
          SCENE_CMD_END: 4,
        }
      },
      altHeaders: [
        undefined,
        {
          name: "someroom_room_0Set_000470",
          map: {
            SCENE_CMD_ECHO_SETTINGS: [0],
            SCENE_CMD_ACTOR_LIST: [3, "someroom_room_0ActorEntry_0000F0"],
            SCENE_CMD_OBJECT_LIST: [1, "someroom_room_0ObjectList_000080"],
            SCENE_CMD_END: [],
          },
          indices: {
            SCENE_CMD_ECHO_SETTINGS: 0,
            SCENE_CMD_ACTOR_LIST: 1,
            SCENE_CMD_OBJECT_LIST: 2,
            SCENE_CMD_END: 3,
          }
        },
        undefined
      ],
      actors: {
        someroom_room_0ActorEntry_0000A0: [
          {id: 0x10, pos: [99,  0, -10], rot: [0, 0x4000, 0], params: 0xABCD, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
          {id: 0x10, pos: [55,  0, -30], rot: [0, 0xABCD, 0], params: 0x5678, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
        ],
        someroom_room_0ActorEntry_0000F0: [
          {id: 0x10, pos: [111,  0, -10], rot: [0, 0x4000, 0], params: 0xABCD, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
          {id: 0x42, pos: [222,  5, -20], rot: [0, 0x4234, 0], params: 0x1234, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
          {id: 0x10, pos: [333,  0, -30], rot: [0, 0xABCD, 0], params: 0x5678, spawnTime: 0, cutsceneIndex: 0, rotMask: 0},
        ]
      },
      objList: {
        someroom_room_0ObjectList_000040: [0x20, 0x21],
        someroom_room_0ObjectList_000080: [0x20],
      },
      mesh: {
        opaque: [0x110],
        transparent: [0x220, 0x330]
      }
    });
  });
});
