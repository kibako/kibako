import { Game } from "../context";
import { parseRoom } from "./rooms";
//import { Decomp } from "./actorTable";

describe('Decomp - Rooms', () => 
{
  test("Example - Fast64 Room", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"
#include "someroom_scene.h"
#include "segment_symbols.h"
#include "command_macros_base.h"
#include "z64cutscene_commands.h"
#include "variables.h"

/**
 * Header Child Day (Default)
*/
#define LENGTH_someroom_ROOM_0_HEADER00_OBJECTLIST 1
#define LENGTH_someroom_ROOM_0_HEADER00_ACTORLIST 2
SceneCmd someroom_room_0_header00[] = {
    SCENE_CMD_ECHO_SETTINGS(0x00),
    SCENE_CMD_ROOM_BEHAVIOR(0x00, 0x00, false, false),
    SCENE_CMD_SKYBOX_DISABLES(false, false),
    SCENE_CMD_TIME_SETTINGS(0xFF, 0xFF, 10),
    SCENE_CMD_ROOM_SHAPE(&someroom_room_0_shapeHeader),
    SCENE_CMD_OBJECT_LIST(LENGTH_someroom_ROOM_0_HEADER00_OBJECTLIST, someroom_room_0_header00_objectList),
    SCENE_CMD_ACTOR_LIST(LENGTH_someroom_ROOM_0_HEADER00_ACTORLIST, someroom_room_0_header00_actorList),
    SCENE_CMD_END(),
};

s16 someroom_room_0_header00_objectList[LENGTH_someroom_ROOM_0_HEADER00_OBJECTLIST] = {
    OBJECT_BOX,
};

ActorEntry someroom_room_0_header00_actorList[LENGTH_someroom_ROOM_0_HEADER00_ACTORLIST] = {
    // Treasure Chest
    {
        /* Actor ID   */ ACTOR_EN_BOX,
        /* Position   */ { -54, -116, 99 },
        /* Rotation   */ { DEG_TO_BINANG(0.000), DEG_TO_BINANG(0.000), DEG_TO_BINANG(0.000) },
        /* Parameters */ 0xABCD
    },

    // Treasure Chest
    {
        /* Actor ID   */ ACTOR_EN_BOX,
        /* Position   */ { -9, -116, 99 },
        /* Rotation   */ { DEG_TO_BINANG(0.000), DEG_TO_BINANG(45.000), DEG_TO_BINANG(0.000) },
        /* Parameters */ 0x1234
    },
};

RoomShapeNormal someroom_room_0_shapeHeader = {
    ROOM_SHAPE_TYPE_NORMAL,
    ARRAY_COUNT(someroom_room_0_shapeDListEntry),
    someroom_room_0_shapeDListEntry,
    someroom_room_0_shapeDListEntry + ARRAY_COUNT(someroom_room_0_shapeDListEntry)
};

RoomShapeDListsEntry someroom_room_0_shapeDListEntry[1] = {
    { someroom_room_0_entry_0_opaque, NULL },
};

Gfx someroom_room_0_entry_0_opaque[] = {
  gsSPDisplayList(someroom_dl_Floor_mesh_layer_Opaque),
  gsSPEndDisplayList(),
};

u64 someroom_dl_Bark06_32_ci8[] = {
  0x0001020304040005, 0x0607060301080709, 0x0a0b0c00080a000d, 0x04070e020d080d0f, 0x0403070409011004, 0x0401061103010102, 0x0b0311000f020d0d, 0x0b021204030f0203, 
  ...
};

u64 someroom_dl_Bark06_32_pal_rgba16[] = {
  0x3147520b41895a0b, 0x49c939896a8d3987, 0x51cb4989394949cb, 0x5a4d39473105624d, 0x29056a4d314541c7, 0x52097acf5a4b6a8f, 0x418772cf3107728f, 0x3185210539452103, 
  0x290320c518833985, 0x290751c94a050000
};

Vtx someroom_dl_Floor_mesh_layer_Opaque_vtx_cull[8] = {
  {{ {-300, -120, 300}, 0, {-16, -16}, {0, 0, 0, 0} }},
  {{ {-300, -44, 300}, 0, {-16, -16}, {0, 0, 0, 0} }},
  {{ {-300, -44, -300}, 0, {-16, -16}, {0, 0, 0, 0} }},
  {{ {-300, -120, -300}, 0, {-16, -16}, {0, 0, 0, 0} }},
  {{ {390, -120, 300}, 0, {-16, -16}, {0, 0, 0, 0} }},
  {{ {390, -44, 300}, 0, {-16, -16}, {0, 0, 0, 0} }},
  {{ {390, -44, -300}, 0, {-16, -16}, {0, 0, 0, 0} }},
  {{ {390, -120, -300}, 0, {-16, -16}, {0, 0, 0, 0} }},
};

Vtx someroom_dl_Floor_mesh_layer_Opaque_vtx_0[10] = {
  {{ {319, -104, -300}, 0, {-3013, -2536}, {255, 255, 255, 255} }},
  {{ {390, -44, 300}, 0, {3837, -3599}, {163, 181, 255, 255} }},
  {{ {390, -44, -300}, 0, {-3013, -3599}, {163, 181, 255, 255} }},
  {{ {319, -104, 300}, 0, {3837, -2536}, {255, 255, 255, 255} }},
  {{ {300, -120, -300}, 0, {-3013, -2259}, {56, 56, 56, 255} }},
  {{ {300, -120, 300}, 0, {3837, -2259}, {0, 0, 0, 255} }},
  {{ {275, -120, 300}, 0, {3837, -1970}, {255, 255, 255, 255} }},
  {{ {275, -120, -300}, 0, {-3013, -1970}, {255, 255, 255, 255} }},
  {{ {-300, -120, 300}, 0, {3837, 4591}, {163, 181, 255, 255} }},
  {{ {-300, -120, -300}, 0, {-3013, 4591}, {163, 181, 255, 255} }},
};

Gfx someroom_dl_Floor_mesh_layer_Opaque_tri_0[] = {
  gsSPVertex(someroom_dl_Floor_mesh_layer_Opaque_vtx_0 + 0, 10, 0),
  gsSP2Triangles(0, 1, 2, 0, 0, 3, 1, 0),
  gsSP2Triangles(4, 3, 0, 0, 4, 5, 3, 0),
  gsSP2Triangles(6, 5, 4, 0, 6, 4, 7, 0),
  gsSP2Triangles(8, 6, 7, 0, 8, 7, 9, 0),
  gsSPEndDisplayList(),
};

Gfx mat_someroom_dl_floor_mat_layerOpaque[] = {
  gsDPPipeSync(),
  gsDPSetCombineLERP(TEXEL0, 0, SHADE, 0, 0, 0, 0, 1, COMBINED, 0, PRIMITIVE, 0, 0, 0, 0, COMBINED),
  gsSPLoadGeometryMode(G_ZBUFFER | G_SHADE | G_CULL_BACK | G_FOG | G_SHADING_SMOOTH),
  gsSPSetOtherMode(G_SETOTHERMODE_H, 4, 20, G_AD_NOISE | G_CD_MAGICSQ | G_CK_NONE | G_TC_FILT | G_TF_BILERP | G_TT_RGBA16 | G_TL_TILE | G_TD_CLAMP | G_TP_PERSP | G_CYC_2CYCLE | G_PM_NPRIMITIVE),
  gsSPSetOtherMode(G_SETOTHERMODE_L, 0, 32, G_AC_NONE | G_ZS_PIXEL | G_RM_FOG_SHADE_A | G_RM_AA_ZB_OPA_SURF2),
  gsSPTexture(65535, 65535, 0, 0, 1),
  gsDPSetPrimColor(0, 0, 255, 255, 255, 255),
  gsDPSetTextureImage(G_IM_FMT_RGBA, G_IM_SIZ_16b, 1, someroom_dl_Bark06_32_pal_rgba16),
  gsDPSetTile(0, 0, 0, 256, 5, 0, G_TX_WRAP | G_TX_NOMIRROR, 0, 0, G_TX_WRAP | G_TX_NOMIRROR, 0, 0),
  gsDPLoadTLUTCmd(5, 38),
  gsDPSetTextureImage(G_IM_FMT_CI, G_IM_SIZ_8b_LOAD_BLOCK, 1, someroom_dl_Bark06_32_ci8),
  gsDPSetTile(G_IM_FMT_CI, G_IM_SIZ_8b_LOAD_BLOCK, 0, 0, 7, 0, G_TX_WRAP | G_TX_NOMIRROR, 0, 0, G_TX_WRAP | G_TX_NOMIRROR, 0, 0),
  gsDPLoadBlock(7, 0, 0, 511, 512),
  gsDPSetTile(G_IM_FMT_CI, G_IM_SIZ_8b, 4, 0, 0, 0, G_TX_WRAP | G_TX_NOMIRROR, 5, 0, G_TX_WRAP | G_TX_NOMIRROR, 5, 0),
  gsDPSetTileSize(0, 0, 0, 124, 124),
  gsSPEndDisplayList(),
};

Gfx someroom_dl_Floor_mesh_layer_Opaque[] = {
  gsSPClearGeometryMode(G_LIGHTING),
  gsSPVertex(someroom_dl_Floor_mesh_layer_Opaque_vtx_cull + 0, 8, 0),
  gsSPSetGeometryMode(G_LIGHTING),
  gsSPCullDisplayList(0, 7),
  gsSPDisplayList(mat_someroom_dl_floor_mat_layerOpaque),
  gsSPDisplayList(someroom_dl_Floor_mesh_layer_Opaque_tri_0),
  gsSPEndDisplayList(),
};
`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_BOX: 0x20,
      }
    };

    const objectTable = {
      list: [],
      enum: {
        OBJECT_BOX: 0x42,
      }
    };

    const addressMap = {
      someroom_room_0_entry_0_opaque: 0x03000110
    };

    const roomData = parseRoom(src, "someroom_room", actorTable, objectTable, addressMap, Game.OOT);
    expect(roomData).toEqual({
      mainHeader: {
        name: "someroom_room_0_header00",
        map: {
          SCENE_CMD_ECHO_SETTINGS: [0x00],
          SCENE_CMD_ROOM_BEHAVIOR: [0x00, 0x00, "false", "false"],
          SCENE_CMD_SKYBOX_DISABLES: ["false", "false"],
          SCENE_CMD_TIME_SETTINGS: [0xFF, 0xFF, 10],
          SCENE_CMD_ROOM_SHAPE: ["&someroom_room_0_shapeHeader"],
          SCENE_CMD_OBJECT_LIST: ["LENGTH_someroom_ROOM_0_HEADER00_OBJECTLIST", "someroom_room_0_header00_objectList"],
          SCENE_CMD_ACTOR_LIST: ["LENGTH_someroom_ROOM_0_HEADER00_ACTORLIST", "someroom_room_0_header00_actorList"],
          SCENE_CMD_END: [],
        },
        indices: {
          SCENE_CMD_ECHO_SETTINGS: 0,
          SCENE_CMD_ROOM_BEHAVIOR: 1,
          SCENE_CMD_SKYBOX_DISABLES: 2,
          SCENE_CMD_TIME_SETTINGS: 3,
          SCENE_CMD_ROOM_SHAPE: 4,
          SCENE_CMD_OBJECT_LIST: 5,
          SCENE_CMD_ACTOR_LIST: 6,
          SCENE_CMD_END: 7,
        }
      },
      altHeaders: [],
      actors: {
        someroom_room_0_header00_actorList: [
          {id: 0x20, pos: [-54, -116, 99], rot: [0, 0, 0],    params: 0xABCD, cutsceneIndex: 0, spawnTime: 0, rotMask: 0},
          {id: 0x20, pos: [-9, -116, 99],  rot: [0, 8192, 0], params: 0x1234, cutsceneIndex: 0, spawnTime: 0, rotMask: 0},
        ]
      },
      objList: {
        someroom_room_0_header00_objectList: [0x42],
      },
      mesh: {
        opaque: [0x110],
        transparent: []
      }
    });
  });

});
