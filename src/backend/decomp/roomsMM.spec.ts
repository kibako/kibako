import { Game } from "../context";
import { parseRoom, sortRoomFiles } from "./rooms";
//import { Decomp } from "./actorTable";

describe('Decomp - MM - Rooms', () => 
{
  test("File Sort - normal", () => {
    const res = sortRoomFiles([
      "Z2_COOLROOM_3.c",
      "Z2_COOLROOM_0.c",
      "Z2_COOLROOM_2.c",
      "Z2_COOLROOM_1.c",
    ]);

    expect(res).toEqual([
      "Z2_COOLROOM_0.c","Z2_COOLROOM_1.c",
      "Z2_COOLROOM_2.c","Z2_COOLROOM_3.c",
    ]);
  });

  test("Example - Single Layer", async () => {
    const src = `
#include "ultra64.h"
#include "z64.h"
#include "macros.h"

SceneCmd Z2_EXAMPLE_room_0Commands[] = {
    SCENE_CMD_ACTOR_LIST(3, Z2_EXAMPLE_room_0ActorEntry_00005C),
    SCENE_CMD_OBJECT_LIST(2, Z2_EXAMPLE_room_0ObjectList_000040),
    SCENE_CMD_ECHO_SETTINGS(0),
    SCENE_CMD_END(),
};

s16 Z2_EXAMPLE_room_0ObjectList_000040[] = {
    OBJECT_ABC,
    OBJECT_DEF,
};

ActorEntry Z2_EXAMPLE_room_0ActorEntry_00005C[] = {
  { ACTOR_EN_ABC,          {   -400,    -10,    100 }, { SPAWN_ROT_FLAGS(    0, 0x0007), SPAWN_ROT_FLAGS(0X15B,
    0x007F),
  SPAWN_ROT_FLAGS(    0, 0x007F) }, 0xABCD },
  { ACTOR_EN_DEF | 0xE000, {    111,      0,    200 }, { SPAWN_ROT_FLAGS( 0X14, 0x0007), SPAWN_ROT_FLAGS( 0X1E,
      0x007F),
  SPAWN_ROT_FLAGS(    0, 0x0070) }, 0x1234 },
  { ACTOR_EN_ABC,          {   -555,     60,    -50}, { SPAWN_ROT_FLAGS(    0, 0x0007), SPAWN_ROT_FLAGS( 0X5A,
      0x007F),
  SPAWN_ROT_FLAGS(    0, 0x007F) }, 0x5678 },
};

PolygonDlist2 Z2_EXAMPLE_room_0PolygonDlist2_00032C[3] = {
  { {  -1000,    210,  -1230 },   1024, Z2_EXAMPLE_room_2DL_001B30, NULL },
  { {  -1200,    220,  -1230 },    512, Z2_EXAMPLE_room_2DL_0035C8, NULL },
  { {  -1300,    230,  -1230 },    128, NULL, Z2_EXAMPLE_room_2DL_006AA0 },
};

u8 Z2_EXAMPLE_room_0_possiblePadding_000FFF = 42;

Gfx Z2_EXAMPLE_room_0DL_000A50[] = {
  gsDPSomeCoolCmd(),
  gsDPSomeCoolCmd(11),
};
`;
    
    const actorTable = {
      list: [],
      enum: {
        ACTOR_EN_ABC: 0x10,
        ACTOR_EN_DEF: 0x42,
      }
    };

    const objectTable = {
      list: [],
      enum: {
        OBJECT_ABC: 0x20,
        OBJECT_DEF: 0x21,
      }
    };

    const addressMap = {
      Z2_EXAMPLE_room_2DL_001B30: 0x03000110,
      Z2_EXAMPLE_room_2DL_0035C8: 0x83000220,
      Z2_EXAMPLE_room_2DL_006AA0: 0x03000330,
    };

    const roomData = parseRoom(src, "Z2_EXAMPLE_room_0", actorTable, objectTable, addressMap, Game.MM);
    expect(roomData).toEqual({
      mainHeader: {
        name: "Z2_EXAMPLE_room_0Commands",
        map: {
          SCENE_CMD_ACTOR_LIST: [3, "Z2_EXAMPLE_room_0ActorEntry_00005C"],
          SCENE_CMD_OBJECT_LIST: [2, "Z2_EXAMPLE_room_0ObjectList_000040"],
          SCENE_CMD_ECHO_SETTINGS: [0],
          SCENE_CMD_END: [],
        },
        indices: {
          SCENE_CMD_ACTOR_LIST: 0,
          SCENE_CMD_OBJECT_LIST: 1,
          SCENE_CMD_ECHO_SETTINGS: 2,
          SCENE_CMD_END: 3,
        }
      },
      altHeaders: [],
      actors: {
        Z2_EXAMPLE_room_0ActorEntry_00005C: [
          {id: 0x10, pos: [-400, -10, 100], rot: [0,    63169, 0], params: 0xABCD, spawnTime: 1023, cutsceneIndex: 127, rotMask: 0},
          {id: 0x42, pos: [ 111,   0, 200], rot: [0x14, 30,    0], params: 0x1234, spawnTime: 1008, cutsceneIndex: 127, rotMask: 0xE000},
          {id: 0x10, pos: [-555,  60, -50], rot: [0,    16384, 0], params: 0x5678, spawnTime: 1023, cutsceneIndex: 127, rotMask: 0},
        ]
      },
      objList: {
        Z2_EXAMPLE_room_0ObjectList_000040: [0x20, 0x21],
      },
      mesh: {
        opaque: [0x110, 0x220],
        transparent: [0x330]
      }
    });
  });
});
