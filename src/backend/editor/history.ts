/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BusName, EventBus } from "../../frontend/helper/eventBus";
import { clamp } from "../utils/float";
import { emptyCollisionData } from "../z64/collision/collisionTypes";
import { RoomEntry } from "../z64/rooms/roomList";
import { SceneEntry } from "../z64/scenes/sceneList";

/**
 * Subset of scene and room data, not all data is stored since it gets way to large.
 * (e.g. 3d-meshes are ignored)
 */
interface HistoryData
{
  scene: SceneEntry;
  rooms: RoomEntry[];
}

export type HistoryRestoreFunction = (s: SceneEntry, r: RoomEntry[]) => void;

let historyIdx = -1;
let history: HistoryData[] = [];

// Prepare for history-storage, Remove large data from scene
function stripScene(scene: SceneEntry) {
  const newScene = {...scene};
  newScene.collision = emptyCollisionData();
  newScene.header = {
    name: newScene.header.name,
    map: {}, indices: {}
  };
  newScene.lighting = [];
  newScene.actors = [...newScene.actors];
  newScene.paths = structuredClone(newScene.paths);
  return newScene;
}

// Prepare for history-storage, Remove large data from room
function stripRoom(room: RoomEntry) {
  const newRoom = {...room};
  newRoom.mesh = undefined;
  newRoom.meshData = {opaque: [], transparent: []};
  newRoom.header = {
    name: newRoom.header.name,
    map: {}, indices: {}
  };
  newRoom.actors = [...newRoom.actors];
  newRoom.objectDependencies = [...newRoom.objectDependencies];
  return newRoom;
}

// tries to find a scene layer by layer-ID, returns null if nothing was found
function getSceneLayer(scene: SceneEntry, layerID: number) {
  return scene.altSetups.find(layer => layer && layer.altId === layerID);
}

// tries to find a room layer by room-ID & layer-ID, returns null if nothing was found
function getRoomLayer(room: RoomEntry, roomId: number, layerID: number) {
  return room.altRooms.find(layer => layer && layer.roomId === roomId && layer.altId === layerID);
}

// clones and patches a new scene ('scene') by taking data from 'history'
function restoreScene(history: SceneEntry, scene: SceneEntry): SceneEntry {
  const newScene = {...scene};
  newScene.actors = structuredClone(history.actors);
  newScene.actorDependency = history.actorDependency;
  newScene.paths = structuredClone(history.paths);
  newScene.soundAmbience = history.soundAmbience;
  newScene.soundBGM = history.soundBGM;
  newScene.soundPreset = history.soundPreset;
  return newScene;
}

// clones and patches a new room ('room') by taking data from 'history'
function restoreRoom(history: RoomEntry, room: RoomEntry): RoomEntry {
  const newRoom = {...room};
  newRoom.actors = structuredClone(history.actors);
  newRoom.objectDependencies = [...history.objectDependencies];
  return newRoom;
}

export function historyAdd(scene: SceneEntry, rooms: RoomEntry[]) 
{
  const histScene = stripScene(scene);
  histScene.altSetups = histScene.altSetups.map(s => s ? stripScene(s) : s);

  const histRooms = rooms.map(r => {
    const newRoom = stripRoom(r);
    newRoom.altRooms = newRoom.altRooms.map(alt => alt ? stripRoom(alt) : alt);
    return newRoom;
  });

  ++historyIdx;
  history.splice(historyIdx, history.length, {
    scene: histScene, 
    rooms: histRooms,
  });

  EventBus.send(BusName.HISTOR_CHANGED);
  return history.length;
}

export function historyRestore(
  undo: boolean,
  scene: SceneEntry, rooms: RoomEntry[], 
  setSceneRooms: HistoryRestoreFunction,
) 
{
  const oldHistoryIdx = historyIdx;
  historyIdx += undo ? -1 : 1;
  historyIdx = clamp(historyIdx, 0, history.length-1);

  if(oldHistoryIdx === historyIdx) {
    return;
  }

  //console.log("History: restore[%s] %d/%d",undo  ? "undo" : "redoo", historyIdx, history.length-1);

  const entry = history[historyIdx];
  if(!entry)return;

  const newScene = restoreScene(entry.scene, scene);
  newScene.altSetups = scene.altSetups.map(s => {
    if(!s)return s;
    const historyAltScene = getSceneLayer(entry.scene, s.altId);
    return (s && historyAltScene) ? restoreScene(historyAltScene, s) : s;
  });

  const newRooms = rooms.map(r => {
    const historyRoom = entry.rooms.find(er => er.roomId === r.roomId);
    if(!historyRoom)return r;

    const newRoom = restoreRoom(historyRoom, r);
    newRoom.altRooms = r.altRooms.map(layer => {
      if(!layer)return layer;
      const historyAltRoom = getRoomLayer(historyRoom, layer.roomId, layer.altId);
      return historyAltRoom ? restoreRoom(historyAltRoom, layer) : layer;
    });
    return newRoom;
  });

  setSceneRooms(newScene, newRooms);
  EventBus.send(BusName.HISTOR_CHANGED);
}

export function historyClear() {
  history = [];
  historyIdx = -1;
  EventBus.send(BusName.HISTOR_CHANGED);
}

export function historyGetState() {
  return {
    current: historyIdx+1,
    total: history.length,
  };
}

export function requestHistoryAdd() {
  EventBus.send(BusName.HISTOR_ADD);
}