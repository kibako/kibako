/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Vector3 } from "../types";

export interface PolygonData {
    type: number;
    vertA: number;
    vertB: number;
    vertC: number;
    normal: Vector3;
    distance: number;
    vertIndex: number[];
};

export interface WaterBox {
    pos: Vector3;
    sizeX: number;
    sizeZ: number;
    flags: number;
};

// No an OOT format, used in this tool as a DTO
export interface CollisionData {
    vertices: number[];
    verticesNoIndex: number[];
    normals: number[]; // normals (as float)
    waterBoxes: WaterBox[];
};

export const emptyCollisionData = (): CollisionData => ({
  normals: [],
  vertices: [],
  verticesNoIndex: [],
  waterBoxes: []
});