/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

/**
 * These addresses are just made-up values that are not the ones found in the actual game.
 * This is used to manually bootstrap and emulate actors without the need to boot the game.
 */
export const MemLayout = 
{
  STACK_PTR      : 0x80_01_4000, // offset to the last stack entry
  
  // Note: this space here is filled with game data, addresses are fixed

  ACTOR_DATA     : 0x80_26_2000, // offset of the current actor struct
  PLAYER_DATA    : 0x80_26_A000, // offset of the current actor (player) struct
  CONTEXT        : 0x80_27_2000, // offset of the global context

  LIGHTS         : 0x00_28_4600, // array of lights (in F3D2EX format)
  
  GFX_CONTEXT    : 0x80_28_5000, // offset of the GFX context the global context is pointing to
  GFX_DL_OPAQUE  : 0x80_29_0000, // opaque display-list output location the GFX context is pointing to
  GFX_DL_OPQ_END : 0x80_29_7FF0,
  GFX_DL_TRANSP  : 0x80_29_8000, // transparent display-list output location the GFX context is pointing to
  GFX_DL_TRP_END : 0x80_29_FFF0,
  GFX_HEAP       : 0x80_30_0000, // MM specific heap
  GFX_HEAP_END   : 0x80_30_FFF0, // MM specific heap
  SCENE_DATA     : 0x80_31_0000, // currently loaded scene (max-size: 0x0002_A220 hyrule field)
  OBJECT_DATA    : 0x80_34_0000, // currently loaded (actor-)object (max-size: 0x0003_AFB0 (twinrowa))
  GAMEPLAY_DATA  : 0x80_41_5000, // global dependency, gameplay data
  FIELD_DGN_DATA : 0x80_47_A000, // global dependency, field or dungeon data (max. in mm&oot: 0x8044_3280)
  OVERLAY        : 0x80_56_5000,
  ROOM_DATA      : 0x80_58_5000, // place for room data (this can fit ALL rooms of a SINLGE scene)
  
  HEAP_START     : 0x80_67_2000, // heap used by the game-state pointer struct
  HEAP_END       : 0x80_78_FFF0, 
  GREG_DATA      : 0x80_79_0000, // register editor data
  PATH_DATA      : 0x00_7A_0000,
  GARBAGE_DATA   : 0x80_7B_0000, // garbage data, used for pointer that expect a non-NULL value, but are not implemented

  RAM_END        : 0x80_80_0000 // End of 8MB
};
