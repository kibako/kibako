/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Context } from "../../context";
import { ActorCategory } from "../actors/actorFlags";
import { SceneActorDependency } from "../scenes/sceneList";
import { resetDisplayList } from "./displayList";
import { MemLayout } from "./memoryLayout";
import { writeActorListEntryArray } from "./structs/actorListEntryArray";
import { Z64Emulator } from "./z64Emulator";

const ALIGNMENT = 8;

export function setupContext(emu: Z64Emulator)
{
    const file = emu.getMemoryAsFile();
    const offsetCtx = emu.resolveAddress(MemLayout.CONTEXT);

    // Setup Global Context (aka. PlayState)
    file.pos(offsetCtx);
    file.write('u32', MemLayout.GFX_CONTEXT);

    file.pos(offsetCtx + 0x74); // game heap struct
    file.writeStackAllocator(MemLayout.HEAP_START, MemLayout.HEAP_END - MemLayout.HEAP_START);

    // Setup Graphics-Context
    const addressGfxCtx = emu.resolveAddress(MemLayout.GFX_CONTEXT);
    file.pos(addressGfxCtx);
    file.write('u32', MemLayout.GFX_DL_OPAQUE);
    file.write('u32', MemLayout.GFX_DL_TRANSP);

    if(Context.isMM())
    {
        // TwoHeadGfxArena in context (overlay)
        const heapSize = MemLayout.GFX_HEAP_END - MemLayout.GFX_HEAP;
        file.pos(addressGfxCtx + 0x2A8);
        file.write('u32', heapSize);
        file.write('u32', MemLayout.GFX_HEAP);
        file.write('u32', MemLayout.GFX_DL_OPAQUE); // start* (@TODO: is this correct (arena id "overlay"))
        file.write('u32', MemLayout.GFX_HEAP);

    }

    // Opaque & Transparent GFX Structures, these point to the actual DPL-Data
    resetDisplayList(emu);

    // Camera Pointer
    file.pos(offsetCtx + (Context.isMM() ? 0x0800 : 0x0790)); // cameraPtrs*[4]
    for(let i=0; i<4; ++i) {
        file.write('u32', offsetCtx + (Context.isMM() ? 0x0220 : 0x01E0));
    }
    file.write('s32', 0); // activeCamId
    file.write('s32', 0); // nextCamId

    // Actor lists
    // ((Player*)(play)->actorCtx.actorLists[ACTORCAT_PLAYER].head)
    const offsetActorCtx = offsetCtx + (Context.isMM() ? 0x1CA0 : 0x01C24);
    file.pos(offsetActorCtx + (Context.isMM() ? 0x0E : 0x08));
    file.write('u8', 2); // actor count (just player and emulated actor)

    file.pos(offsetActorCtx + (Context.isMM() ? 0x10 : 0x0C));
    writeActorListEntryArray(file, {
        [ActorCategory.PLAYER]: [MemLayout.PLAYER_DATA]
    });
}

export function allocMemory(emu: Z64Emulator, size: number): number {
    const ram = emu.getMemoryView();
    const offsetArena = emu.resolveAddress(MemLayout.CONTEXT) + 0x74; // heap-arena offset

    const oldPtr = ram.getUint32(offsetArena + 4 + 4) >>> 0;

    let newPtr = oldPtr + size;
    newPtr = Math.ceil(newPtr / ALIGNMENT) * ALIGNMENT;

    ram.setUint32(offsetArena + 4 + 4, newPtr >>> 0);
    return newPtr;
}

export interface ObjStatusEntry {
    id: number;
    ptr: number;
};

export function setObjectContext(emu: Z64Emulator, deps: SceneActorDependency, objs: ObjStatusEntry[])
{
    const file = emu.getMemoryAsFile();

    const objContextOffset = Context.isMM() ? 0x17D88 : 0x117A4;
    file.pos(emu.resolveAddress(MemLayout.CONTEXT) + objContextOffset); // ObjectContext

    const statusEntries: ObjStatusEntry[] = [
        ...objs,
        {
            id: 1, 
            ptr: MemLayout.GAMEPLAY_DATA
        },
        {
            id: (deps === SceneActorDependency.FIELD) ? 2 : 3,
            ptr: MemLayout.FIELD_DGN_DATA
        }
    ];

    file.write('u32', 0); // *spaceStart;
    file.write('u32', 0); // *spaceEnd;
    file.write('u8', statusEntries.length); // num
    file.write('u8', 0); // index of obj?
    file.write('u8', statusEntries.length-2); // gameplay index
    file.write('u8', statusEntries.length-1); // filed/dungeon index

    // "status" array
    for(const entry of statusEntries) 
    {
        const start = file.pos();
        file.write('s16', entry.id);
        file.skip(2); // align
        file.write('u32', entry.ptr>>>0);

        file.pos(start + 0x44);
    }
}

const BASE_REG = (n: number, r: number) => n * (6*16) + r;
const SREG = (r: number) => BASE_REG(1, r);

const ANIMATION_SPEED = 3; // same for oot and mm

export function setRegisterEditorData(emu: Z64Emulator) 
{
  const gRegEditorPtr = emu.addressMap.gRegEditor || 0;
  if(gRegEditorPtr) {
    const f = emu.getMemoryAsFile();
    f.pos(emu.resolveAddress(gRegEditorPtr));
    f.write('u32', MemLayout.GREG_DATA); // set pointer to data

    const regDataPtr = emu.resolveAddress(MemLayout.GREG_DATA) + 0x14;
    
     // animation speed (both oot and mm)
    f.pos(regDataPtr + SREG(30) * 2);
    f.write("s16", ANIMATION_SPEED);

    if(Context.isMM() && emu.addressMap.Game_UpdateFramerateVariables) {
        emu.executeFunction(emu.addressMap.Game_UpdateFramerateVariables, [ANIMATION_SPEED]);
    }
  } 
}