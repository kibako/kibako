/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BinaryFile } from "backend/utils/binaryFile";
import { SceneLighting } from "backend/z64/lighting/lighting";
import { VectorRGBFloat, Vector3 } from "backend/z64/types";

export function writeLight(file: BinaryFile, color: VectorRGBFloat, dir?: Vector3)
{
  const offset = file.pos();
  file.write('u8', color[0]);
  file.write('u8', color[1]);
  file.write('u8', color[2]);
  file.write('u8', 0);
  file.write('u8', color[0]);
  file.write('u8', color[1]);
  file.write('u8', color[2]);
  file.write('u8', 0);

  if(dir) {
    file.write('s8', dir[0]);
    file.write('s8', dir[1]);
    file.write('s8', dir[2]);
    file.write('u8', 0);
  } else {
    file.write('u32', 0);
  }

  file.alignTo(8); // moveMem forces an 8-byte size length

  return offset;
}

function writeLightInfo(file: BinaryFile, color: VectorRGBFloat, dir: Vector3) 
{
  const offset = file.pos();
  file.write('u8', 1); // type = directional
  file.skip(1); // padding
  file.write('s8', dir[0]);
  file.write('s8', dir[1]);
  file.write('s8', dir[2]);
  file.write('u8', color[0]);
  file.write('u8', color[1]);
  file.write('u8', color[2]);
  file.skip(6); // union padding
  return offset;
}

export function writeLightContext(file: BinaryFile, lighting: SceneLighting, dataOffset: number) 
{
  file.posPushAndSet(dataOffset);

    // actual data (LightInfo)
    const offsetInfo0 = writeLightInfo(file, lighting.diff0Color, lighting.diff0Dir);
    file.alignTo(8);
    const offsetInfo1 = writeLightInfo(file, lighting.diff1Color, lighting.diff1Dir);
    file.alignTo(8);

    // linked list (LightNode)
    const lastOffset = file.pos();
    file.write('u32', offsetInfo0); // *info
    file.write('u32', 0);           // *prev
    let nextOffset = file.pos()+4;
    file.write('u32', nextOffset); // *next

    file.write('u32', offsetInfo1); // *info
    file.write('u32', lastOffset);  // *prev
    file.write('u32', 0);

  file.posPop();

  file.write('u32', lastOffset);
  file.write('u8', lighting.ambientColor[0]);
  file.write('u8', lighting.ambientColor[1]);
  file.write('u8', lighting.ambientColor[2]);
  file.write('u8', 0xF4);

  file.write('u8', lighting.fogColor[0]);
  file.write('u8', lighting.fogColor[1]);
  file.write('u8', lighting.fogColor[2]);
  file.write('s16', lighting.forStart);
  file.write('s16', lighting.drawDist);
}