/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Context } from "../../context";
import { BinaryFile } from "../../utils/binaryFile";
import { Vector3 } from "../types";
import { MemLayout } from "./memoryLayout";
import { Z64Emulator } from "./z64Emulator";

const DATA_STRUCT_SIZE = 0x29C;
const COLLIDER_INFO_SIZE = 0x28;

// @TODO: read from decomp
const COLLISION_CHECK_AT_MAX = 50;
const COLLISION_CHECK_AC_MAX = 60;
const COLLISION_CHECK_OC_MAX = 50;
const COLLISION_CHECK_OC_LINE_MAX = 3;

export enum ColliderType {
  Sphere = 0, 
  Cylinder = 1, 
  Tris = 2, 
  Quad = 3
}

interface ColliderBase {
  type: number;
  uuid: number;
}

export interface CollCylinder extends ColliderBase {
  radius: number;
  height: number;
  shiftY: number;
  pos: Vector3;
}

export interface CollSphere extends ColliderBase {
  pos: Vector3;
  radius: number;
}

export interface CollTris extends ColliderBase {
  vertices: Float32Array,
  normals: Float32Array,
}

export interface ColliderData
{
  cylinder: CollCylinder[];
  spheres: CollSphere[];
  triangles: CollTris[];
};

let currentUUID = 0;

const parseCylinder = (ram: BinaryFile, type: number): CollCylinder => {
  ram.skip(COLLIDER_INFO_SIZE);
  const cyl: CollCylinder = {
    type,
    uuid: ++currentUUID,
    radius: ram.read("s16"),
    height: ram.read("s16"),
    shiftY: ram.read("s16"),
    pos: ram.readVector3("s16"),
  };

  cyl.pos[1] += cyl.height * 0.5;
  return cyl;
};

const parseSphere = (ram: BinaryFile, emu: Z64Emulator, type: number): CollSphere[] => {
  const count = ram.read("s32");
  const res: CollSphere[] = [];

  const ptrNorm = emu.resolveAddress(ram.read("u32"));
  ram.posPushAndSet(ptrNorm);

  for(let i=0; i<count; ++i) {
    ram.skip(COLLIDER_INFO_SIZE + 8); // model-space sphere
    res.push({
      type,
      uuid: ++currentUUID,
      pos: ram.readVector3("s16"),
      radius: ram.read("s16")
    });
    ram.skip(8);
  }
  return res;
};

const parseTris = (ram: BinaryFile, emu: Z64Emulator, type: number): CollTris => {
  const count = ram.read("s32");
  const res: CollTris = {
    type,
    uuid: ++currentUUID,
    vertices: new Float32Array(count*3*3),
    normals: new Float32Array(count*3*3),
  }

  let v=0;
  let n=0;

  const ptrNorm = emu.resolveAddress(ram.read("u32"));
  ram.pos(ptrNorm);

  for(let i=0; i<count; ++i) {
    ram.skip(COLLIDER_INFO_SIZE);

    res.vertices[v++] = ram.read("f32");
    res.vertices[v++] = ram.read("f32");
    res.vertices[v++] = ram.read("f32");

    res.vertices[v++] = ram.read("f32");
    res.vertices[v++] = ram.read("f32");
    res.vertices[v++] = ram.read("f32");

    res.vertices[v++] = ram.read("f32");
    res.vertices[v++] = ram.read("f32");
    res.vertices[v++] = ram.read("f32");

    const norm = ram.readVector3("f32");
    ram.skip(4); // distance

    for (let nb=0; nb<3; ++nb) {
      res.normals[n++] = norm[0];
      res.normals[n++] = norm[1];
      res.normals[n++] = norm[2];
    }
  }
  return res;
};


const parseQuad = (ram: BinaryFile, type: number): CollTris => {
  ram.skip(COLLIDER_INFO_SIZE);
  const res: CollTris = {
    type,
    uuid: ++currentUUID,
    vertices: new Float32Array(2*3*3),
    normals: new Float32Array(2*3*3),
  }

  res.vertices[0] = ram.read("f32");
  res.vertices[1] = ram.read("f32");
  res.vertices[2] = ram.read("f32");

  res.vertices[3] = ram.read("f32");
  res.vertices[4] = ram.read("f32");
  res.vertices[5] = ram.read("f32");

  res.vertices[6] = ram.read("f32");
  res.vertices[7] = ram.read("f32");
  res.vertices[8] = ram.read("f32");

  res.vertices[9] = res.vertices[6];
  res.vertices[10] = res.vertices[7];
  res.vertices[11] = res.vertices[8];

  res.vertices[12] = ram.read("f32");
  res.vertices[13] = ram.read("f32");
  res.vertices[14] = ram.read("f32");

  res.vertices[15] = res.vertices[3];
  res.vertices[16] = res.vertices[4];
  res.vertices[17] = res.vertices[5];

  let n=0;
  for (let nb=0; nb<2*3; ++nb) {
    res.normals[n++] = 0; // @TODO: calc
    res.normals[n++] = 1;
    res.normals[n++] = 0;
  }
  return res;
};

export const clearCollider = (emu: Z64Emulator) => {
  const ram = emu.getMemoryAsFile();
  const baseColl = emu.resolveAddress(MemLayout.CONTEXT) + 0x11E60;
  ram.buffer.fill(0, baseColl, baseColl + DATA_STRUCT_SIZE);
};

/**
 * Reads actor colliders and clear the context for the next frame.
 * This does NOT perform collision checks
 * @param emu 
 * @returns 
 */
export const readAndClearCollider = (emu: Z64Emulator): ColliderData => {
  const ram = emu.getMemoryAsFile();
 
  const baseColl = emu.resolveAddress(MemLayout.CONTEXT) + (Context.isMM() ? 0x18884 :  0x11E60);
  ram.pos(baseColl);
  
  // Read Data
  const data = {
    countAttack: ram.read("s16"),
    sacFlags: ram.read("u16"),
    attack: ram.readArray("u32", COLLISION_CHECK_AT_MAX),
    countAttackColl: ram.read("s32"),
    attackColl: ram.readArray("u32", COLLISION_CHECK_AC_MAX),
    countObject: ram.read("s32"),
    objectColl: ram.readArray("u32", COLLISION_CHECK_OC_MAX),
    countLine: ram.read("s32"),
    lines: ram.readArray("u32", COLLISION_CHECK_OC_LINE_MAX),
  };
  const collEndPos = ram.pos();

  //console.log(data);

  // read collisder
  const coll: ColliderData = {
    cylinder: [],
    spheres: [],
    triangles: []
  };

  const parseArray = (arr: number[], type: number) => {
    for(const ptr of arr) {
      if(!ptr)break;
      const ptrNorm = emu.resolveAddress(ptr);
      ram.pos(ptrNorm + 0x15); // shape
      const shape = ram.read('u8') as ColliderType;
      ram.skip(2);
  
      switch(shape) {
        case ColliderType.Sphere: coll.spheres.push(...parseSphere(ram, emu, type)); break;
        case ColliderType.Cylinder: coll.cylinder.push(parseCylinder(ram, type)); break;
        case ColliderType.Tris: coll.triangles.push(parseTris(ram, emu, type)); break;
        case ColliderType.Quad: coll.triangles.push(parseQuad(ram, type)); break;
      }
    }
  };

  parseArray(data.attack, 0);
  parseArray(data.attackColl, 1);
  parseArray(data.objectColl, 2);

  // Clear data (zero out)
  ram.buffer.fill(0, baseColl, collEndPos);

  return coll;
};
