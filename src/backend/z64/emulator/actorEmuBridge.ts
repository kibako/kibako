/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BinaryFile } from "backend/utils/binaryFile";
import { vec3Add } from "backend/utils/math";
import { vec3, vec2 } from "gl-matrix";
import { Context } from "../../context";
import { Actor } from "../actors/actorList";
import { ActorOverlay } from "../actors/actorOverlayTable";
import { radAngleToS16 } from "../math/angle";
import { SceneEntry } from "../scenes/sceneList";
import { Vector3 } from "../types";
import { ColliderData, readAndClearCollider } from "./collisionEmuBridge";
import { MemLayout } from "./memoryLayout";
import { Z64Emulator } from "./z64Emulator";

/**
 * Data that is emulated / modified by emulation.
 * This should only be used to display data, not for saving it.
 */
export interface ActorEmuData {
    pos: Vector3,
    scale: Vector3,
    velocity: Vector3,
    speed: number,
    gravity: number,
    flags: number,
    minVelocityY: number,
    collider: ColliderData;
};

export enum EmuCommandType {
    START, // start emulation
    PAUSE, // pause emulation
    STOP, // stop & reset emulation
    FPS_SET, // sets new fps value (20, 30 or 60)
    MM_DAY, // set new emulation time (MM only)
    MM_TIME, // set new emulation time (MM only)
}

export interface EmuCommand {
    type: EmuCommandType;
    actorUUID: string;
    value?: any;
}

const writeActorTransforms = (
    file: BinaryFile, baseOffset: number, 
    pos: Vector3|vec3, rot: Vector3, posFocus?: Vector3|vec3
) =>
{
    const offsetFocus = Context.isMM() ? 0x3C : 0x38;
    // "home", "world" and "focus"
    for(let posOffset of [0x08, 0x24, offsetFocus]) 
    {
        file.pos(baseOffset + posOffset);

        if(posOffset === offsetFocus && posFocus) {
            file.writeVector3('f32', posFocus);    
        } else {
            file.writeVector3('f32', pos);
        }
        
        file.writeVector3('s16', rot);
    }
}

export const initActorData = (emu: Z64Emulator, overlay: ActorOverlay, actor: Actor) =>
{
    if(!overlay.file)return;

    const actorFile = emu.getMemoryAsFile();
    let base = emu.resolveAddress(MemLayout.ACTOR_DATA);

    actorFile.pos(base);
    actorFile.write('s16', actor.id);
    actorFile.skip(1);
    actorFile.write('u8', actor.roomId);
    actorFile.write('u32', 0);

    actorFile.pos(base + 0x1C);
    actorFile.write('u16', actor.initValue);

    actorFile.pos(base + 0x1F);
    actorFile.write('u8', 3); // targetMode

    // set the initial, and the two runtime pos/rot structs
    writeActorTransforms(actorFile, base, actor.pos, actor.rot);

    if(Context.isMM()) {
      // @TODO: set day bits
      // @TODO: set cutscene id
      base += 8; // extra mm stuff
    }

    actorFile.pos(base + 0x50);
    actorFile.writeVector3('f32', [0.01, 0.01, 0.01]); // scale

    //actorFile.pos(base + 0x68);
    //actorFile.write('f32', 1.0); // speed (@TODO: do i need to set this?)

    actorFile.pos(base + 0x70);
    actorFile.write('f32', -20.0); // minVelocityY
    
    actorFile.pos(base + 0x7D);
    actorFile.write('u8', 50); // floorBgId

    actorFile.pos(base + 0xB4); // actor shape
    actorFile.writeVector3('s16', actor.rot); // this seems to be the only only rotation used for rendering

    actorFile.pos(base + 0xF4);
    actorFile.write('f32', 1000.0); // uncullZoneForward
    actorFile.write('f32', 350.0); // uncullZoneScale
    actorFile.write('f32', 700.0); // uncullZoneDownward

    actorFile.pos(base + 0x100); // last pos
    actorFile.writeVector3('f32', actor.pos);

    actorFile.pos(base + 0x115); // aka: isDrawn
    actorFile.write('u8', 1);

    actorFile.pos(base + 0x117); // aka: naviEnemyId
    actorFile.write('u8', 0xFF); // "NONE"

    actorFile.pos(base + 0x128);
    actorFile.write('u32', overlay.file.ptrInit);
    actorFile.write('u32', overlay.file.ptrDestroy);
    actorFile.write('u32', overlay.file.ptrUpdate);
    actorFile.write('u32', overlay.file.ptrDraw);
    
    actorFile.pos(base + 0x138);

    // @TODO: is this wrong? (is pointer)
    actorFile.write('u32', overlay.file.objectId); // overlayEntry 
    // Collision stuff

    actorFile.pos(base + 0x080); // aka: floorHeight or CollisionPoly*
    if(Context.isOOT()) {
      actorFile.write('f32', actor.pos[1]);
    } else {
      actorFile.write('u32', 0); // pointer to the floor
    }
}

export const setDayTime = (emu: Z64Emulator, day: number, time: number) => {
    const saveAddress = emu.resolveAddress(emu.addressMap.gSaveContext);
    if(saveAddress) {
        const ram = emu.getMemoryView();
        ram.setUint32(saveAddress + 0x18, day);
        ram.setUint16(saveAddress + 0x0C, time);
    }

};

export const setActorGlobalVars = (emu: Z64Emulator, frame: number) =>
{
    const file = emu.getMemoryAsFile();
    file.pos(emu.resolveAddress(MemLayout.CONTEXT) + 0x98); // same in oot & mm
    file.write('u32', 1); // running
    file.write('u32', frame); // frames
}

export const setActorPlayerPos = (emu: Z64Emulator, actor: Actor, playerPos: vec3) =>
{
    const f = emu.getMemoryAsFile();
    let base = emu.resolveAddress(MemLayout.ACTOR_DATA);
    if(Context.isMM()) {
      base += 8; // extra mm stuff in the beginning
    }

    let headPos = vec3.create();
    vec3.add(headPos, playerPos, [0, 40, 0]);

    let distSq = vec3.sqrDist(playerPos, actor.pos);
    let distXZ = vec2.dist([playerPos[0], playerPos[2]], [actor.pos[0], actor.pos[2]]);
    let distY = actor.pos[1] - playerPos[1];

    const dx = playerPos[0] - actor.pos[0];
    const dz = playerPos[2] - actor.pos[2];
    const yaw = Math.floor(Math.atan2(dx, dz) / Math.PI * 0x7FFF);

    // Player Data in the emulated actor
    f.pos(base + 0x08A); // aka: yawTowardsPlayer
    f.write('s16', yaw);

    f.pos(base + 0x08C); // aka: xyzDistToPlayerSq
    f.write('f32', distSq);

    f.pos(base + 0x090); // aka: xzDistToPlayer
    f.write('f32', distXZ);

    f.pos(base + 0x094); // aka: yDistToPlayer
    f.write('f32', distY);

    f.pos(base + 0x10C); // aka: isTargeted
    f.write('u8', 1);

    // Player data in the player actor itself
    const basePlayer = emu.resolveAddress(MemLayout.PLAYER_DATA);
    writeActorTransforms(f, basePlayer, playerPos, [0,0,0], headPos);
}

export const setSaveData = (emu: Z64Emulator, isChild: boolean) => 
{ 
    const f = emu.getMemoryAsFile();
    // Player save data
    const offsetSave = emu.resolveAddress(emu.addressMap.gSaveContext);

    if(Context.isOOT()) {
      f.pos(offsetSave + 0x04); // age (0=adult, 1=child)
      f.write('u32', isChild ? 1 : 0);
    }
}

export const setCameraData = (emu: Z64Emulator, camPos: Vector3, camDir: Vector3, camRot: Vector3) =>
{
    const file = emu.getMemoryAsFile();
    const offsetCtx = emu.resolveAddress(MemLayout.CONTEXT);
    const offsetCam = offsetCtx + (Context.isMM() ? 0x0220 : 0x01E0);
    const offsetView = offsetCtx + 0x00B8; // same in mm

    const camAt = vec3Add(camPos, camDir);

    // View
    file.pos(offsetView + 0x28);
    file.writeVector3('f32', camPos);  // eye
    file.writeVector3('f32', camAt);  // at
    file.writeVector3('f32', [0, 1, 0]); // up

    // Camera
    file.pos(offsetCam + 0x0050);
    file.writeVector3('f32', camAt);  // at
    file.writeVector3('f32', camPos);  // eye
    file.writeVector3('f32', [0, 1, 0]); // up
    file.writeVector3('f32', camPos);  // eyeNext
    file.writeVector3('f32', [0, 0, 0]); // quakeOffset
    file.write('u32', MemLayout.CONTEXT); // *play
    file.write('u32', MemLayout.PLAYER_DATA); // *player

    file.pos(offsetCam + 0x013A);
    file.writeVector3('s16', [
        radAngleToS16(camRot[0]),
        radAngleToS16(camRot[1]),
        radAngleToS16(camRot[2]),
    ]);  // camDir


    // @TODO: calculate billboard-matrix and set segment 0x01

    // Calc billboard matrix and set segment
    //file.pos(MemLayout.BILLBOARD_MAT);
}

export const setScenePathData = (emu: Z64Emulator, scene: SceneEntry) => {
    const ram = emu.getMemoryAsFile();
    const offsetCtx = emu.resolveAddress(MemLayout.CONTEXT);

    ram.pos(offsetCtx + (Context.isMM() ? 0x18864 : 0x11E08));
    ram.write('u32', MemLayout.PATH_DATA);
    
    ram.pos(MemLayout.PATH_DATA);

    let offsetVector = (scene.paths.length * 8) + 8; 

    for(const path of scene.paths) {
        ram.write('u8', path.points.length);
        ram.write('u8', path.additionalPathIdx); // MM only, oot doesn't care
        ram.write('u16', path.customValue); // MM only, oot doesn't care
        ram.write('u32', MemLayout.PATH_DATA + offsetVector);

        ram.posPushAndSet(MemLayout.PATH_DATA + offsetVector);
        for(const pt of path.points) {
            ram.writeVector3('s16', pt);
        }
        offsetVector += path.points.length * 6;
        ram.posPop();
    }
};

export const readActor = (emu: Z64Emulator): ActorEmuData => {
    const f = emu.getMemoryAsFile();
    const base = emu.resolveAddress(MemLayout.ACTOR_DATA);

    f.pos(base + 0x24);
    const pos = f.readVector3('f32');

    f.pos(base + (Context.isMM() ? 0x58 : 0x50));
    const scale = f.readVector3('f32');

    f.pos(base + (Context.isMM() ? 0x64 : 0x5C));
    const velocity = f.readVector3('f32');

    f.pos(base + (Context.isMM() ? 0x108 : 0x100)); // last pos
    f.writeVector3('f32', pos);

    pos[0] += velocity[0];
    pos[1] += velocity[1];
    pos[2] += velocity[2];
    f.pos(base + 0x24);
    f.writeVector3('f32', pos);

    f.pos(base + (Context.isMM() ? 0x70 : 0x68));
    const speed = f.read('f32');
    const gravity = f.read('f32');
    const minVelocityY = f.read('f32');

    f.pos(base + 0x04); // flags
    const flags = f.read('u32') >>> 0;

    let collider: ColliderData;
    try {
        collider = readAndClearCollider(emu);
    } catch(e) {
        console.error("Error parsing collision", e);
        collider = {cylinder: [], spheres: [], triangles: []};
    }

    return {pos, scale, velocity, flags, speed, gravity, minVelocityY, collider};
};

/**
 * Reads the current actor functions, these may change after init/update
 * or when the actor deletes itself (update becomes NULL).
 * @param emu 
 * @returns 
 */
export const readActorFunction = (emu: Z64Emulator) => {
    const actorFile = emu.getMemoryAsFile();

    const funcOffset = Context.isMM() ? 0x130 : 0x128;
    actorFile.pos(emu.resolveAddress(MemLayout.ACTOR_DATA) + funcOffset);

    return {
        init: actorFile.read('u32'),
        destroy: actorFile.read('u32'),
        update: actorFile.read('u32'),
        draw: actorFile.read('u32'),
    };
};

/**
 * Return the bank index (which loaded object to use and set in the segment-table)
 * @param emu 
 * @returns bank index
 */
export const readActorBankIndex = (emu: Z64Emulator) => {
    const offsetBankIdx = emu.resolveAddress(MemLayout.ACTOR_DATA) + 0x1E;
    return emu.getMemoryView().getUint8(offsetBankIdx);
};