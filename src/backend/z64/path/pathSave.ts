/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

/**
 * ================
 * NOTE:
 * A lot in this file is just there to keep the correct order of path-entries.
 * Vanilla oot can store path-data in a different order than the def. into the pathway struct.
 * I must handle this to guarantee correct checksums, the game itself doesn't care.
 * ================
 */

import { Context } from "../../context";
import { parseFileVars } from "../../decomp/c/parser";
import { injectVar, removeVar } from "../../decomp/c/writer";
import { ScenePath } from "../../decomp/parser/parsePaths";
import { SceneDataHeader } from "../../decomp/scenes";

export function getIndexedName(baseName: string, i: number) {
  return baseName + "_" + i.toString(16).toUpperCase().padStart(4, '0');
}

export function getStringOrderMapping(nameList: string[]) 
{
  const nameMap: Record<string, number> = {};
  for(const [i, name] of nameList.entries())nameMap[name] = i;

  const idxMap: number[] = [];
  const sortedNames = [...nameList].sort();
  for(const n of nameList) {
    idxMap.push(sortedNames.indexOf(n));
  }
  return idxMap;
}

export function savePaths(header: SceneDataHeader, src: string, srcHeader: string, paths: ScenePath[]): [string, string] 
{
  // check if the scene already has paths
  let oldPathVar = "";
  const newHeaderVar = parseFileVars(src, header.name).find(v => v.name === header.name);
  if(newHeaderVar) {
    const newPathHeader = newHeaderVar.data.find((entry: any) => entry[0] === "SCENE_CMD_PATH_LIST");
    if(newPathHeader)oldPathVar = newPathHeader[newPathHeader.length-1];
  }

  // we need to insert the path data after the light settings
  let refVarName = "";
  if(header.map.SCENE_CMD_ENV_LIGHT_SETTINGS) {
    refVarName = (header.map.SCENE_CMD_ENV_LIGHT_SETTINGS[1] || "") + "";
    if(refVarName)refVarName += "[";
  }

  // data-vars can have a different order than in the difinition, keep old indices
  let varIndexMap: number[] = [];

  // Path already existed, remove old data
  if(oldPathVar)
  {
    const varsToDelete = [];

    // scane for old variables
    const pathVar = parseFileVars(src, oldPathVar).filter(v => v.name === oldPathVar);
    
    if(pathVar.length > 0) {
      const oldPointVars = pathVar[0].data.map((d: any) => d[d.length-1]) as string[];
      varsToDelete.push(...pathVar.map(p => p.name), ...oldPointVars);
      varIndexMap = getStringOrderMapping(oldPointVars);
    }

    // delete old variable in source and header
    
    for(const delVar of varsToDelete) {
      const newSrc = removeVar(src, delVar);
      if(!newSrc)throw new Error("Failed to delete old source-var: " + delVar);
      src = newSrc;

      const newHeader = removeVar(srcHeader, delVar);
      if(!newHeader)throw new Error("Failed to delete old header-var: " + delVar);
      srcHeader = newHeader;
    }
  }

  // insert new paths
  if(paths.length) {
    const pathVarName = header.name + "_pathway";

    let pointDataStr = "";
    let pointHeaderStr = "";

    // Fix header variables
    header.map.SCENE_CMD_PATH_LIST = [pathVarName];
    if(!header.indices.SCENE_CMD_PATH_LIST) {
      header.indices.SCENE_CMD_PATH_LIST = Object.keys(header.indices).length;
    }

    // Generate points
    const pathDataArray: string[] = [];
    const pathDataList: string[] = [];

    for(let [i, path] of paths.entries()) 
    {
      let targetIdx = varIndexMap[i];
      if(targetIdx === undefined) {
        targetIdx = pathDataArray.length;
      }

      const pointsVarName = getIndexedName(pathVarName, targetIdx);
      if(Context.isMM()) {
        pathDataList.push(`  { ${path.points.length}, ${path.additionalPathIdx}, ${path.customValue}, ${pointsVarName} },`);  
      } else {
        pathDataList.push(`  { ${path.points.length}, ${pointsVarName} },`);  
      }

      // Note: in vanilla, path-points are stored in reverse order
      pathDataArray[targetIdx] = `\nVec3s ${pointsVarName}[${path.points.length}] = {\n` 
        + path.points.map(p => `    {  ${Math.round(p[0])}, ${Math.round(p[1])}, ${Math.round(p[2])}  }`)
                     .join(",\n") 
      + "\n};\n";
      
      pointHeaderStr += `extern Vec3s ${pointsVarName}[];\n`;
    }

    pointDataStr += pathDataArray.filter(Boolean).join("\n");

    // Generate Pathway
    pointDataStr += `\nPath ${pathVarName}[${paths.length}] = {\n` + pathDataList.join("\n") + "\n};";
    pointHeaderStr += `extern Path ${pathVarName}[];`;

    // avoid growing lines
    pointDataStr = pointDataStr.replaceAll("\n\n", "\n");
    pointHeaderStr = pointHeaderStr.replaceAll("\n\n", "\n");

    // Insert points / pathway
    src = injectVar(src, refVarName, pointDataStr);
    srcHeader = injectVar(srcHeader, refVarName, pointHeaderStr);

  } else {
    // If empty, also remove from header
    delete header.map.SCENE_CMD_PATH_LIST;
    delete header.indices.SCENE_CMD_PATH_LIST;
  }

  return [src, srcHeader];
}