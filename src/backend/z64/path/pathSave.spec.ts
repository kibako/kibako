/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { getIndexedName, getStringOrderMapping } from "./pathSave";

describe('Z64 - PathSave', () => 
{
  test("Indexed Name", () => {
    expect(getIndexedName("base", 0)).toBe("base_0000");
    expect(getIndexedName("base", 1)).toBe("base_0001");
    expect(getIndexedName("base", 255)).toBe("base_00FF");
    expect(getIndexedName("base", 0xABCD)).toBe("base_ABCD");
    expect(getIndexedName("base", 0xABCDE)).toBe("base_ABCDE");
  });

  test("getStringOrderMapping scene", () => {
    const idxMap = getStringOrderMapping([
      "test_scenePathwayList_0007D4",
      "test_scenePathwayList_0007F4",
      "test_scenePathwayList_00073C",
      "test_scenePathwayList_00083C",
      "test_scenePathwayList_000824",
      "test_scenePathwayList_000830"
    ]);
    expect(idxMap).toEqual([
      1, 2, 0, 5, 3, 4
    ]);
  });

  test("getStringOrderMapping example A", () => {
    const idxMap = getStringOrderMapping([
      "test_scenePathwayList_0002",
      "test_scenePathwayList_0001",
      "test_scenePathwayList_000A",
    ]);
    expect(idxMap).toEqual([
      1, 0, 2
    ]);
  });

  test("getStringOrderMapping example B", () => {
    const idxMap = getStringOrderMapping([
      "test_scenePathwayList_000A",
      "test_scenePathwayList_000B",
      "test_scenePathwayList_000C",
    ]);
    expect(idxMap).toEqual([
      0, 1, 2
    ]);
  });
});