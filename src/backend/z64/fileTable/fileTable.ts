/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

/**
 * Returns the file table offset by searching for it tin the rom.
 * If for some reason none was found, zero will be returned.
 * @param view ROM view
 */
export const searchFileTableOffset = (view: DataView) => 
{
    for(let offset=4; offset<view.byteLength; offset+=4) 
    {
        // this is the first entry in the table, which is the same for all n64 versions
        if(
            view.getUint32(offset, false) === 0x0000_1060 &&
            view.getUint32(offset - 4, false) === 0 &&
            view.getUint32(offset + 4, false) === 0 &&
            view.getUint32(offset + 8, false) === 0 &&
            view.getUint32(offset + 12, false) ===  0x0000_1060
        ){
            return offset - 4;
        }
    }
    return 0;
};