/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Vector3, VectorRGBFloat } from "../types";

export interface SceneLighting
{
  ambientColor: VectorRGBFloat,

  diff0Color: VectorRGBFloat,
  diff0Dir: Vector3,

  diff1Color: VectorRGBFloat,
  diff1Dir: Vector3,

  fogColor: VectorRGBFloat,
  forStart: number,
  drawDist: number
};
