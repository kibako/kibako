/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export const soundDataAmbient = [
  {value:  "0", name: "Standard night [day/night cycle]"},
  {value:  "1", name: "Standard night [Kakariko]"},
  {value:  "2", name: "Distant storm [Graveyard]"},
  {value:  "3", name: "Howling wind and cawing [Ganon's Castle]"},
  {value:  "4", name: "Wind + night birds [Kokiri]"},
  {value:  "5", name: "Wind + crickets"},
  {value:  "6", name: "Wind"},
  {value:  "7", name: "Howling wind"},
  {value:  "8", name: "Wind + crickets"},
  {value:  "9", name: "Wind + crickets"},
  {value: "10", name: "Tubed howling wind [Wasteland]"},
  {value: "11", name: "Tubed howling wind [Colossus]"},
  {value: "12", name: "Wind"},
  {value: "13", name: "Wind + crickets"},
  {value: "14", name: "Wind + crickets"},
  {value: "15", name: "Wind + birds"},
  {value: "16", name: "Wind + crickets"},
  {value: "17", name: "[Unused 0x11]"},
  {value: "18", name: "Wind + crickets"},
  {value: "19", name: "Day music always playing"},
  {value: "20", name: "Silence"},
  {value: "21", name: "[Unused 0x15]"},
  {value: "22", name: "Silence"},
  {value: "23", name: "High tubed wind + rain"},
  {value: "24", name: "Silence"},
  {value: "25", name: "Silence"},
  {value: "26", name: "High tubed wind + rain"},
  {value: "27", name: "Silence"},
  {value: "28", name: "Rain"},
  {value: "29", name: "High tubed wind + rain"},
  {value: "30", name: "Silence"},
  {value: "31", name: "High tubed wind + rain "},
];