/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

// Taken from: https://wiki.cloudmodding.com/oot/Scenes_and_Rooms
export const soundDataBGM = [
  {
    value: "0",
    name: "[Nothing]"
  },
  {
    value: "1",
    name: "[Nothing 0x01]"
  },
  {
    value: "2",
    name: "Hyrule Field"
  },
  {
    value: "3",
    name: "Hyrule Field (Initial Segment)"
  },
  {
    value: "4",
    name: "Hyrule Field (Segment 1)"
  },
  {
    value: "5",
    name: "Hyrule Field (Segment 2)"
  },
  {
    value: "6",
    name: "Hyrule Field (Segment 3)"
  },
  {
    value: "7",
    name: "Hyrule Field (Segment 4)"
  },
  {
    value: "8",
    name: "Hyrule Field (Segment 5)"
  },
  {
    value: "9",
    name: "Hyrule Field (Segment 6)"
  },
  {
    value: "10",
    name: "Hyrule Field (Segment 7)"
  },
  {
    value: "11",
    name: "Hyrule Field (Segment 8)"
  },
  {
    value: "12",
    name: "Hyrule Field (Segment 9)"
  },
  {
    value: "13",
    name: "Hyrule Field (Segment 10)"
  },
  {
    value: "14",
    name: "Hyrule Field (Segment 11)"
  },
  {
    value: "15",
    name: "Hyrule Field (Enemy Approaches)"
  },
  {
    value: "16",
    name: "Hyrule Field (Enemy Near 1)"
  },
  {
    value: "17",
    name: "Hyrule Field (Enemy Near 2)"
  },
  {
    value: "18",
    name: "Hyrule Field (Enemy Near 3)"
  },
  {
    value: "19",
    name: "Hyrule Field (Enemy Near 4)"
  },
  {
    value: "20",
    name: "Hyrule Field (Enemy Near 1)"
  },
  {
    value: "21",
    name: "Hyrule Field (Enemy Near 2)"
  },
  {
    value: "22",
    name: "Hyrule Field (Enemy Near 3)"
  },
  {
    value: "23",
    name: "Hyrule Field (Enemy Near 4)"
  },
  {
    value: "24",
    name: "Dodongo's Cavern"
  },
  {
    value: "25",
    name: "Kakariko Village (Adult)"
  },
  {
    value: "26",
    name: "Enemy Battle"
  },
  {
    value: "27",
    name: "Boss Battle"
  },
  {
    value: "28",
    name: "Inside the Deku Tree"
  },
  {
    value: "29",
    name: "Market"
  },
  {
    value: "30",
    name: "Title Theme"
  },
  {
    value: "31",
    name: "House",
  },
  {
    value: "32",
    name: "Game Over"
  },
  {
    value: "33",
    name: "Boss Clear"
  },
  {
    value: "34",
    name: "Item Get"
  },
  {
    value: "35",
    name: "Opening Ganon"
  },
  {
    value: "36",
    name: "Heart Get"
  },
  {
    value: "37",
    name: "Prelude Of Light"
  },
  {
    value: "38",
    name: "Inside Jabu-Jabu's Belly"
  },
  {
    value: "39",
    name: "Kakariko Village (Child)"
  },
  {
    value: "40",
    name: "Great Fairy's Fountain"
  },
  {
    value: "41",
    name: "Zelda's Theme"
  },
  {
    value: "42",
    name: "Fire Temple"
  },
  {
    value: "43",
    name: "Open Treasure Chest"
  },
  {
    value: "44",
    name: "Forest Temple"
  },
  {
    value: "45",
    name: "Hyrule Castle Courtyard"
  },
  {
    value: "46",
    name: "Ganondorf's Theme"
  },
  {
    value: "47",
    name: "Lon Lon Ranch"
  },
  {
    value: "48",
    name: "Goron City"
  },
  {
    value: "49",
    name: "Hyrule Field Morning Theme"
  },
  {
    value: "50",
    name: "Spiritual Stone Get"
  },
  {
    value: "51",
    name: "Bolero of Fire"
  },
  {
    value: "52",
    name: "Minuet of Woods"
  },
  {
    value: "53",
    name: "Serenade of Water"
  },
  {
    value: "54",
    name: "Requiem of Spirit"
  },
  {
    value: "55",
    name: "Nocturne of Shadow"
  },
  {
    value: "56",
    name: "Mini-Boss Battle"
  },
  {
    value: "57",
    name: "Obtain Small Item"
  },
  {
    value: "58",
    name: "Temple of Time"
  },
  {
    value: "59",
    name: "Escape from Lon Lon Ranch"
  },
  {
    value: "60",
    name: "Kokiri Forest"
  },
  {
    value: "61",
    name: "Obtain Fairy Ocarina"
  },
  {
    value: "62",
    name: "Lost Woods"
  },
  {
    value: "63",
    name: "Spirit Temple"
  },
  {
    value: "64",
    name: "Horse Race"
  },
  {
    value: "65",
    name: "Horse Race Goal"
  },
  {
    value: "66",
    name: "Ingo's Theme"
  },
  {
    value: "67",
    name: "Obtain Medallion"
  },
  {
    value: "68",
    name: "Ocarina Saria's Song"
  },
  {
    value: "69",
    name: "Ocarina Epona's Song"
  },
  {
    value: "70",
    name: "Ocarina Zelda's Lullaby"
  },
  {
    value: "71",
    name: "Sun's Song"
  },
  {
    value: "72",
    name: "Song of Time"
  },
  {
    value: "73",
    name: "Song of Storms"
  },
  {
    value: "74",
    name: "Fairy Flying"
  },
  {
    value: "75",
    name: "Deku Tree"
  },
  {
    value: "76",
    name: "Windmill Hut"
  },
  {
    value: "77",
    name: "Legend of Hyrule"
  },
  {
    value: "78",
    name: "Shooting Gallery"
  },
  {
    value: "79",
    name: "Sheik's Theme"
  },
  {
    value: "80",
    name: "Zora's Domain"
  },
  {
    value: "81",
    name: "Enter Zelda"
  },
  {
    value: "82",
    name: "Goodbye to Zelda"
  },
  {
    value: "83",
    name: "Master Sword"
  },
  {
    value: "84",
    name: "Ganon Intro"
  },
  {
    value: "85",
    name: "Shop"
  },
  {
    value: "86",
    name: "Chamber of the Sages"
  },
  {
    value: "87",
    name: "File Select"
  },
  {
    value: "88",
    name: "Ice Cavern"
  },
  {
    value: "89",
    name: "Open Door of Temple of Time"
  },
  {
    value: "90",
    name: "Kaepora Gaebora's Theme"
  },
  {
    value: "91",
    name: "Shadow Temple"
  },
  {
    value: "92",
    name: "Water Temple"
  },
  {
    value: "93",
    name: "Ganon's Castle Bridge"
  },
  {
    value: "94",
    name: "Ocarina of Time"
  },
  {
    value: "95",
    name: "Gerudo Valley"
  },
  {
    value: "96",
    name: "Potion Shop"
  },
  {
    value: "97",
    name: "Kotake & Koume's Theme"
  },
  {
    value: "98",
    name: "Escape from Ganon's Castle"
  },
  {
    value: "99",
    name: "Ganon's Castle Under Ground"
  },
  {
    value: "100",
    name: "Ganondorf Battle"
  },
  {
    value: "101",
    name: "Ganon Battle"
  },
  {
    value: "102",
    name: "Seal of Six Sages"
  },
  {
    value: "103",
    name: "End Credits I"
  },
  {
    value: "104",
    name: "End Credits II"
  },
  {
    value: "105",
    name: "End Credits III"
  },
  {
    value: "106",
    name: "End Credits IV"
  },
  {
    value: "107",
    name: "King Dodongo & Volvagia Boss Battle"
  },
  {
    value: "108",
    name: "Mini-Game"
  },
  {value: "109", name: "[Nothing 0x6D]"},
  {value: "110", name: "[Nothing 0x6E]"},
  {value: "111", name: "[Nothing 0x6F]"},
  {value: "112", name: "[Nothing 0x70]"},
  {value: "113", name: "[Nothing 0x71]"},
  {value: "114", name: "[Nothing 0x72]"},
  {value: "115", name: "[Nothing 0x73]"},
  {value: "116", name: "[Nothing 0x74]"},
  {value: "117", name: "[Nothing 0x75]"},
  {value: "118", name: "[Nothing 0x76]"},
  {value: "119", name: "[Nothing 0x77]"},
  {value: "120", name: "[Nothing 0x78]"},
  {value: "121", name: "[Nothing 0x79]"},
  {value: "122", name: "[Nothing 0x7A]"},
  {value: "123", name: "[Nothing 0x7B]"},
  {value: "124", name: "[Nothing 0x7C]"},
  {value: "125", name: "[Nothing 0x7D]"},
  {value: "126", name: "[Nothing 0x7E]"},
  {value: "127", name: "[Nothing 0x7F]"},
];
