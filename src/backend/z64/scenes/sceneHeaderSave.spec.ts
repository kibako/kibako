/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { SceneDataHeader } from "../../decomp/scenes";
import { saveSceneHeader } from "./sceneHeaderSave";

describe('Z64 - SceneHeaderSave', () => 
{
  test("Empty", () => {
    const header: SceneDataHeader = {
      name: "header_var",
      map: {},
      indices: {}
    };
    expect(() => saveSceneHeader(header, "")).toThrowError("Could not patch source (headers)!");
  });

  test("Non-Matching Var", () => {
    let src = `
      SceneCmd other_header_var[] = {
        COOL_COMMAND(),
      };
    `;
    const header: SceneDataHeader = {
      name: "header_var",
      map: {},
      indices: {}
    };
    expect(() => saveSceneHeader(header,src)).toThrowError("Could not patch source (headers)!");
  });

  test("Example - simple", () => {
    let src = `
SceneCmd header_var[] = {
  COOL_COMMAND(),
  SCENE_CMD_END()
};`;

    const header: SceneDataHeader = {
      name: "header_var",
      map: {
        COMMAND_A: [1,2,3],
        COMMAND_B: ["CONST_A", '"STR"'],
      },
      indices: {
        COMMAND_A: 0,
        COMMAND_B: 1,
      }
    };
    src = saveSceneHeader(header, src);

    expect(src).toEqual(`
SceneCmd header_var[] = {
    COMMAND_A(1, 2, 3),
    COMMAND_B(CONST_A, "STR"),
    SCENE_CMD_END()
};`
    );
  });

  test("Example - ignore wrong end command", () => {
    let src = `
SceneCmd header_var[] = {
  COOL_COMMAND(),
  SCENE_CMD_END()
};`;

    const header: SceneDataHeader = {
      name: "header_var",
      map: {
        COMMAND_A: [1,2,3],
        SCENE_CMD_END: [],
        COMMAND_B: ["CONST_A", '"STR"'],
      },
      indices: {
        COMMAND_A: 0,
        SCENE_CMD_END: 1,
        COMMAND_B: 2,
      }
    };
    src = saveSceneHeader(header, src);

    expect(src).toEqual(`
SceneCmd header_var[] = {
    COMMAND_A(1, 2, 3),
    COMMAND_B(CONST_A, "STR"),
    SCENE_CMD_END()
};`
    );
  });

  test("Example - order", () => {
    let src = `
SceneCmd header_var[] = {
  COOL_COMMAND(),
};`;

    const header: SceneDataHeader = {
      name: "header_var",
      map: {
        COMMAND_A: [1,2,3],
        COMMAND_B: ["CONST_A", '"STR"'],
        COMMAND_C: [32, 54],
      },
      indices: {
        COMMAND_A: 1,
        COMMAND_B: 2,
        COMMAND_C: 0,
      }
    };
    src = saveSceneHeader(header, src);

    expect(src).toEqual(`
SceneCmd header_var[] = {
    COMMAND_C(32, 54),
    COMMAND_A(1, 2, 3),
    COMMAND_B(CONST_A, "STR"),
    SCENE_CMD_END()
};`
    );
  });

  test("Example - order (gaps / dupe)", () => {
    let src = `
SceneCmd header_var[] = {
  COOL_COMMAND(),
};`;

    const header: SceneDataHeader = {
      name: "header_var",
      map: {
        COMMAND_A: [2],
        COMMAND_B: [2],
        COMMAND_C: [5],
        COMMAND_D: [0],
      },
      indices: {
        COMMAND_A: 2,
        COMMAND_B: 2,
        COMMAND_C: 5,
        COMMAND_D: 0,
      }
    };
    src = saveSceneHeader(header, src);

    expect(src).toEqual(`
SceneCmd header_var[] = {
    COMMAND_D(0),
    COMMAND_A(2),
    COMMAND_B(2),
    COMMAND_C(5),
    SCENE_CMD_END()
};`
    );
  });

  test("Example - deleted header", () => {
    let src = `
SceneCmd header_var[] = {
  COOL_COMMAND(),
};`;

    const header: SceneDataHeader = {
      name: "header_var",
      map: {
        COMMAND_A: [42],
        COMMAND_D: [43],
      },
      indices: {
        COMMAND_A: 3,
        COMMAND_B: 2,
        COMMAND_C: 1,
        COMMAND_D: 0,
      }
    };
    src = saveSceneHeader(header, src);

    expect(src).toEqual(`
SceneCmd header_var[] = {
    COMMAND_D(43),
    COMMAND_A(42),
    SCENE_CMD_END()
};`
    );
  });

  test("Example - no headers (add end command)", () => {
    let src = `
SceneCmd header_var[] = {
  COOL_COMMAND(),
};`;

    const header: SceneDataHeader = {
      name: "header_var",
      map: {
      },
      indices: {
        COMMAND_A: 3,
        COMMAND_B: 2,
        COMMAND_C: 1,
        COMMAND_D: 0,
      }
    };
    src = saveSceneHeader(header, src);

    expect(src).toEqual(`
SceneCmd header_var[] = {
    SCENE_CMD_END()
};`
    );
  });
});