/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { GpuObject, GpuObjectVertexSize } from "./data/object";
import { Z64Emulator } from "../emulator/z64Emulator";
import { VRamAddress } from "../../z64/types";
import { emptyGpuTileInfo, GpuTexture } from "./data/texture";
import Config from "../../../config";

export const emulateDisplayList = (emu: Z64Emulator, offsets: VRamAddress[]): GpuObject  => {

  if(Config.F3D_DEBUG_MODE) {
    emu.mipsi.setLogLevel(0b11111);
  }

  emu.mipsi.gpuF3DEX2Reset();
  emu.mipsi.gpuF3DEX2SyncSegmentAddress(emu.segmentTableAddress);

  const textures: GpuTexture[] = [];
  Z64Emulator.onGpuEmitTexture = (address, width, height, format, bpp) => 
  {
    const tile = emptyGpuTileInfo();
    tile.width = width;
    tile.height = height;
    tile.bitSize = bpp;
    tile.format = format;
    tile.uuid = "tex-"+address+"-"+textures.length+(+new Date());

    const byteSize = width * height * 4;
    textures.push({
      globalIndex: textures.length,
      rgbaBuffer: new Uint8Array(emu.getMemoryBuffer()).slice(address, address + byteSize),
      tile
    });
  };
  

  let byteSize = 0;
  for(const offset of offsets) {
    byteSize = emu.mipsi.gpuF3DEX2ParseDisplayList(offset);
  }

  emu.mipsi.setLogLevel(0b11);
  
  const buffer = new ArrayBuffer(byteSize);
  const view = new DataView(buffer);

  if(byteSize === 0) {
    return {buffer, view, textures: [], currentVertex: 0};
  }

  const address = emu.mipsi.gpuF3DEX2GetOutputPointer();
  emu.copyFromMemory(new Uint8Array(buffer), byteSize, address);
  
  return {
    buffer, view,
    textures,
    currentVertex: byteSize / GpuObjectVertexSize
  };
}

