/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */
import { emulateDisplayList } from "../gpu/executeDisplayList";

import { SavestateSlot, Z64Emulator } from '../emulator/z64Emulator';
import { Actor, GenericActor } from "./actorList";
import { ActorEmuData, readActor, setActorGlobalVars, setActorPlayerPos, setCameraData, setDayTime, setScenePathData } from "../emulator/actorEmuBridge";
import { calcZ64MeshAABB, Z64Mesh } from "../mesh/mesh";
import { createAABB } from "../../utils/aabb";
import { resetDisplayList } from "../emulator/displayList";
import { emptyGpuObject } from "../gpu/data/object";
import { Vector3, VRamAddress } from "../types";
import { vec3 } from "gl-matrix";
import { RoomEntry } from "../rooms/roomList";
import { actorOverlayTable, objectTable } from "../../main";
import { SceneActorDependency, SceneEntry } from "../scenes/sceneList";
import { clearCollider } from "../emulator/collisionEmuBridge";
import { actorTable } from "../../decomp/actorTable";
import { SegmentOffsetType } from "../offsets";
import Config from "../../../config";
import { Context } from "../../context";

const LOG_TIME = false;

export interface ActorEmuResult {
    emuData: ActorEmuData | undefined;
    z64Mesh: Z64Mesh | undefined;
};

export interface ActorEmuEnvData {
    frame: number;
    playerPos: vec3;
    camPos: Vector3;
    camDir: Vector3;
    camRot: Vector3;

    day: number; // MM only
    time: number;  // MM only?
}

const DEF_INIT_FRAME_COUNT = 2;

// Overrides to better display certain-actors in the editor, keep exceptions to a minimum
const initFrameOverride: Record<string, number> = {
    "ACTOR_ITEM_B_HEART": 24, // Heart-container, slowly scales up
    "ACTOR_EN_TEST": 18, // Stalfos, squashed in the ground, take a while to rise
    "ACTOR_EN_BROB": 1, // weird square-jabu tounges, starts attacking link
    "ACTOR_EN_WF": 5, // wolf, rises from ground
    "ACTOR_EN_DEKUBABA": 1, // starts attacking
    "ACTOR_BG_HIDAN_FIREWALL": 8, // about half the height
    //"ACTOR_EN_DODOJR": 14, // [too slow] under ground
    "ACTOR_BG_HAKA_MEGANEBG": 1, // random objects (gate, rotating shadow thingy)
    "ACTOR_EN_DHA": 1,
};

function getInitFrameTime(actor: GenericActor, envData: ActorEmuEnvData): number {
    const actorName = actorTable.list[actor.id]?.name || "";
    //console.log(actorName);

    // Stalfos dropping down
    if(actorName === "ACTOR_EN_TEST" && actor.initValue === 3) {
        return DEF_INIT_FRAME_COUNT;
    }

    // hands that grab you, more player away to keep them upright
    if(actorName === "ACTOR_EN_DHA") {
        envData.playerPos = [actor.pos[0] + 200, actor.pos[1], actor.pos[2] - 200];
    }

    // black plane that fades when getting close, put the player just far enough to be half-transparent
    if(actorName === "ACTOR_EN_HOLL") {
        envData.playerPos = [actor.pos[0] + 90, actor.pos[1], actor.pos[2] - 90];
    }
    
    return initFrameOverride[actorName] || DEF_INIT_FRAME_COUNT;
}

/**
 * Returns data specific to the given actor instance (emulation-data and GPU objects)
 * This data might change depending on the paramter value or even the position.
 * Note: This function might take longer to execute, since it's executing actors functions in an emulator.
 * @param actor current actor instance
 * @param actorOverlay overlay
 * @param objEntry obj table entry
 */
  export const emulateActor = (
    actor: Actor, scene: SceneEntry, room: RoomEntry, envData: ActorEmuEnvData, 
    takeSavestate: boolean = false, loadSavestate: boolean = false
  ) : ActorEmuResult => 
  {
    if(Config.DISABLE_ACTORS) {
        return {emuData: undefined, z64Mesh: undefined};
    }
    
    if(LOG_TIME)console.time("== Emulate actor")
    const actorOverlay = actorOverlayTable.overlays[actor.id];

    if(!actorOverlay || !actorOverlay.file) {
        console.warn("Could not init Actor: 0x" + actor.id.toString(16));
        if(LOG_TIME)console.timeEnd("== Emulate actor");
        return {emuData: undefined, z64Mesh: undefined};
    }
    
    let emuData: ActorEmuData | undefined;
    let z64Mesh: Z64Mesh = {
        uuid: [actor.uuid, actor.initValue, ...actor.pos, ...actor.rot, envData.frame, scene.paths.length].join("_"),
        gpuObjectOpaque: emptyGpuObject(),
        gpuObjectTransparent: emptyGpuObject(),
        aabb: createAABB()
    };

    let dplAddressOpaque: VRamAddress = 0;
    let dplAddressTransparent: VRamAddress = 0;

    // Load or reset Emulator
    const z64Emu = Z64Emulator.getInstance();
    
    if(loadSavestate && z64Emu.restoreSavestate(SavestateSlot.ACTOR, actor.uuid)) 
    {
        setActorGlobalVars(z64Emu, envData.frame);
        if(Context.isMM())setDayTime(z64Emu, envData.day, envData.time);

        setActorPlayerPos(z64Emu, actor, envData.playerPos);
        setCameraData(z64Emu, envData.camPos, envData.camDir, envData.camRot);
        setScenePathData(z64Emu, scene);

        if(LOG_TIME)console.time("Actor update&draw");
        
        z64Emu.callSceneDraw(scene);

        z64Emu.executeActorUpdate();

        const drawAddr = z64Emu.executeActorDraw();
        dplAddressOpaque = drawAddr[0];
        dplAddressTransparent = drawAddr[1];

        if(LOG_TIME)console.timeEnd("Actor update&draw");

    } else {
        z64Emu.mipsi?.clearCache();

        // Scene data
        if(LOG_TIME)console.time("load scene collision");

        const sceneIdStr = scene.sceneId.toString();
        if(!z64Emu.restoreSavestate(SavestateSlot.SCENE, sceneIdStr)) 
        {
            console.error("Scene not loaded, this should not happen!");
            return {emuData: undefined, z64Mesh: undefined};
        }

        if(LOG_TIME)console.timeEnd("load scene collision");

        if(LOG_TIME)console.time("load objects");

        const allocOverlay = z64Emu.loadActor(actorOverlay, actor);
        const objEntry = objectTable.objects[actorOverlay.file.objectId];

        const initFrames = loadSavestate ? DEF_INIT_FRAME_COUNT : getInitFrameTime(actor, envData);

        // Actor-Object dependency
        if(objEntry) {
            z64Emu.loadObject(objEntry);
        }

        // Room dependencies
        for(const objDepId of room.objectDependencies) 
        {
            if(objectTable.objects[objDepId]) {
              z64Emu.loadObject(objectTable.objects[objDepId]);
            }
        }

        // Global dependencies
        if(scene.actorDependency === SceneActorDependency.FIELD) {
            z64Emu.loadFieldData();
        } else if(scene.actorDependency === SceneActorDependency.DUNGEON) {
            z64Emu.loadDungeonData();
        }

        z64Emu.setSegmentAddress(SegmentOffsetType.ROOM, room.ramOffsetStart);

        if(LOG_TIME)console.timeEnd("load objects");

        setActorPlayerPos(z64Emu, actor, envData.playerPos);
        setCameraData(z64Emu, envData.camPos, envData.camDir, envData.camRot);
        setScenePathData(z64Emu, scene);
        
        if(Context.isMM())setDayTime(z64Emu, envData.day, envData.time);

        if(LOG_TIME)console.time("init/update/run");

        // Init actor manually
        z64Emu.executeActorInit(allocOverlay);

        // Run update+draw for a few frames, some actors take some time to be visible
        for(let i=0; i<initFrames; ++i) {
            setActorGlobalVars(z64Emu, i);

            z64Emu.executeActorUpdate();

            if(i !== (initFrames-1)) {
                resetDisplayList(z64Emu);
                clearCollider(z64Emu);
            }
        }

        const drawAddr = z64Emu.executeActorDraw();
        dplAddressOpaque = drawAddr[0];
        dplAddressTransparent = drawAddr[1];

        if(LOG_TIME)console.timeEnd("init/update/run");
    }

    // Extract emulated data
    emuData = readActor(z64Emu);

    // GPU Objects
    try{
        if(LOG_TIME)console.time("executeDisplayList");
        
        const gpuObjOpaque = emulateDisplayList(z64Emu, [dplAddressOpaque]);
        const gpuObjTransparent = emulateDisplayList(z64Emu, [dplAddressTransparent]);

        resetDisplayList(z64Emu);

        if(LOG_TIME)console.timeEnd("executeDisplayList");

        z64Mesh.gpuObjectOpaque = gpuObjOpaque;
        z64Mesh.gpuObjectTransparent = gpuObjTransparent;

    } catch(e) {
        console.error(e);
    }

    if(takeSavestate) {
        z64Emu.takeSavestate(SavestateSlot.ACTOR, actor.uuid);
    }

    z64Mesh.aabb = calcZ64MeshAABB(z64Mesh);

    if(LOG_TIME)console.timeEnd("== Emulate actor");
    return {emuData, z64Mesh};
};
