//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
export const chestData: Array<{name: string, value: string}> = [
  {
    "value": "0",
    "name": "[Unused 0x0]"
  },
  {
    "value": "1",
    "name": "Bombs (5)"
  },
  {
    "value": "2",
    "name": "Deku Nuts (5)"
  },
  {
    "value": "3",
    "name": "Bombchus (10)"
  },
  {
    "value": "4",
    "name": "Fairy Bow"
  },
  {
    "value": "5",
    "name": "Fairy Slingshot"
  },
  {
    "value": "6",
    "name": "Boomerang"
  },
  {
    "value": "7",
    "name": "Deku Stick (1)"
  },
  {
    "value": "8",
    "name": "Hookshot"
  },
  {
    "value": "9",
    "name": "Longshot"
  },
  {
    "value": "10",
    "name": "Lens of Truth"
  },
  {
    "value": "11",
    "name": "Zelda's Letter"
  },
  {
    "value": "12",
    "name": "Ocarina of Time"
  },
  {
    "value": "13",
    "name": "Megaton Hammer"
  },
  {
    "value": "14",
    "name": "Cojiro"
  },
  {
    "value": "15",
    "name": "Empty Bottle"
  },
  {
    "value": "16",
    "name": "Red Potion"
  },
  {
    "value": "17",
    "name": "Green Potion"
  },
  {
    "value": "18",
    "name": "Blue Potion"
  },
  {
    "value": "19",
    "name": "Bottled Fairy"
  },
  {
    "value": "20",
    "name": "Milk Bottle"
  },
  {
    "value": "21",
    "name": "Ruto's Letter"
  },
  {
    "value": "22",
    "name": "Magic Bean"
  },
  {
    "value": "23",
    "name": "Skull Mask"
  },
  {
    "value": "24",
    "name": "Spooky Mask"
  },
  {
    "value": "25",
    "name": "Chicken"
  },
  {
    "value": "26",
    "name": "Keaton Mask"
  },
  {
    "value": "27",
    "name": "Bunny Hood"
  },
  {
    "value": "28",
    "name": "Mask of Truth"
  },
  {
    "value": "29",
    "name": "Pocket Egg"
  },
  {
    "value": "30",
    "name": "Pocket Cucco"
  },
  {
    "value": "31",
    "name": "Odd Mushroom"
  },
  {
    "value": "32",
    "name": "Odd Potion"
  },
  {
    "value": "33",
    "name": "Poacher's Saw"
  },
  {
    "value": "34",
    "name": "Broken Goron's Sword"
  },
  {
    "value": "35",
    "name": "Prescription"
  },
  {
    "value": "36",
    "name": "Eyeball Frog"
  },
  {
    "value": "37",
    "name": "World's Finest Eye Drops"
  },
  {
    "value": "38",
    "name": "Claim Check"
  },
  {
    "value": "39",
    "name": "Kokiri's Sword"
  },
  {
    "value": "40",
    "name": "Giant's Knife"
  },
  {
    "value": "41",
    "name": "Deku Shield"
  },
  {
    "value": "42",
    "name": "Hylian Shield"
  },
  {
    "value": "43",
    "name": "Mirror Shield"
  },
  {
    "value": "44",
    "name": "Goron Tunic"
  },
  {
    "value": "45",
    "name": "Zora Tunic"
  },
  {
    "value": "46",
    "name": "Iron Boots"
  },
  {
    "value": "47",
    "name": "Hover Boots"
  },
  {
    "value": "48",
    "name": "Big Quiver"
  },
  {
    "value": "49",
    "name": "Biggest Quiver"
  },
  {
    "value": "50",
    "name": "Bomb Bag (20)"
  },
  {
    "value": "51",
    "name": "Big Bomb Bag (30)"
  },
  {
    "value": "52",
    "name": "Biggest Bomb Bag (40)"
  },
  {
    "value": "53",
    "name": "Silver Gauntlets"
  },
  {
    "value": "54",
    "name": "Golden Gauntlets"
  },
  {
    "value": "55",
    "name": "Silver Scale"
  },
  {
    "value": "56",
    "name": "Golden Scale"
  },
  {
    "value": "57",
    "name": "Stone of Agony"
  },
  {
    "value": "58",
    "name": "Gerudo Membership Card"
  },
  {
    "value": "59",
    "name": "Fairy Ocarina"
  },
  {
    "value": "60",
    "name": "[Unused 0x3c]"
  },
  {
    "value": "61",
    "name": "Heart Container"
  },
  {
    "value": "62",
    "name": "Piece of Heart"
  },
  {
    "value": "63",
    "name": "Boss Key"
  },
  {
    "value": "64",
    "name": "Compass"
  },
  {
    "value": "65",
    "name": "Map"
  },
  {
    "value": "66",
    "name": "Small Key"
  },
  {
    "value": "67",
    "name": "Small Magic Jar"
  },
  {
    "value": "68",
    "name": "Large Magic Jar"
  },
  {
    "value": "69",
    "name": "Adult Wallet"
  },
  {
    "value": "70",
    "name": "Giant Wallet"
  },
  {
    "value": "71",
    "name": "Weird Egg"
  },
  {
    "value": "72",
    "name": "Recovery Heart"
  },
  {
    "value": "73",
    "name": "Arrows (5)"
  },
  {
    "value": "74",
    "name": "Arrows (10)"
  },
  {
    "value": "75",
    "name": "Arrows (30)"
  },
  {
    "value": "76",
    "name": "Green Rupee"
  },
  {
    "value": "77",
    "name": "Blue Rupee"
  },
  {
    "value": "78",
    "name": "Red Rupee"
  },
  {
    "value": "79",
    "name": "[Unused 0x4f]"
  },
  {
    "value": "80",
    "name": "Milk Refill"
  },
  {
    "value": "81",
    "name": "Goron Mask"
  },
  {
    "value": "82",
    "name": "Zora Mask"
  },
  {
    "value": "83",
    "name": "Gerudo Mask"
  },
  {
    "value": "84",
    "name": "Goron Bracelet"
  },
  {
    "value": "85",
    "name": "Purple Rupee (50)"
  },
  {
    "value": "86",
    "name": "Huge/Yellow Rupee (200)"
  },
  {
    "value": "87",
    "name": "Biggoron's Sword"
  },
  {
    "value": "88",
    "name": "Fire Arrow"
  },
  {
    "value": "89",
    "name": "Ice Arrow"
  },
  {
    "value": "90",
    "name": "Light Arrow"
  },
  {
    "value": "91",
    "name": "Gold Skulltula Token"
  },
  {
    "value": "92",
    "name": "Din's Fire"
  },
  {
    "value": "93",
    "name": "Farore's Wind"
  },
  {
    "value": "94",
    "name": "Nayru's Love"
  },
  {
    "value": "95",
    "name": "Deku Seeds Bag (30)"
  },
  {
    "value": "96",
    "name": "Deku Seeds Bag (40)"
  },
  {
    "value": "97",
    "name": "Deku Stick (5)"
  },
  {
    "value": "98",
    "name": "Deku Stick (10)"
  },
  {
    "value": "99",
    "name": "[Unused 0x63]"
  },
  {
    "value": "100",
    "name": "Deku Nuts (10)"
  },
  {
    "value": "101",
    "name": "Bombs (1)"
  },
  {
    "value": "102",
    "name": "Bombs (10)"
  },
  {
    "value": "103",
    "name": "Bombs (20)"
  },
  {
    "value": "104",
    "name": "Bombs (30)"
  },
  {
    "value": "105",
    "name": "Deku Seeds (30)"
  },
  {
    "value": "106",
    "name": "Bombchus (5)"
  },
  {
    "value": "107",
    "name": "Bombchus (20)"
  },
  {
    "value": "108",
    "name": "Bottled Fish"
  },
  {
    "value": "109",
    "name": "Bottled Bug"
  },
  {
    "value": "110",
    "name": "Blue Fire"
  },
  {
    "value": "111",
    "name": "Bottled Poe"
  },
  {
    "value": "112",
    "name": "Bottled Big Poe"
  },
  {
    "value": "113",
    "name": "Door Key (Treasure Box Shop)"
  },
  {
    "value": "114",
    "name": "Loser Green Rupee (Treasure Box Shop)"
  },
  {
    "value": "115",
    "name": "Loser Blue Rupee (Treasure Box Shop)"
  },
  {
    "value": "116",
    "name": "Loser Red Rupee (Treasure Box Shop)"
  },
  {
    "value": "117",
    "name": "Winner Purple Rupee (Treasure Box Shop)"
  },
  {
    "value": "118",
    "name": "Winner Piece of Heart (Treasure Box Shop)"
  },
  {
    "value": "119",
    "name": "Deku Stick Upgrade (20)"
  },
  {
    "value": "120",
    "name": "Deku Stick Upgrade (30)"
  },
  {
    "value": "121",
    "name": "Deku Nut Upgrade (30)"
  },
  {
    "value": "122",
    "name": "Deku Nut Upgrade (40)"
  },
  {
    "value": "123",
    "name": "Deku Seeds Bag (50)"
  },
  {
    "value": "124",
    "name": "Ice Trap"
  }
];
