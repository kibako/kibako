//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
import { ActorData } from '../actorDataType';
export const actorData: ActorData = {
  "ACTOR_PLAYER": {
    "name": "Link / Spawn point",
    "category": "ACTORCAT_PLAYER",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3840,
        "values": [
          {
            "value": "0",
            "name": "Invisible, Can't Control"
          },
          {
            "value": "256",
            "name": "Master Sword Pull and Drop Animation"
          },
          {
            "value": "512",
            "name": "Spawn: Blue Warp"
          },
          {
            "value": "768",
            "name": "Spawn: Immobile"
          },
          {
            "value": "1024",
            "name": "Spawn: Exit via Grotto"
          },
          {
            "value": "1280",
            "name": "Spawn: Warp Song"
          },
          {
            "value": "1536",
            "name": "Spawn: Farore's Wind"
          },
          {
            "value": "1792",
            "name": "Spawn: Thrown Out"
          },
          {
            "value": "3328",
            "name": "Stand"
          },
          {
            "value": "3584",
            "name": "Slow Walk Forward"
          },
          {
            "value": "3840",
            "name": "Retain Previous Velocity"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Camera ID (nullable)",
        "mask": 255
      }
    ],
    "notes": "Last 2 digits = Camera Initial Focus //Data defined by scene Link spawns in\n+00FF No Special Positioning"
  },
  "ACTOR_EN_TEST": {
    "name": "Stalfos",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_SK2"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Invisible (Lens of Truth), Mini-Boss Theme"
          },
          {
            "value": "1",
            "name": "Rises from Ground, Mini-Boss Theme"
          },
          {
            "value": "2",
            "name": "Rises from Ground"
          },
          {
            "value": "3",
            "name": "Drops From Sky on Approach"
          },
          {
            "value": "4",
            "name": "Rises from Ground, Mini-Boss Theme"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_GIRLA": {
    "name": "Shops",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 1,
        "type": "number",
        "name": "Shop Item",
        "mask": 50
      }
    ],
    "notes": "Intended to be spawned by the Shopkeeper actor"
  },
  "ACTOR_EN_PART": {
    "name": "Disintegrating Blue Flame",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "1",
            "name": "Green Flame"
          },
          {
            "value": "3",
            "name": "Purple Flame"
          },
          {
            "value": "4",
            "name": "Red Bubbles"
          },
          {
            "value": "5",
            "name": "Invisible Hovering Object w/Shadow, bursts into purple flames"
          },
          {
            "value": "9",
            "name": "Green Flame"
          },
          {
            "value": "10",
            "name": "Green Flame"
          },
          {
            "value": "11",
            "name": "Purple Flame"
          },
          {
            "value": "12",
            "name": "Green Flame"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_LIGHT": {
    "name": "Flame",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Large Orange Flame"
          },
          {
            "value": "1",
            "name": "Large Orange Flame"
          },
          {
            "value": "2",
            "name": "Large Blue Flame"
          },
          {
            "value": "3",
            "name": "Large Green Flame"
          },
          {
            "value": "4",
            "name": "Small Orange Flame"
          },
          {
            "value": "5",
            "name": "Large Orange Flame"
          },
          {
            "value": "6",
            "name": "Large Green Flame"
          },
          {
            "value": "7",
            "name": "Large Blue Flame"
          },
          {
            "value": "8",
            "name": "Large Magenta Flame"
          },
          {
            "value": "9",
            "name": "Large Pale Orange Flame"
          },
          {
            "value": "10",
            "name": "Large Pale Yellow Flame"
          },
          {
            "value": "11",
            "name": "Large Pale Green Flame"
          },
          {
            "value": "12",
            "name": "Large Pale Pink Flame"
          },
          {
            "value": "13",
            "name": "Large Pale Purple Flame"
          },
          {
            "value": "14",
            "name": "Large Pale Indigo Flame"
          },
          {
            "value": "15",
            "name": "Large Pale Blue Flame"
          },
          {
            "value": "33776",
            "name": "Candle Flame"
          },
          {
            "value": "65535",
            "name": "Faint Blue Aura"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DOOR": {
    "name": "Wooden Door",
    "category": "ACTORCAT_DOOR",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 896,
        "values": [
          {
            "value": "0",
            "name": "Loads Room"
          },
          {
            "value": "128",
            "name": "Small Key Locked Door"
          },
          {
            "value": "256",
            "name": "Loads Room"
          },
          {
            "value": "384",
            "name": "Scene Transition"
          },
          {
            "value": "512",
            "name": "Ajar (slams on approach)"
          },
          {
            "value": "640",
            "name": "Displays Textbox"
          },
          {
            "value": "768",
            "name": "Time Locked (Unlocked between 18:00 and 21:00)"
          },
          {
            "value": "896",
            "name": "Loads Room"
          }
        ]
      },
      {
        "typeFilter": [
          640
        ],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Message ID (+0200)",
        "mask": 63
      },
      {
        "typeFilter": [
          128
        ],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Double Door",
        "mask": 64
      }
    ],
    "notes": "Model depends on the scene"
  },
  "ACTOR_EN_BOX": {
    "name": "Treasure Chest",
    "category": "ACTORCAT_CHEST",
    "objDeps": [
      "OBJECT_BOX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Large"
          },
          {
            "value": "4096",
            "name": "Large, Appears, Clear Flag"
          },
          {
            "value": "8192",
            "name": "Boss Key's Chest"
          },
          {
            "value": "12288",
            "name": "Large, Falling, Switch Flag"
          },
          {
            "value": "16384",
            "name": "Large, Invisible"
          },
          {
            "value": "20480",
            "name": "Small"
          },
          {
            "value": "24576",
            "name": "Small, Invisible"
          },
          {
            "value": "28672",
            "name": "Small, Appears, Clear Flag"
          },
          {
            "value": "32768",
            "name": "Small, Falls, Switch Flag"
          },
          {
            "value": "36864",
            "name": "Large, Appears, Zelda's Lullaby"
          },
          {
            "value": "40960",
            "name": "Large, Appears, Sun's Song Triggered"
          },
          {
            "value": "45056",
            "name": "Large, Appears, Switch Flag"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 5,
        "type": "chest",
        "name": "Content",
        "mask": 4064
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Chest",
        "mask": 31
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_DY_YOSEIZO": {
    "name": "Great Fairy",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_DY_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": "Hardcoded cutscene camera points to -19, -2 ,-1000"
  },
  "ACTOR_BG_HIDAN_FIREWALL": {
    "name": "Proximity Activated Firewall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [],
    "notes": "Missing variables"
  },
  "ACTOR_EN_POH": {
    "name": "Poe",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_POH"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Graveyard Poe"
          },
          {
            "value": "1",
            "name": "Graveyard Poe"
          },
          {
            "value": "2",
            "name": "(006E) Sharp (Composer Brothers)"
          },
          {
            "value": "3",
            "name": "(006E) Flat (Composer Brothers)"
          },
          {
            "value": "4",
            "name": "Graveyard Poe"
          }
        ]
      }
    ],
    "notes": "May need another object"
  },
  "ACTOR_EN_OKUTA": {
    "name": "Octorok",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_OKUTA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Shots per Round",
        "mask": 65280
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Projectile",
        "mask": 255
      }
    ],
    "notes": "First 2 digits: Rocks fired per volley"
  },
  "ACTOR_BG_YDAN_SP": {
    "name": "Webs",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_YDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Web-Covered Hole"
          },
          {
            "value": "4096",
            "name": "Vertical Web Wall"
          },
          {
            "value": "8192",
            "name": "Web-Covered Hole"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "flag",
        "name": "Switch",
        "mask": 4032
      }
    ],
    "notes": "+0x3F00 = Switch Flag (Set when destroyed)\n      +0x0FC0 = Switch Flag (Web burns when set)"
  },
  "ACTOR_EN_BOM": {
    "name": "Bomb",
    "category": "ACTORCAT_EXPLOSIVE",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Bomb"
          },
          {
            "value": "1",
            "name": "Bomb Shadow"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WALLMAS": {
    "name": "Wallmaster",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_WALLMASTER"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Timer Spawn (every 6 seconds)"
          },
          {
            "value": "1",
            "name": "Proximity Spawn"
          },
          {
            "value": "2",
            "name": "Spawn on Switch Flag"
          }
        ]
      },
      {
        "typeFilter": [
          2
        ],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+ 0x3F00 = Switch Flag (Flag to spawn on. Used by type 02 only)"
  },
  "ACTOR_EN_DODONGO": {
    "name": "Dodongo",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_DODONGO"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          },
          {
            "value": "0",
            "name": "Two-Foot Dodongo"
          },
          {
            "value": "1",
            "name": "Two-Foot Dodongo"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_FIREFLY": {
    "name": "Keese",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_FIREFLY"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 32767,
        "values": [
          {
            "value": "0",
            "name": "Fire Keese"
          },
          {
            "value": "2",
            "name": "Aggressive Keese"
          },
          {
            "value": "3",
            "name": "Roosting Keese"
          },
          {
            "value": "4",
            "name": "Ice Keese"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Invisible",
        "mask": 32768
      }
    ],
    "notes": "+8000 = Invisible"
  },
  "ACTOR_EN_HORSE": {
    "name": "Epona",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HORSE"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Epona"
          },
          {
            "value": "32768",
            "name": "No Epona"
          },
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ITEM00": {
    "name": "Collectable Item",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Green Rupee"
          },
          {
            "value": "1",
            "name": "Blue Rupee"
          },
          {
            "value": "2",
            "name": "Red Rupee"
          },
          {
            "value": "3",
            "name": "Recovery Heart"
          },
          {
            "value": "4",
            "name": "Bomb"
          },
          {
            "value": "5",
            "name": "Arrow (1)"
          },
          {
            "value": "6",
            "name": "Heart Piece"
          },
          {
            "value": "8",
            "name": "Deku Seeds (5) or Arrows (5)"
          },
          {
            "value": "9",
            "name": "Deku Seeds (5) or Arrows (10)"
          },
          {
            "value": "10",
            "name": "Deku Seeds (5) or Arrows (30)"
          },
          {
            "value": "11",
            "name": "Bombs (5)"
          },
          {
            "value": "12",
            "name": "Deku Nut"
          },
          {
            "value": "13",
            "name": "Deku Stick"
          },
          {
            "value": "14",
            "name": "Large Magic Jar"
          },
          {
            "value": "15",
            "name": "Small Magic Jar"
          },
          {
            "value": "16",
            "name": "Deku Seeds (5) or Arrows (5)"
          },
          {
            "value": "17",
            "name": "Small Key"
          },
          {
            "value": "18",
            "name": "Flexible Drop"
          },
          {
            "value": "19",
            "name": "Giant Orange Rupee"
          },
          {
            "value": "20",
            "name": "Large Purple Rupee"
          },
          {
            "value": "21",
            "name": "Deku Shield"
          },
          {
            "value": "22",
            "name": "Hylian Shield"
          },
          {
            "value": "23",
            "name": "Zora Tunic"
          },
          {
            "value": "24",
            "name": "Goron Tunic"
          },
          {
            "value": "25",
            "name": "Bombs (5)"
          },
          {
            "value": "26",
            "name": "Invisible Item"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Collectible",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Collectible Flag"
  },
  "ACTOR_EN_ARROW": {
    "name": "Arrow",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Fire Arrow"
          },
          {
            "value": "1",
            "name": "Wooden Arrow"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ELF": {
    "name": "Fairy",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Navi"
          },
          {
            "value": "1",
            "name": "Bottled Healing Fairy"
          },
          {
            "value": "2",
            "name": "Roaming Healing Fairy"
          },
          {
            "value": "4",
            "name": "Group of Healing Fairies"
          },
          {
            "value": "5",
            "name": "Fairy Healing You"
          },
          {
            "value": "7",
            "name": "Roaming Large Healing Fairy"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_NIW": {
    "name": "Cucco",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_NIW"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "4",
            "name": "Flees when approached"
          },
          {
            "value": "8",
            "name": "Doesn't Flee"
          },
          {
            "value": "14",
            "name": "Non-Targetable"
          },
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TITE": {
    "name": "Tektite",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_TITE"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Red Tektite"
          },
          {
            "value": "65534",
            "name": "Blue Tektite"
          },
          {
            "value": "0",
            "name": "Normal Tektite"
          },
          {
            "value": "4",
            "name": "Dies Instantly"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_REEBA": {
    "name": "Leever",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_REEBA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Big",
        "mask": 1
      }
    ],
    "notes": "Clear flag ignores this monster"
  },
  "ACTOR_EN_PEEHAT": {
    "name": "Peahat",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_PEEHAT"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Flying Peahat"
          },
          {
            "value": "1",
            "name": "Peahat Larva"
          },
          {
            "value": "65535",
            "name": "Burrowed Peahat"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_BUTTE": {
    "name": "Butterfly",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Can Morph into Fairy",
        "mask": 1
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_INSECT": {
    "name": "Bug",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Large Bug"
          },
          {
            "value": "2",
            "name": "Three Small Bugs"
          },
          {
            "value": "3",
            "name": "Small Bug"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_FISH": {
    "name": "Fish",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Flopping"
          },
          {
            "value": "1",
            "name": "Swimming, Doesn't Flee"
          },
          {
            "value": "2",
            "name": "Swimming, Reacts to Link"
          },
          {
            "value": "65535",
            "name": "Swimming, Flees"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_HOLL": {
    "name": "Room Changing Black Plane",
    "category": "ACTORCAT_DOOR",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 448,
        "values": [
          {
            "value": "0",
            "name": "Vertical, two way, fades black when approached"
          },
          {
            "value": "64",
            "name": "Up/Down, one-way, fades black when approached"
          },
          {
            "value": "128",
            "name": "Up/Down, two-way, no fade"
          },
          {
            "value": "192",
            "name": "Vertical, enabled on switch flag, fades black when approached"
          },
          {
            "value": "256",
            "name": "Vertical, two-way, no fade"
          },
          {
            "value": "320",
            "name": "Up/Down, two-way, fades black when approached"
          }
        ]
      },
      {
        "typeFilter": [
          192
        ],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_SCENE_CHANGE": {
    "name": "(Unknown scene change)",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZF": {
    "name": "Lizalfos",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_ZF"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Lizalfos mini-boss, drops from ceiling"
          },
          {
            "value": "1",
            "name": "Lizalfos mini-boss, no mini-boss music"
          },
          {
            "value": "128",
            "name": "Lizalfos, no mini-boss music"
          },
          {
            "value": "254",
            "name": "Dinolfos, no mini-boss music"
          },
          {
            "value": "255",
            "name": "Lizalfos, no mini-boss music, drops from ceiling"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Nullable Switch Flag Lizalfos miniboss spawns when entering room from door bound to this switch flag\nThe Lizalfos miniboss (Type 00 and 01) has special behavior to it.\nType 00 and 01 actors must be spawned together for the miniboss to be defeated.\nTo differentiate the two encounters (as both are in room 3), the switch flag is used. This flag matches the switch flag of the door used to enter the room"
  },
  "ACTOR_EN_HATA": {
    "name": "Wooden Post with Red Cloth",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_HATA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_DODONGO": {
    "name": "King Dodongo",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_KINGDODONGO"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_GOMA": {
    "name": "Queen Gohma",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_GOMA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ZL1": {
    "name": "Princess Zelda Child",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ZL1"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_VIEWER": {
    "name": "Cutscene Actors",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Impa's Horse"
          },
          {
            "value": "257",
            "name": "Impa on Horse"
          },
          {
            "value": "514",
            "name": "Zelda"
          },
          {
            "value": "771",
            "name": "Ganondorf on Horse (Stationary)"
          },
          {
            "value": "1028",
            "name": "Ganondorf's Horse (Stationary)"
          },
          {
            "value": "1535",
            "name": "Ganondorf on Horse (Riding) + Flames"
          },
          {
            "value": "1791",
            "name": "Ganondorf's Horse (Galloping)"
          },
          {
            "value": "2047",
            "name": "Ganondorf hands crossed"
          },
          {
            "value": "2303",
            "name": "Ganondorf Bowing to King"
          },
          {
            "value": "2559",
            "name": "Ganondorf floating (Curse You)"
          }
        ]
      }
    ],
    "notes": "They need their respective objects to load"
  },
  "ACTOR_EN_GOMA": {
    "name": "Gohma Larva",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_GOL"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Falls, lands on ground, hatches in moments"
          },
          {
            "value": "3",
            "name": "Crashes"
          },
          {
            "value": "6",
            "name": "Normal egg on ground"
          },
          {
            "value": "8",
            "name": "Invisible until it hatches (or with Lens of Truth)"
          },
          {
            "value": "10",
            "name": "Destroyed"
          },
          {
            "value": "100",
            "name": "Puffs of Blue Smoke (dies away when Link approaches)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_PUSHBOX": {
    "name": "Alpha Cube Copy",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [],
    "notes": "Unused"
  },
  "ACTOR_EN_BUBBLE": {
    "name": "Shabom",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BUBBLE"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_DOOR_SHUTTER": {
    "name": "Lifting Wooden Door",
    "category": "ACTORCAT_DOOR",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 960,
        "values": [
          {
            "value": "0",
            "name": "Lifting door"
          },
          {
            "value": "64",
            "name": "Front side attached to Room Clear"
          },
          {
            "value": "128",
            "name": "Front side attached to Switch"
          },
          {
            "value": "192",
            "name": "Back side permanently locked"
          },
          {
            "value": "320",
            "name": "Boss door"
          },
          {
            "value": "448",
            "name": "Front side attached to Switch, Back to Room Clear"
          },
          {
            "value": "704",
            "name": "Key Locked door"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+003F = Switch Flag //Used by switch flag iron bar and locked doors\nDepending on the scene ID, they need certain objects loaded as the first one in the object list. Scene IDs:\n00: needs object 0036\n01: needs object 002B\n02: needs object 0096\n04: needs object 002C\n05: needs object 0059\n06: needs object 016D\n07-08: needs object 0187\n09: needs object 006B\n0A: needs object 0139\n0B: needs object 004D\n0D: needs object 0179"
  },
  "ACTOR_EN_DODOJR": {
    "name": "Baby Dodongo",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_DODOJR"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_BDFIRE": {
    "name": "King Dodongo's Fire",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_KINGDODONGO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOOM": {
    "name": "Boomerang",
    "category": "ACTORCAT_MISC",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TORCH2": {
    "name": "Dark Link",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_TORCH2"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_BILI": {
    "name": "Biri",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BL"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TP": {
    "name": "Taliparisan",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_TP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ST": {
    "name": "Skulltula",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_ST"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal"
          },
          {
            "value": "1",
            "name": "Big"
          },
          {
            "value": "2",
            "name": "Invisible"
          },
          {
            "value": "3",
            "name": "+ Normal"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_BW": {
    "name": "Torch Slug",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BW"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_A_OBJ": {
    "name": "Gameplay_keep Objects",
    "category": "ACTORCAT_MISC",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Small grey stone block"
          },
          {
            "value": "1",
            "name": "Large grey stone block"
          },
          {
            "value": "2",
            "name": "Huge grey stone block"
          },
          {
            "value": "3",
            "name": "Small grey stone block, rotates when you stand on it"
          },
          {
            "value": "4",
            "name": "Large grey stone block, rotates when you stand on it"
          },
          {
            "value": "5",
            "name": "Small grey stone cube"
          },
          {
            "value": "6",
            "name": "Crashes"
          },
          {
            "value": "7",
            "name": "Grass clump"
          },
          {
            "value": "8",
            "name": "Small tree stump"
          },
          {
            "value": "9",
            "name": "Oblong Signpost (unbreakable)"
          },
          {
            "value": "10",
            "name": "Arrow Signpost"
          },
          {
            "value": "12",
            "name": "Black knobby thing"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Message ID",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Message ID (+0300)"
  },
  "ACTOR_EN_EIYER": {
    "name": "Stinger",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_EI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Four spinning a circle, but once you kill one, the rest are gone as well"
          },
          {
            "value": "1",
            "name": "Three in formation, sink under floor and do not activate"
          },
          {
            "value": "2",
            "name": "Two in formation, sink under floor and do not activate"
          },
          {
            "value": "3",
            "name": "One in formation, sink under floor and do not activate"
          },
          {
            "value": "10",
            "name": "Single"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_RIVER_SOUND": {
    "name": "Ambient Sound Effects",
    "category": "ACTORCAT_BG",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "River (Multi-Point)"
          },
          {
            "value": "1",
            "name": "Stream"
          },
          {
            "value": "2",
            "name": "Magma"
          },
          {
            "value": "3",
            "name": "Waterfall"
          },
          {
            "value": "4",
            "name": "Small Stream"
          },
          {
            "value": "5",
            "name": "Stream"
          },
          {
            "value": "6",
            "name": "Fire Temple's Lower Ambient Noise"
          },
          {
            "value": "7",
            "name": "Fire Temple's Higher Ambient Noise"
          },
          {
            "value": "8",
            "name": "Water Dripping (Well)"
          },
          {
            "value": "9",
            "name": "River"
          },
          {
            "value": "10",
            "name": "Market gibberish"
          },
          {
            "value": "11",
            "name": "Decrease current BGM volume"
          },
          {
            "value": "13",
            "name": "Proximity Saria's Song"
          },
          {
            "value": "14",
            "name": "Howling wind"
          },
          {
            "value": "15",
            "name": "Gurgling"
          },
          {
            "value": "16",
            "name": "Temple of Light's dripping sounds"
          },
          {
            "value": "17",
            "name": "Low booming-likish sound"
          },
          {
            "value": "18",
            "name": "Quake/Collapse"
          },
          {
            "value": "19",
            "name": "Fairy Fountain"
          },
          {
            "value": "20",
            "name": "Torches"
          },
          {
            "value": "21",
            "name": "Cows"
          },
          {
            "value": "22",
            "name": "Outside of the ambient noise domain"
          }
        ]
      },
      {
        "typeFilter": [
          0,
          4,
          5
        ],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID (River)",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Path ID (Used for sound sources, Type 00, 04 and 05 only)"
  },
  "ACTOR_EN_HORSE_NORMAL": {
    "name": "Normal Horse",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HORSE_NORMAL"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "1",
            "name": "Tan, Runs Around"
          },
          {
            "value": "15",
            "name": "Tan, Runs Around"
          },
          {
            "value": "16",
            "name": "Tan, Runs Away"
          },
          {
            "value": "65535",
            "name": "Tan, Runs Around"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_OSSAN": {
    "name": "Shopkeeper",
    "category": "ACTORCAT_NPC",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Kokiri Shopkeeper, Objects 0x0FC, 0x101, 0x102"
          },
          {
            "value": "1",
            "name": "Kakariko Potion Shopkeeper, Object 0x159"
          },
          {
            "value": "2",
            "name": "Bombchu Shopkeeper, spawn when King Dodongo is beaten, Object 0x165"
          },
          {
            "value": "3",
            "name": "Market Potion Shopkeeper, Object 0x159"
          },
          {
            "value": "4",
            "name": "Bazaar Shopkeeper, Object 0x05B"
          },
          {
            "value": "5",
            "name": "Unused? Shopkeeper, Object 0x05B"
          },
          {
            "value": "6",
            "name": "Unused? Shopkeeper, Object 0x05B"
          },
          {
            "value": "7",
            "name": "Zora Shopkeeper, Object 0x0FE"
          },
          {
            "value": "8",
            "name": "Goron Shopkeeper, Object 0x05B"
          },
          {
            "value": "9",
            "name": "Unused? Shopkeeper, Object 0x05B"
          },
          {
            "value": "10",
            "name": "Happy Mask Shopkeeper, Object 0x13E"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_TREEMOUTH": {
    "name": "Deku Tree's Jaw",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT04_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_DODOAGO": {
    "name": "Dodongo Skull's Jaw",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_DDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag\n      Spawns when a specific switch flag is set\nOut-of-bounds Plane (typ.)"
  },
  "ACTOR_BG_HIDAN_DALM": {
    "name": "Lower Part of Megaton Statue",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Lower Part (Flame decal)"
          },
          {
            "value": "1",
            "name": "Top Part (Face decal)"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Switch Flag"
  },
  "ACTOR_BG_HIDAN_HROCK": {
    "name": "Huge Stone Spike Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3840,
        "values": [
          {
            "value": "0",
            "name": "Huge"
          },
          {
            "value": "256",
            "name": "Big"
          },
          {
            "value": "512",
            "name": "Small"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": "+0x00FF = Switch Flag"
  },
  "ACTOR_EN_HORSE_GANON": {
    "name": "Ganondorf's Horse",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HORSE_GANON"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_ROCK": {
    "name": "Fire Temple Stone Block",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Large Face Cube"
          },
          {
            "value": "1",
            "name": "Squat Cube"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_RSEKIZOU": {
    "name": "Spinning Flame Thrower Statue",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_SEKIZOU": {
    "name": "Stationary Flame Thrower Statue",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "2-way diagonal"
          },
          {
            "value": "1",
            "name": "4-way plus"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_SIMA": {
    "name": "Sink Stone Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Sinking"
          },
          {
            "value": "1",
            "name": "Sliding"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_SYOKU": {
    "name": "Fire Temple Stone Block",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_XC": {
    "name": "Sheik",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_XC"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Idle"
          },
          {
            "value": "1",
            "name": "Unknown"
          },
          {
            "value": "2",
            "name": "Unknown"
          },
          {
            "value": "3",
            "name": "Unknown"
          },
          {
            "value": "4",
            "name": "Transformation into Zelda"
          },
          {
            "value": "5",
            "name": "Nocturne CS"
          },
          {
            "value": "6",
            "name": "Minuet CS"
          },
          {
            "value": "7",
            "name": "Bolero CS"
          },
          {
            "value": "8",
            "name": "Serenade CS"
          },
          {
            "value": "9",
            "name": "First Meeting at ToT"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_CURTAIN": {
    "name": "Flame Circle",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Large, turns off with room clear."
          },
          {
            "value": "4096",
            "name": "Large, plays cutscene, switches off for long time with ticking sound. Turns off permanently with chest flag."
          },
          {
            "value": "8192",
            "name": "Small, switches off for long time silently."
          },
          {
            "value": "12288",
            "name": "Large, plays cutscene, switches off for short time with ticking sound."
          },
          {
            "value": "16384",
            "name": "Disabled, small, switches on for short time."
          },
          {
            "value": "20480",
            "name": "Disabled, large, can be switched on and off."
          },
          {
            "value": "24576",
            "name": "Large, switches off permanently, won't spawn if room is cleared."
          },
          {
            "value": "28672",
            "name": "Nothing"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "flag",
        "name": "Chest",
        "mask": 4032
      }
    ],
    "notes": "+0x0FC0 = Chest Flag\n+0x003F = Switch Flag"
  },
  "ACTOR_BG_SPOT00_HANEBASI": {
    "name": "Drawbridge",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT00_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Chains"
          },
          {
            "value": "65535",
            "name": "Drawbridge"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_MB": {
    "name": "Ground Pounding Moblin",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_MB"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Club Moblin"
          },
          {
            "value": "255",
            "name": "Spear Moblin"
          }
        ]
      },
      {
        "typeFilter": [
          255
        ],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = path Id (Spear Moblins only)"
  },
  "ACTOR_EN_BOMBF": {
    "name": "Bomb Flower",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_BOMBF"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Bomb"
          },
          {
            "value": "1",
            "name": "Invisible Bomb"
          },
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ZL2": {
    "name": "Adult Zelda (Cutscene)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ZL2",
      "OBJECT_ZL2_ANIME1"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Light Arrows CS"
          },
          {
            "value": "1",
            "name": "Pre-Credits"
          },
          {
            "value": "4",
            "name": "Post Castle Collapse"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_FSLIFT": {
    "name": "Elevator Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OE2": {
    "name": "Lock-on Blue Spot? (Unknown)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OE2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_YDAN_HASI": {
    "name": "Back and Forth Moving Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_YDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Floating Platform"
          },
          {
            "value": "1",
            "name": "Water Plane"
          },
          {
            "value": "2",
            "name": "3 Raising platforms"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_YDAN_MARUTA": {
    "name": "Rotating Spike Cylinder",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_YDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Rotating Spike Cylinder"
          },
          {
            "value": "256",
            "name": "Deku tree ladder falling when hit by slingshot (switchflag?)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_GANONDROF": {
    "name": "Phantom Ganon",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_GND"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AM": {
    "name": "Armos",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_AM"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Statue"
          },
          {
            "value": "1",
            "name": "Enemy"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DEKUBABA": {
    "name": "Deku Baba",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_DEKUBABA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Small"
          },
          {
            "value": "1",
            "name": "Big"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_M_FIRE1": {
    "name": "Deku Nuts (from Link)",
    "category": "ACTORCAT_MISC",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_M_THUNDER": {
    "name": "Spin Attack",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DDAN_JD": {
    "name": "Rising Stone Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_DDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": "+0x3F = Switch Flag to make the platform rise more, hardcoded cutscene camera points to 420,-180 ,-1180"
  },
  "ACTOR_BG_BREAKWALL": {
    "name": "Dodongo's Cavern Objects",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_BWALL"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 24576,
        "values": [
          {
            "value": "0",
            "name": "Bombable Wall, triggers intro cutscene"
          },
          {
            "value": "8192",
            "name": "Bombable Wall"
          },
          {
            "value": "16384",
            "name": "Bombable Floor"
          },
          {
            "value": "24576",
            "name": "Lava Cover"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Play Discovery Sound",
        "mask": 32768
      }
    ],
    "notes": "+0x3F = Switch Flag"
  },
  "ACTOR_EN_JJ": {
    "name": "Lord Jabu-Jabu",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_JJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Lord Jabu-Jabu"
          }
        ]
      }
    ],
    "notes": "The actor spawns the collision data"
  },
  "ACTOR_EN_HORSE_ZELDA": {
    "name": "Zelda's Horse",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HORSE_ZELDA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DDAN_KD": {
    "name": "Stone Stairs",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_DDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_DOOR_WARP1": {
    "name": "Blue Warp",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_WARP1"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Normal (adult Link)"
          },
          {
            "value": "65534",
            "name": "Spawning in from crystal"
          },
          {
            "value": "0",
            "name": "Normal"
          },
          {
            "value": "1",
            "name": "Nothing"
          },
          {
            "value": "2",
            "name": "Blue warp, disappears"
          },
          {
            "value": "3",
            "name": "Giant purple crystal/magic enclosure"
          },
          {
            "value": "4",
            "name": "Yellow warp, disappears"
          },
          {
            "value": "5",
            "name": "Blue warp, doesn't warp you"
          },
          {
            "value": "6",
            "name": "Spawn in from child blue warp"
          },
          {
            "value": "7",
            "name": "Blue warp, warping animation"
          },
          {
            "value": "8",
            "name": "Tan warp, disappears"
          },
          {
            "value": "9",
            "name": "Green warp, disappears"
          },
          {
            "value": "10",
            "name": "Red warp, disappears"
          },
          {
            "value": "11",
            "name": "Area fails to load"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_SYOKUDAI": {
    "name": "Torch",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SYOKUDAI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Golden Torch"
          },
          {
            "value": "4096",
            "name": "Puzzle Torch"
          },
          {
            "value": "8192",
            "name": "Wooden Torch"
          }
        ]
      },
      {
        "typeFilter": [
          4096
        ],
        "target": "params",
        "shift": 6,
        "type": "number",
        "name": "# of Torches",
        "mask": 960
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Lit",
        "mask": 1024
      }
    ],
    "notes": "+0x3C0 = Torches to light to solve puzzle (Timed Torch)\n\t+0x3F = Switch Flag"
  },
  "ACTOR_ITEM_B_HEART": {
    "name": "Heart Container",
    "category": "ACTORCAT_MISC",
    "objDeps": [
      "OBJECT_GI_HEARTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DEKUNUTS": {
    "name": "Deku Scrub",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_DEKUNUTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Nuts shot per volley",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_MENKURI_KAITEN": {
    "name": "Large Rotating Stone Ring",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MENKURI_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_MENKURI_EYE": {
    "name": "Statue Eyes",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MENKURI_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag\n      Requires 4 actors of this id to work"
  },
  "ACTOR_EN_VALI": {
    "name": "Bari",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_VALI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MIZU_MOVEBG": {
    "name": "Dragon Head Statue",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MIZU_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Floating Square platform, sloped bottom"
          },
          {
            "value": "4096",
            "name": "Floating Square platform"
          },
          {
            "value": "8192",
            "name": "Floating Square platform, tapered on bottom"
          },
          {
            "value": "12288",
            "name": "Dragon Head Statue"
          },
          {
            "value": "16384",
            "name": "Dragon Head Statue"
          },
          {
            "value": "20480",
            "name": "Dragon Head Statue"
          },
          {
            "value": "24576",
            "name": "Dragon Head Statue"
          },
          {
            "value": "28672",
            "name": "Moving Square Platform with Hookshot Target"
          }
        ]
      },
      {
        "typeFilter": [
          28672
        ],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID",
        "mask": 3840
      },
      {
        "typeFilter": [
          28672
        ],
        "target": "params",
        "shift": 4,
        "type": "number",
        "name": "Speed",
        "mask": 240
      },
      {
        "typeFilter": [
          28672
        ],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Start point on path",
        "mask": 15
      },
      {
        "typeFilter": [
          16384,
          20480,
          24576
        ],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x0F00 = Path to follow //Type 7 only\n+0x00F0 = Speed //Type 7 only\n+0x000F = Start Point on Path //Type 7 only"
  },
  "ACTOR_BG_MIZU_WATER": {
    "name": "Water Temple Water",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MIZU_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Switches to lowest (Triforce seems to activate itself for some reason in the ToT)"
          },
          {
            "value": "1",
            "name": "Stays at ground level (Triforce isn't affected in ToT)"
          },
          {
            "value": "6658",
            "name": "Water Temple Map 3, Standard Quest"
          },
          {
            "value": "12547",
            "name": "Water Temple Map 6, Standard Quest"
          },
          {
            "value": "6148",
            "name": "Water Temple Map 14, Standard Quest"
          },
          {
            "value": "65280",
            "name": "Water Temple (most rooms), Standard Quest"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_ARMS_HOOK": {
    "name": "Hookshot",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_LINK_BOY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FHG": {
    "name": "Phantom Ganon's Horse",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_FHG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MORI_HINERI": {
    "name": "Twisted Hallway",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_HINERI1A"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Unknown Flag",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 14,
        "type": "number",
        "name": "Unknown",
        "mask": 16384
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 15,
        "type": "number",
        "name": "Unknown",
        "mask": 32768
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_BB": {
    "name": "Bubble",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BB"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "251",
            "name": "Large Green Bubble"
          },
          {
            "value": "252",
            "name": "Green Bubble"
          },
          {
            "value": "253",
            "name": "White Bubble"
          },
          {
            "value": "254",
            "name": "Fire Bubble"
          },
          {
            "value": "255",
            "name": "Blue Bubble"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Path Id"
  },
  "ACTOR_BG_TOKI_HIKARI": {
    "name": "Temple of Time Windows",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_TOKI_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal"
          },
          {
            "value": "1",
            "name": "Invisible"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_YUKABYUN": {
    "name": "Flying Floor Tile",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_YUKABYUN"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Time until it rises",
        "mask": 255
      }
    ],
    "notes": "+0x00FF = Time until tile rises\nVariable is first incremented by 1, then time until tile rises is calculated as T * 0x10 + 0x14 frames. For example:\n-FFFF = 0x14 frames\n-0000 = 0x24 frames\n-0001 = 0x34 frames etc."
  },
  "ACTOR_BG_TOKI_SWD": {
    "name": "Master Sword",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_TOKI_OBJECTS",
      "OBJECT_EFC_STAR_FIELD",
      "OBJECT_EFC_TW"
    ],
    "params": [],
    "notes": "Use with actors 008C and 008B for the cutscene to work"
  },
  "ACTOR_EN_FHG_FIRE": {
    "name": "Phantom Ganon's Lightning Attack",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_FHG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MJIN": {
    "name": "Warp Pad",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MJIN"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "(0068) Ocarina"
          },
          {
            "value": "1",
            "name": "(0062) Light Medallion"
          },
          {
            "value": "2",
            "name": "(0063) Shadow Medallion"
          },
          {
            "value": "3",
            "name": "(0064) Fire Medallion"
          },
          {
            "value": "4",
            "name": "(0065) Water Medallion"
          },
          {
            "value": "5",
            "name": "(0066) Spirit Medallion"
          },
          {
            "value": "6",
            "name": "(0067) Forest Medallion"
          }
        ]
      }
    ],
    "notes": "Add the object in brackets for it to work"
  },
  "ACTOR_BG_HIDAN_KOUSI": {
    "name": "Metal Gate",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Yellow with corner removed"
          },
          {
            "value": "1",
            "name": "Green and rectangular"
          },
          {
            "value": "2",
            "name": "Yellow/green and rectangular, looks rusty"
          },
          {
            "value": "3",
            "name": "Crashes, but not without drawing a huge black thing that I can only assume is a gate..."
          },
          {
            "value": "4",
            "name": "Crashes before it can even light the area..."
          },
          {
            "value": "5",
            "name": "+ Crashes, no sign of a gate"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_DOOR_TOKI": {
    "name": "Door of Time",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_TOKI_OBJECTS"
    ],
    "params": [],
    "notes": "Spawned by actor 008C"
  },
  "ACTOR_BG_HIDAN_HAMSTEP": {
    "name": "Hammer Steps in Fire Temple",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Hammer-triggered Stone Steps"
          },
          {
            "value": "1",
            "name": "Platform, one sided"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_EN_BIRD": {
    "name": "Alpha Bird",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_BIRD"
    ],
    "params": [],
    "notes": "Unused"
  },
  "ACTOR_EN_WOOD02": {
    "name": "Greenery",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_WOOD02"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Large tree"
          },
          {
            "value": "1",
            "name": "Medium tree"
          },
          {
            "value": "2",
            "name": "Small tree"
          },
          {
            "value": "3",
            "name": "Group of trees"
          },
          {
            "value": "4",
            "name": "Medium tree"
          },
          {
            "value": "5",
            "name": "Medium tree, dark brown trunk, greener leaves"
          },
          {
            "value": "6",
            "name": "Group of trees, dark brown trunk, yellow leaves"
          },
          {
            "value": "7",
            "name": "Medium tree, dark brown trunk, yellow leaves"
          },
          {
            "value": "8",
            "name": "Group of trees, dark brown trunk, greener leaves"
          },
          {
            "value": "9",
            "name": "Medium tree, dark brown trunk, greener leaves"
          },
          {
            "value": "10",
            "name": "Ugly tree from Kakariko Village"
          },
          {
            "value": "11",
            "name": "Bush"
          },
          {
            "value": "12",
            "name": "Large bush"
          },
          {
            "value": "13",
            "name": "Group of bushes"
          },
          {
            "value": "14",
            "name": "Bush"
          },
          {
            "value": "15",
            "name": "Group of large bushes"
          },
          {
            "value": "16",
            "name": "Large bush"
          },
          {
            "value": "17",
            "name": "Dark bush"
          },
          {
            "value": "18",
            "name": "Large dark bush"
          },
          {
            "value": "19",
            "name": "Group of dark bushes"
          },
          {
            "value": "20",
            "name": "Dark bush"
          },
          {
            "value": "21",
            "name": "Group of large dark bushes"
          },
          {
            "value": "22",
            "name": "Large dark bush"
          },
          {
            "value": "25",
            "name": "Dancing dark bush"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "collectible",
        "name": "Item",
        "mask": 65280
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Gold Skulltula Low Byte",
        "mask": 65280
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Gold Skulltula High Byte",
        "mask": 255
      }
    ],
    "notes": "+0xFF00 = Controls item(s) dropped (trees only)\n-0000 Random\n-0800 Deku seeds\n-0900 Magic jars\n-0A00 Bombs\n-0B00 Three Hearts\n-0C00 Three Blue Rupees\n-0D00 Random\n-0E00 Random\n-0F00 Nothing\n+0xFF00 = Gold Skulltula spawn var low byte if rz != 0\nZROT 0x00FF = Gold Skulltula spawn var high byte (See Gold Skulltula Actor 0095)"
  },
  "ACTOR_EN_LIGHTBOX": {
    "name": "Alpha Large Noisy Stone",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_LIGHTBOX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal, flashes blue when struck with sword"
          },
          {
            "value": "1",
            "name": "Frickin' huge, doesn't flash when struck"
          },
          {
            "value": "2",
            "name": "Even bigger?"
          },
          {
            "value": "3",
            "name": "Ridiculously huge?"
          },
          {
            "value": "4",
            "name": "Small size, flashes blue when struck"
          }
        ]
      }
    ],
    "notes": "Unused"
  },
  "ACTOR_EN_PU_BOX": {
    "name": "Alpha Stone Cube",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_PU_BOX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Stone cube, side struck with sword flashes blue"
          },
          {
            "value": "1",
            "name": "Slightly larger"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Size",
        "mask": 15
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TRAP": {
    "name": "Sliding Spike Trap",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_TRAP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 48,
        "values": [
          {
            "value": "0",
            "name": "4-Way Attack"
          },
          {
            "value": "16",
            "name": "Line Loop"
          },
          {
            "value": "32",
            "name": "Circle Loop"
          }
        ]
      },
      {
        "typeFilter": [
          0,
          32
        ],
        "target": "params",
        "shift": 12,
        "type": "number",
        "name": "Speed",
        "mask": 61440
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Radius",
        "mask": 3840
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Move North",
        "mask": 1
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Move South",
        "mask": 2
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Move West",
        "mask": 4
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Move East",
        "mask": 8
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_AROW_TRAP": {
    "name": "Beta Arrow Trap",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_VASE": {
    "name": "Alpha Orange Pot",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_VASE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TA": {
    "name": "Talon",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_TA"
    ],
    "params": [],
    "notes": "Text ID when he sleeps: 702A"
  },
  "ACTOR_EN_TK": {
    "name": "Dampe Grave Digging Game",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_TK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MORI_BIGST": {
    "name": "Lowering Circular Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_OBJECTS",
      "OBJECT_MORI_TEX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "Lowers when a clear flag and the switch flag are set"
  },
  "ACTOR_BG_MORI_ELEVATOR": {
    "name": "Forest Temple Elevator",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_OBJECTS",
      "OBJECT_MORI_TEX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_MORI_KAITENKABE": {
    "name": "Forest Temple Rotating Walls",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_OBJECTS",
      "OBJECT_MORI_TEX"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MORI_RAKKATENJO": {
    "name": "Forest Temple Ceiling",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_OBJECTS",
      "OBJECT_MORI_TEX"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_VM": {
    "name": "Beamos",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_VM"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Attacking Beamos (2 health)"
          },
          {
            "value": "1",
            "name": "Attacking Beamos (1 health)"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Sight Distance (increments of 40.0f)",
        "mask": 65280
      }
    ],
    "notes": "XX00: 05 in dodongo's cavern, 08 in ganon's castle entrance"
  },
  "ACTOR_DEMO_EFFECT": {
    "name": "Cutscene Objects",
    "category": "ACTORCAT_BG",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Crystal Light, used by Triforce in Hyrule Creation CS"
          },
          {
            "value": "1",
            "name": "Flame, used by Din in Hyrule Creation CS"
          },
          {
            "value": "2",
            "name": "Blue Orb, used by Din in Hyrule Creation CS"
          },
          {
            "value": "3",
            "name": "Large Green Cone, used by Farore in Hyrule Creation CS"
          },
          {
            "value": "4",
            "name": "Din"
          },
          {
            "value": "5",
            "name": "Nayru"
          },
          {
            "value": "6",
            "name": "Farore"
          },
          {
            "value": "7",
            "name": "Blue Light Ring, used by Din and Farore in Hyrule Creation CS"
          },
          {
            "value": "8",
            "name": "Triforce"
          },
          {
            "value": "9",
            "name": "Fire Medallion"
          },
          {
            "value": "10",
            "name": "Water Medallion"
          },
          {
            "value": "11",
            "name": "Forest Medallion"
          },
          {
            "value": "12",
            "name": "Spirit Medallion"
          },
          {
            "value": "13",
            "name": "Shadow Medallion"
          },
          {
            "value": "14",
            "name": "Light Medallion"
          },
          {
            "value": "15",
            "name": "Time Warp Effect from Temple of Time"
          },
          {
            "value": "16",
            "name": "Blue Light Ring that shrinks, used by Din in Hyrule Creation CS"
          },
          {
            "value": "17",
            "name": "Vertical Light Effect used by Triforce"
          },
          {
            "value": "18",
            "name": "Light Ball from the 6 Sages when sealing Ganondorf"
          },
          {
            "value": "19",
            "name": "Kokiri Emerald"
          },
          {
            "value": "20",
            "name": "Goron Ruby"
          },
          {
            "value": "21",
            "name": "Zora Sapphire"
          },
          {
            "value": "22",
            "name": "Dust, used in Light Arrow CS"
          },
          {
            "value": "23",
            "name": "Light Ball, used by Zelda"
          },
          {
            "value": "24",
            "name": "Large Block of Time Time Warp Effect"
          },
          {
            "value": "25",
            "name": "Small Block of Time Time Warp Effect"
          }
        ]
      },
      {
        "typeFilter": [
          18
        ],
        "target": "params",
        "shift": 0,
        "type": "enum",
        "name": "Light Color",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Red Light"
          },
          {
            "value": "1",
            "name": "Blue Light"
          },
          {
            "value": "2",
            "name": "Green Light"
          },
          {
            "value": "3",
            "name": "Orange Light"
          },
          {
            "value": "4",
            "name": "Yellow Light"
          },
          {
            "value": "5",
            "name": "Purple Light"
          },
          {
            "value": "6",
            "name": "Green Light 2"
          }
        ]
      },
      {
        "typeFilter": [
          18
        ],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Light Large",
        "mask": 3840
      }
    ],
    "notes": ""
  },
  "ACTOR_DEMO_KANKYO": {
    "name": "Cutscene Environment Effects/Objects",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_TOKI_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Blue Rain Effect, used by Goddesses/Triforce CS"
          },
          {
            "value": "1",
            "name": "Blue Rain Effect, used in the Master Sword CS"
          },
          {
            "value": "2",
            "name": "Giant Rock 1"
          },
          {
            "value": "3",
            "name": "Giant Rock 2"
          },
          {
            "value": "4",
            "name": "Giant Rock 3"
          },
          {
            "value": "5",
            "name": "Giant Rock 4"
          },
          {
            "value": "6",
            "name": "Giant Rock 5"
          },
          {
            "value": "7",
            "name": "Fluffy Clouds"
          },
          {
            "value": "8",
            "name": "Bolero 3D Note (not implemented)"
          },
          {
            "value": "9",
            "name": "Serenade 3D Note (not implemented)"
          },
          {
            "value": "10",
            "name": "Requiem 3D Note (not implemented)"
          },
          {
            "value": "11",
            "name": "? 3D Note (not implemented)"
          },
          {
            "value": "12",
            "name": "? 3D Note (not implemented)"
          },
          {
            "value": "13",
            "name": "Door of Time"
          },
          {
            "value": "14",
            "name": "Yellow Light, used in Master Sword's chamber"
          },
          {
            "value": "15",
            "name": "Warp Song leaving effect"
          },
          {
            "value": "16",
            "name": "Warp Song arriving effect"
          },
          {
            "value": "17",
            "name": "Orange Sparkly Effect, used by appearing chests"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_FWBIG": {
    "name": "Large Firewall",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Stationary with switch and hardcoded camera cutscene"
          },
          {
            "value": "256",
            "name": "Follows you"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_FLOORMAS": {
    "name": "Floormaster",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_WALLMASTER"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Invisible",
        "mask": 32768
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_HEISHI1": {
    "name": "Castle Courtyard Guards",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_SD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "0",
            "name": "Slow Patrolling Guard"
          },
          {
            "value": "256",
            "name": "Fast Patrolling Guard"
          },
          {
            "value": "512",
            "name": "Slow Patrolling Guard, can skip some waypoints"
          },
          {
            "value": "768",
            "name": "Fast Patrolling Guard, can skip some waypoints"
          },
          {
            "value": "1280",
            "name": "Nighttime Standing Guard"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Path ID",
        "mask": 255
      }
    ],
    "notes": "Rupees circle spawned if Path ID == 3"
  },
  "ACTOR_EN_RD": {
    "name": "Redead/Gibdo",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_RD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "253",
            "name": "Rising Gibdo"
          },
          {
            "value": "254",
            "name": "Gibdo"
          },
          {
            "value": "255",
            "name": "Redead, doesn't mourn"
          },
          {
            "value": "0",
            "name": "Redead, doesn't mourn if walking"
          },
          {
            "value": "1",
            "name": "Redead"
          },
          {
            "value": "2",
            "name": "Crying Redead"
          },
          {
            "value": "3",
            "name": "Invisible Redead"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 32512
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Can't Freeze Player",
        "mask": 32768
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_PO_SISTERS": {
    "name": "Poe Sisters",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_PO_SISTERS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 768,
        "values": [
          {
            "value": "0",
            "name": "Meg, Purple Poe"
          },
          {
            "value": "256",
            "name": "Joelle, Red Poe"
          },
          {
            "value": "512",
            "name": "Beth, Blue Poe"
          },
          {
            "value": "768",
            "name": "Amy, Green Poe"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 10,
        "type": "number",
        "name": "Fake Megs Y-Rot (+0x4000; used by game engine)",
        "mask": 3072
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Triggers CS (used by game engine)",
        "mask": 4096
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HEAVY_BLOCK": {
    "name": "Golden Gauntlets Rock",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HEAVY_OBJECT"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Sinks into the ground"
          },
          {
            "value": "1",
            "name": "Breaks on impact"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_BG_PO_EVENT": {
    "name": "Poe Puzzle Block",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_PO_SISTERS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "0",
            "name": "??? Amy"
          },
          {
            "value": "256",
            "name": "??? Amy"
          },
          {
            "value": "512",
            "name": "Joelle Painting Puzzle"
          },
          {
            "value": "768",
            "name": "Beth Painting Puzzle"
          },
          {
            "value": "1024",
            "name": "??? Amy"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag"
  },
  "ACTOR_OBJ_MURE": {
    "name": "Fish, Bugs, or Butterflies",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 31,
        "values": [
          {
            "value": "2",
            "name": "Fish"
          },
          {
            "value": "3",
            "name": "Bug"
          },
          {
            "value": "4",
            "name": "Butterfly"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 12,
        "type": "number",
        "name": "Spawn Count",
        "mask": 61440
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Spawn Count",
        "mask": 1792
      }
    ],
    "notes": "0xF000 -> 0 to 15\n+0x0700 Spawn Count (0 or 8 or 9 or 12)\n+0000 12 spawns\n+0100 9 spawns\n+0200 8 spawns\n+1000 1 spawn"
  },
  "ACTOR_EN_SW": {
    "name": "Skullwalltula/Golden Skulltula",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ST"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 57344,
        "values": [
          {
            "value": "0",
            "name": "Skullwalltula"
          },
          {
            "value": "32768",
            "name": "Gold Skulltula"
          }
        ]
      },
      {
        "typeFilter": [
          32768
        ],
        "target": "params",
        "shift": 0,
        "type": "enum",
        "name": "Location",
        "mask": 7936,
        "values": [
          {
            "value": "1",
            "name": "Deku Tree"
          },
          {
            "value": "2",
            "name": "Dodongo's Cavern"
          },
          {
            "value": "3",
            "name": "Jabu-Jabu's Belly"
          },
          {
            "value": "4",
            "name": "Forest Temple"
          },
          {
            "value": "5",
            "name": "Fire Temple"
          },
          {
            "value": "6",
            "name": "Water Temple"
          },
          {
            "value": "7",
            "name": "Spirit Temple"
          },
          {
            "value": "8",
            "name": "Shadow Temple"
          },
          {
            "value": "9",
            "name": "Bottom of the Well"
          },
          {
            "value": "10",
            "name": "Ice Cavern"
          },
          {
            "value": "11",
            "name": "Hyrule Field"
          },
          {
            "value": "12",
            "name": "Lon-Lon Ranch"
          },
          {
            "value": "13",
            "name": "Kokiri Forest"
          },
          {
            "value": "14",
            "name": "Lost Woods/SFM"
          },
          {
            "value": "15",
            "name": "Castle Town/Ganon's Castle"
          },
          {
            "value": "16",
            "name": "DMT/Goron City"
          },
          {
            "value": "17",
            "name": "Kakariko Village"
          },
          {
            "value": "18",
            "name": "Zora's Fountain/River"
          },
          {
            "value": "19",
            "name": "Lake Hylia"
          },
          {
            "value": "20",
            "name": "Gerudo Valley"
          },
          {
            "value": "21",
            "name": "Gerudo Fortress"
          },
          {
            "value": "22",
            "name": "Desert Colossus/Wasteland"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Skulltula Flag",
        "mask": 255
      },
      {
        "typeFilter": [
          32768
        ],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Spawns at night",
        "mask": 8192
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_FD": {
    "name": "Flying Volvagia",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_FD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJECT_KANKYO": {
    "name": "Environmental effects",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Flying Fairies, Dustmotes"
          },
          {
            "value": "2",
            "name": "Lightning"
          },
          {
            "value": "3",
            "name": "Snow"
          },
          {
            "value": "4",
            "name": "Electric Spark Effect, object 0x0A1"
          },
          {
            "value": "5",
            "name": "Ganon's Castle Barrier Colored Light Beams, object 0x179"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DU": {
    "name": "Darunia",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FD": {
    "name": "Fire Dancer",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_FW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HORSE_LINK_CHILD": {
    "name": "Child Epona",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HORSE_LINK_CHILD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DOOR_ANA": {
    "name": "Hole in Ground Exit",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 768,
        "values": [
          {
            "value": "0",
            "name": "Visible"
          },
          {
            "value": "256",
            "name": "Appears on Song of Storms"
          },
          {
            "value": "512",
            "name": "Appears on explosion or Megaton Hammer"
          }
        ]
      },
      {
        "typeFilter": [
          0,
          256,
          512
        ],
        "target": "zrot",
        "shift": 0,
        "type": "enum",
        "name": "Destination",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Generic Grotto"
          },
          {
            "value": "1",
            "name": "Big Skulltula"
          },
          {
            "value": "2",
            "name": "Heart Piece Scrub"
          },
          {
            "value": "3",
            "name": "Two Redeads"
          },
          {
            "value": "4",
            "name": "Three Deku Salescrubs"
          },
          {
            "value": "5",
            "name": "Webbed"
          },
          {
            "value": "6",
            "name": "Octorock"
          },
          {
            "value": "7",
            "name": "Two Deku Salescrubs (Deku Nut Upgrade)"
          },
          {
            "value": "8",
            "name": "Two Wolfos"
          },
          {
            "value": "9",
            "name": "Bombable Walls"
          },
          {
            "value": "10",
            "name": "Two Deku Salescrubs (Green Potion)"
          },
          {
            "value": "11",
            "name": "Tektite"
          },
          {
            "value": "12",
            "name": "Forest Stage"
          },
          {
            "value": "13",
            "name": "Cow"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 5,
        "type": "chest",
        "name": "Content",
        "mask": 224
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Chest",
        "mask": 31
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Small Fairy Fountain",
        "mask": 61440
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT02_OBJECTS": {
    "name": "Graveyard Objects",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT02_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Shadow Temple's Eye of Truth Door"
          },
          {
            "value": "1",
            "name": "Small Square Patch of Ground, child only, blocks entrance to Dampe's Grave"
          },
          {
            "value": "2",
            "name": "Royal Tomb Grave, despawn if the Royal Tomb CS is watched and the scene is the graveyard"
          },
          {
            "value": "3",
            "name": "Thunderbolt used with the electric spark effect"
          },
          {
            "value": "4",
            "name": "Light Aura, appears when Royal Tomb Grave explodes"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Switch Flag"
  },
  "ACTOR_BG_HAKA": {
    "name": "Gravestone",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HAKA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Default"
          },
          {
            "value": "1",
            "name": "Plays discovery sfx when pulled back"
          },
          {
            "value": "2",
            "name": "Typical"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_MAGIC_WIND": {
    "name": "Farore's Wind",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal (no sphere)"
          },
          {
            "value": "1",
            "name": "Dissipating (no sphere)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_MAGIC_FIRE": {
    "name": "Din's Fire",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RU1": {
    "name": "Young Ruto",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_RU1"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Blue Warp and Ruto"
          },
          {
            "value": "1",
            "name": "Leaning Ruto"
          },
          {
            "value": "2",
            "name": "Ruto, First Encounter"
          },
          {
            "value": "3",
            "name": "Ruto, after falling down the hole in Jabu-Jabu"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_FD2": {
    "name": "Hole Volvagia",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_FD2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FD_FIRE": {
    "name": "Flare Dancer Fire Attack",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Flies, lands, dies in a bit"
          },
          {
            "value": "32768",
            "name": "Flies, lands, creeps towards Link, dies in a bit"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DH": {
    "name": "Deadhand",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_DH"
    ],
    "params": [],
    "notes": "does not appear unless his hands exist"
  },
  "ACTOR_EN_DHA": {
    "name": "Deadhand's Arms",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_DH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RL": {
    "name": "Rauru",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_RL"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Light Medallion and post-Spirit/Shadow Medallions"
          },
          {
            "value": "2",
            "name": "Ganon's Castle and post-Ganon"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ENCOUNT1": {
    "name": "Enemy Spawner",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 63488,
        "values": [
          {
            "value": "0",
            "name": "Leever"
          },
          {
            "value": "2048",
            "name": "Red Tektite"
          },
          {
            "value": "4096",
            "name": "Stalchildren"
          },
          {
            "value": "6144",
            "name": "Wolfos"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "number",
        "name": "# of Simultaneaous Spawns",
        "mask": 1984
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "# of Rounds",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Spawn Range",
        "mask": 65535
      }
    ],
    "notes": "+0xFF = Enemies to Spawn\n-0088 When spawning Leevers, 88 is used in Haunted Wasteland\n-00A4 When spawning Stalchildren, loads two at a time. Used in Hyrule Field\n-00F2 When spawning Leevers, F2 is used in Desert Colossus"
  },
  "ACTOR_DEMO_DU": {
    "name": "Sage Darunia (Cutscene)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DU"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Fire Medallion (Default)"
          },
          {
            "value": "1",
            "name": "Goron Ruby"
          },
          {
            "value": "2",
            "name": "Chamber After Ganon"
          },
          {
            "value": "3",
            "name": "Credits"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_DEMO_IM": {
    "name": "Impa",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_IM"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Shadow Medallion"
          },
          {
            "value": "2",
            "name": "Chamber After Ganon/Ganon's Castle"
          },
          {
            "value": "3",
            "name": "Escort"
          },
          {
            "value": "4",
            "name": "Hyrule Field after ZL"
          },
          {
            "value": "5",
            "name": "First Time Escort"
          },
          {
            "value": "6",
            "name": "Credits"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_DEMO_TRE_LGT": {
    "name": "Treasure Chest Light",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_BOX"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FW": {
    "name": "Core Fire Dancer Creature",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_FW"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "No damage"
          },
          {
            "value": "1",
            "name": "One-Hit Kill"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_VB_SIMA": {
    "name": "Volvagia Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_FD"
    ],
    "params": [],
    "notes": "Spawned by volvagia"
  },
  "ACTOR_EN_VB_BALL": {
    "name": "Volvagia Rocks",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_FD"
    ],
    "params": [],
    "notes": "Spawned by volvagia"
  },
  "ACTOR_BG_HAKA_MEGANE": {
    "name": "Fake/Invisible Objects",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_HAKA_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "3",
            "name": "Rock Wall with Skull"
          },
          {
            "value": "4",
            "name": "Black square with large skull face"
          },
          {
            "value": "5",
            "name": "Shadow Temple Boss Room platforms"
          },
          {
            "value": "6",
            "name": "Wall of Skulls"
          },
          {
            "value": "7",
            "name": "Shadow Temple Floor"
          },
          {
            "value": "8",
            "name": "Massive Platform"
          },
          {
            "value": "9",
            "name": "Wall with bluish?, fat bricks texture (one sided)"
          },
          {
            "value": "10",
            "name": "Shadow Temple Diamond Room (before big key) Fake Walls"
          },
          {
            "value": "11",
            "name": "Wall with purplish?, fat brick texture (both side)"
          },
          {
            "value": "12",
            "name": "Room 11's invisible spikes, invisible hookshot point."
          }
        ]
      }
    ],
    "notes": "Need Object 0x69"
  },
  "ACTOR_BG_HAKA_MEGANEBG": {
    "name": "Invisible Objects/Metal Gate",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HAKA_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Moving stone platforms"
          },
          {
            "value": "1",
            "name": "Rising and falling stone platforms"
          },
          {
            "value": "2",
            "name": "Spinning black platform"
          },
          {
            "value": "3",
            "name": "Metal grate"
          },
          {
            "value": "4",
            "name": "Same as 0001, but graphics are glitched"
          }
        ]
      },
      {
        "typeFilter": [
          3
        ],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Switch Flag //Type 03 Only"
  },
  "ACTOR_BG_HAKA_SHIP": {
    "name": "Shadow Temple Ship",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HAKA_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag for Zelda's Lullaby spot\n      Camera cutscene points to X 4738 Y -1395 Z -2006"
  },
  "ACTOR_BG_HAKA_SGAMI": {
    "name": "Spinning Scythe Statues",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Shadow Temple Scythes (Visible)"
          },
          {
            "value": "1",
            "name": "Shadow Temple Scythes (Invisible)"
          },
          {
            "value": "256",
            "name": "Ice Cavern Spinning Blade"
          }
        ]
      }
    ],
    "notes": "May require additional object"
  },
  "ACTOR_EN_HEISHI2": {
    "name": "Gate Guards",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_SD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "2",
            "name": "Hyrule Castle guard"
          },
          {
            "value": "5",
            "name": "Death Mountain gate guard"
          },
          {
            "value": "6",
            "name": "Ceremonial guard"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ENCOUNT2": {
    "name": "Falling Rock Spawner",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_EFC_STAR_FIELD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FIRE_ROCK": {
    "name": "Flying Rubble",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_EFC_STAR_FIELD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Large"
          },
          {
            "value": "1",
            "name": "Medium"
          },
          {
            "value": "2",
            "name": "Small"
          },
          {
            "value": "3",
            "name": "Large"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_BROB": {
    "name": "Flobbery Muscle Block",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BROB"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Height Modifier",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Smaller Size",
        "mask": 4096
      }
    ],
    "notes": ""
  },
  "ACTOR_MIR_RAY": {
    "name": "Sun Block Switch (Hardcoded Positions)",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_MIR_RAY"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "1",
            "name": "Spirit Temple's Sun-Block Room"
          },
          {
            "value": "2",
            "name": "Spirit Temple's Single Cobra-Mirror Room"
          },
          {
            "value": "3",
            "name": "Spirit Temple's Four Armos Room"
          },
          {
            "value": "4",
            "name": "Spirit Temple's Topmost Room"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT09_OBJ": {
    "name": "Gerudo Valley Objects",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT09_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Bridge Sides"
          },
          {
            "value": "1",
            "name": "Broken Bridge"
          },
          {
            "value": "2",
            "name": "Bridge as Child"
          },
          {
            "value": "3",
            "name": "Tent"
          },
          {
            "value": "4",
            "name": "Repaired Bridge"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT18_OBJ": {
    "name": "Statue from Darunia's Room",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT18_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Statue, no spear, movable with Adult Link"
          },
          {
            "value": "1",
            "name": "Spear only, climbable"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_VA": {
    "name": "Barinade",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_BV"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Barinade"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HAKA_TUBO": {
    "name": "Open Topped Skull Statue",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HAKA_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Collectible",
        "mask": 63
      }
    ],
    "notes": "Drops a small key if the room isn't 12, if room is 12 it'll act like the 3 spinning pot room before Bongo-Bongo"
  },
  "ACTOR_BG_HAKA_TRAP": {
    "name": "Shadow temple objects",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HAKA_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Guillotine Blade (Slow)"
          },
          {
            "value": "1",
            "name": "Spiked Box on Chain"
          },
          {
            "value": "2",
            "name": "Spiked Wooden Wall, moving"
          },
          {
            "value": "3",
            "name": "Opposite Spiked Wooden Wall, moving"
          },
          {
            "value": "4",
            "name": "Propeller, blows wind"
          },
          {
            "value": "5",
            "name": "Guillotine Blade (Fast)"
          },
          {
            "value": "6",
            "name": "+ Graphical glitches, same as 2 or 3 (unsure)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HAKA_HUTA": {
    "name": "Coffin Lid",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HAKACH_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "256",
            "name": "Spawns gibdo"
          },
          {
            "value": "512",
            "name": "Spawns 2 keese"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag"
  },
  "ACTOR_BG_HAKA_ZOU": {
    "name": "Bird Statue/Bombables",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_HAKA_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Giant Bird Statue, bombing it will make it fall"
          },
          {
            "value": "1",
            "name": "Bombable Wall of Skulls"
          },
          {
            "value": "2",
            "name": "Bombable Rubble (Object 0x8D)"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_BG_SPOT17_FUNEN": {
    "name": "Crater Smoke Cone",
    "category": "ACTORCAT_SWITCH",
    "objDeps": [
      "OBJECT_SPOT17_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_ITM": {
    "name": "Shooting Gallery Game",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_MAN": {
    "name": "Shooting Gallery Man",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OSSAN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TANA": {
    "name": "Shop Shelves",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SHOP_DUNGEN"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Wooden (Default)"
          },
          {
            "value": "1",
            "name": "Stone (Zora)"
          },
          {
            "value": "2",
            "name": "Granite (Goron)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_NB": {
    "name": "Nabooru",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_NB"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "2",
            "name": "Spirit Medallion"
          },
          {
            "value": "2",
            "name": "Chamber After Ganon/Ganon's Castle"
          },
          {
            "value": "3",
            "name": "Kidnapping CS"
          },
          {
            "value": "4",
            "name": "Iron Knuckle"
          },
          {
            "value": "5",
            "name": "Credits"
          },
          {
            "value": "6",
            "name": "Crawlspace"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_MO": {
    "name": "Morpha",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_MO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SB": {
    "name": "Shell Blade",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_SB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BIGOKUTA": {
    "name": "Big Octorok",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BIGOKUTA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Sits there, mini-boss music starts, slice it to make it move (no teleport), green target"
          },
          {
            "value": "1",
            "name": "Already running around, yellow target this time, no music"
          }
        ]
      }
    ],
    "notes": "Slicing it in the back will make it jump to the y location of its room, which is typically below most floors"
  },
  "ACTOR_EN_KAREBABA": {
    "name": "Withered Deku Baba",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_DEKUBABA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal Deku Baba"
          },
          {
            "value": "1",
            "name": "Straight Deku Baba"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_BDAN_OBJECTS": {
    "name": "Jabu-Jabu Objects",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_BDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Giant Octo's Platform"
          },
          {
            "value": "1",
            "name": "Elevator Platform"
          },
          {
            "value": "2",
            "name": "Water Square"
          },
          {
            "value": "3",
            "name": "Lowering Platform"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Nullable Switch Flag"
  },
  "ACTOR_DEMO_SA": {
    "name": "Sage Saria (Cutscene)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_SA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Chamber of Sages, gives Forest Medallion"
          },
          {
            "value": "2",
            "name": "Chamber After Ganon/Ganon's Castle"
          },
          {
            "value": "4",
            "name": "Credits, Death Mountain Trail"
          },
          {
            "value": "5",
            "name": "Fairy Ocarina Cutscene"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_DEMO_GO": {
    "name": "Gorons (Cutscene)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Unknown"
          },
          {
            "value": "1",
            "name": "Unknown"
          },
          {
            "value": "2",
            "name": "Unknown"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_IN": {
    "name": "Ingo",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_IN"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "1",
            "name": "Unknown"
          },
          {
            "value": "2",
            "name": "Unknown"
          },
          {
            "value": "4",
            "name": "Unknown"
          },
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TR": {
    "name": "Koume and Kotake",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_TR"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Koume"
          },
          {
            "value": "1",
            "name": "Kotake"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT16_BOMBSTONE": {
    "name": "Giant Boulder Blocking Dodongo's Cavern",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT16_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "255",
            "name": "Default"
          },
          {
            "value": "1",
            "name": "Debris"
          },
          {
            "value": "2",
            "name": "Debris"
          },
          {
            "value": "3",
            "name": "Debris"
          },
          {
            "value": "4",
            "name": "Debris"
          },
          {
            "value": "5",
            "name": "Debris"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HIDAN_KOWARERUKABE": {
    "name": "Cracked Stone Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_HIDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Cracked stone floor"
          },
          {
            "value": "1",
            "name": "Bombable stone wall"
          },
          {
            "value": "2",
            "name": "Large bombable stone wall"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_BG_BOMBWALL": {
    "name": "2D Bombable Wall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Play Discovery Sound",
        "mask": 32768
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT08_ICEBLOCK": {
    "name": "Ice Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT08_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 512,
        "values": [
          {
            "value": "0",
            "name": "Iceberg"
          },
          {
            "value": "512",
            "name": "Ice Ramp"
          }
        ]
      },
      {
        "typeFilter": [
          0
        ],
        "target": "params",
        "shift": 0,
        "type": "enum",
        "name": "Action",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Floating"
          },
          {
            "value": "1",
            "name": "Floating (?)"
          },
          {
            "value": "2",
            "name": "Floating, rotating"
          },
          {
            "value": "3",
            "name": "Orbiting Twins"
          },
          {
            "value": "4",
            "name": "Static"
          }
        ]
      },
      {
        "typeFilter": [
          128
        ],
        "target": "params",
        "shift": 0,
        "type": "enum",
        "name": "Size",
        "mask": 240,
        "values": [
          {
            "value": "0",
            "name": "Large"
          },
          {
            "value": "1",
            "name": "Medium"
          },
          {
            "value": "2",
            "name": "Small"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_RU2": {
    "name": "Adult Ruto",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_RU2"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Water Medallion"
          },
          {
            "value": "2",
            "name": "Chamber After Ganon/Ganon's Castle"
          },
          {
            "value": "3",
            "name": "Credits"
          },
          {
            "value": "4",
            "name": "Water Temple Meeting"
          }
        ]
      },
      {
        "typeFilter": [
          4
        ],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_DEKUJR": {
    "name": "Deku Tree Sprout",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DEKUJR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MIZU_UZU": {
    "name": "Water Noise",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_MIZU_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOT06_OBJECTS": {
    "name": "Lake Hylia Objects",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT06_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "0",
            "name": "Temple Gate"
          },
          {
            "value": "256",
            "name": "Gate lock"
          },
          {
            "value": "512",
            "name": "Water plane"
          },
          {
            "value": "768",
            "name": "Ice Block blocking Zora's Domain Entrance (Adult only)"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": "+0xFF = Nullable Switch Flag"
  },
  "ACTOR_BG_ICE_OBJECTS": {
    "name": "Moveable Ice Block",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_ICE_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKA_WATER": {
    "name": "Bottom of the well water level changer",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_HAKACH_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag //If set, water level is lowered"
  },
  "ACTOR_EN_MA2": {
    "name": "Adult Malon",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MA2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOM_CHU": {
    "name": "Bombchu",
    "category": "ACTORCAT_EXPLOSIVE",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HORSE_GAME_CHECK": {
    "name": "Horseback Games",
    "category": "ACTORCAT_BG",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "1",
            "name": "Ingo Race"
          },
          {
            "value": "2",
            "name": "Gerudo Archery"
          },
          {
            "value": "3",
            "name": "Unused?"
          },
          {
            "value": "4",
            "name": "Malon Race"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_TW": {
    "name": "Twinrova",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_TW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RR": {
    "name": "Like-Like",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_RR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BA": {
    "name": "Tentacle from Inside Lord Jabu-Jabu",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BXA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Reddish brown"
          },
          {
            "value": "1",
            "name": "Green"
          },
          {
            "value": "2",
            "name": "Grayish blue with some red"
          },
          {
            "value": "3",
            "name": "Already dead"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_BX": {
    "name": "Blocking Tentacle from Jabu-Jabu",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_BXA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Reddish brown"
          },
          {
            "value": "1",
            "name": "Green"
          },
          {
            "value": "2",
            "name": "Grayish blue with some red"
          },
          {
            "value": "3",
            "name": "Corrupt textures, still visible and works"
          },
          {
            "value": "4",
            "name": "Dark brownish"
          },
          {
            "value": "5",
            "name": "Blackish gray"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ANUBICE": {
    "name": "Anubis Body",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_ANUBICE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ANUBICE_FIRE": {
    "name": "Anubis Fire Attack",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_ANUBICE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MORI_HASHIGO": {
    "name": "Ladder with Clasp (Unused)",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_OBJECTS",
      "OBJECT_MORI_TEX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_MORI_HASHIRA4": {
    "name": "Gates and Rotating Pillars",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_OBJECTS",
      "OBJECT_MORI_TEX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Rotating platforms"
          },
          {
            "value": "1",
            "name": "Metal Gate"
          }
        ]
      },
      {
        "typeFilter": [
          1
        ],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_MORI_IDOMIZU": {
    "name": "Forest Temple Well Water",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MORI_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT16_DOUGHNUT": {
    "name": "Death Mountain Cloud Ring",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_EFC_DOUGHNUT"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Gray"
          },
          {
            "value": "1",
            "name": "Expanding, gray"
          },
          {
            "value": "2",
            "name": "Expanding, gray"
          },
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_BDAN_SWITCH": {
    "name": "Switch from Inside Lord Jabu-Jabu",
    "category": "ACTORCAT_SWITCH",
    "objDeps": [
      "OBJECT_BDAN_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Floor Blue Switch, release when not stood on"
          },
          {
            "value": "1",
            "name": "Floor Heavy Switch (Need Ruto to press it)"
          },
          {
            "value": "2",
            "name": "Standard Floor Yellow Switch"
          },
          {
            "value": "3",
            "name": "Standard Tall Yellow Switch"
          },
          {
            "value": "4",
            "name": "On/Off Toggle Tall Yellow Switch"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_EN_MA1": {
    "name": "Young Malon",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MA1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_GANON": {
    "name": "Boss Ganondorf",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_GANON",
      "OBJECT_GANON_ANIME1",
      "OBJECT_GANON_ANIME2"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          },
          {
            "value": "0",
            "name": "Begins battle"
          },
          {
            "value": "1",
            "name": "Ganondorf death sequence"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_SST": {
    "name": "Bongo Bongo",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_SST"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_NY": {
    "name": "Spike Enemy",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_NY"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal"
          },
          {
            "value": "1",
            "name": "Does not open"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_FR": {
    "name": "Frogs and Ocarina Spot",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_FR"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Frog Game Ocarina Spot"
          },
          {
            "value": "1",
            "name": "Yellow Frog"
          },
          {
            "value": "2",
            "name": "Blue Frog"
          },
          {
            "value": "3",
            "name": "Red Frog"
          },
          {
            "value": "4",
            "name": "Purple Frog"
          },
          {
            "value": "5",
            "name": "White Frog"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_ITEM_SHIELD": {
    "name": "Deku Shield",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_LINK_CHILD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Collectible Deku Shield"
          },
          {
            "value": "1",
            "name": "Burning Deku shield"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_ICE_SHELTER": {
    "name": "Large Red Ice",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_ICE_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "0",
            "name": "Large crystal"
          },
          {
            "value": "256",
            "name": "Smaller crystal"
          },
          {
            "value": "512",
            "name": "Crystal platform"
          },
          {
            "value": "768",
            "name": "Meltable ice sheet"
          },
          {
            "value": "1024",
            "name": "Giant crystal"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": "0xFF = Nullable Switch Flag"
  },
  "ACTOR_EN_ICE_HONO": {
    "name": "Blue Flame",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Group of small blue flames, disappear"
          },
          {
            "value": "1",
            "name": "Blue flame, disappears"
          },
          {
            "value": "65535",
            "name": "Blue flame, targetable"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_ITEM_OCARINA": {
    "name": "Ocarina of Time",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_GI_OCARINA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Ocarina of Time being tossed higher in air"
          },
          {
            "value": "1",
            "name": "Ocarina of Time being tossed in air"
          },
          {
            "value": "2",
            "name": "Ocarina of Time"
          },
          {
            "value": "3",
            "name": "Collectible Ocarina of Time"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_MAGIC_DARK": {
    "name": "Nayru's Love",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal"
          },
          {
            "value": "1",
            "name": "No bright sphere, first one dies off quickly"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_DEMO_6K": {
    "name": "Cutscene Effects",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Rain of multi-colored light balls, used in Rainbow Bridge CS"
          },
          {
            "value": "1",
            "name": "Lots of small particles, used with the White 'Black Hole'"
          },
          {
            "value": "2",
            "name": "White 'Black Hole' Effect, used in the Ganondorf's Sealing CS"
          },
          {
            "value": "3",
            "name": "Red Light Ball, used in Ganon's Castle"
          },
          {
            "value": "4",
            "name": "Green Light Ball, used in Ganon's Castle"
          },
          {
            "value": "5",
            "name": "Yellow Light Ball, used in Ganon's Castle"
          },
          {
            "value": "6",
            "name": "Purple Light Ball, used in Ganon's Castle"
          },
          {
            "value": "7",
            "name": "Orange Light Ball, used in Ganon's Castle"
          },
          {
            "value": "8",
            "name": "Blue Light Ball, used in Ganon's Castle"
          },
          {
            "value": "9",
            "name": "Koume/Kotake Red Light Ball Attack"
          },
          {
            "value": "10",
            "name": "Koume/Kotake Blue Light Ball Attack"
          },
          {
            "value": "11",
            "name": "'Nabooru Disappearing' Orange Particles"
          },
          {
            "value": "12",
            "name": "Ganondorf's Light Ball Attack Purple Loading Effect"
          },
          {
            "value": "13",
            "name": "Ganondorf's Light Ball Attack, used in Zelda Fleeing CS"
          },
          {
            "value": "14",
            "name": "Red Light Ball with particles, used in the LLR/DMT part of the credits"
          },
          {
            "value": "15",
            "name": "Green Light Ball with particles, used in the LLR/DMT part of the credits"
          },
          {
            "value": "16",
            "name": "Yellow Light Ball with particles, used in ToT Cutscenes"
          },
          {
            "value": "17",
            "name": "Purple Light Ball with particles, used in the LLR/DMT part of the credits"
          },
          {
            "value": "18",
            "name": "Orange Light Ball with particles, used in the LLR/DMT part of the credits"
          },
          {
            "value": "19",
            "name": "Blue Light Ball with particles, used in the LLR/DMT part of the credits"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ANUBICE_TAG": {
    "name": "Anubis",
    "category": "ACTORCAT_SWITCH",
    "objDeps": [
      "OBJECT_ANUBICE"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Trigger Range (increments of 40)",
        "mask": 65535
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_HAKA_GATE": {
    "name": "Shadow Temple Truth Spinner",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_HAKA_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Rotatable Bird Statue"
          },
          {
            "value": "1",
            "name": "Circular Trap Door Platform"
          },
          {
            "value": "2",
            "name": "Gate"
          },
          {
            "value": "3",
            "name": "Skull Top"
          },
          {
            "value": "4",
            "name": "Glitchy graphics, can't see object, behaves like the gate"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_BG_SPOT15_SAKU": {
    "name": "Hyrule Castle Gate",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_SPOT15_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": "Not market town gate"
  },
  "ACTOR_BG_JYA_GOROIWA": {
    "name": "Giant Rolling Boulder",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GOROIWA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Normal"
          },
          {
            "value": "3072",
            "name": "Follow patch faster and repeat it"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_JYA_ZURERUKABE": {
    "name": "Moving Brick Wall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_JYA_COBRA": {
    "name": "Rotating Cobra Mirror",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Large shadow"
          },
          {
            "value": "1",
            "name": "Small Shadow"
          },
          {
            "value": "2",
            "name": "No shadow"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "The sun switch has to be between 252 and 308 units from the cobra mirror to be activated\n+0x3F = Switch Flag"
  },
  "ACTOR_BG_JYA_KANAAMI": {
    "name": "Climbable Mesh Wall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_FISHING": {
    "name": "Fishing Game, Fish, and Fisherman",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_FISH"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Wearing red vest over blue shirt, no hat"
          },
          {
            "value": "100",
            "name": "Normal fish"
          },
          {
            "value": "115",
            "name": "Hylian Loach"
          },
          {
            "value": "256",
            "name": "Huge Hylian Loach"
          },
          {
            "value": "32768",
            "name": "Pond stuff, still no hat"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_OSHIHIKI": {
    "name": "Push Block",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Small Push Block"
          },
          {
            "value": "1",
            "name": "Medium Push Block"
          },
          {
            "value": "2",
            "name": "Large Push Block"
          },
          {
            "value": "3",
            "name": "Huge Push Block"
          }
        ]
      },
      {
        "typeFilter": [
          0,
          1,
          2,
          3
        ],
        "target": "params",
        "shift": 0,
        "type": "enum",
        "name": "Color (Scene Based)",
        "mask": 192,
        "values": [
          {
            "value": "0",
            "name": "Deku Tree"
          },
          {
            "value": "1",
            "name": "Dodongo's Cavern"
          },
          {
            "value": "2",
            "name": "Forest Temple"
          },
          {
            "value": "3",
            "name": "Fire Temple"
          },
          {
            "value": "4",
            "name": "Water Temple"
          },
          {
            "value": "5",
            "name": "Spirit Temple"
          },
          {
            "value": "6",
            "name": "Shadow Temple"
          },
          {
            "value": "7",
            "name": "Ganon's Castle"
          },
          {
            "value": "8",
            "name": "Gerudo Training Grounds"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Puzzle Solution",
        "mask": 4
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_GATE_SHUTTER": {
    "name": "Death Mountain Trail Gate",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_SPOT01_MATOYAB"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EFF_DUST": {
    "name": "Dust Motes Gathering Together",
    "category": "ACTORCAT_NPC",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Rushing white particles"
          },
          {
            "value": "1",
            "name": "Rushing white particles to one point (???)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT01_FUSYA": {
    "name": "Windmill Sails",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT01_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOT01_IDOHASHIRA": {
    "name": "Kakariko Village Well Crossbeams",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT01_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOT01_IDOMIZU": {
    "name": "Kakariko Well Water",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT01_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_PO_SYOKUDAI": {
    "name": "Golden Torch Stand (Poe Sisters)",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SYOKUDAI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "0",
            "name": "Purple (Meg)"
          },
          {
            "value": "256",
            "name": "Red (Joelle)"
          },
          {
            "value": "512",
            "name": "Blue (Beth)"
          },
          {
            "value": "768",
            "name": "Green (Amy)"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag"
  },
  "ACTOR_BG_GANON_OTYUKA": {
    "name": "Floor of Ganondorf's Room",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GANON"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOT15_RRBOX": {
    "name": "Milk Crate",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT15_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_UMAJUMP": {
    "name": "Lon Lon Ranch Horse Jumping Fence",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_UMAJUMP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Fence"
          },
          {
            "value": "1",
            "name": "Nothing"
          },
          {
            "value": "65535",
            "name": "Fence"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_ARROW_FIRE": {
    "name": "Fire Arrow",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_ARROW_ICE": {
    "name": "Ice Arrow",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_ARROW_LIGHT": {
    "name": "Light Arrow",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_ITEM_ETCETERA": {
    "name": "GI Collectables (Shields, Bottles, etc)",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Bottle"
          },
          {
            "value": "1",
            "name": "Bottle with Ruto's Letter"
          },
          {
            "value": "2",
            "name": "Hylian shield"
          },
          {
            "value": "3",
            "name": "Quiver"
          },
          {
            "value": "4",
            "name": "Silver Scale"
          },
          {
            "value": "5",
            "name": "Golden Scale"
          },
          {
            "value": "6",
            "name": "Small Key"
          },
          {
            "value": "7",
            "name": "Fire Arrow"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_KIBAKO": {
    "name": "Small Wooden Crate",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "collectible",
        "name": "Item",
        "mask": 31
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Collectible",
        "mask": 16128
      }
    ],
    "notes": "Set to 0xFFFF to spawn nothing"
  },
  "ACTOR_OBJ_TSUBO": {
    "name": "Breakable Pot",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "collectible",
        "name": "Item",
        "mask": 31
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 9,
        "type": "flag",
        "name": "Collectible",
        "mask": 32256
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Use Object_Tsubo",
        "mask": 256
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WONDER_ITEM": {
    "name": "Invisible Collectibles",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 63488,
        "values": [
          {
            "value": "0",
            "name": "Collectible gave if each tag points are reached (no order)"
          },
          {
            "value": "2048",
            "name": "Tag Point (no order)"
          },
          {
            "value": "4096",
            "name": "Gives collectible if in range"
          },
          {
            "value": "6144",
            "name": "Invisible Collectible"
          },
          {
            "value": "10240",
            "name": "Collectible gave if each tag points are reached in order"
          },
          {
            "value": "12288",
            "name": "Tag Point (sets order)"
          },
          {
            "value": "14336",
            "name": "Sets Switch Flag if in range (no collectible)"
          },
          {
            "value": "16384",
            "name": "Bomberman Soldier"
          },
          {
            "value": "18432",
            "name": "Collectible gave if player rolls"
          }
        ]
      },
      {
        "typeFilter": [
          0,
          4096,
          6144,
          10240,
          18432
        ],
        "target": "params",
        "shift": 0,
        "type": "enum",
        "name": "Collectible Type",
        "mask": 1984,
        "values": [
          {
            "value": "0",
            "name": "Deku Nuts"
          },
          {
            "value": "1",
            "name": "Heart Piece"
          },
          {
            "value": "2",
            "name": "Large Magic Jar"
          },
          {
            "value": "3",
            "name": "Small Magic Jar"
          },
          {
            "value": "4",
            "name": "Recovery Heart"
          },
          {
            "value": "5",
            "name": "Arrows (5)"
          },
          {
            "value": "6",
            "name": "Arrows (10)"
          },
          {
            "value": "7",
            "name": "Arrows (30)"
          },
          {
            "value": "8",
            "name": "Green Rupee"
          },
          {
            "value": "9",
            "name": "Blue Rupee"
          },
          {
            "value": "10",
            "name": "Red Rupee"
          },
          {
            "value": "11",
            "name": "Flex Drop"
          },
          {
            "value": "12",
            "name": "Random Drop"
          }
        ]
      },
      {
        "typeFilter": [
          6144
        ],
        "target": "zrot",
        "shift": 0,
        "type": "enum",
        "name": "Spawn Trigger",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Sword Slash"
          },
          {
            "value": "1",
            "name": "Arrows"
          },
          {
            "value": "2",
            "name": "Hammer"
          },
          {
            "value": "3",
            "name": "Explosions"
          },
          {
            "value": "4",
            "name": "Slingshot"
          },
          {
            "value": "5",
            "name": "Boomerang"
          },
          {
            "value": "6",
            "name": "Hookshot"
          }
        ]
      },
      {
        "typeFilter": [
          4096,
          18432
        ],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "# of Collectibles",
        "mask": 255
      },
      {
        "typeFilter": [
          2048,
          12288
        ],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Tag Point #",
        "mask": 255
      },
      {
        "typeFilter": [
          0,
          10240
        ],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Total of Tag Points",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_IK": {
    "name": "Iron Knuckle",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_IK"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Nabooru Knuckle"
          },
          {
            "value": "1",
            "name": "White, sitting"
          },
          {
            "value": "2",
            "name": "Black, standing"
          },
          {
            "value": "3",
            "name": "White, standing"
          },
          {
            "value": "11",
            "name": "White, no armor falls off when at low health"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Nullable Switch Flag"
  },
  "ACTOR_DEMO_IK": {
    "name": "Iron Knuckle Armor Pieces",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_IK"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Unknown"
          },
          {
            "value": "1",
            "name": "Unknown"
          },
          {
            "value": "2",
            "name": "Unknown"
          },
          {
            "value": "3",
            "name": "Unknown"
          },
          {
            "value": "4",
            "name": "Unknown"
          },
          {
            "value": "5",
            "name": "Unknown"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_SKJ": {
    "name": "Skullkid",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_SKJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 64512,
        "values": [
          {
            "value": "0",
            "name": "Saria's Song HP Skull Kid"
          },
          {
            "value": "1024",
            "name": "Ocarina Game Left Skull Kid"
          },
          {
            "value": "2048",
            "name": "Ocarina Game Right Skull Kid"
          },
          {
            "value": "5120",
            "name": "Saria's Song HP Ocarina Spot"
          },
          {
            "value": "6144",
            "name": "Memory Game Ocarina Spot"
          },
          {
            "value": "64512",
            "name": "Enemy Skull Kid"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_SKJNEEDLE": {
    "name": "Skullkid's Needle",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_SKJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_G_SWITCH": {
    "name": "Silver Rupee/Large Rotating Pot",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Silver Rupee Tracker (handles puzzle resolution)"
          },
          {
            "value": "4096",
            "name": "Silver Rupee (the puzzle itself)"
          },
          {
            "value": "8192",
            "name": "Horseback Archery Pot"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "number",
        "name": "Count",
        "mask": 4032
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "You need to use one tracker if using silver rupees"
  },
  "ACTOR_DEMO_EXT": {
    "name": "Vortex (Koume/Kotake kidnap Nabooru)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_FHG"
    ],
    "params": [],
    "notes": "Used when Koume and Kotake kidnap Nabooru"
  },
  "ACTOR_DEMO_SHD": {
    "name": "Bongo Bongo Shadow",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNS": {
    "name": "Business Scrub",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SHOPNUTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Deku Nuts"
          },
          {
            "value": "1",
            "name": "Deku Sticks"
          },
          {
            "value": "2",
            "name": "Piece of Heart (10 rupees)"
          },
          {
            "value": "3",
            "name": "Deku Seeds"
          },
          {
            "value": "4",
            "name": "Deku Shield"
          },
          {
            "value": "5",
            "name": "Bombs"
          },
          {
            "value": "6",
            "name": "Deku Seeds"
          },
          {
            "value": "7",
            "name": "Red Potion"
          },
          {
            "value": "8",
            "name": "Green Potion"
          },
          {
            "value": "9",
            "name": "Deku Stick Upgrade"
          },
          {
            "value": "10",
            "name": "Deku Nut Upgrade"
          },
          {
            "value": "11",
            "name": "Never speaks, and Link is sadly very patient..."
          },
          {
            "value": "12",
            "name": "Pocket Egg?"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_ELF_MSG": {
    "name": "Navi Information Spot",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 32768,
        "values": [
          {
            "value": "0",
            "name": "Talk when in range"
          },
          {
            "value": "32768",
            "name": "C-Up Prompt"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_HONOTRAP": {
    "name": "Eye Switch and Flame that Follows Link",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Stone Eye"
          },
          {
            "value": "1",
            "name": "Heat-Seeking flame"
          },
          {
            "value": "2",
            "name": "Immobile flame, dies"
          },
          {
            "value": "3",
            "name": "Flame sound effect"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TUBO_TRAP": {
    "name": "Flying Magic Pot",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "0",
            "name": "Heart or Green Rupee"
          },
          {
            "value": "256",
            "name": "Bombs"
          },
          {
            "value": "512",
            "name": "Deku Seeds"
          },
          {
            "value": "768",
            "name": "Deku Nuts"
          },
          {
            "value": "1024",
            "name": "Deku Seeds"
          },
          {
            "value": "1280",
            "name": "Giant Purple Rupee"
          },
          {
            "value": "1536",
            "name": "Goron Tunic"
          },
          {
            "value": "65280",
            "name": "Nothing"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Collectible Flag"
  },
  "ACTOR_OBJ_ICE_POLY": {
    "name": "Ice That Covers Switches",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT03_TAKI": {
    "name": "Zora's River Waterfall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT03_OBJECT"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT07_TAKI": {
    "name": "Zora's Domain Waterfall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT07_OBJECT"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Waterfall and Pool"
          },
          {
            "value": "1",
            "name": "King's Chamber Water"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_FZ": {
    "name": "Freezard",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_FZ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Immobile"
          },
          {
            "value": "65535",
            "name": "Mobile"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_PO_RELAY": {
    "name": "Dampe's Ghost",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_TK"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Collectible",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_RELAY_OBJECTS": {
    "name": "Rotating Windmill Mechanism",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_RELAY_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "0",
            "name": "Rotating platform"
          },
          {
            "value": "256",
            "name": "Dampé Race Stone Door"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": "+0xFF = Nullable Switch Flag"
  },
  "ACTOR_EN_DIVING_GAME": {
    "name": "Zora Diving Game",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ZO"
    ],
    "params": [],
    "notes": "Hardcoded cutscene camera: -149, 943, -1020"
  },
  "ACTOR_EN_KUSA": {
    "name": "Grass Clump, doesn't regrow",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3,
        "values": [
          {
            "value": "0",
            "name": "Normal shrub. Random drops."
          },
          {
            "value": "1",
            "name": "Cut-able, regenerating grass"
          },
          {
            "value": "2",
            "name": "Cut-able grass"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 4,
        "type": "number",
        "name": "Spawn Bugs",
        "mask": 16
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Random Drop Table (Nullable)",
        "mask": 65280
      }
    ],
    "notes": "+0x0010 = Spawns set of 3 bugs\n+0xFF00 = Random Drop Table\n-FF00 No Table"
  },
  "ACTOR_OBJ_BEAN": {
    "name": "Bean Planting Spot",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MAMENOKI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path Id",
        "mask": 7936
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Speed",
        "mask": 3
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x1F00 = Path Id //1F00 = null\n+0x003F = Switch Flag\nZROT +0x0003 = Speed, Acceleration\n-0000 3.0, 0.3\n-0001 10.0, 0.5\n-0002 30.0, 0.5\n-0003 3.0, 0.3"
  },
  "ACTOR_OBJ_BOMBIWA": {
    "name": "Bombable Rock",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_BOMBIWA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Puzzle Solved Sound",
        "mask": 32768
      }
    ],
    "notes": "+0x8000 = Play Puzzle Solved When Destroyed\n+0x3F = Switch Flag"
  },
  "ACTOR_OBJ_SWITCH": {
    "name": "Dungeon Switches",
    "category": "ACTORCAT_SWITCH",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 7,
        "values": [
          {
            "value": "0",
            "name": "Floor"
          },
          {
            "value": "1",
            "name": "Rusted Floor"
          },
          {
            "value": "2",
            "name": "Eye"
          },
          {
            "value": "3",
            "name": "Crystal"
          },
          {
            "value": "4",
            "name": "Targetable Crystal"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Toggle",
        "mask": 16
      },
      {
        "typeFilter": [
          0,
          1
        ],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Reset",
        "mask": 32
      },
      {
        "typeFilter": [
          0,
          1
        ],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Reset Inverted (Flag Only)",
        "mask": 48
      },
      {
        "typeFilter": [
          3,
          4
        ],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Flag Sync",
        "mask": 64
      },
      {
        "typeFilter": [
          2
        ],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Frozen",
        "mask": 128
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_ELEVATOR": {
    "name": "Huge Stone Switch/Stone Elevator",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_D_ELEVATOR"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 1,
        "values": [
          {
            "value": "0",
            "name": "Large"
          },
          {
            "value": "1",
            "name": "Small"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 12,
        "type": "number",
        "name": "Rise Height (80 unit increments)",
        "mask": 61440
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Travel Speed (1: Slow; 15: Fast)",
        "mask": 3840
      }
    ],
    "notes": "0xF000 = Rise Height //80 unit increments\n0x0F00 = Platform Travel Speed"
  },
  "ACTOR_OBJ_LIFT": {
    "name": "Square Collapsing Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_D_LIFT"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Fall Timer Duration",
        "mask": 1792
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 2,
        "type": "flag",
        "name": "Switch",
        "mask": 252
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Small",
        "mask": 2
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_HSBLOCK": {
    "name": "Stone Hookshot Target",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_D_HSBLOCK"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3,
        "values": [
          {
            "value": "0",
            "name": "Hookshot Target Tower"
          },
          {
            "value": "1",
            "name": "Hookshot Target Tower, lifts up on Switch Flag"
          },
          {
            "value": "2",
            "name": "Square Hookshot Target"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Frozen",
        "mask": 32
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_OKARINA_TAG": {
    "name": "Play Ocarina Here Spot",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 64512,
        "values": [
          {
            "value": "1024",
            "name": "Zora River, Zelda's Lullaby Spot (no cutscene)"
          },
          {
            "value": "2048",
            "name": "Windmill, Song of Storms spot (cutscene)"
          },
          {
            "value": "4096",
            "name": "Temple of Time, Song of Time spot (cutscene)"
          },
          {
            "value": "5120",
            "name": "Learn Sun's Song spot"
          },
          {
            "value": "6144",
            "name": "Royal Family Tomb, Zelda's Lullaby spot"
          },
          {
            "value": "7168",
            "name": "Puzzle Spot"
          }
        ]
      },
      {
        "typeFilter": [
          7168
        ],
        "target": "params",
        "shift": 0,
        "type": "enum",
        "name": "Song to Play",
        "mask": 960,
        "values": [
          {
            "value": "0",
            "name": "Saria's Song"
          },
          {
            "value": "1",
            "name": "Epona's Song"
          },
          {
            "value": "2",
            "name": "Zelda's Lullaby"
          },
          {
            "value": "3",
            "name": "Sun's Song"
          },
          {
            "value": "4",
            "name": "Song of Time"
          },
          {
            "value": "5",
            "name": "Song of Storms"
          }
        ]
      },
      {
        "typeFilter": [
          7168
        ],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Range",
        "mask": 65535
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_YABUSAME_MARK": {
    "name": "Horseback Archery Target",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "End Targets"
          },
          {
            "value": "1",
            "name": "Mounted Target (Small)"
          },
          {
            "value": "2",
            "name": "Mounted Target (Big)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_GOROIWA": {
    "name": "Rolling Boulder",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GOROIWA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 768,
        "values": [
          {
            "value": "0",
            "name": "Follows path, disappears, reappears at first waypoint"
          },
          {
            "value": "256",
            "name": "Follows path, breaks into pieces, reappears at first waypoint"
          },
          {
            "value": "512",
            "name": "Follows path, treating it as a closed loop"
          },
          {
            "value": "768",
            "name": "Follows path, halts, reverses, repeat"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Path Id",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Behavior after colliding",
        "mask": 1
      }
    ],
    "notes": "+0xFF = Path Id\nROTZ 0x0001 = Behavior after colliding with Link\n-0000 Reverse path\n-0001 Follow path"
  },
  "ACTOR_EN_EX_RUPPY": {
    "name": "Sparkling Rupee",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Sparkling blue rupee"
          },
          {
            "value": "1",
            "name": "Huge Magenta Rupee, explodes when touched"
          },
          {
            "value": "2",
            "name": "Blue, red, and orange rupees, explode when touched"
          },
          {
            "value": "3",
            "name": "Random-colored rupees, collectible"
          },
          {
            "value": "4",
            "name": "Green rupees underground, not collectible"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TORYO": {
    "name": "Head Carpenter",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_TORYO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DAIKU": {
    "name": "Carpenters",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DAIKU"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3,
        "values": [
          {
            "value": "0",
            "name": "Ichiro"
          },
          {
            "value": "1",
            "name": "Sabooro"
          },
          {
            "value": "2",
            "name": "Jiro"
          },
          {
            "value": "3",
            "name": "Shiro"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Freedom Switch Flag",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Fight Switch Flag",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 4,
        "type": "number",
        "name": "Path ID",
        "mask": 240
      }
    ],
    "notes": "Talking softlocks if outside of intended scene numbers"
  },
  "ACTOR_EN_NWC": {
    "name": "Alpha Blue Balls",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_NWC"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BLKOBJ": {
    "name": "Dark Link's Illusion Room",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_BLKOBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ITEM_INBOX": {
    "name": "Item Inbox (???)",
    "category": "ACTORCAT_NPC",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Get Item Draw ID",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Chest",
        "mask": 7936
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_GE1": {
    "name": "White Clothed Gerudo",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_GE1"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Shoos you away"
          },
          {
            "value": "1",
            "name": "Gate Manager (Gerudo Fortress)"
          },
          {
            "value": "4",
            "name": "Generic Guard (Gerudo Fortress)"
          },
          {
            "value": "5",
            "name": "Stands By Cow (Gerudo Valley)"
          },
          {
            "value": "69",
            "name": "Horseback Archery"
          },
          {
            "value": "70",
            "name": "Gerudo Training Grounds Guard"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_OBJ_BLOCKSTOP": {
    "name": "Block Save Point",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag //Sets flag when block is placed on top of actor"
  },
  "ACTOR_EN_SDA": {
    "name": "Alpha Link Shadow Activator",
    "category": "ACTORCAT_BOSS",
    "objDeps": [],
    "params": [],
    "notes": "Unused. Works on Link and Signs. Check in Spirit Temple under light, works for all values except 1"
  },
  "ACTOR_EN_CLEAR_TAG": {
    "name": "Alpha Arwing",
    "category": "ACTORCAT_BOSS",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Cutscene start"
          },
          {
            "value": "1",
            "name": "No cutscene"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_NIW_LADY": {
    "name": "Cucco Lady",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ANE"
    ],
    "params": [],
    "notes": "Text ID 503E as adult, gives you an egg"
  },
  "ACTOR_EN_GM": {
    "name": "Medigoron",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag //Won't say his normal dialog unless the flag Medigoron is bound to is set"
  },
  "ACTOR_EN_MS": {
    "name": "Bean Seller",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_HS": {
    "name": "Carpenter's Son",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_HS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": "Text ID 10B1 when sleeping"
  },
  "ACTOR_BG_INGATE": {
    "name": "Ingo's Gates",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_INGATE"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3,
        "values": [
          {
            "value": "0",
            "name": "Standard"
          },
          {
            "value": "1",
            "name": "Cutscene Gate, Left Part"
          },
          {
            "value": "3",
            "name": "Cutscene Gate, Right Part"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_KANBAN": {
    "name": "Square Signpost",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_KANBAN"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "X"
          },
          {
            "value": "1",
            "name": "Hyrule Field"
          },
          {
            "value": "2",
            "name": "Hyrule Castle Town"
          },
          {
            "value": "3",
            "name": "The Temple of Time"
          },
          {
            "value": "4",
            "name": "Dead End"
          },
          {
            "value": "5",
            "name": "Kakariko Village / Death Mountain Trail / Starting Point"
          },
          {
            "value": "6",
            "name": "Kakariko Village Graveyard"
          },
          {
            "value": "7",
            "name": "Dark! Narrow! Scary! / Well of Three Features"
          },
          {
            "value": "8",
            "name": "Death Mountain / No passage without a / Royal Decree!"
          },
          {
            "value": "9",
            "name": "Death Mountain Trail"
          },
          {
            "value": "10",
            "name": "Dodongo's Cavern / Don't enter without permission!"
          },
          {
            "value": "11",
            "name": "Land of the Gorons / Goron City"
          },
          {
            "value": "12",
            "name": "Zora's River / Watch out for swift current / and strong undertow."
          },
          {
            "value": "13",
            "name": "The Shadow will yield only to one / with the eye of truth, handed / down in Kakariko Village."
          },
          {
            "value": "14",
            "name": "Zora's Domain"
          },
          {
            "value": "15",
            "name": "Zora's Fountain / Don't disturb Lord Jabu-Jabu! / –King Zora XVI"
          },
          {
            "value": "16",
            "name": "Forest Training Center / Don't recklessly cut signs– / read them carefully!"
          },
          {
            "value": "17",
            "name": "All those reckless enough to / venture into the desert–please drop by our shop. / Carpet Merchant"
          },
          {
            "value": "18",
            "name": "Just ahead: / Great Deku Tree's Meadow"
          },
          {
            "value": "19",
            "name": "Forest Temple"
          },
          {
            "value": "20",
            "name": "The Lost Woods"
          },
          {
            "value": "21",
            "name": "Talon and Malon's / Lon Lon Ranch"
          },
          {
            "value": "22",
            "name": "The Great Ingo's / Ingo Ranch"
          },
          {
            "value": "23",
            "name": "Lake Hylia"
          },
          {
            "value": "24",
            "name": "Lakeside Laboratory / Daily trying to get to the bottom / of the mysteries of Lake Hylia! / –Lake Scientist"
          },
          {
            "value": "25",
            "name": "Gerudo Valley"
          },
          {
            "value": "26",
            "name": "Horseback Archery Range / Skilled players are welcome! / Current record: # Points"
          },
          {
            "value": "27",
            "name": "Gerudo Training Ground / Only registered members are / allowed!"
          },
          {
            "value": "28",
            "name": "Haunted Wasteland / If you chase a mirage, the / desert will swallow you. / Only one path is true!"
          },
          {
            "value": "29",
            "name": "Spirit Temple"
          },
          {
            "value": "30",
            "name": "Kokiri Shop / We have original forest goods!"
          },
          {
            "value": "31",
            "name": "LINK's House"
          },
          {
            "value": "32",
            "name": "Forest folk shall not leave these woods."
          },
          {
            "value": "33",
            "name": "Follow the trail along the edge of / the cliff and you will reach / Goron City, home of the Gorons."
          },
          {
            "value": "34",
            "name": "Natural Wonder / Bomb Flower / Danger! Do not uproot!"
          },
          {
            "value": "35",
            "name": "Death Mountain Summit / Entrance to the crater ahead / Beware of intense heat!"
          },
          {
            "value": "36",
            "name": "King Zora's Throne Room / To hear the King's royal / proclamations, stand on the / platform and speak to him."
          },
          {
            "value": "37",
            "name": "If you can stop my wild rolling, you might get something great. / –Hot Rodder Goron"
          },
          {
            "value": "38",
            "name": "Only one with the eye of truth / will find the stone umbrella / that protects against the / rain of blades."
          },
          {
            "value": "39",
            "name": "Only one who has sacred feet / can cross the valley of the dead."
          },
          {
            "value": "40",
            "name": "The record time of those / who raced against me was: / ##\"##\" / –Dampé the Gravekeeper"
          },
          {
            "value": "41",
            "name": "Shooting Gallery / etc."
          },
          {
            "value": "42",
            "name": "Treasure Chest Shop / We don't necessarily sell them..."
          },
          {
            "value": "43",
            "name": "High Dive Practice Spot / Are you confident / in your diving skill?"
          },
          {
            "value": "44",
            "name": "032c"
          },
          {
            "value": "45",
            "name": "Mountain Summit / Danger Ahead - Keep Out"
          },
          {
            "value": "46",
            "name": "Happy Mask Shop! / Now hiring happiness / delivery men!"
          },
          {
            "value": "47",
            "name": "Bombchu Bowling Alley / You can experience the / latest in Bomb technology!"
          },
          {
            "value": "48",
            "name": "Bazaar / We have a little bit of everything!"
          },
          {
            "value": "49",
            "name": "Potion Shop / We have the best quality / potions!"
          },
          {
            "value": "50",
            "name": "Goron Shop / Mountaineering supplies!"
          },
          {
            "value": "51",
            "name": "Zora Shop / We have fresh fish!"
          },
          {
            "value": "52",
            "name": "Heart-Pounding Gravedigging Tour! / From 18:00 to 21:00 Hyrule Time / –Dampé the Gravekeeper"
          },
          {
            "value": "53",
            "name": "Heart-Pounding Gravedigging Tour! / Tours are cancelled until a new / gravekeeper is found. We / apologize for any inconvenience."
          },
          {
            "value": "54",
            "name": "Thrust Attack Signs! / To thrust with your sword, press / CS toward your target while / Z Targeting, then press B."
          },
          {
            "value": "55",
            "name": "Hole of “Z” / Let's go through this small / hole! / Stand in front of the hole and / push CS towards it. When the / Action Icon shows “Enter,” press / A to crawl into the hole. / Pay attention to what the Action / Icon says!"
          },
          {
            "value": "56",
            "name": "Cut Grass With Your Sword / If you just swing with B, you'll / cut horizontally. If you hold Z as / you swing, you'll cut vertically."
          },
          {
            "value": "57",
            "name": "Hyrule Castle / Lon Lon Ranch"
          },
          {
            "value": "58",
            "name": "You are here: Hyrule Castle / This way to Lon Lon Ranch"
          },
          {
            "value": "59",
            "name": "Just Ahead / King Zora's Chamber / Show the proper respect!"
          },
          {
            "value": "60",
            "name": "House of the Great Mido / Boss of the Kokiri"
          },
          {
            "value": "61",
            "name": "House of the Know-It-All Brothers"
          },
          {
            "value": "62",
            "name": "House of Twins"
          },
          {
            "value": "63",
            "name": "Saria's House"
          },
          {
            "value": "64",
            "name": "View Point with Z Targeting / When you have no object to look / at, you can just look forward / with Z. / Stop moving and then change the / direction you are facing, or hold / down Z for a little while. / This can help you get oriented in / the direction you want to face. / It's quite convenient! / If you hold down Z, you can / walk sideways while facing / straight ahead. / Walking sideways can be a very / important technique in dungeon / corridors. Turn around and try doing this right now."
          },
          {
            "value": "65",
            "name": "Stepping Stones in the Pond / If you boldly go in the direction / you want to jump, you will leap / automatically. / If you hop around on the stones, / you'll become happier!"
          },
          {
            "value": "66",
            "name": "No Diving Allowed / –It won't do you any good!"
          },
          {
            "value": "67",
            "name": "Switch Targeting / If you see a \\/ icon above an / object, you can target it with Z. / ... / You can target the stones next to this sign for practice!"
          },
          {
            "value": "68",
            "name": "Forest Stage / We are waiting to see your / beautiful face! / Win fabulous prizes!"
          },
          {
            "value": "69",
            "name": "Visit the / House of the Know-It-All Brothers / to get answers to all your / item-related questions!"
          },
          {
            "value": "70",
            "name": "Pocket Egg"
          }
        ]
      }
    ],
    "notes": "Message ID //Value + 0x0300"
  },
  "ACTOR_EN_HEISHI3": {
    "name": "Hyrule Castle Guard",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_SD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Whistleblower"
          },
          {
            "value": "1",
            "name": "Stands there, does nothing"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_NIW": {
    "name": "Hopping Cucco, Not Solid",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_NIW"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Non-solid cucco, hops oddly every once in awhile and only goes in one direction"
          },
          {
            "value": "1",
            "name": "Invisible, solid cucco, doesn't move, can be attacked but will only smoke and molt"
          },
          {
            "value": "2",
            "name": "Invisible cucco, cannot be attacked it seems, no idea what it does, but you can hear it"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ATTACK_NIW": {
    "name": "Attacking Cucco",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_NIW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOT01_IDOSOKO": {
    "name": "Stone Blocking Well Entrance",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT01_MATOYA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_SA": {
    "name": "Saria",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_SA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WONDER_TALK": {
    "name": "Checkable Spot",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 63488,
        "values": [
          {
            "value": "2048",
            "name": "Temple of Time stone altar dialog"
          },
          {
            "value": "4096",
            "name": "Gravekeeper's diary dialog"
          },
          {
            "value": "6144",
            "name": "Royal Composer Sharp's grave dialog"
          },
          {
            "value": "8192",
            "name": "\"Royal Family Tomb\" dialog"
          },
          {
            "value": "10240",
            "name": "Royal Composer Flat's grave dialog"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag //Spot deactivates when flag is set"
  },
  "ACTOR_BG_GJYO_BRIDGE": {
    "name": "Rainbow Bridge",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GJYO_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DS": {
    "name": "Potion Shop Lady",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MK": {
    "name": "Lake Hylia Doctor",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOM_BOWL_MAN": {
    "name": "Bombchu Bowling Girl",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_BG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOM_BOWL_PIT": {
    "name": "Bombchu Bowling Pit",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OWL": {
    "name": "Kaepora Gaebora",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OWL"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 960,
        "values": [
          {
            "value": "0",
            "name": "Does nothing"
          },
          {
            "value": "64",
            "name": "Outside of Kokiri Forest exit, auto talks"
          },
          {
            "value": "128",
            "name": "Near Hyrule Castle, auto talks"
          },
          {
            "value": "192",
            "name": "In front of Kakariko Village, auto talks"
          },
          {
            "value": "256",
            "name": "Between Lake Hylia and Gerudo Valley, auto talks"
          },
          {
            "value": "320",
            "name": "In front of Lake Hylia, auto talks"
          },
          {
            "value": "384",
            "name": "Nothing"
          },
          {
            "value": "448",
            "name": "Lake Hylia, manual talk, talon grab shortcut"
          },
          {
            "value": "512",
            "name": "Death Mountain summit, manual talk, talon grab shortcut"
          },
          {
            "value": "576",
            "name": "Death Mountain summit, manual talk, talon grab shortcut"
          },
          {
            "value": "640",
            "name": "Desert Colossus, auto talks"
          },
          {
            "value": "704",
            "name": "Lost Woods, before meeting Saria, auto talks"
          },
          {
            "value": "768",
            "name": "Lost Woods, after meeting Saria, auto talks"
          },
          {
            "value": "832",
            "name": "Outside of Kokiri Forest exit, auto talks, Head position alternates?"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag"
  },
  "ACTOR_EN_ISHI": {
    "name": "Rock",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 1,
        "values": [
          {
            "value": "0",
            "name": "Small rock, random drops"
          },
          {
            "value": "1",
            "name": "Large light-gray rock, can't pick up as Young Link, no drop"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Drop Table",
        "mask": 3840
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 12,
        "type": "flag",
        "name": "Switch",
        "mask": 61440
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "flag",
        "name": "Switch",
        "mask": 192
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Spawn Bugs",
        "mask": 16
      }
    ],
    "notes": "+0x10 = Spawns Bugs\n-0000 No\n-0010 Yes\n+0x0F00 = Drop Table //0-12, anything higher becomes drop table 0\n+0xF000 = Switch Flag (High Bits)\n+0xC0 = Switch Flag (Low Bits)"
  },
  "ACTOR_OBJ_HANA": {
    "name": "Grave Flower",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3,
        "values": [
          {
            "value": "0",
            "name": "Flower for grave"
          },
          {
            "value": "1",
            "name": "Non-liftable small rock"
          },
          {
            "value": "2",
            "name": "Uncuttable small shrub"
          },
          {
            "value": "3",
            "name": "Erratic collision data"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_LIGHTSWITCH": {
    "name": "Light Activated Sun Switch/Sun Block",
    "category": "ACTORCAT_SWITCH",
    "objDeps": [
      "OBJECT_LIGHTSWITCH"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 48,
        "values": [
          {
            "value": "0",
            "name": "Stays On"
          },
          {
            "value": "16",
            "name": "On/Off Toggle"
          },
          {
            "value": "32",
            "name": "Activated while enlightened"
          },
          {
            "value": "48",
            "name": "Burns"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "With Block",
        "mask": 1
      }
    ],
    "notes": "+0x3F00 switch flag"
  },
  "ACTOR_OBJ_MURE2": {
    "name": "Circle of Bushes",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Circle of shrubs with one in the middle"
          },
          {
            "value": "1",
            "name": "Scattered shrubs"
          },
          {
            "value": "2",
            "name": "Circle of rocks"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Random Drop Table",
        "mask": 3840
      }
    ],
    "notes": "+0xFF00 = Random Drop Table"
  },
  "ACTOR_EN_GO": {
    "name": "Unused Gorons",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 240,
        "values": [
          {
            "value": "0",
            "name": "Link the Goron"
          },
          {
            "value": "16",
            "name": "Fire Temple Generic"
          },
          {
            "value": "32",
            "name": "DMT DC Entrance"
          },
          {
            "value": "48",
            "name": "DMT Rolling"
          },
          {
            "value": "64",
            "name": "DMT Near Bomb Flower"
          },
          {
            "value": "80",
            "name": "Goron City Entrance"
          },
          {
            "value": "96",
            "name": "Goron City Island"
          },
          {
            "value": "112",
            "name": "Goron City Lost Woods"
          },
          {
            "value": "128",
            "name": "Unused"
          },
          {
            "value": "144",
            "name": "Biggoron"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Path ID (Nullable)",
        "mask": 15
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_FU": {
    "name": "Windmill Man",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_FU"
    ],
    "params": [],
    "notes": "5034 as adult, 5035 when playing ocarina in front of him as adult, crash if no cutscene ?"
  },
  "ACTOR_EN_CHANGER": {
    "name": "Treasure Chests from Treasure Box Game",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_JYA_MEGAMI": {
    "name": "Large Stone Face (Spirit Temple)",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_JYA_LIFT": {
    "name": "Hanging Platform (Spirit Temple)",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_JYA_BIGMIRROR": {
    "name": "Large Circular Mirror (Spirit Temple)",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_JYA_BOMBCHUIWA": {
    "name": "Light Blocking Rock (Spirit Temple)",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "Bombable, targetable\n      +0x3F00 = Switch Flag\n      hardcoded cutscene position at -1160, 686, -880\n      spawn light  at -946, 477, -894"
  },
  "ACTOR_BG_JYA_AMISHUTTER": {
    "name": "Circular, Lifting Metal Shutter (Spirit Temple)",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [],
    "notes": "Hookable, automatically rises as you approach it"
  },
  "ACTOR_BG_JYA_BOMBIWA": {
    "name": "Bombable Rock Wall (Spirit Temple)",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT18_BASKET": {
    "name": "Giant Three Sided Goron Statue",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT18_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Collectible",
        "mask": 63
      }
    ],
    "notes": "+0x3F00 = Switch Flag //Lid disappears, starts spinning on floor\n+0x003F = Collectible Flag //Heart Piece"
  },
  "ACTOR_EN_GANON_ORGAN": {
    "name": "Ganondorf's Organ",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_GANON"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SIOFUKI": {
    "name": "Water Spout",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SIOFUKI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Rising"
          },
          {
            "value": "4096",
            "name": "Lowering"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Size of Jet",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "xrot",
        "shift": 0,
        "type": "number",
        "name": "Height",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "yrot",
        "shift": 0,
        "type": "number",
        "name": "Timer in seconds",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "flag",
        "name": "Switch",
        "mask": 4032
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Chest",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_STREAM": {
    "name": "Whirlpool Effect",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_STREAM"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Tall and narrow"
          },
          {
            "value": "1",
            "name": "Short and wide"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_MM": {
    "name": "Running Man",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MM"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Path ID",
        "mask": 255
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_KO": {
    "name": "Kokiri",
    "category": "ACTORCAT_NPC",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Standing boy lifting a rock"
          },
          {
            "value": "1",
            "name": "Standing girl near Fado"
          },
          {
            "value": "2",
            "name": "Boxing boy"
          },
          {
            "value": "3",
            "name": "Blocking boy"
          },
          {
            "value": "4",
            "name": "Backflipping boy"
          },
          {
            "value": "5",
            "name": "Sitting girl on Shop"
          },
          {
            "value": "6",
            "name": "Standing girl near Mido's House"
          },
          {
            "value": "7",
            "name": "Know-It-All Bro teaching about HUD icons"
          },
          {
            "value": "8",
            "name": "Know-It-All Bro teaching about map and items"
          },
          {
            "value": "9",
            "name": "Sitting girl in house"
          },
          {
            "value": "10",
            "name": "Standing girl in Shop"
          },
          {
            "value": "11",
            "name": "Know-It-All Bro teaching about C-Up"
          },
          {
            "value": "12",
            "name": "Fado"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_KZ": {
    "name": "King Zora",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_KZ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID (Nullable)",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WEATHER_TAG": {
    "name": "Weather Effects",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Cloudy Market"
          },
          {
            "value": "1",
            "name": "Cloudy Ranch"
          },
          {
            "value": "2",
            "name": "Snowy Zora's Domain"
          },
          {
            "value": "3",
            "name": "Rainy Lake Hylia"
          },
          {
            "value": "4",
            "name": "Cloudy Death Mountain"
          },
          {
            "value": "5",
            "name": "Thunderstrorm Kakariko"
          },
          {
            "value": "6",
            "name": "Sandstorm Intensity"
          },
          {
            "value": "7",
            "name": "Thunderstrorm Graveyard"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Effect Proximity (per 100 units)",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SST_FLOOR": {
    "name": "Bongo Bongo Floor",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SST"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ANI": {
    "name": "Kakariko Village Rooftop Man",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ANI"
    ],
    "params": [],
    "notes": "Text ID 5052 as adult"
  },
  "ACTOR_EN_EX_ITEM": {
    "name": "Mini-Game Displayed GI Models",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Bomb Bag (Bombchu Bowling given prize)"
          },
          {
            "value": "1",
            "name": "Heart Piece (Bombchu Bowling given prize)"
          },
          {
            "value": "2",
            "name": "Bombchus (Bombchu Bowling given prize)"
          },
          {
            "value": "3",
            "name": "Bombs (Bombchu Bowling given prize)"
          },
          {
            "value": "4",
            "name": "Purple Rupee (Bombchu Bowling given prize)"
          },
          {
            "value": "5",
            "name": "Bomb Bag (Bombchu Bowing prize preview)"
          },
          {
            "value": "6",
            "name": "Heart Piece (Bombchu Bowing prize preview)"
          },
          {
            "value": "7",
            "name": "Bombchus (Bombchu Bowing prize preview)"
          },
          {
            "value": "8",
            "name": "Bombs (Bombchu Bowing prize preview)"
          },
          {
            "value": "9",
            "name": "Purple Rupee (Bombchu Bowing prize preview)"
          },
          {
            "value": "10",
            "name": "Green Rupee (Chest Game)"
          },
          {
            "value": "11",
            "name": "Blue Rupee (Chest Game)"
          },
          {
            "value": "12",
            "name": "Red Rupee (Chest Game)"
          },
          {
            "value": "14",
            "name": "Purple Rupee (Unused Chest Game Rupee)"
          },
          {
            "value": "15",
            "name": "Small Key (Chest Game)"
          },
          {
            "value": "16",
            "name": "Din's Fire"
          },
          {
            "value": "17",
            "name": "Farore's Wind"
          },
          {
            "value": "18",
            "name": "Nayru's Love"
          },
          {
            "value": "19",
            "name": "Bullet Bag"
          }
        ]
      }
    ],
    "notes": "Each type needs its own object to load"
  },
  "ACTOR_BG_JYA_IRONOBJ": {
    "name": "Brick Pillar (Spirit Temple)",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_JYA_IRON"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 1,
        "values": [
          {
            "value": "0",
            "name": "Brick pillar"
          },
          {
            "value": "1",
            "name": "Brick throne"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_JS": {
    "name": "Bombchu Salesman",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_JS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JSJUTAN": {
    "name": "Flying Carpet",
    "category": "ACTORCAT_NPC",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_CS": {
    "name": "Graveyard Boy",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_CS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Path Id",
        "mask": 255
      }
    ],
    "notes": "+0xFF = Path Id"
  },
  "ACTOR_EN_MD": {
    "name": "Mido",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "256",
            "name": "Blocking Deku Tree"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_HY": {
    "name": "Hylians",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OS_ANIME"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 127,
        "values": [
          {
            "value": "0",
            "name": "Richard's Owner"
          },
          {
            "value": "1",
            "name": "Woman in white, blues and yellow near Apothicary"
          },
          {
            "value": "2",
            "name": "Bearded man in white and green near Bazaar"
          },
          {
            "value": "3",
            "name": "Sakon (Jogging Man)"
          },
          {
            "value": "4",
            "name": "Staunch man in black and green"
          },
          {
            "value": "5",
            "name": "Begging man"
          },
          {
            "value": "6",
            "name": "Old woman in white"
          },
          {
            "value": "7",
            "name": "Old man in blue near Bombchu Bowling"
          },
          {
            "value": "8",
            "name": "Woman in lilac near Bombchu Bowling"
          },
          {
            "value": "9",
            "name": "Laughing man in red and white"
          },
          {
            "value": "10",
            "name": "Explaining man in blue and white"
          },
          {
            "value": "11",
            "name": "'Dancing' woman in blue and yellow near Archery Game"
          },
          {
            "value": "12",
            "name": "Watchtower man in crimson"
          },
          {
            "value": "13",
            "name": "Red haired man in green and lilac"
          },
          {
            "value": "14",
            "name": "Bearded, red haired man in green and white"
          },
          {
            "value": "15",
            "name": "Old bald man in brown"
          },
          {
            "value": "16",
            "name": "Man in white"
          },
          {
            "value": "17",
            "name": "Man from Impa's House"
          },
          {
            "value": "19",
            "name": "Old bald man in purple"
          },
          {
            "value": "20",
            "name": "Man in two shades of green"
          }
        ]
      },
      {
        "typeFilter": [
          3,
          7
        ],
        "target": "params",
        "shift": 7,
        "type": "number",
        "name": "Path ID",
        "mask": 1920
      }
    ],
    "notes": "+0x780 = Path Id //NPCs 03 and 07 only"
  },
  "ACTOR_EN_GANON_MANT": {
    "name": "Ganondorf's Mantle",
    "category": "ACTORCAT_BOSS",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OKARINA_EFFECT": {
    "name": "Rain and Lightning",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MAG": {
    "name": "Title Screen Actor",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_MAG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DOOR_GERUDO": {
    "name": "Fortress Metal Grate Door",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_DOOR_GERUDO"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Collectible",
        "mask": 7936
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_ELF_MSG2": {
    "name": "Targetable Navi Spot",
    "category": "ACTORCAT_BG",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Don't clear after dialog",
        "mask": 1
      },
      {
        "typeFilter": [],
        "target": "yrot",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 7,
        "type": "flag",
        "name": "Switch",
        "mask": 128
      },
      {
        "typeFilter": [],
        "target": "xrot",
        "shift": 0,
        "type": "bool",
        "name": "Target Range",
        "mask": 8
      }
    ],
    "notes": "See actor 11B for variables"
  },
  "ACTOR_DEMO_GT": {
    "name": "Ganon's Tower Collapsing",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GT"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Unknown"
          },
          {
            "value": "1",
            "name": "Unknown"
          },
          {
            "value": "2",
            "name": "Unknown"
          },
          {
            "value": "5",
            "name": "Unknown"
          },
          {
            "value": "6",
            "name": "Unknown"
          },
          {
            "value": "7",
            "name": "Unknown"
          },
          {
            "value": "23",
            "name": "Unknown"
          },
          {
            "value": "24",
            "name": "Unknown"
          },
          {
            "value": "35",
            "name": "Unknown"
          },
          {
            "value": "36",
            "name": "Unknown"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_PO_FIELD": {
    "name": "Hyrule Field Poes/Big Poes",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_PO_FIELD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Big Poe"
          },
          {
            "value": "65535",
            "name": "Normal poe, circles around you"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EFC_ERUPC": {
    "name": "Lava Sprites",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_EFC_ERUPC"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_ZG": {
    "name": "Metal Bars",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ZG"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0x3F00 = Switch Flag"
  },
  "ACTOR_EN_HEISHI4": {
    "name": "Hyrule Guard",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_SD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Time teller"
          },
          {
            "value": "4",
            "name": "Stands by Impa's House, tells time"
          },
          {
            "value": "7",
            "name": "Dying guard in Market Town"
          },
          {
            "value": "8",
            "name": "Stands in Market Town at night"
          }
        ]
      }
    ],
    "notes": "Text ID 5063, then 5064"
  },
  "ACTOR_EN_ZL3": {
    "name": "Adult Zelda",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ZL2"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Zelda in Ganondorf Fight (used by game engine)"
          },
          {
            "value": "1",
            "name": "Zelda in Ganon Fight (used by game engine)"
          },
          {
            "value": "3",
            "name": "Zelda in Castle Collapse"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 4,
        "type": "number",
        "name": "Path ID",
        "mask": 240
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_BOSS_GANON2": {
    "name": "Ganon",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_GANON2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KAKASI": {
    "name": "Scarecrow",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_KA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TAKARA_MAN": {
    "name": "Treasure Chest Shop Man",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_TS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MAKEOSHIHIKI": {
    "name": "Push Block (Hardcoded Positions)",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Ignore Switch Flag (1st Destination)",
        "mask": 16384
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Ignore Switch Flag (2nd Destination)",
        "mask": 64
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "bool",
        "name": "Is Small",
        "mask": 1
      }
    ],
    "notes": "Requires Goron Bracelet or better to move as child or adult"
  },
  "ACTOR_OCEFF_SPOT": {
    "name": "Sun's Song effect",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_END_TITLE": {
    "name": "The End Message",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Draw Nintendo Logo",
        "mask": 1
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TORCH": {
    "name": "Treasure Chest (Variable chosen by Hole)",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": "Contents determined by the grotto (actor 009B) variable"
  },
  "ACTOR_DEMO_EC": {
    "name": "Ranch NPC's (Ending Cutscene)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_EC"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Drunken Ingo"
          },
          {
            "value": "1",
            "name": "Drunken Talon"
          },
          {
            "value": "2",
            "name": "Windmill man breakdancing"
          },
          {
            "value": "3",
            "name": "Encouraging Kokiri Boy"
          },
          {
            "value": "4",
            "name": "Encouraging Kokiri Girl"
          },
          {
            "value": "5",
            "name": "White-haired man in blue"
          },
          {
            "value": "6",
            "name": "Black-bearded man in white and green"
          },
          {
            "value": "7",
            "name": "Bushy-haired woman in red and black"
          },
          {
            "value": "8",
            "name": "Little old lady"
          },
          {
            "value": "9",
            "name": "Carpenter Boss, Singing"
          },
          {
            "value": "10",
            "name": "Carpenter, Singing"
          },
          {
            "value": "11",
            "name": "Carpenter, Singing"
          },
          {
            "value": "12",
            "name": "Carpenter, Singing"
          },
          {
            "value": "13",
            "name": "Carpenter, Singing"
          },
          {
            "value": "14",
            "name": "Kokiri Boy, Dancing"
          },
          {
            "value": "15",
            "name": "Kokiri Girl, Dancing"
          },
          {
            "value": "16",
            "name": "Gerudo, Line-dancing 1"
          },
          {
            "value": "17",
            "name": "Gerudo, Line-dancing 2"
          },
          {
            "value": "18",
            "name": "Gerudo, spikey-haired, Line-dancing"
          },
          {
            "value": "19",
            "name": "Dancing Zora"
          },
          {
            "value": "20",
            "name": "King Zora"
          },
          {
            "value": "21",
            "name": "Mido, sitting"
          },
          {
            "value": "22",
            "name": "Floating cucco"
          },
          {
            "value": "23",
            "name": "Floating cucco 2"
          },
          {
            "value": "24",
            "name": "Walking cucco"
          },
          {
            "value": "25",
            "name": "Cucco Lady"
          },
          {
            "value": "26",
            "name": "Potion Shopkeeper"
          },
          {
            "value": "27",
            "name": "Happy Mask Salesman"
          },
          {
            "value": "28",
            "name": "Fisherman"
          },
          {
            "value": "29",
            "name": "Bombchu Shopkeeper"
          },
          {
            "value": "30",
            "name": "Dancing Goron"
          },
          {
            "value": "31",
            "name": "Belly slapping Goron"
          },
          {
            "value": "32",
            "name": "Biggoron, dancing"
          },
          {
            "value": "33",
            "name": "Medigoron, lying down"
          },
          {
            "value": "34",
            "name": "Singing Malon"
          }
        ]
      }
    ],
    "notes": "Each variable requires it's own object"
  },
  "ACTOR_SHOT_SUN": {
    "name": "Fire Arrow Shot/Sun's Song Fairy",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "1",
            "name": "Lake Hylia's Sun Hitbox"
          },
          {
            "value": "64",
            "name": "Big Fairy, spawns with Sun's Song"
          },
          {
            "value": "65",
            "name": "Big Fairy, spawns with Song of Storms"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DY_EXTRA": {
    "name": "Great Fairy's Pink and Yellow Beams",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_DY_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Pink spiral beam"
          },
          {
            "value": "1",
            "name": "Green spiral beam"
          },
          {
            "value": "2",
            "name": "Purple spiral beam"
          },
          {
            "value": "3",
            "name": "Blue spiral beam"
          },
          {
            "value": "4",
            "name": "+ Black spiral beam"
          },
          {
            "value": "12",
            "name": "Black and green spiral beam"
          },
          {
            "value": "13",
            "name": "Gray and green spiral beam"
          },
          {
            "value": "14",
            "name": "Light blue spiral beam"
          },
          {
            "value": "15",
            "name": "Light purple spiral beam"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WONDER_TALK2": {
    "name": "Checkable Spot",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 49152,
        "values": [
          {
            "value": "0",
            "name": "Checkable, Sets Switch"
          },
          {
            "value": "16384",
            "name": "Instant Text"
          },
          {
            "value": "32768",
            "name": "Checkable, Disappears on Switch"
          },
          {
            "value": "49152",
            "name": "Z-Target, No Text"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "number",
        "name": "Message ID",
        "mask": 16320
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3FC0 = Message ID //0x0200 + Value\n-0000 Hi! I'm a talking door! //Unused\n-0040 Strange... this door doesn't open...\n-0080 Strong iron bars are blocking the door. You can't open them with your hands!\n-00C0 You need a Key to open a door that is locked or chained.\n-0100 You need a special key to open this door.\n-0140 Be quiet! It's only time! I, Dampé the Gravekeeper, am in bed now! Go away and play! Maybe you can find a ghost in the daytime?\n-0180 It's time now. The Gravedigging Tour is over now! I, Dampé the gravekeeper, am in bed! Go away and play! Maybe you'll find a ghost!\n-01C0 Happy Mask Shop / Please read this sign before you use this shop...\n-0200 Shadow Temple... here is gathered Hyrule's bloody history of greed and hatred...\n-0240 What is hidden in the darkness... Tricks full of ill will... You can't see the way forward...\n-0280 One who gains the eye of truth will be able to see what is hidden in the darkness.\n-02C0 Something strange is covering the entrance. You must solve the puzzle in this room to make the entrance open.\n-0300 Giant dead Dodongo... when it sees red, a new way to go will be open.\n-0340 Treasure Chest Contest / Temporarily Closed / Open Tonight!\n-0380 Medicine Shop / Closed until morning...\n-03C0 Shooting Gallery / Open only during the day\n-0400 Happy Mask Shop / Now hiring part-time / Apply during the day\n-0440 Bazaar / Open only during the day\n-0480 Show me the light!\n-04C0 One with the eye of truth shall be guided to the Spirit Temple by an inviting ghost.\n-0500 Those who wish to open the path sleeping at the bottom of the lake must play the song passed down by the Royal Family.\n-0540 Those who wish to open the gate on the far heights, play the song passed down by the Royal Family.\n-0580 Those who find a Small Key can advance to the next room. Those who don't can go home! //Unused\n-05C0 If you wish to speak to me, do so from the platform.\n-0600 Hi, LINK! Look this way! Look over here with Z, and talk to me with A.\n-0640 The current time is: time.\n-0680 Shine light on the living dead...\n-06C0 Those who break into the Royal Tomb will be obstructed by the lurkers in the dark.\n-0700 Hey, you! Young man, over there! Look over here, inside the cell!\n-0740 My little boy isn't here right now... I think he went to play in the graveyard...\n-0780 Oh, my boy is asleep right now. Please come back some other time to play with him!\n-07C0 When water fills the lake, shoot for the morning light.\n-0800 If you want to travel to the future, you should return here with the power of silver from the past.\n-0840 If you want to proceed to the past, you should return here with the pure heart of a child.\n-0880 This door is currently being refurbished. //Unused\n-08C0 It looks like something used to be set in this stand...\n-0900 Make my beak face the skull of truth. The alternative is descent into the deep darkness.\n-0940 This is not the correct key... the door won't open!\n-0980 Granny's Potion Shop / Closed / Gone for Field Study / Please come again! –Granny\n-09C0 Who's there? What a bad kid, trying to enter from the rear door! Such a bad kid... I have to tell you some juicy gossip! The boss carpenter has a son... He's the guy who sits under the tree every night... Don't tell the boss I told you that!\n-0A00 Look at this!\n-0A40 Malon's gone to sleep! I'm goin' to sleep now, too. Come back again when it's light out!\n-0A80 LINK's Records! / Spiders squished: 0 / Largest fish caught: 0 pounds / Marathon time: 00\"00\" / Horse race time: 00\"00\" / Horseback archery: 0 points\n-0AC0 The crest of the Royal Family is inscribed here.\n-0B00 R.I.P. / Here lie the souls of those who swore fealty to the Royal Family of Hyrule / The Sheikah, guardians of the Royal Family and founders of Kakariko, watch over these spirits in their eternal slumber.\n-0B40 Sleepless Waterfall / The flow of this waterfall serves the King of Hyrule. When the king slumbers, so too do these falls.\n-0B80 Some frogs are looking at you from underwater...\n-0BC0 You're standing on a soft carpet for guests... it feels so plush under your feet!\n-0C00 If you can overcome the trials in the chambers ahead, then and only then will you be qualified to hold our secret treasure!\n-0C40 If you desire to acquire our hidden treasure, you must strive to obtain the keys hidden in each chamber!\n-0C80 Defeat all the enemies in a limited time!\n-0CC0 Collect the underwater gems!\n-0D00 Cross the sea of fire!\n-0D40 Find a secret passage in this room!\n-0D80 Blind the eyes of the statue!\n-0DC0 One with silver hands shall move a giant block!\n-0E00 Without the necessary items, one will be confounded by impossible mysteries.\n-0E40 Gather the jewels of white, while avoiding traps and danger!\n-0E80 Fishing Pond / The fish are really biting today!\n-0EC0 ...???\n-0F00 The Shadow will yield only to one with the eye of truth, handed down in Kakariko Village.\n-0F40 You borrowed a Pocket Egg! ...\n+0x3F = Switch Flag"
  },
  "ACTOR_EN_GE2": {
    "name": "Patrolling Gerudo",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_GLA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Turns around, can't move, whistle-blower"
          },
          {
            "value": "1",
            "name": "Won't turn around, can't move, whistle-blower"
          },
          {
            "value": "2",
            "name": "Purple Gerudo, acts like the one that gives you the membership card"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Range of movements (type 0)",
        "mask": 3840
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_ROOMTIMER": {
    "name": "Room Timer",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Timer in Seconds",
        "mask": 1023
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 10,
        "type": "flag",
        "name": "Switch",
        "mask": 64512
      }
    ],
    "notes": "+0xFC00 = Switch Flag //Set on room clear\n+ 0x3FF = Timer in seconds (capped to 10 minutes)\n-03FF No Timer"
  },
  "ACTOR_EN_SSH": {
    "name": "Cursed Skulltula People",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_SSH"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Main Hyliantula"
          },
          {
            "value": "1",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "2",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "3",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "4",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "5",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "6",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "7",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "8",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "9",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "10",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "11",
            "name": "Hyliantula without an arm"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_STH": {
    "name": "Uncursed Skulltula People",
    "category": "ACTORCAT_NPC",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Main Hyliantula"
          },
          {
            "value": "1",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "2",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "3",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "4",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "5",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "6",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "7",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "8",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "9",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "10",
            "name": "Hyliantula without an arm"
          },
          {
            "value": "11",
            "name": "Hyliantula without an arm"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE": {
    "name": "Zelda's Lullaby Effect",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Purple wormhole"
          },
          {
            "value": "1",
            "name": "Blue wormhole"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_OCEFF_STORM": {
    "name": "Song of Storms Effect",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Regular effects, storm begins"
          },
          {
            "value": "1",
            "name": "Texture-spanning effect only, no storm, music isn't affected, effect does not dissipate"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WEIYER": {
    "name": "Stingray",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_EI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT05_SOKO": {
    "name": "Sacred Forest Meadow Water and Gate",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT05_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Large flat square floor tile, makes water noises when walking on it"
          },
          {
            "value": "1",
            "name": "Large vertical gate"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0x3F00 = Switch Flag //Type 01 only"
  },
  "ACTOR_BG_JYA_1FLIFT": {
    "name": "Stone Platform",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_JYA_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "+0x3F = Switch Flag"
  },
  "ACTOR_BG_JYA_HAHENIRON": {
    "name": "Pillar after being hit by Iron Knuckle",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_JYA_IRON"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Chair Crumble"
          },
          {
            "value": "1",
            "name": "Pillar Crumble"
          },
          {
            "value": "2",
            "name": "Round stone thing, explodes as soon as area loads"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT12_GATE": {
    "name": "Gerudo Fortress wooden gate",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT12_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": "+0xFF = Nullable Switch Flag, has cutscene with absolute position\n      Hardcoded cutscene camera: -1134, 112, -3075"
  },
  "ACTOR_BG_SPOT12_SAKU": {
    "name": "Gerudo Training Area Gate",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT12_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 255
      }
    ],
    "notes": "+0xFF = Nullable Switch Flag\n      Camera cutscene points to X 38 Y 333 Z -1097"
  },
  "ACTOR_EN_HINTNUTS": {
    "name": "Green Deku Scrubs",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_HINTNUTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65280,
        "values": [
          {
            "value": "6656",
            "name": "Scrub on path to Deku Slingshot"
          },
          {
            "value": "39680",
            "name": "231/312 Hint scrub"
          },
          {
            "value": "39936",
            "name": "Final Deku Scrub"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Puzzle Scrub #",
        "mask": 255
      }
    ],
    "notes": "+0xFF = used to set up 231/312 puzzle\n-00 Normal, talks when caught\n-01 First scrub to hit //0001 for no dialog?\n-02 Second scrub to hit //0002 for no dialog?\n-03 Third scrub to hit, talks when caught"
  },
  "ACTOR_EN_NUTSBALL": {
    "name": "Deku Scrub's Attack",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Mad Scrubs"
          },
          {
            "value": "1",
            "name": "Hint Scrubs"
          },
          {
            "value": "2",
            "name": "Business Scrubs"
          },
          {
            "value": "3",
            "name": "Forest Stage Judge"
          },
          {
            "value": "4",
            "name": "Forest Stage Patron"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT00_BREAK": {
    "name": "Broken Drawbridge",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT00_BREAK"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Broken Drawbridge"
          },
          {
            "value": "1",
            "name": "Fences (Hyrule Field)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_SHOPNUTS": {
    "name": "Grounded Business Scrub",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_SHOPNUTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Deku Nuts"
          },
          {
            "value": "1",
            "name": "Deku Sticks"
          },
          {
            "value": "2",
            "name": "Piece of Heart"
          },
          {
            "value": "3",
            "name": "Deku Seeds"
          },
          {
            "value": "4",
            "name": "Deku Shield"
          },
          {
            "value": "5",
            "name": "Bombs"
          },
          {
            "value": "6",
            "name": "Deku Seeds"
          },
          {
            "value": "7",
            "name": "Red Potion"
          },
          {
            "value": "8",
            "name": "Green Potion"
          },
          {
            "value": "9",
            "name": "Deku Stick Upgrade"
          },
          {
            "value": "10",
            "name": "Deku Nut Upgrade"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_IT": {
    "name": "Dampe Game Collectables",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GELDB": {
    "name": "Gerudo Fighter",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_GELDB"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Fight Switch Flag",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Collectible",
        "mask": 65280
      }
    ],
    "notes": "+0x3F00 = switch flag when you defeat her AND collectible flag for her key\n      (if the collectible flag XX is already set she will not give you a key)\n      ZRot = switch to make her spawn"
  },
  "ACTOR_OCEFF_WIPE2": {
    "name": "Epona's Song Effect",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE3": {
    "name": "Saria's Song Effect",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NIW_GIRL": {
    "name": "Child Chasing Cucco in Hyrule Market",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_GR",
      "OBJECT_NIW"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DOG": {
    "name": "Dog",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DOG"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Dirty blond dog"
          },
          {
            "value": "1",
            "name": "Chocolate brown dog"
          },
          {
            "value": "2",
            "name": "Red dog"
          },
          {
            "value": "3",
            "name": "Multicolored, flashing dog"
          },
          {
            "value": "4",
            "name": "Red dog"
          },
          {
            "value": "5",
            "name": "Multicolored, flashing dog"
          },
          {
            "value": "6",
            "name": "Dark dog (black fur)"
          },
          {
            "value": "7",
            "name": "Green dog"
          },
          {
            "value": "8",
            "name": "Red dog"
          },
          {
            "value": "9",
            "name": "Purple dog"
          },
          {
            "value": "10",
            "name": "Red dog"
          },
          {
            "value": "11",
            "name": "Green dog"
          },
          {
            "value": "12",
            "name": "Green dog"
          },
          {
            "value": "13",
            "name": "Black dog"
          },
          {
            "value": "14",
            "name": "Black dog"
          },
          {
            "value": "15",
            "name": "Purple dog"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 4,
        "type": "number",
        "name": "Path ID",
        "mask": 240
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Dog ID (0 for Richard)",
        "mask": 3840
      }
    ],
    "notes": "+0x0FF0 = Direction it runs //Orientation relative to the object. Also, if you befriend a dog and return to the place with the dog and change this value, another dog will spawn\n-0000 SEE\n-0010 NNW\n-0020 N\n-0030 First NNW, then SE\n-0040 First NNW, then SE\n-0050 First NNW, then SE\n-0060 N\n-0070 First NNW, then S\n-0080 First NNW, then SE\n-0090 N\n-00A0 First NNW, then SE\n-00B0 NW\n-00C0 NW\n-00D0 NW\n-00E0 NW\n-00F0+ First NNW, then SEE\n-0F00+ No dog"
  },
  "ACTOR_EN_SI": {
    "name": "Golden Skulltula Token",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_ST"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path id (type 3)",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT01_OBJECTS2": {
    "name": "Semi-Built Shooting Gallery",
    "category": "ACTORCAT_BG",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 7,
        "values": [
          {
            "value": "0",
            "name": "Potion Shop Poster"
          },
          {
            "value": "1",
            "name": "Shooting Gallery Poster"
          },
          {
            "value": "2",
            "name": "Bazaar Poster"
          },
          {
            "value": "3",
            "name": "Shooting Gallery (Partially Complete)"
          },
          {
            "value": "4",
            "name": "Shooting Gallery (Complete)"
          }
        ]
      },
      {
        "typeFilter": [
          3
        ],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_COMB": {
    "name": "Beehive",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "collectible",
        "name": "Item",
        "mask": 31
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Collectible",
        "mask": 16128
      }
    ],
    "notes": "graphics very glitchy in certain areas"
  },
  "ACTOR_BG_SPOT11_BAKUDANKABE": {
    "name": "Desert Colossus Bombable Wall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT11_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_KIBAKO2": {
    "name": "Crate",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_KIBAKO2"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "xrot",
        "shift": 0,
        "type": "collectible",
        "name": "Item",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Skulltula Var. (Nullable)",
        "mask": 65535
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "flag",
        "name": "Collectible",
        "mask": 63
      }
    ],
    "notes": "0xFFFF = Spawned Skulltula Variable (see actor 0095)\n-FFFF No Skulltula\nX ROTATION +0xFF = Collectible Item Dropped (see actor 0015)\nZ ROTATION +0x3F = Collectible Flag"
  },
  "ACTOR_EN_DNT_DEMO": {
    "name": "Deku Stage Cutscene",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DNT_JIJI": {
    "name": "Deku Stage Judge",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DNS"
    ],
    "params": [],
    "notes": "Spawned by Deku Panel"
  },
  "ACTOR_EN_DNT_NOMAL": {
    "name": "Deku Panel",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "65535",
            "name": "Default"
          }
        ]
      }
    ],
    "notes": "Yields deku seed upgrade"
  },
  "ACTOR_EN_GUEST": {
    "name": "Man in Purple Pants and White Top",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_BOJ"
    ],
    "params": [],
    "notes": "may cause crashes in some areas"
  },
  "ACTOR_BG_BOM_GUARD": {
    "name": "Bombchu Bowling Alley Game",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_BOWL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HS2": {
    "name": "Carpenter's Son (Child)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_HS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_KEKKAI": {
    "name": "Ganon's Tower Magic Barrier",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_DEMO_KEKKAI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Magic Barrier"
          },
          {
            "value": "1",
            "name": "Blue Magic Core"
          },
          {
            "value": "2",
            "name": "Yellow Magic Core"
          },
          {
            "value": "3",
            "name": "Red Magic Core"
          },
          {
            "value": "4",
            "name": "Purple Magic Core"
          },
          {
            "value": "5",
            "name": "Orange Magic Core"
          },
          {
            "value": "6",
            "name": "Green Magic Core"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT08_BAKUDANKABE": {
    "name": "Zora's Fountain Bombable Wall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP",
      "OBJECT_SPOT08_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "Graphics may glitch when it's destroyed, but will fix after it disappears\n+0x3F = Switch Flag"
  },
  "ACTOR_BG_SPOT17_BAKUDANKABE": {
    "name": "Death Mountain Crater Bombable Wall",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP",
      "OBJECT_SPOT17_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "Graphics may glitch when it's destroyed, but will fix after it disappears"
  },
  "ACTOR_OBJ_MURE3": {
    "name": "Tower of Rupees",
    "category": "ACTORCAT_BG",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 57344,
        "values": [
          {
            "value": "0",
            "name": "Five Blue Rupees stacked vertically"
          },
          {
            "value": "8192",
            "name": "Five Green Rupees in a row"
          },
          {
            "value": "16384",
            "name": "Six Green Rupees in a circle with a Red Rupee in the center"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_TG": {
    "name": "Honey and Darling",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MU"
    ],
    "params": [],
    "notes": "Text ID 7025, then 7026 then 7025…."
  },
  "ACTOR_EN_MU": {
    "name": "Haggling Townspeople",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MU"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Blue/green textures"
          },
          {
            "value": "1",
            "name": "Green/brown textures"
          },
          {
            "value": "2",
            "name": "Green/red textures"
          },
          {
            "value": "3",
            "name": "Purple/red textures"
          },
          {
            "value": "4",
            "name": "Orange/red textures"
          },
          {
            "value": "5",
            "name": "Purple/black textures"
          },
          {
            "value": "6",
            "name": "Flashing purple/blue textures"
          },
          {
            "value": "7",
            "name": "Flashing purple/cyan/green textures, depends on movement of camera"
          },
          {
            "value": "8",
            "name": "Purple/red textures"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_GO2": {
    "name": "Gorons and Biggoron",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 31,
        "values": [
          {
            "value": "0",
            "name": "Big Rollin' Goron"
          },
          {
            "value": "1",
            "name": "Link the Goron"
          },
          {
            "value": "2",
            "name": "Biggoron"
          },
          {
            "value": "3",
            "name": "Generic Fire Temple Goron"
          },
          {
            "value": "4",
            "name": "Goron from DMT near a bomb flower"
          },
          {
            "value": "5",
            "name": "Rolling Goron from DMT"
          },
          {
            "value": "6",
            "name": "Goron near Dodongo's Cavern Entrance"
          },
          {
            "value": "7",
            "name": "Goron at the entrance of Goron City"
          },
          {
            "value": "8",
            "name": "Goron on the island from Goron City"
          },
          {
            "value": "9",
            "name": "Goron near Darunia's room"
          },
          {
            "value": "10",
            "name": "Goron in the stairwell in Goron City"
          },
          {
            "value": "11",
            "name": "Goron near Lost Woods"
          },
          {
            "value": "12",
            "name": "Goron talking about the Great Fairy in DMT"
          },
          {
            "value": "13",
            "name": "Goron in Market's Bazaar"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 10,
        "type": "number",
        "name": "Fire Temple Goron ID (Nullable)",
        "mask": 64512
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 5,
        "type": "number",
        "name": "Path ID (Nullable)",
        "mask": 992
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Total Waypoints (Goron Link's path)",
        "mask": 255
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WF": {
    "name": "Wolfos",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_WF"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Normal"
          },
          {
            "value": "1",
            "name": "White Wolfos, mini-boss music starts"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Nullable Switch Flag"
  },
  "ACTOR_EN_SKB": {
    "name": "Stalchildren",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_SKB"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 1,
        "type": "number",
        "name": "Size",
        "mask": 1038
      }
    ],
    "notes": "//0000 to 1098 controls the size, where 0000 is normal size and 1098 is as large as is possible for the machine to handle (may vary depending on the level).\n//From F02F to FFFF, the Stalchild will appear but will somehow invert everything. In other words, it walks upside-down underneath the ground.\n//Really weird. FFFF is smallest, F02F is supposedly largest (I haven't been able to see it, though)."
  },
  "ACTOR_DEMO_GJ": {
    "name": "Ganon Battle Rubble",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 127,
        "values": [
          {
            "value": "4",
            "name": "Around Arena, indestructible rubble"
          },
          {
            "value": "8",
            "name": "Rubble Pile 1 (where Ganondorf rises)"
          },
          {
            "value": "9",
            "name": "Rubble Pile 2 (where Ganondorf rises)"
          },
          {
            "value": "10",
            "name": "Rubble Pile 3 (where Ganondorf rises)"
          },
          {
            "value": "11",
            "name": "Rubble Pile 4 (where Ganondorf rises)"
          },
          {
            "value": "12",
            "name": "Rubble Pile 5 (where Ganondorf rises)"
          },
          {
            "value": "13",
            "name": "Rubble Pile 6 (where Ganondorf rises)"
          },
          {
            "value": "14",
            "name": "Rubble Pile 7 (where Ganondorf rises)"
          },
          {
            "value": "16",
            "name": "'Sun' destructible rubble, drops collectible (draw depends on Ganon)"
          },
          {
            "value": "17",
            "name": "'Wall' destructible rubble, drops collectible (draw depends on Ganon)"
          },
          {
            "value": "22",
            "name": "Tall destructible rubble, drops collectible (draw depends on Ganon)"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 10,
        "type": "collectible",
        "name": "Item",
        "mask": 64512
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 7,
        "type": "number",
        "name": "Collectible Amount",
        "mask": 896
      }
    ],
    "notes": ""
  },
  "ACTOR_DEMO_GEFF": {
    "name": "Crumbling Stone",
    "category": "ACTORCAT_BOSS",
    "objDeps": [
      "OBJECT_GEFF"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Large chunk"
          },
          {
            "value": "1",
            "name": "Medium chunk"
          },
          {
            "value": "2",
            "name": "Small chunk"
          },
          {
            "value": "3",
            "name": "Large chunk"
          },
          {
            "value": "4",
            "name": "Medium chunk"
          },
          {
            "value": "5",
            "name": "Small chunk"
          },
          {
            "value": "6",
            "name": "Large chunk"
          },
          {
            "value": "7",
            "name": "Medium chunk"
          },
          {
            "value": "8",
            "name": "Small chunk"
          },
          {
            "value": "9",
            "name": "Small chunk"
          }
        ]
      }
    ],
    "notes": "non-solid, possibly Ganon's Tower rubble"
  },
  "ACTOR_BG_GND_FIREMEIRO": {
    "name": "Sinkable Lava Platform",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_DEMO_KEKKAI"
    ],
    "params": [],
    "notes": "Narrow, maze-like platform (sinks)"
  },
  "ACTOR_BG_GND_DARKMEIRO": {
    "name": "Invisible/Transparent Platforms",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_DEMO_KEKKAI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Invisible Path"
          },
          {
            "value": "1",
            "name": "Glass Block, appears on Switch Flag"
          },
          {
            "value": "2",
            "name": "Invisible Timer"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": "+0xFF00 = Nullable Switch Flag //If null, Enabled and ignoring switch flag input\n//The timer works as such:\n//The clear block's invisible timer actor manipulates the value of three switch flags:\n//The Switch Flag (FlagA), Flag + 1 (FlagB), Flag + 2 (FlagC)\n//FlagA is set to 0 if FlagB and FlagC are 0\n//FlagA is set to 1 and timer started if either FlagB or FlagC are 1\n//Before timer ends, FlagA is set to 0\n//Aprox. 1 second later, FlagB and FlagC are zeroed\n//Clear Block actors disappear ~2 seconds after the Switch Flag is zeroed to account for\n//FlagA being zeroed before the timer runs out"
  },
  "ACTOR_BG_GND_SOULMEIRO": {
    "name": "Web Blocked Light Window",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_DEMO_KEKKAI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Ceiling Web"
          },
          {
            "value": "1",
            "name": "Light Source (draws when web burned)"
          },
          {
            "value": "2",
            "name": "Light Floor (draws when web burned)"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_GND_NISEKABE": {
    "name": "2D Stone Wall",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_DEMO_KEKKAI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Square Stone"
          },
          {
            "value": "1",
            "name": "Stone Brick"
          },
          {
            "value": "2",
            "name": "Similar to 1, but looks like a different texture"
          },
          {
            "value": "3",
            "name": "+ Graphics glitchy"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_GND_ICEBLOCK": {
    "name": "Large Ice Cube",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_DEMO_KEKKAI"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Unknown",
        "mask": 255
      }
    ],
    "notes": "//Actor 00D6 is used in Ice Cavern"
  },
  "ACTOR_EN_GB": {
    "name": "Poe Collector",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_PS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GS": {
    "name": "Gossip Stone",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "This is a Gossip Stone!"
          },
          {
            "value": "1",
            "name": "They say you can swim faster by continuously pressing B."
          },
          {
            "value": "2",
            "name": "They say there is a secret near the lone tree which is not far from the river in the northwest part of Hyrule Field."
          },
          {
            "value": "3",
            "name": "They say that there is a secret on the road that leads to Lake Hylia."
          },
          {
            "value": "4",
            "name": "They say that Biggoron's Sword is super sharp and will never break."
          },
          {
            "value": "5",
            "name": "They say that Medigoron didn't really think about his own size, so his store is really cramped."
          },
          {
            "value": "6",
            "name": "They say that Malon set the original record in the obstacle course of Lon Lon Ranch."
          },
          {
            "value": "7",
            "name": "They say that Malon of Lon Lon Ranch hopes a knight in shining armor will come and sweep her off her feet someday."
          },
          {
            "value": "8",
            "name": "They say that Ruto, the Zora princess who is known for her selfish nature, likes a certain boy..."
          },
          {
            "value": "9",
            "name": "They say that players who select the “HOLD” option for “Z TARGETING” are the real “Zelda players!”"
          },
          {
            "value": "10",
            "name": "They say that there is a secret near a tree in Kakariko Village."
          },
          {
            "value": "11",
            "name": "They say that, contrary to her elegant image, Princess Zelda of Hyrule Castle is, in fact, a tomboy!"
          },
          {
            "value": "12",
            "name": "They say that Princess Zelda's nanny is actually one of the Sheikah, who many thought had died out."
          },
          {
            "value": "13",
            "name": "They say there is a man who can always be found running around in Hyrule Field."
          },
          {
            "value": "14",
            "name": "They say that it is against the rules to use glasses at the Treasure Chest Shop in Hyrule Castle Town Market."
          },
          {
            "value": "15",
            "name": "They say that the chicken lady goes to the Lakeside Laboratory to study how to breed pocket-sized Cuccos."
          },
          {
            "value": "16",
            "name": "They say that Gerudos sometimes come to Hyrule Castle Town to look for boyfriends."
          },
          {
            "value": "17",
            "name": "They say that the thief named Nabooru, who haunts this area, is a Gerudo."
          },
          {
            "value": "18",
            "name": "They say that if you get close to a butterfly while holding a Deku Stick in your hands, something good will happen."
          },
          {
            "value": "19",
            "name": "They say that you may find something new in dungeons that you have already finished."
          },
          {
            "value": "20",
            "name": "They say that Gerudos worship Ganondorf almost like a god."
          },
          {
            "value": "21",
            "name": "They say that there is a secret around the entrance to Gerudo Valley."
          },
          {
            "value": "22",
            "name": "They say that the owl named Kaepora Gaebora is the reincarnation of an ancient Sage."
          },
          {
            "value": "23",
            "name": "They say that strange owl, Kaepora Gaebora, may look big and heavy, but its character is rather lighthearted."
          },
          {
            "value": "24",
            "name": "They say that the horse Ganondorf rides is a solid black Gerudo stallion."
          },
          {
            "value": "25",
            "name": "They say that Ganondorf is not satisfied with ruling only the Gerudo and aims to conquer all of Hyrule!"
          },
          {
            "value": "26",
            "name": "They say that the treasure you can earn in the Gerudo's Training Ground is not as great as you would expect, given its difficulty!"
          },
          {
            "value": "27",
            "name": "They say that there is a switch that you can activate only by using the Spin Attack."
          },
          {
            "value": "28",
            "name": "They say that it's possible to find a total of 100 Gold Skulltulas throughout Hyrule."
          },
          {
            "value": "29",
            "name": "They say that when non-fairy folk enter the Lost Woods, they become monsters!"
          },
          {
            "value": "30",
            "name": "They say that the small holes in the ground that you can find all over Hyrule make perfect breeding ground for bugs."
          },
          {
            "value": "31",
            "name": "They say that the Kokiri are always followed by small fairies."
          },
          {
            "value": "32",
            "name": "They say that one Kokiri has left the forest, but he is still alive!"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Collectible",
        "mask": 16128
      }
    ],
    "notes": "+0xFF00 Collectible Flag (fairy)"
  },
  "ACTOR_BG_MIZU_BWALL": {
    "name": "Water Temple Bombable Walls",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_MIZU_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Floor"
          },
          {
            "value": "1",
            "name": "Cracked Wall"
          },
          {
            "value": "2",
            "name": "Unused"
          },
          {
            "value": "3",
            "name": "Stinger Room 1"
          },
          {
            "value": "4",
            "name": "Stinger Room 2"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 16128
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_MIZU_SHUTTER": {
    "name": "Metal Gate",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_MIZU_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Vertical"
          },
          {
            "value": "4096",
            "name": "Horizontal"
          },
          {
            "value": "8192",
            "name": "+ Invalid"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "number",
        "name": "Timer (Seconds)",
        "mask": 4032
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": "0x0FC0 = Timer (Seconds)\n+0x3F = Switch Flag"
  },
  "ACTOR_EN_DAIKU_KAKARIKO": {
    "name": "Carpenters in Kakariko",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_DAIKU"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 255,
        "values": [
          {
            "value": "0",
            "name": "Ichiro"
          },
          {
            "value": "1",
            "name": "Sabooro"
          },
          {
            "value": "2",
            "name": "Jiro"
          },
          {
            "value": "3",
            "name": "Shiro"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path Id",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 = Path\n-FF None"
  },
  "ACTOR_BG_BOWL_WALL": {
    "name": "Bombchu Bowling Alley Wall",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_BOWL"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "First Wall"
          },
          {
            "value": "1",
            "name": "Second Wall"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_WALL_TUBO": {
    "name": "Bombchu Bowling Bullseyes/Pits (used by game engine)",
    "category": "ACTORCAT_PROP",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "First Wall"
          },
          {
            "value": "1",
            "name": "Second Wall"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_PO_DESERT": {
    "name": "Haunted Wasteland Guiding Poe",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_PO_FIELD"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Path ID",
        "mask": 65280
      }
    ],
    "notes": "+0xFF00 path ID"
  },
  "ACTOR_EN_CROW": {
    "name": "Guay",
    "category": "ACTORCAT_ENEMY",
    "objDeps": [
      "OBJECT_CROW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DOOR_KILLER": {
    "name": "Fake Stomping Door",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_DOOR_KILLER"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Fake door"
          },
          {
            "value": "1",
            "name": "Debris"
          },
          {
            "value": "2",
            "name": "Debris"
          },
          {
            "value": "3",
            "name": "Debris"
          },
          {
            "value": "4",
            "name": "Debris"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 65280
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_SPOT11_OASIS": {
    "name": "Refilling Oasis",
    "category": "ACTORCAT_BG",
    "objDeps": [
      "OBJECT_SPOT11_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOT18_FUTA": {
    "name": "Lid to the Giant Goron Pot",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT18_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOT18_SHUTTER": {
    "name": "Goron Room Door",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_SPOT18_OBJ"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Horizontal Slide",
        "mask": 256
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_MA3": {
    "name": "Adult Malon (Ranch)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MA2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_COW": {
    "name": "Cow",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_COW"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Cow"
          },
          {
            "value": "1",
            "name": "Tail only"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_ICE_TURARA": {
    "name": "Ice Stalagmite/Stalagtite",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_ICE_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Stalagmite (floor)"
          },
          {
            "value": "1",
            "name": "Stalactite (ceiling)"
          },
          {
            "value": "2",
            "name": "Regrowing Stalactite (ceiling)"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_ICE_SHUTTER": {
    "name": "2D Ice Bars",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_ICE_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 3,
        "values": [
          {
            "value": "0",
            "name": "vertical, clear flag"
          },
          {
            "value": "1",
            "name": "vertical, switch softlock"
          },
          {
            "value": "2",
            "name": "horizontal, clear flag"
          }
        ]
      },
      {
        "typeFilter": [
          1
        ],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Switch",
        "mask": 3840
      }
    ],
    "notes": "+0x0F00 switchflag"
  },
  "ACTOR_EN_KAKASI2": {
    "name": "Scarecrow Song Spot",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_KA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 6,
        "type": "number",
        "name": "Spawn Range Y",
        "mask": 192
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Spawn Range XZ",
        "mask": 65535
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_KAKASI3": {
    "name": "Bonooru the Scarecrow",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_KA"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 2,
        "type": "number",
        "name": "Spawn Range Y",
        "mask": 1020
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Spawn Range XZ",
        "mask": 65535
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE4": {
    "name": "Song of Time Effect",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Scarecrow song effect?"
          },
          {
            "value": "1",
            "name": "purple"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_EG": {
    "name": "Void Trigger",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_ZL2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MENKURI_NISEKABE": {
    "name": "Circular Piece of Stone Wall",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_MENKURI_OBJECTS"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Circular piece of false stone wall"
          },
          {
            "value": "1",
            "name": "Square piece of false stone wall"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ZO": {
    "name": "Zora",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ZO"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 63,
        "values": [
          {
            "value": "0",
            "name": "Zora near the ladder/grotto"
          },
          {
            "value": "1",
            "name": "Zora near the shop"
          },
          {
            "value": "2",
            "name": "Zora near the fishes"
          },
          {
            "value": "3",
            "name": "Zora near the Lake Hylia exit"
          },
          {
            "value": "4",
            "name": "Zora near the grotto platform"
          },
          {
            "value": "5",
            "name": "Zora between ladder and grotto platform"
          },
          {
            "value": "6",
            "name": "Zora near the Lakeside Laboratory"
          },
          {
            "value": "7",
            "name": "Zora near the Domain exit"
          },
          {
            "value": "8",
            "name": "Zora from Zora Shop"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Dialog",
        "mask": 15
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_MAKEKINSUTA": {
    "name": "Skulltula Sprouting from Bean Spot",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "number",
        "name": "Location",
        "mask": 7936
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "number",
        "name": "Skulltula Flag (Mask: 0x00FF)",
        "mask": 16639
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Spawns at night",
        "mask": 8192
      }
    ],
    "notes": "See Actor 0095 for variable"
  },
  "ACTOR_EN_GE3": {
    "name": "Gerudo giving you Membership Card",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_GELDB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TIMEBLOCK": {
    "name": "Time Block",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_TIMEBLOCK",
      "OBJECT_EFC_TW"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 32768,
        "values": [
          {
            "value": "0",
            "name": "Child Visible"
          },
          {
            "value": "32768",
            "name": "Adult Visible"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 11,
        "type": "number",
        "name": "Song of Time Distance",
        "mask": 14336
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "number",
        "name": "Color",
        "mask": 7
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Appears on Switch Flag",
        "mask": 1024
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Small",
        "mask": 256
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Age Inverted",
        "mask": 64
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_HAMISHI": {
    "name": "Bronze Boulder",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_FIELD_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ZL4": {
    "name": "Princess Zelda (Child - Cutscene)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_ZL4"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MM2": {
    "name": "Running Man (Adult)",
    "category": "ACTORCAT_NPC",
    "objDeps": [
      "OBJECT_MM"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Tent, starts the race"
          },
          {
            "value": "1",
            "name": "Waiting in Lost Woods, ends the race"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_BG_JYA_BLOCK": {
    "name": "Young Link Silver Block",
    "category": "ACTORCAT_PROP",
    "objDeps": [
      "OBJECT_GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_OBJ_WARP2BLOCK": {
    "name": "Exchangeable Time Block",
    "category": "ACTORCAT_ITEMACTION",
    "objDeps": [
      "OBJECT_TIMEBLOCK"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 32768,
        "values": [
          {
            "value": "0",
            "name": "Invisible"
          },
          {
            "value": "32768",
            "name": "Visible"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 11,
        "type": "number",
        "name": "Range",
        "mask": 14336
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Is Small",
        "mask": 256
      }
    ],
    "notes": ""
  }
};
