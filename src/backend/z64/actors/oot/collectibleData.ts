//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
export const collectibleData: Array<{name: string, value: string}> = [
  {
    "value": "0",
    "name": "Green Rupee"
  },
  {
    "value": "1",
    "name": "Blue Rupee"
  },
  {
    "value": "2",
    "name": "Red Rupee"
  },
  {
    "value": "3",
    "name": "Recovery Heart"
  },
  {
    "value": "4",
    "name": "Bombs (5)"
  },
  {
    "value": "5",
    "name": "Arrows (1)"
  },
  {
    "value": "6",
    "name": "Heart Piece"
  },
  {
    "value": "7",
    "name": "Heart Container"
  },
  {
    "value": "8",
    "name": "Arrows (5)"
  },
  {
    "value": "9",
    "name": "Arrows (10)"
  },
  {
    "value": "10",
    "name": "Arrows (30)"
  },
  {
    "value": "11",
    "name": "Bombs (5)"
  },
  {
    "value": "12",
    "name": "Deku Nuts"
  },
  {
    "value": "13",
    "name": "Deku Sticks"
  },
  {
    "value": "14",
    "name": "Large Magic Jar"
  },
  {
    "value": "15",
    "name": "Small Magic Jar"
  },
  {
    "value": "16",
    "name": "Deku Seeds"
  },
  {
    "value": "17",
    "name": "Small Key"
  },
  {
    "value": "18",
    "name": "Flexible Drop"
  },
  {
    "value": "19",
    "name": "Orange Rupee (200)"
  },
  {
    "value": "20",
    "name": "Purple Rupee (50)"
  },
  {
    "value": "21",
    "name": "Deku Shield"
  },
  {
    "value": "22",
    "name": "Hylian Shield"
  },
  {
    "value": "23",
    "name": "Zora Tunic"
  },
  {
    "value": "24",
    "name": "Goron Tunic"
  },
  {
    "value": "25",
    "name": "Bombs (None/Fake)"
  }
];
