//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
import { ActorData } from '../actorDataType';
export const actorData: ActorData = {
  "ACTOR_PLAYER": {
    "name": "Player",
    "category": "",
    "objDeps": [
      "OBJECT_UNSET_0"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TEST": {
    "name": "En_Test",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GIRLA": {
    "name": "En_GirlA",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PART": {
    "name": "En_Part",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_LIGHT": {
    "name": "Flame",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 15,
        "values": [
          {
            "value": "0",
            "name": "Large Orange Flame"
          },
          {
            "value": "1",
            "name": "Large Orange Flame"
          },
          {
            "value": "2",
            "name": "Large Blue Flame"
          },
          {
            "value": "3",
            "name": "Large Green Flame"
          },
          {
            "value": "4",
            "name": "Small Orange Flame"
          },
          {
            "value": "5",
            "name": "Large Orange Flame"
          },
          {
            "value": "6",
            "name": "Large Green Flame"
          },
          {
            "value": "7",
            "name": "Large Blue Flame"
          },
          {
            "value": "8",
            "name": "Large Magenta Flame"
          },
          {
            "value": "9",
            "name": "Large Pale Orange Flame"
          },
          {
            "value": "10",
            "name": "Large Pale Yellow Flame"
          },
          {
            "value": "11",
            "name": "Large Pale Green Flame"
          },
          {
            "value": "12",
            "name": "Large Pale Pink Flame"
          },
          {
            "value": "13",
            "name": "Large Pale Purple Flame"
          },
          {
            "value": "14",
            "name": "Large Pale Indigo Flame"
          },
          {
            "value": "15",
            "name": "Large Pale Blue Flame"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_DOOR": {
    "name": "Wooden Door",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOX": {
    "name": "Treasure Chest",
    "category": "",
    "objDeps": [
      "OBJECT_BOX"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 61440,
        "values": [
          {
            "value": "0",
            "name": "Golden"
          },
          {
            "value": "4096",
            "name": "Golden - Appears - Clear Flag"
          },
          {
            "value": "8192",
            "name": "Boss Key Chest"
          },
          {
            "value": "12288",
            "name": "Golden - Falls - Switch Flag"
          },
          {
            "value": "16384",
            "name": "Golden - Invisible"
          },
          {
            "value": "20480",
            "name": "Wooden"
          },
          {
            "value": "24576",
            "name": "Wooden - Invisible"
          },
          {
            "value": "28672",
            "name": "Wooden - Clear Flag"
          },
          {
            "value": "32768",
            "name": "Wooden - Falls - Switch Flag"
          },
          {
            "value": "36864",
            "name": "Crash"
          },
          {
            "value": "40960",
            "name": "Crash"
          },
          {
            "value": "45056",
            "name": "Golden - Appears - Switch Flag"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 5,
        "type": "chest",
        "name": "Content",
        "mask": 4064
      },
      {
        "typeFilter": [],
        "target": "zrot",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 511
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Chest",
        "mask": 31
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_PAMETFROG": {
    "name": "Gekko and Snapper Miniboss",
    "category": "",
    "objDeps": [
      "OBJECT_BIGSLIME"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OKUTA": {
    "name": "Octorok",
    "category": "",
    "objDeps": [
      "OBJECT_OKUTA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOM": {
    "name": "Powder Keg",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WALLMAS": {
    "name": "Wallmaster",
    "category": "",
    "objDeps": [
      "OBJECT_WALLMASTER"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DODONGO": {
    "name": "Dodongo",
    "category": "",
    "objDeps": [
      "OBJECT_DODONGO"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 65535,
        "values": [
          {
            "value": "0",
            "name": "Regular"
          },
          {
            "value": "1",
            "name": "Large"
          }
        ]
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_FIREFLY": {
    "name": "Keese",
    "category": "",
    "objDeps": [
      "OBJECT_FIREFLY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HORSE": {
    "name": "Child Epona (Cutscenes)",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ITEM00": {
    "name": "Collectible Items",
    "category": "",
    "objDeps": [
      "OBJECT_UNSET_0"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "collectible",
        "name": "Item",
        "mask": 255
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 8,
        "type": "flag",
        "name": "Collectible",
        "mask": 32512
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "bool",
        "name": "Obtain on Load",
        "mask": 32768
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_ARROW": {
    "name": "Arrow",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ELF": {
    "name": "Healing Fairy and Tatl (Gameplay)",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NIW": {
    "name": "Cucco",
    "category": "",
    "objDeps": [
      "OBJECT_NIW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TITE": {
    "name": "Tektite",
    "category": "",
    "objDeps": [
      "OBJECT_TITE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PEEHAT": {
    "name": "Peahat",
    "category": "",
    "objDeps": [
      "OBJECT_PH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BUTTE": {
    "name": "Butterfly",
    "category": "",
    "objDeps": [
      "GAMEPLAY_FIELD_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_INSECT": {
    "name": "Bug",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FISH": {
    "name": "Fish",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HOLL": {
    "name": "Black Room Transition Plane",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DINOFOS": {
    "name": "Dinolfos",
    "category": "",
    "objDeps": [
      "OBJECT_DINOFOS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HATA": {
    "name": "Red Flag on Post",
    "category": "",
    "objDeps": [
      "OBJECT_HATA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZL1": {
    "name": "Child Zelda",
    "category": "",
    "objDeps": [
      "OBJECT_ZL1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_VIEWER": {
    "name": "En_Viewer",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BUBBLE": {
    "name": "Shabom",
    "category": "",
    "objDeps": [
      "OBJECT_BUBBLE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DOOR_SHUTTER": {
    "name": "Dungeon Door",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOOM": {
    "name": "Zora Fins",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TORCH2": {
    "name": "Elegy Statues",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MINIFROG": {
    "name": "Frog",
    "category": "",
    "objDeps": [
      "OBJECT_FR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ST": {
    "name": "Skulltula",
    "category": "",
    "objDeps": [
      "OBJECT_ST"
    ],
    "params": [
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "type",
        "name": "Type",
        "mask": 64,
        "values": [
          {
            "value": "0",
            "name": "Default"
          },
          {
            "value": "64",
            "name": "Invisible"
          }
        ]
      },
      {
        "typeFilter": [],
        "target": "params",
        "shift": 0,
        "type": "flag",
        "name": "Switch",
        "mask": 63
      }
    ],
    "notes": ""
  },
  "ACTOR_EN_A_OBJ": {
    "name": "Directional Sign and Square Sign [Early]",
    "category": "",
    "objDeps": [
      "OBJECT_UNSET_0"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_WTURN": {
    "name": "Stone Tower Temple Inverter",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RIVER_SOUND": {
    "name": "Sound Effects I",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OSSAN": {
    "name": "Middle-Aged Man",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FAMOS": {
    "name": "Death Armos",
    "category": "",
    "objDeps": [
      "OBJECT_FAMOS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOMBF": {
    "name": "Bomb Flower",
    "category": "",
    "objDeps": [
      "OBJECT_BOMBF"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AM": {
    "name": "Armos",
    "category": "",
    "objDeps": [
      "OBJECT_AM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DEKUBABA": {
    "name": "Deku Baba",
    "category": "",
    "objDeps": [
      "OBJECT_DEKUBABA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_M_FIRE1": {
    "name": "Deku Nut Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_M_THUNDER": {
    "name": "Spin Attack and Sword Beam Effects",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_BREAKWALL": {
    "name": "Post Office Objects",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DOOR_WARP1": {
    "name": "Blue Warp",
    "category": "",
    "objDeps": [
      "OBJECT_WARP1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SYOKUDAI": {
    "name": "Torch",
    "category": "",
    "objDeps": [
      "OBJECT_SYOKUDAI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ITEM_B_HEART": {
    "name": "Heart Container (Boss Lairs)",
    "category": "",
    "objDeps": [
      "OBJECT_GI_HEARTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DEKUNUTS": {
    "name": "Mad Scrub",
    "category": "",
    "objDeps": [
      "OBJECT_DEKUNUTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BBFALL": {
    "name": "Red Bubble",
    "category": "",
    "objDeps": [
      "OBJECT_BB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ARMS_HOOK": {
    "name": "Hookshot",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BB": {
    "name": "Blue Bubble",
    "category": "",
    "objDeps": [
      "OBJECT_BB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_KEIKOKU_SPR": {
    "name": "Fountain Water",
    "category": "",
    "objDeps": [
      "OBJECT_KEIKOKU_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WOOD02": {
    "name": "Greenery",
    "category": "",
    "objDeps": [
      "OBJECT_WOOD02"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DEATH": {
    "name": "Gomess",
    "category": "",
    "objDeps": [
      "OBJECT_DEATH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MINIDEATH": {
    "name": "Gomess' Bats",
    "category": "",
    "objDeps": [
      "OBJECT_DEATH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_VM": {
    "name": "Beamos",
    "category": "",
    "objDeps": [
      "OBJECT_VM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_EFFECT": {
    "name": "Demo_Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_KANKYO": {
    "name": "Environment Effects",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FLOORMAS": {
    "name": "Floormaster",
    "category": "",
    "objDeps": [
      "OBJECT_WALLMASTER"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RD": {
    "name": "Redead",
    "category": "",
    "objDeps": [
      "OBJECT_RD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_F40_FLIFT": {
    "name": "Stone Tower Temple Elevator [Early]",
    "category": "",
    "objDeps": [
      "OBJECT_F40_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_UNSET_4E": {
    "name": "Golden Gauntlets Rock (JP 1.0 Only)",
    "category": "",
    "objDeps": [
      "OBJECT_UNSET_0"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MURE": {
    "name": "Fish, Bugs, Butterflies",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SW": {
    "name": "(Golden) Skulltula",
    "category": "",
    "objDeps": [
      "OBJECT_ST"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJECT_KANKYO": {
    "name": "Environment Effects 2?",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HORSE_LINK_CHILD": {
    "name": "Child Epona (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_HORSE_LINK_CHILD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DOOR_ANA": {
    "name": "Grotto Hole",
    "category": "",
    "objDeps": [
      "GAMEPLAY_FIELD_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENCOUNT1": {
    "name": "En_Encount1",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_TRE_LGT": {
    "name": "Treasure Chest Glow",
    "category": "",
    "objDeps": [
      "OBJECT_BOX"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENCOUNT2": {
    "name": "Majora Balloon",
    "category": "",
    "objDeps": [
      "OBJECT_FUSEN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FIRE_ROCK": {
    "name": "Rock and Beam of Light [OoT]",
    "category": "",
    "objDeps": [
      "OBJECT_EFC_STAR_FIELD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_CTOWER_ROT": {
    "name": "Clock Tower Helix Path",
    "category": "",
    "objDeps": [
      "OBJECT_CTOWER_ROT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_MIR_RAY": {
    "name": "Mirror Shield Light Ray I [?]",
    "category": "",
    "objDeps": [
      "OBJECT_MIR_RAY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SB": {
    "name": "Shellblade",
    "category": "",
    "objDeps": [
      "OBJECT_SB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BIGSLIME": {
    "name": "Mad Jelly",
    "category": "",
    "objDeps": [
      "OBJECT_BIGSLIME"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KAREBABA": {
    "name": "Deku Baba",
    "category": "",
    "objDeps": [
      "OBJECT_DEKUBABA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_IN": {
    "name": "Gorman Bros.",
    "category": "",
    "objDeps": [
      "OBJECT_IN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RU": {
    "name": "Adult Ruto [OoT]",
    "category": "",
    "objDeps": [
      "OBJECT_RU2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOM_CHU": {
    "name": "Bombchu",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HORSE_GAME_CHECK": {
    "name": "En_Horse_Game_Check",
    "category": "",
    "objDeps": [
      "OBJECT_HORSE_GAME_CHECK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RR": {
    "name": "Like Like",
    "category": "",
    "objDeps": [
      "OBJECT_RR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FR": {
    "name": "En_Fr",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FISHING": {
    "name": "Fishing Pond Owner (JP 1.0 Only)",
    "category": "",
    "objDeps": [
      "OBJECT_UNSET_0"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_OSHIHIKI": {
    "name": "Pushable Block",
    "category": "",
    "objDeps": [
      "GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EFF_DUST": {
    "name": "Spin Attack Charge Particles",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_UMAJUMP": {
    "name": "Horse Jumping Fence",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ARROW_FIRE": {
    "name": "Fire Arrow",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ARROW_ICE": {
    "name": "Ice Arrow",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ARROW_LIGHT": {
    "name": "Light Arrow",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ITEM_ETCETERA": {
    "name": "Item_Etcetera",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_KIBAKO": {
    "name": "Small Wooden Crate",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TSUBO": {
    "name": "Pot",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_IK": {
    "name": "Iron Knuckle",
    "category": "",
    "objDeps": [
      "OBJECT_IK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_SHD": {
    "name": "Demo_Shd",
    "category": "",
    "objDeps": [
      "OBJECT_FWALL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNS": {
    "name": "Deku Scrub Guard (Royal Chamber)",
    "category": "",
    "objDeps": [
      "OBJECT_DNS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ELF_MSG": {
    "name": "Elf_Msg",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HONOTRAP": {
    "name": "En_Honotrap",
    "category": "",
    "objDeps": [
      "GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TUBO_TRAP": {
    "name": "Flying Pot",
    "category": "",
    "objDeps": [
      "GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ICE_POLY": {
    "name": "Ice Sparkle Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FZ": {
    "name": "Freezard",
    "category": "",
    "objDeps": [
      "OBJECT_FZ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KUSA": {
    "name": "Cut-able Grass",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_BEAN": {
    "name": "Magic Bean Plant",
    "category": "",
    "objDeps": [
      "OBJECT_MAMENOKI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_BOMBIWA": {
    "name": "Bombable Rock",
    "category": "",
    "objDeps": [
      "OBJECT_BOMBIWA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SWITCH": {
    "name": "Dungeon Switches",
    "category": "",
    "objDeps": [
      "GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_LIFT": {
    "name": "Dampé's House Elevator",
    "category": "",
    "objDeps": [
      "OBJECT_D_LIFT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HSBLOCK": {
    "name": "Stone Hookshot Pillar",
    "category": "",
    "objDeps": [
      "OBJECT_D_HSBLOCK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OKARINA_TAG": {
    "name": "Ocarina Song Spot",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GOROIWA": {
    "name": "Snowball and Rolling Boulder [?]",
    "category": "",
    "objDeps": [
      "OBJECT_GOROIWA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DAIKU": {
    "name": "Carpenter (Clock Town)",
    "category": "",
    "objDeps": [
      "OBJECT_DAIKU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NWC": {
    "name": "Cucco Chick",
    "category": "",
    "objDeps": [
      "OBJECT_NWC"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ITEM_INBOX": {
    "name": "Item_Inbox",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GE1": {
    "name": "Pirate Lieutenant",
    "category": "",
    "objDeps": [
      "OBJECT_GE1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_BLOCKSTOP": {
    "name": "Obj_Blockstop",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SDA": {
    "name": "Dynamic Shadow (Glitchy)",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_CLEAR_TAG": {
    "name": "En_Clear_Tag",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GM": {
    "name": "Gorman",
    "category": "",
    "objDeps": [
      "OBJECT_IN2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MS": {
    "name": "Magic Bean Seller",
    "category": "",
    "objDeps": [
      "OBJECT_MS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HS": {
    "name": "Grog",
    "category": "",
    "objDeps": [
      "OBJECT_HS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_INGATE": {
    "name": "Boat Cruise Canoe",
    "category": "",
    "objDeps": [
      "OBJECT_SICHITAI_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KANBAN": {
    "name": "Square Signpost",
    "category": "",
    "objDeps": [
      "OBJECT_KANBAN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ATTACK_NIW": {
    "name": "Attacking Cucco",
    "category": "",
    "objDeps": [
      "OBJECT_NIW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MK": {
    "name": "Marine Scientist",
    "category": "",
    "objDeps": [
      "OBJECT_MK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OWL": {
    "name": "Owl",
    "category": "",
    "objDeps": [
      "OBJECT_OWL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ISHI": {
    "name": "Rock",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HANA": {
    "name": "Orange Graveyard Flower",
    "category": "",
    "objDeps": [
      "OBJECT_HANA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_LIGHTSWITCH": {
    "name": "Sun Switch",
    "category": "",
    "objDeps": [
      "OBJECT_LIGHTSWITCH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MURE2": {
    "name": "Grass and Rock Cluster",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FU": {
    "name": "Honey and Darling",
    "category": "",
    "objDeps": [
      "OBJECT_MU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_STREAM": {
    "name": "Water Spout",
    "category": "",
    "objDeps": [
      "OBJECT_STREAM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MM": {
    "name": "Rock Sirloin",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WEATHER_TAG": {
    "name": "En_Weather_Tag",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ANI": {
    "name": "Part-Timer",
    "category": "",
    "objDeps": [
      "OBJECT_ANI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JS": {
    "name": "Moon Child",
    "category": "",
    "objDeps": [
      "OBJECT_OB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OKARINA_EFFECT": {
    "name": "Song of Storms Effect I [?]",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MAG": {
    "name": "Title Logo",
    "category": "",
    "objDeps": [
      "OBJECT_MAG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ELF_MSG2": {
    "name": "Elf_Msg2",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_F40_SWLIFT": {
    "name": "Stone Tower Temple Platform [Early]",
    "category": "",
    "objDeps": [
      "OBJECT_F40_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KAKASI": {
    "name": "Scarecrow",
    "category": "",
    "objDeps": [
      "OBJECT_KA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MAKEOSHIHIKI": {
    "name": "Obj_Makeoshihiki",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_SPOT": {
    "name": "Sun's Song Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TORCH": {
    "name": "Treasure Chest (Grotto)",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_SHOT_SUN": {
    "name": "Shot_Sun",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ROOMTIMER": {
    "name": "Obj_Roomtimer",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SSH": {
    "name": "Cursed Skulltula Man",
    "category": "",
    "objDeps": [
      "OBJECT_SSH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE": {
    "name": "Song of Time Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_STORM": {
    "name": "Song of Storms Effect II [?]",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_DEMO": {
    "name": "Cutscene Trigger",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MINISLIME": {
    "name": "Jelly Droplets",
    "category": "",
    "objDeps": [
      "OBJECT_BIGSLIME"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NUTSBALL": {
    "name": "Deku Nut Projectile",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE2": {
    "name": "Epona's Song Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE3": {
    "name": "Saria's Song Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DG": {
    "name": "Dog",
    "category": "",
    "objDeps": [
      "OBJECT_DOG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SI": {
    "name": "Gold Skulltula Token",
    "category": "",
    "objDeps": [
      "OBJECT_ST"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_COMB": {
    "name": "Beehive",
    "category": "",
    "objDeps": [
      "OBJECT_COMB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_KIBAKO2": {
    "name": "Wooden Crate",
    "category": "",
    "objDeps": [
      "OBJECT_KIBAKO2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HS2": {
    "name": "En_Hs2",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MURE3": {
    "name": "Rupee Cluster",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TG": {
    "name": "Honey and Darling (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_MU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WF": {
    "name": "Wolfos",
    "category": "",
    "objDeps": [
      "OBJECT_WF"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SKB": {
    "name": "Stalchild",
    "category": "",
    "objDeps": [
      "OBJECT_SKB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GS": {
    "name": "Gossip Stone",
    "category": "",
    "objDeps": [
      "OBJECT_GS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SOUND": {
    "name": "Sound Effects II",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_CROW": {
    "name": "Guay",
    "category": "",
    "objDeps": [
      "OBJECT_CROW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_COW": {
    "name": "Cow",
    "category": "",
    "objDeps": [
      "OBJECT_COW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE4": {
    "name": "Scarecrow's Song Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZO": {
    "name": "Zora [Early]",
    "category": "",
    "objDeps": [
      "OBJECT_ZO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MAKEKINSUTA": {
    "name": "Obj_Makekinsuta",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GE3": {
    "name": "Aveil",
    "category": "",
    "objDeps": [
      "OBJECT_GELDB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HAMISHI": {
    "name": "Bronze Boulder",
    "category": "",
    "objDeps": [
      "GAMEPLAY_FIELD_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZL4": {
    "name": "En_Zl4",
    "category": "",
    "objDeps": [
      "OBJECT_STK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MM2": {
    "name": "Postman's Letter to Himself",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DOOR_SPIRAL": {
    "name": "Spiral Staircase",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_PZLBLOCK": {
    "name": "Majora Pushblock",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TOGE": {
    "name": "Blade Trap",
    "category": "",
    "objDeps": [
      "OBJECT_TRAP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ARMOS": {
    "name": "Armos Statue",
    "category": "",
    "objDeps": [
      "OBJECT_AM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_BOYO": {
    "name": "Green Bumper",
    "category": "",
    "objDeps": [
      "OBJECT_BOYO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GRASSHOPPER": {
    "name": "Dragonfly",
    "category": "",
    "objDeps": [
      "OBJECT_GRASSHOPPER"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_GRASS": {
    "name": "Obj_Grass",
    "category": "",
    "objDeps": [
      "GAMEPLAY_FIELD_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_GRASS_CARRY": {
    "name": "Obj_Grass_Carry",
    "category": "",
    "objDeps": [
      "GAMEPLAY_FIELD_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_GRASS_UNIT": {
    "name": "Grass Cluster",
    "category": "",
    "objDeps": [
      "GAMEPLAY_FIELD_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_FIRE_WALL": {
    "name": "Proximity-Activated Firewall",
    "category": "",
    "objDeps": [
      "OBJECT_FWALL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BU": {
    "name": "En_Bu",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENCOUNT3": {
    "name": "Circle of Light [?]",
    "category": "",
    "objDeps": [
      "OBJECT_BIG_FWALL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JSO": {
    "name": "Garo Master I [?]",
    "category": "",
    "objDeps": [
      "OBJECT_JSO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_CHIKUWA": {
    "name": "Falling Block Row",
    "category": "",
    "objDeps": [
      "OBJECT_D_LIFT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KNIGHT": {
    "name": "Igos du Ikana and Henchmen [?]",
    "category": "",
    "objDeps": [
      "OBJECT_KNIGHT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WARP_TAG": {
    "name": "Warp to Trial Entrance",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AOB_01": {
    "name": "Mamamu Yan",
    "category": "",
    "objDeps": [
      "OBJECT_AOB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOJ_01": {
    "name": "En_Boj_01",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOJ_02": {
    "name": "En_Boj_02",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOJ_03": {
    "name": "En_Boj_03",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENCOUNT4": {
    "name": "En_Encount4",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOM_BOWL_MAN": {
    "name": "Bomber I [?]",
    "category": "",
    "objDeps": [
      "OBJECT_CS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_MAN": {
    "name": "Shooting Gallery Proprietors [?]",
    "category": "",
    "objDeps": [
      "OBJECT_SHN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_ICICLE": {
    "name": "Icicle",
    "category": "",
    "objDeps": [
      "OBJECT_ICICLE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_CROW": {
    "name": "Guay (Shooting Gallery)",
    "category": "",
    "objDeps": [
      "OBJECT_CROW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOJ_04": {
    "name": "En_Boj_04",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_CNE_01": {
    "name": "Thin Woman in Blue Dress [OoT]",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BBA_01": {
    "name": "Bomb Shop Proprietor's Mother [Early]",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BJI_01": {
    "name": "Shikashi",
    "category": "",
    "objDeps": [
      "OBJECT_BJI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPDWEB": {
    "name": "Spiderweb",
    "category": "",
    "objDeps": [
      "OBJECT_SPDWEB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MT_TAG": {
    "name": "En_Mt_tag",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_01": {
    "name": "Odolwa",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS01"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_02": {
    "name": "Twinmold",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS02"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_03": {
    "name": "Gyorg",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS03"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_04": {
    "name": "Wart",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS04"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_05": {
    "name": "Bio Deku Baba",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS05"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_06": {
    "name": "Igos du Ikana [?]",
    "category": "",
    "objDeps": [
      "OBJECT_KNIGHT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_07": {
    "name": "Majora",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS07"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DY_YOSEIZO": {
    "name": "Great Fairy",
    "category": "",
    "objDeps": [
      "OBJECT_DY_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOJ_05": {
    "name": "En_Boj_05",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SOB1": {
    "name": "En_Sob1",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GO": {
    "name": "Goron",
    "category": "",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RAF": {
    "name": "Carnivorous Lilypad",
    "category": "",
    "objDeps": [
      "OBJECT_RAF"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_FUNEN": {
    "name": "Stone Tower Smoke Plume [Early]",
    "category": "",
    "objDeps": [
      "OBJECT_FUNEN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_RAILLIFT": {
    "name": "Elevator (Deku Palace and Woodfall Temple) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_RAILLIFT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_NUMA_HANA": {
    "name": "Wooden Flower",
    "category": "",
    "objDeps": [
      "OBJECT_NUMA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_FLOWERPOT": {
    "name": "Potted Plant",
    "category": "",
    "objDeps": [
      "OBJECT_FLOWERPOT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SPINYROLL": {
    "name": "Spiked Log (Horizontal)",
    "category": "",
    "objDeps": [
      "OBJECT_SPINYROLL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_HINA": {
    "name": "Boss Remains (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_BSMASK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_WF": {
    "name": "Wolfos (Shooting Gallery)",
    "category": "",
    "objDeps": [
      "OBJECT_WF"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SKATEBLOCK": {
    "name": "Ice Pushblock",
    "category": "",
    "objDeps": [
      "GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ICEBLOCK": {
    "name": "Frozen Enemy Ice Block",
    "category": "",
    "objDeps": [
      "OBJECT_ICE_BLOCK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BIGPAMET": {
    "name": "Snapper (Mini-Boss)",
    "category": "",
    "objDeps": [
      "OBJECT_TL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_DEKUNUTS": {
    "name": "Mad Scrub (Shooting Gallery)",
    "category": "",
    "objDeps": [
      "OBJECT_DEKUNUTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ELF_MSG3": {
    "name": "Elf_Msg3",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FG": {
    "name": "Frog II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_FR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_RAVINE": {
    "name": "Tree Trunk",
    "category": "",
    "objDeps": [
      "OBJECT_KEIKOKU_DEMO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_SA": {
    "name": "Dm_Sa",
    "category": "",
    "objDeps": [
      "OBJECT_STK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SLIME": {
    "name": "Chuchu",
    "category": "",
    "objDeps": [
      "OBJECT_SLIME"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PR": {
    "name": "Desbreko",
    "category": "",
    "objDeps": [
      "OBJECT_PR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TOUDAI": {
    "name": "Clock Tower Spotlight",
    "category": "",
    "objDeps": [
      "OBJECT_F53_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ENTOTU": {
    "name": "Clock Town 2D Chimney Backdrop",
    "category": "",
    "objDeps": [
      "OBJECT_F53_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_BELL": {
    "name": "Stock Pot Inn Bell",
    "category": "",
    "objDeps": [
      "OBJECT_F52_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SYATEKI_OKUTA": {
    "name": "Octorok (Shooting Gallery)",
    "category": "",
    "objDeps": [
      "OBJECT_OKUTA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SHUTTER": {
    "name": "Clock Town Bank Shutter",
    "category": "",
    "objDeps": [
      "OBJECT_F53_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_ZL": {
    "name": "Child Zelda (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_ZL4"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ELFGRP": {
    "name": "Group of Stray Fairies",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_TSG": {
    "name": "Deku Door/Spotlights",
    "category": "",
    "objDeps": [
      "OBJECT_OPEN_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BAGUO": {
    "name": "Nejiron",
    "category": "",
    "objDeps": [
      "OBJECT_GMO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_VSPINYROLL": {
    "name": "Spiked Log (Vertical)",
    "category": "",
    "objDeps": [
      "OBJECT_SPINYROLL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SMORK": {
    "name": "Romani Ranch Chimney Smoke",
    "category": "",
    "objDeps": [
      "OBJECT_F53_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TEST2": {
    "name": "En_Test2",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TEST3": {
    "name": "Kafei",
    "category": "",
    "objDeps": [
      "OBJECT_TEST3"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TEST4": {
    "name": "Three-Day Timer",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BAT": {
    "name": "Bad Bat",
    "category": "",
    "objDeps": [
      "OBJECT_BAT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SEKIHI": {
    "name": "Mikau's Grave and Song Pedestals [Early]",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WIZ": {
    "name": "Wizzrobe",
    "category": "",
    "objDeps": [
      "OBJECT_WIZ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WIZ_BROCK": {
    "name": "Wizzrobe Warp Platform",
    "category": "",
    "objDeps": [
      "OBJECT_WIZ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WIZ_FIRE": {
    "name": "Wizzrobe Fire Attack",
    "category": "",
    "objDeps": [
      "OBJECT_WIZ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EFF_CHANGE": {
    "name": "Camera Refocuser",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_STATUE": {
    "name": "Elegy Statue Light Beam [?]",
    "category": "",
    "objDeps": [
      "OBJECT_SMTOWER"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_FIRESHIELD": {
    "name": "Circle of Flames",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_LADDER": {
    "name": "Ladder",
    "category": "",
    "objDeps": [
      "OBJECT_LADDER"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MKK": {
    "name": "Black and White Boes",
    "category": "",
    "objDeps": [
      "OBJECT_MKK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_GETITEM": {
    "name": "Great Fairy's Mask and Great Fairy's Sword",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNB": {
    "name": "En_Dnb",
    "category": "",
    "objDeps": [
      "OBJECT_HANAREYAMA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNH": {
    "name": "Boat Cruise Target Spot",
    "category": "",
    "objDeps": [
      "OBJECT_TRO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNK": {
    "name": "Mad Scrubs (Cutscenes)",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNQ": {
    "name": "Deku King",
    "category": "",
    "objDeps": [
      "OBJECT_DNO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_KEIKOKU_SAKU": {
    "name": "Spiked Iron Fence",
    "category": "",
    "objDeps": [
      "OBJECT_KEIKOKU_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HUGEBOMBIWA": {
    "name": "Powder Keg Boulder",
    "category": "",
    "objDeps": [
      "OBJECT_BOMBIWA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FIREFLY2": {
    "name": "En_Firefly2",
    "category": "",
    "objDeps": [
      "OBJECT_FIREFLY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RAT": {
    "name": "Real Bombchu",
    "category": "",
    "objDeps": [
      "OBJECT_RAT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WATER_EFFECT": {
    "name": "Dripping Water",
    "category": "",
    "objDeps": [
      "OBJECT_WATER_EFFECT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KUSA2": {
    "name": "Keaton Grass Cluster",
    "category": "",
    "objDeps": [
      "GAMEPLAY_FIELD_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SPOUT_FIRE": {
    "name": "Proximity-Activated Firewall",
    "category": "",
    "objDeps": [
      "OBJECT_FWALL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DBLUE_MOVEBG": {
    "name": "Great Bay Temple Gears",
    "category": "",
    "objDeps": [
      "OBJECT_DBLUE_OBJECT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DY_EXTRA": {
    "name": "Great Fairy Healing Beam",
    "category": "",
    "objDeps": [
      "OBJECT_DY_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BAL": {
    "name": "Tingle (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_BAL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GINKO_MAN": {
    "name": "Bank Teller, Sakon, Twin Jugglers",
    "category": "",
    "objDeps": [
      "OBJECT_BOJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WARP_UZU": {
    "name": "Pirates' Fortress Telescope",
    "category": "",
    "objDeps": [
      "OBJECT_WARP_UZU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_DRIFTICE": {
    "name": "Drifting Ice Platform",
    "category": "",
    "objDeps": [
      "OBJECT_DRIFTICE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_LOOK_NUTS": {
    "name": "Deku Scrub Guard (Palace Gardens)",
    "category": "",
    "objDeps": [
      "OBJECT_DNK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MUSHI2": {
    "name": "En_Mushi2",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FALL": {
    "name": "The Moon",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MM3": {
    "name": "Postman (Counting Game)",
    "category": "",
    "objDeps": [
      "OBJECT_MM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_CRACE_MOVEBG": {
    "name": "Deku Shrine Door",
    "category": "",
    "objDeps": [
      "OBJECT_CRACE_OBJECT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNO": {
    "name": "Deku Butler",
    "category": "",
    "objDeps": [
      "OBJECT_DNJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PR2": {
    "name": "Skullfish",
    "category": "",
    "objDeps": [
      "OBJECT_PR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PRZ": {
    "name": "Skullfish - Defeated",
    "category": "",
    "objDeps": [
      "OBJECT_PR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JSO2": {
    "name": "Garo Master II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_JSO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ETCETERA": {
    "name": "Deku Flower",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_EGOL": {
    "name": "Eyegore",
    "category": "",
    "objDeps": [
      "OBJECT_EG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MINE": {
    "name": "Spiked Metal Mine",
    "category": "",
    "objDeps": [
      "OBJECT_NY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_PURIFY": {
    "name": "Poisoned/Purified Water Elements",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TRU": {
    "name": "Koume (Gameplay) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_TRU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TRT": {
    "name": "Kotake (No Broom) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_TRT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TEST5": {
    "name": "Spring Water",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TEST6": {
    "name": "Song of Time Cutscene Effects",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AZ": {
    "name": "Beaver Bros.",
    "category": "",
    "objDeps": [
      "OBJECT_AZ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ESTONE": {
    "name": "Eyegore Rubble",
    "category": "",
    "objDeps": [
      "OBJECT_EG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKUGIN_POST": {
    "name": "Snowhead Temple Central Pillar",
    "category": "",
    "objDeps": [
      "OBJECT_HAKUGIN_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_OPSTAGE": {
    "name": "Opening Cutscene Objects",
    "category": "",
    "objDeps": [
      "OBJECT_KEIKOKU_DEMO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_STK": {
    "name": "Skull Kid",
    "category": "",
    "objDeps": [
      "OBJECT_STK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR00": {
    "name": "Tatl and Tael (Cutscenes) II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_DELF"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR01": {
    "name": "Woodfall Temple Rises Cutscene Objects",
    "category": "",
    "objDeps": [
      "OBJECT_MTORIDE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR02": {
    "name": "Clock Tower Roof Cutscene - OoT and Majora's Mask",
    "category": "",
    "objDeps": [
      "OBJECT_STK2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR03": {
    "name": "Happy Mask Salesman (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_OSN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR04": {
    "name": "Tatl and Tael (Cutscenes) I [?]",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR05": {
    "name": "Masks (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_DMASK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR06": {
    "name": "Mountain Village Cutscene Objects [?]",
    "category": "",
    "objDeps": [
      "OBJECT_YUKIYAMA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR07": {
    "name": "Milk Bar Stage (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_MILKBAR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR08": {
    "name": "Turtle (Cutscenes) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_KAMEJIMA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_CHAR09": {
    "name": "Giant Bee (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_BEE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TOKEIDAI": {
    "name": "Clock Tower and Light Beam",
    "category": "",
    "objDeps": [
      "OBJECT_OBJ_TOKEIDAI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MNK": {
    "name": "Monkey",
    "category": "",
    "objDeps": [
      "OBJECT_MNK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_EGBLOCK": {
    "name": "Eyegore Block",
    "category": "",
    "objDeps": [
      "OBJECT_EG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GUARD_NUTS": {
    "name": "Deku Scrub Guard (Palace Entrance) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_DNK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKUGIN_BOMBWALL": {
    "name": "Snowhead Temple Bombable Wall",
    "category": "",
    "objDeps": [
      "OBJECT_HAKUGIN_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TOKEI_TOBIRA": {
    "name": "Clock Tower Doors",
    "category": "",
    "objDeps": [
      "OBJECT_TOKEI_TOBIRA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKUGIN_ELVPOLE": {
    "name": "Snowhead Temple Punchable Pillar Inserts",
    "category": "",
    "objDeps": [
      "OBJECT_HAKUGIN_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MA4": {
    "name": "Romani I [?]",
    "category": "",
    "objDeps": [
      "OBJECT_MA1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TWIG": {
    "name": "Beaver Race Ring",
    "category": "",
    "objDeps": [
      "OBJECT_TWIG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PO_FUSEN": {
    "name": "Poe Balloon",
    "category": "",
    "objDeps": [
      "OBJECT_PO_FUSEN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DOOR_ETC": {
    "name": "En_Door_Etc",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BIGOKUTA": {
    "name": "Big Octo",
    "category": "",
    "objDeps": [
      "OBJECT_BIGOKUTA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_ICEFLOE": {
    "name": "Ice Arrow Platform",
    "category": "",
    "objDeps": [
      "OBJECT_ICEFLOE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_OCARINALIFT": {
    "name": "Triforce Elevator",
    "category": "",
    "objDeps": [
      "OBJECT_RAILLIFT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TIME_TAG": {
    "name": "En_Time_Tag",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_OPEN_SHUTTER": {
    "name": "Deku Emblem Door",
    "category": "",
    "objDeps": [
      "OBJECT_OPEN_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_OPEN_SPOT": {
    "name": "Skull Kid Spotlights",
    "category": "",
    "objDeps": [
      "OBJECT_OPEN_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_FU_KAITEN": {
    "name": "Honey and Darling's Shop Rotating Platform",
    "category": "",
    "objDeps": [
      "OBJECT_FU_KAITEN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_AQUA": {
    "name": "Poured Water",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ELFORG": {
    "name": "Stray Fairy",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ELFBUB": {
    "name": "Stray Fairy Bubble",
    "category": "",
    "objDeps": [
      "OBJECT_BUBBLE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FU_MATO": {
    "name": "Honey and Darling's Shop Target",
    "category": "",
    "objDeps": [
      "OBJECT_FU_MATO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FU_KAGO": {
    "name": "Honey and Darling's Shop Basket",
    "category": "",
    "objDeps": [
      "OBJECT_FU_MATO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OSN": {
    "name": "Happy Mask Salesman (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_OSN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_CTOWER_GEAR": {
    "name": "Clock Tower Gear",
    "category": "",
    "objDeps": [
      "OBJECT_CTOWER_ROT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TRT2": {
    "name": "Kotake (Broom) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_TRT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TOKEI_STEP": {
    "name": "Clock Tower Roof Door",
    "category": "",
    "objDeps": [
      "OBJECT_TOKEI_STEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_LOTUS": {
    "name": "Lilypad",
    "category": "",
    "objDeps": [
      "OBJECT_LOTUS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KAME": {
    "name": "Snapper",
    "category": "",
    "objDeps": [
      "OBJECT_TL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TAKARAYA_WALL": {
    "name": "Treasure Chest Game Proximity-Activated Wall",
    "category": "",
    "objDeps": [
      "OBJECT_TAKARAYA_OBJECTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_FU_MIZU": {
    "name": "Honey and Darling's Shop Moat",
    "category": "",
    "objDeps": [
      "OBJECT_FU_KAITEN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SELLNUTS": {
    "name": "Business Scrub (Flying) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_DNT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DKJAIL_IVY": {
    "name": "Woodfall Prison Ivy",
    "category": "",
    "objDeps": [
      "OBJECT_DKJAIL_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_VISIBLOCK": {
    "name": "Lens of Truth Platform",
    "category": "",
    "objDeps": [
      "OBJECT_VISIBLOCK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TAKARAYA": {
    "name": "Treasure Chest Game Employee",
    "category": "",
    "objDeps": [
      "OBJECT_BG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TSN": {
    "name": "Fisherman (Great Bay)",
    "category": "",
    "objDeps": [
      "OBJECT_TSN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DS2N": {
    "name": "Potion Shop Proprietor (Updated) [OoT]",
    "category": "",
    "objDeps": [
      "OBJECT_DS2N"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FSN": {
    "name": "Curiosity Shop Proprietor",
    "category": "",
    "objDeps": [
      "OBJECT_FSN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SHN": {
    "name": "Swamp Tourist Center Guide",
    "category": "",
    "objDeps": [
      "OBJECT_SHN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_STOP_HEISHI": {
    "name": "Soldier (Gate Guard)",
    "category": "",
    "objDeps": [
      "OBJECT_SDN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_BIGICICLE": {
    "name": "Ice Block",
    "category": "",
    "objDeps": [
      "OBJECT_BIGICICLE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_LIFT_NUTS": {
    "name": "Deku Scrub Playground Employee",
    "category": "",
    "objDeps": [
      "OBJECT_DNT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TK": {
    "name": "Dampé",
    "category": "",
    "objDeps": [
      "OBJECT_TK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MARKET_STEP": {
    "name": "West Clock Town Steps",
    "category": "",
    "objDeps": [
      "OBJECT_MARKET_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_LUPYGAMELIFT": {
    "name": "Deku Scrub Playground Elevator",
    "category": "",
    "objDeps": [
      "OBJECT_RAILLIFT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TEST7": {
    "name": "Song of Soaring Cutscene Activator",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_LIGHTBLOCK": {
    "name": "Dissolvable Light Block",
    "category": "",
    "objDeps": [
      "OBJECT_LIGHTBLOCK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_MIR_RAY2": {
    "name": "Mirror Shield Reflectable Spotlight [?]",
    "category": "",
    "objDeps": [
      "OBJECT_MIR_RAY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_WDHAND": {
    "name": "Dexihand",
    "category": "",
    "objDeps": [
      "OBJECT_WDHAND"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GAMELUPY": {
    "name": "Deku Scrub Playground Rupee",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DANPEI_MOVEBG": {
    "name": "Dampé's House Objects",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SNOWWD": {
    "name": "Snow-Covered Tree",
    "category": "",
    "objDeps": [
      "OBJECT_SNOWWD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PM": {
    "name": "Postman (Delivering Letters)",
    "category": "",
    "objDeps": [
      "OBJECT_MM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GAKUFU": {
    "name": "2D Music Staff",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ELF_MSG4": {
    "name": "Elf_Msg4",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ELF_MSG5": {
    "name": "Elf_Msg5",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_COL_MAN": {
    "name": "Piece of Heart",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TALK_GIBUD": {
    "name": "Gibdo (Ikana Well)",
    "category": "",
    "objDeps": [
      "OBJECT_RD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GIANT": {
    "name": "Giant",
    "category": "",
    "objDeps": [
      "OBJECT_GIANT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SNOWBALL": {
    "name": "Large Snowball",
    "category": "",
    "objDeps": [
      "OBJECT_GOROIWA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BOSS_HAKUGIN": {
    "name": "Goht",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS_HAKUGIN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GB2": {
    "name": "Ghost Hut Proprietor",
    "category": "",
    "objDeps": [
      "OBJECT_PS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ONPUMAN": {
    "name": "Monkey Instrument Prompt",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_TOBIRA01": {
    "name": "Goron Shrine Gate",
    "category": "",
    "objDeps": [
      "OBJECT_SPOT11_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TAG_OBJ": {
    "name": "En_Tag_Obj",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_DHOUSE": {
    "name": "Dampé's House Facade",
    "category": "",
    "objDeps": [
      "OBJECT_DHOUSE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HAKAISI": {
    "name": "Gravestone",
    "category": "",
    "objDeps": [
      "OBJECT_HAKAISI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKUGIN_SWITCH": {
    "name": "Goron Link Switch",
    "category": "",
    "objDeps": [
      "OBJECT_GORONSWITCH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SNOWMAN": {
    "name": "Big and Small Eeno",
    "category": "",
    "objDeps": [
      "OBJECT_SNOWMAN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_TG_SW": {
    "name": "TG_Sw",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PO_SISTERS": {
    "name": "Poe Sisters",
    "category": "",
    "objDeps": [
      "OBJECT_PO_SISTERS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PP": {
    "name": "Hiploop",
    "category": "",
    "objDeps": [
      "OBJECT_PP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HAKUROCK": {
    "name": "Goht Debris",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS_HAKUGIN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HANABI": {
    "name": "Fireworks",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_DOWSING": {
    "name": "Obj_Dowsing",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_WIND": {
    "name": "Wind Funnel",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RACEDOG": {
    "name": "Dog (Doggie Racetrack)",
    "category": "",
    "objDeps": [
      "OBJECT_DOG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KENDO_JS": {
    "name": "Swordsman",
    "category": "",
    "objDeps": [
      "OBJECT_JS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_BOTIHASIRA": {
    "name": "Captain Keeta Race Gatepost",
    "category": "",
    "objDeps": [
      "OBJECT_BOTIHASIRA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FISH2": {
    "name": "Marine Research Lab Fish",
    "category": "",
    "objDeps": [
      "OBJECT_FB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PST": {
    "name": "Postbox",
    "category": "",
    "objDeps": [
      "OBJECT_PST"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_POH": {
    "name": "Poe",
    "category": "",
    "objDeps": [
      "OBJECT_PO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SPIDERTENT": {
    "name": "Tent-Shaped Spider Web",
    "category": "",
    "objDeps": [
      "OBJECT_SPIDERTENT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZORAEGG": {
    "name": "Zora Egg",
    "category": "",
    "objDeps": [
      "OBJECT_ZORAEGG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KBT": {
    "name": "Zubora",
    "category": "",
    "objDeps": [
      "OBJECT_KBT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GG": {
    "name": "Darmani's Ghost I [?]",
    "category": "",
    "objDeps": [
      "OBJECT_GG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MARUTA": {
    "name": "Swordsman's School Practice Log",
    "category": "",
    "objDeps": [
      "OBJECT_MARUTA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SNOWBALL2": {
    "name": "Small Snowball",
    "category": "",
    "objDeps": [
      "OBJECT_GOROIWA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GG2": {
    "name": "Darmani's Ghost II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_GG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_GHAKA": {
    "name": "Darmani's Gravestone",
    "category": "",
    "objDeps": [
      "OBJECT_GHAKA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DNP": {
    "name": "Deku Princess",
    "category": "",
    "objDeps": [
      "OBJECT_DNQ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DAI": {
    "name": "Biggoron",
    "category": "",
    "objDeps": [
      "OBJECT_DAI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_GORON_OYU": {
    "name": "Hot Spring Water",
    "category": "",
    "objDeps": [
      "OBJECT_OYU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KGY": {
    "name": "Gabora",
    "category": "",
    "objDeps": [
      "OBJECT_KGY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_INVADEPOH": {
    "name": "En_Invadepoh",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GK": {
    "name": "Goron Elder's Son",
    "category": "",
    "objDeps": [
      "OBJECT_GK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AN": {
    "name": "Anju (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_AN1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BEE": {
    "name": "Giant Bee (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_BEE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OT": {
    "name": "Seahorse",
    "category": "",
    "objDeps": [
      "OBJECT_OT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DRAGON": {
    "name": "Deep Python",
    "category": "",
    "objDeps": [
      "OBJECT_UTUBO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_DORA": {
    "name": "Swordsman's School Gong",
    "category": "",
    "objDeps": [
      "OBJECT_DORA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BIGPO": {
    "name": "Big Poe",
    "category": "",
    "objDeps": [
      "OBJECT_BIGPO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_KENDO_KANBAN": {
    "name": "Swordsman's School Wooden Board",
    "category": "",
    "objDeps": [
      "OBJECT_DORA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HARIKO": {
    "name": "Cow Figurine",
    "category": "",
    "objDeps": [
      "OBJECT_HARIKO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_STH": {
    "name": "En_Sth",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_SINKAI_KABE": {
    "name": "Bg_Sinkai_Kabe",
    "category": "",
    "objDeps": [
      "OBJECT_SINKAI_KABE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKA_CURTAIN": {
    "name": "Beneath the Grave Curtain",
    "category": "",
    "objDeps": [
      "OBJECT_HAKA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_KIN2_BOMBWALL": {
    "name": "Oceanside Spider House Bombable Wall",
    "category": "",
    "objDeps": [
      "OBJECT_KIN2_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_KIN2_FENCE": {
    "name": "Oceanside Spider House Fireplace Grate",
    "category": "",
    "objDeps": [
      "OBJECT_KIN2_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_KIN2_PICTURE": {
    "name": "Oceanside Spider House Skull Kid Painting",
    "category": "",
    "objDeps": [
      "OBJECT_KIN2_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_KIN2_SHELF": {
    "name": "Oceanside Spider House Drawers and Bookshelf",
    "category": "",
    "objDeps": [
      "OBJECT_KIN2_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RAIL_SKB": {
    "name": "Circle of Stalchildren",
    "category": "",
    "objDeps": [
      "OBJECT_SKB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JG": {
    "name": "Goron Elder",
    "category": "",
    "objDeps": [
      "OBJECT_JG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TRU_MT": {
    "name": "Koume (Boat Cruise) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_TRU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_UM": {
    "name": "Cremia's Cart",
    "category": "",
    "objDeps": [
      "OBJECT_UM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NEO_REEBA": {
    "name": "Leever",
    "category": "",
    "objDeps": [
      "OBJECT_RB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_MBAR_CHAIR": {
    "name": "Milk Bar Chair",
    "category": "",
    "objDeps": [
      "OBJECT_MBAR_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKANA_BLOCK": {
    "name": "Bg_Ikana_Block",
    "category": "",
    "objDeps": [
      "GAMEPLAY_DANGEON_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKANA_MIRROR": {
    "name": "Stone Tower Temple Mirror",
    "category": "",
    "objDeps": [
      "OBJECT_IKANA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKANA_ROTARYROOM": {
    "name": "Stone Tower Temple Rotating Room",
    "category": "",
    "objDeps": [
      "OBJECT_IKANA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DBLUE_BALANCE": {
    "name": "Great Bay Temple See-Saw",
    "category": "",
    "objDeps": [
      "OBJECT_DBLUE_OBJECT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DBLUE_WATERFALL": {
    "name": "Great Bay Temple Water Spout",
    "category": "",
    "objDeps": [
      "OBJECT_DBLUE_OBJECT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KAIZOKU": {
    "name": "Pirate [?]",
    "category": "",
    "objDeps": [
      "OBJECT_KZ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GE2": {
    "name": "Patrolling Pirate Guard",
    "category": "",
    "objDeps": [
      "OBJECT_GLA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MA_YTS": {
    "name": "Romani II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_MA1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MA_YTO": {
    "name": "Cremia",
    "category": "",
    "objDeps": [
      "OBJECT_MA2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TOKEI_TURRET": {
    "name": "South Clock Town Objects",
    "category": "",
    "objDeps": [
      "OBJECT_TOKEI_TURRET"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_DBLUE_ELEVATOR": {
    "name": "Great Bay Temple Elevator",
    "category": "",
    "objDeps": [
      "OBJECT_DBLUE_OBJECT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_WARPSTONE": {
    "name": "Owl Statue",
    "category": "",
    "objDeps": [
      "OBJECT_SEK"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZOG": {
    "name": "Mikau",
    "category": "",
    "objDeps": [
      "OBJECT_ZOG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ROTLIFT": {
    "name": "Deku Moon Trial Rotating Platform",
    "category": "",
    "objDeps": [
      "OBJECT_ROTLIFT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_JG_GAKKI": {
    "name": "Goron Elder's Drum",
    "category": "",
    "objDeps": [
      "OBJECT_JG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_INIBS_MOVEBG": {
    "name": "Twinmold's Lair Objects [?]",
    "category": "",
    "objDeps": [
      "OBJECT_INIBS_OBJECT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZOT": {
    "name": "Zora (Land)",
    "category": "",
    "objDeps": [
      "OBJECT_ZO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TREE": {
    "name": "Fork-Branched Tree",
    "category": "",
    "objDeps": [
      "OBJECT_TREE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_Y2LIFT": {
    "name": "Pirates' Fortress Mesh Elevator",
    "category": "",
    "objDeps": [
      "OBJECT_KAIZOKU_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_Y2SHUTTER": {
    "name": "Pirates' Fortress Interior Door",
    "category": "",
    "objDeps": [
      "OBJECT_KAIZOKU_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_BOAT": {
    "name": "Pirates' Fortress Boat",
    "category": "",
    "objDeps": [
      "OBJECT_KAIZOKU_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_TARU": {
    "name": "Barrel",
    "category": "",
    "objDeps": [
      "OBJECT_TARU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HUNSUI": {
    "name": "Geyser",
    "category": "",
    "objDeps": [
      "OBJECT_HUNSUI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JC_MATO": {
    "name": "Boat Cruise Target",
    "category": "",
    "objDeps": [
      "OBJECT_TRU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_MIR_RAY3": {
    "name": "Mirror Shield Light Ray II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_MIR_RAY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZOB": {
    "name": "Japas",
    "category": "",
    "objDeps": [
      "OBJECT_ZOB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_ELF_MSG6": {
    "name": "Elf_Msg6",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_NOZOKI": {
    "name": "Obj_Nozoki",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TOTO": {
    "name": "Toto",
    "category": "",
    "objDeps": [
      "OBJECT_ZM"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RAILGIBUD": {
    "name": "Gibdo (Ikana Canyon)",
    "category": "",
    "objDeps": [
      "OBJECT_RD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BABA": {
    "name": "Bomb Shop Proprietor's Mother",
    "category": "",
    "objDeps": [
      "OBJECT_BBA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SUTTARI": {
    "name": "Sakon",
    "category": "",
    "objDeps": [
      "OBJECT_BOJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZOD": {
    "name": "Tijo",
    "category": "",
    "objDeps": [
      "OBJECT_ZOD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KUJIYA": {
    "name": "Lottery Shop Kiosk",
    "category": "",
    "objDeps": [
      "OBJECT_KUJIYA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GEG": {
    "name": "Don Gero",
    "category": "",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_KINOKO": {
    "name": "Mushroom Scent Cloud",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_YASI": {
    "name": "Palm Tree",
    "category": "",
    "objDeps": [
      "OBJECT_OBJ_YASI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TANRON1": {
    "name": "Swarm of Moths",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TANRON2": {
    "name": "Wart's Bubbles",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS04"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TANRON3": {
    "name": "Gyorg's Fish",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS03"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_CHAN": {
    "name": "Goron Village Chandelier",
    "category": "",
    "objDeps": [
      "OBJECT_OBJ_CHAN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZOS": {
    "name": "Evan",
    "category": "",
    "objDeps": [
      "OBJECT_ZOS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_S_GORO": {
    "name": "Goron (Goron Shrine and Bomb Shop) [?]",
    "category": "",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NB": {
    "name": "Anju's Grandmother (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_NB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JA": {
    "name": "Jugglers",
    "category": "",
    "objDeps": [
      "OBJECT_BOJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_F40_BLOCK": {
    "name": "Stone Tower Temple Shifting Block",
    "category": "",
    "objDeps": [
      "OBJECT_F40_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_F40_SWITCH": {
    "name": "Elegy Statue Switch",
    "category": "",
    "objDeps": [
      "OBJECT_F40_SWITCH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PO_COMPOSER": {
    "name": "Composer Brothers",
    "category": "",
    "objDeps": [
      "OBJECT_PO_COMPOSER"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_GURUGURU": {
    "name": "Guru-Guru",
    "category": "",
    "objDeps": [
      "OBJECT_FU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE5": {
    "name": "Sonata of Awakening Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_STONE_HEISHI": {
    "name": "Shiro",
    "category": "",
    "objDeps": [
      "OBJECT_SDN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE6": {
    "name": "Song of Soaring Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SCOPENUTS": {
    "name": "Business Scrub (Telescope)",
    "category": "",
    "objDeps": [
      "OBJECT_DNT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SCOPECROW": {
    "name": "Guay (Observatory Telescope)",
    "category": "",
    "objDeps": [
      "OBJECT_CROW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OCEFF_WIPE7": {
    "name": "Song of Healing Effect",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EFF_KAMEJIMA_WAVE": {
    "name": "Turtle's Tsunami",
    "category": "",
    "objDeps": [
      "OBJECT_KAMEJIMA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HG": {
    "name": "Pamela's Father (Normal)",
    "category": "",
    "objDeps": [
      "OBJECT_HARFGIBUD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HGO": {
    "name": "Pamela's Father (Cursed)",
    "category": "",
    "objDeps": [
      "OBJECT_HARFGIBUD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZOV": {
    "name": "Lulu",
    "category": "",
    "objDeps": [
      "OBJECT_ZOV"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AH": {
    "name": "Anju's Mother (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_AH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HGDOOR": {
    "name": "Music Box House Cupboard Doors",
    "category": "",
    "objDeps": [
      "OBJECT_HGDOOR"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKANA_BOMBWALL": {
    "name": "Stone Tower Temple Bombable Floor Tile and Wall",
    "category": "",
    "objDeps": [
      "OBJECT_IKANA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKANA_RAY": {
    "name": "Stone Tower Temple Light Ray [?]",
    "category": "",
    "objDeps": [
      "OBJECT_IKANA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKANA_SHUTTER": {
    "name": "Stone Tower Temple Lattice Door",
    "category": "",
    "objDeps": [
      "OBJECT_IKANA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKA_BOMBWALL": {
    "name": "Beneath the Grave Bombable Wall",
    "category": "",
    "objDeps": [
      "OBJECT_HAKA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_HAKA_TOMB": {
    "name": "Flat's Tomb",
    "category": "",
    "objDeps": [
      "OBJECT_HAKA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SC_RUPPE": {
    "name": "Large Rotating Green Rupee",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKNV_DOUKUTU": {
    "name": "Sharp's Cave",
    "category": "",
    "objDeps": [
      "OBJECT_IKNV_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKNV_OBJ": {
    "name": "Ikana Canyon Objects",
    "category": "",
    "objDeps": [
      "OBJECT_IKNV_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PAMERA": {
    "name": "Pamela",
    "category": "",
    "objDeps": [
      "OBJECT_PAMERA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_HSSTUMP": {
    "name": "Hookshot Stump",
    "category": "",
    "objDeps": [
      "OBJECT_HSSTUMP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HIDDEN_NUTS": {
    "name": "Mad Scrub (Sleeping)",
    "category": "",
    "objDeps": [
      "OBJECT_HINTNUTS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ZOW": {
    "name": "Zora (Water)",
    "category": "",
    "objDeps": [
      "OBJECT_ZO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TALK": {
    "name": "En_Talk",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AL": {
    "name": "Madame Aroma (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_AL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TAB": {
    "name": "Mr. Barten",
    "category": "",
    "objDeps": [
      "OBJECT_TAB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NIMOTSU": {
    "name": "Bomb Shop Bag",
    "category": "",
    "objDeps": [
      "OBJECT_BOJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HIT_TAG": {
    "name": "En_Hit_Tag",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RUPPECROW": {
    "name": "Guay (Circling Clock Town)",
    "category": "",
    "objDeps": [
      "OBJECT_CROW"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TANRON4": {
    "name": "Flock of Seagulls",
    "category": "",
    "objDeps": [
      "OBJECT_TANRON4"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TANRON5": {
    "name": "En_Tanron5",
    "category": "",
    "objDeps": [
      "OBJECT_BOSS02"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_TANRON6": {
    "name": "Swarm of Giant Bees",
    "category": "",
    "objDeps": [
      "OBJECT_TANRON5"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DAIKU2": {
    "name": "Carpenter (Milk Road)",
    "category": "",
    "objDeps": [
      "OBJECT_DAIKU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_MUTO": {
    "name": "Mutoh (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_TORYO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BAISEN": {
    "name": "Captain Viscen",
    "category": "",
    "objDeps": [
      "OBJECT_BAI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HEISHI": {
    "name": "Soldier (Mayor's House)",
    "category": "",
    "objDeps": [
      "OBJECT_SDN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DEMO_HEISHI": {
    "name": "Soldier (Cutscenes) I [?]",
    "category": "",
    "objDeps": [
      "OBJECT_SDN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DT": {
    "name": "Mayor Dotour (Gameplay)",
    "category": "",
    "objDeps": [
      "OBJECT_DT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_CHA": {
    "name": "Laundry Pool Bell [?]",
    "category": "",
    "objDeps": [
      "OBJECT_CHA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_DINNER": {
    "name": "Cremia and Romani's Dinner",
    "category": "",
    "objDeps": [
      "OBJECT_OBJ_DINNER"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EFF_LASTDAY": {
    "name": "Moon Fall Effects",
    "category": "",
    "objDeps": [
      "OBJECT_LASTDAY"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKANA_DHARMA": {
    "name": "Ancient Castle of Ikana Punchable Pillar Segments",
    "category": "",
    "objDeps": [
      "OBJECT_IKANA_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AKINDONUTS": {
    "name": "Traveling Business Scrub",
    "category": "",
    "objDeps": [
      "OBJECT_DNT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EFF_STK": {
    "name": "Skull Kid Moon-Summoning Effects [?]",
    "category": "",
    "objDeps": [
      "OBJECT_STK2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_IG": {
    "name": "Link the Goron",
    "category": "",
    "objDeps": [
      "OBJECT_DAI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RG": {
    "name": "Goron (Goron Racetrack)",
    "category": "",
    "objDeps": [
      "OBJECT_OF1D_MAP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_OSK": {
    "name": "Igos du Ikana and Henchmen's Heads",
    "category": "",
    "objDeps": [
      "OBJECT_IKN_DEMO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_STH2": {
    "name": "En_Sth2",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_YB": {
    "name": "Kamaro",
    "category": "",
    "objDeps": [
      "OBJECT_YB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RZ": {
    "name": "Rosa Sister",
    "category": "",
    "objDeps": [
      "OBJECT_RZ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_SCOPECOIN": {
    "name": "En_Scopecoin",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BJT": {
    "name": "Hand in Toilet",
    "category": "",
    "objDeps": [
      "OBJECT_BJT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOMJIMA": {
    "name": "Jim I [?]",
    "category": "",
    "objDeps": [
      "OBJECT_CS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOMJIMB": {
    "name": "Jim II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_CS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOMBERS": {
    "name": "Bomber II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_CS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOMBERS2": {
    "name": "Bomber (Hideout Guard)",
    "category": "",
    "objDeps": [
      "OBJECT_CS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BOMBAL": {
    "name": "Majora Balloon (North Clock Town)",
    "category": "",
    "objDeps": [
      "OBJECT_FUSEN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MOON_STONE": {
    "name": "Moon's Tear",
    "category": "",
    "objDeps": [
      "OBJECT_GI_RESERVE00"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MU_PICT": {
    "name": "Obj_Mu_Pict",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKNINSIDE": {
    "name": "Ancient Castle of Ikana Objects [?]",
    "category": "",
    "objDeps": [
      "OBJECT_IKNINSIDE_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EFF_ZORABAND": {
    "name": "Blue Spotlight Effect",
    "category": "",
    "objDeps": [
      "OBJECT_ZORABAND"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_KEPN_KOYA": {
    "name": "Gorman Track Buildings",
    "category": "",
    "objDeps": [
      "OBJECT_KEPN_KOYA"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_USIYANE": {
    "name": "Cow Barn Roof (Exterior)",
    "category": "",
    "objDeps": [
      "OBJECT_OBJ_USIYANE"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_NNH": {
    "name": "Deku Butler's Son",
    "category": "",
    "objDeps": [
      "OBJECT_NNH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_KZSAKU": {
    "name": "Metal Portcullis",
    "category": "",
    "objDeps": [
      "OBJECT_KZSAKU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_MILK_BIN": {
    "name": "Chateau Romani Delivery Bottle",
    "category": "",
    "objDeps": [
      "OBJECT_OBJ_MILK_BIN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_KITAN": {
    "name": "Keaton",
    "category": "",
    "objDeps": [
      "OBJECT_KITAN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_ASTR_BOMBWALL": {
    "name": "Astral Observatory Bombable Wall",
    "category": "",
    "objDeps": [
      "OBJECT_ASTR_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_IKNIN_SUSCEIL": {
    "name": "Hot Checkered Ceiling [?]",
    "category": "",
    "objDeps": [
      "OBJECT_IKNINSIDE_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BSB": {
    "name": "Captain Keeta",
    "category": "",
    "objDeps": [
      "OBJECT_BSB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RECEPGIRL": {
    "name": "Mayor's Receptionist",
    "category": "",
    "objDeps": [
      "OBJECT_BG"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_THIEFBIRD": {
    "name": "Takkuri",
    "category": "",
    "objDeps": [
      "OBJECT_THIEFBIRD"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_JGAME_TSN": {
    "name": "Fisherman (Fisherman's Jumping Game)",
    "category": "",
    "objDeps": [
      "OBJECT_TSN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_JGAME_LIGHT": {
    "name": "Torch Stand (Fisherman's Jumping Game)",
    "category": "",
    "objDeps": [
      "OBJECT_SYOKUDAI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_YADO": {
    "name": "Stockpot Inn Window",
    "category": "",
    "objDeps": [
      "OBJECT_YADO_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_SYOTEN": {
    "name": "Ikana Canyon Curse Lifted Effects",
    "category": "",
    "objDeps": [
      "OBJECT_SYOTEN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DEMO_MOONEND": {
    "name": "Moon (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_MOONEND"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_LBFSHOT": {
    "name": "Rainbow Hookshot Pillar",
    "category": "",
    "objDeps": [
      "OBJECT_LBFSHOT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_BG_LAST_BWALL": {
    "name": "Link Moon Trial Bombable and Climbable Walls",
    "category": "",
    "objDeps": [
      "OBJECT_LAST_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_AND": {
    "name": "Anju (Wedding Dress)",
    "category": "",
    "objDeps": [
      "OBJECT_AND"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_INVADEPOH_DEMO": {
    "name": "Invader Poe (Cutscenes)",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_DANPEILIFT": {
    "name": "Deku Shrine and Snowhead Temple Elevator [?]",
    "category": "",
    "objDeps": [
      "OBJECT_OBJ_DANPEILIFT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_FALL2": {
    "name": "Falling Moon",
    "category": "",
    "objDeps": [
      "OBJECT_FALL2"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_AL": {
    "name": "Madame Aroma (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_AL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_AN": {
    "name": "Anju Cutscene Animations",
    "category": "",
    "objDeps": [
      "OBJECT_AN1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_AH": {
    "name": "Anju's Mother (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_AH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_NB": {
    "name": "Anju's Grandmother (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_NB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_DRS": {
    "name": "Wedding Dress Mannequin",
    "category": "",
    "objDeps": [
      "OBJECT_DRS"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENDING_HERO": {
    "name": "Mayor Dotour (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_DT"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_BAL": {
    "name": "Tingle (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_BAL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_PAPER": {
    "name": "Tingle Confetti",
    "category": "",
    "objDeps": [
      "OBJECT_BAL"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_HINT_SKB": {
    "name": "Stalchild (Oceanside Spider House)",
    "category": "",
    "objDeps": [
      "OBJECT_SKB"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_TAG": {
    "name": "Dm_Tag",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_BH": {
    "name": "Brown Bird",
    "category": "",
    "objDeps": [
      "OBJECT_BH"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENDING_HERO2": {
    "name": "Viscen (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_BAI"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENDING_HERO3": {
    "name": "Mutoh (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_TORYO"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENDING_HERO4": {
    "name": "Soldier (Cutscenes) II [?]",
    "category": "",
    "objDeps": [
      "OBJECT_SDN"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENDING_HERO5": {
    "name": "Carpenter (Cutscenes)",
    "category": "",
    "objDeps": [
      "OBJECT_DAIKU"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_ENDING_HERO6": {
    "name": "En_Ending_Hero6",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_DM_GM": {
    "name": "Dm_Gm",
    "category": "",
    "objDeps": [
      "OBJECT_AN1"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_SWPRIZE": {
    "name": "Obj_Swprize",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_INVISIBLE_RUPPE": {
    "name": "Invisible - Rupee",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_OBJ_ENDING": {
    "name": "Epilogue Cutscene Objects",
    "category": "",
    "objDeps": [
      "OBJECT_ENDING_OBJ"
    ],
    "params": [],
    "notes": ""
  },
  "ACTOR_EN_RSN": {
    "name": "Bomb Shop Proprietor",
    "category": "",
    "objDeps": [
      "GAMEPLAY_KEEP"
    ],
    "params": [],
    "notes": ""
  }
};
