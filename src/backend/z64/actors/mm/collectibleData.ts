//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
export const collectibleData: Array<{name: string, value: string}> = [
  {
    "value": "0",
    "name": "Green Rupee"
  },
  {
    "value": "1",
    "name": "Blue Rupee"
  },
  {
    "value": "2",
    "name": "Red Rupee"
  },
  {
    "value": "3",
    "name": "Recovery Heart"
  },
  {
    "value": "4",
    "name": "Bomb (5)"
  },
  {
    "value": "5",
    "name": "Arrows (10)"
  },
  {
    "value": "6",
    "name": "Heart Piece"
  },
  {
    "value": "7",
    "name": "Heart Container"
  },
  {
    "value": "8",
    "name": "Arrows (20)"
  },
  {
    "value": "9",
    "name": "Arrows (30)"
  },
  {
    "value": "10",
    "name": "Arrows (50)"
  },
  {
    "value": "11",
    "name": "Bomb (5)"
  },
  {
    "value": "12",
    "name": "Deku Nut (1)"
  },
  {
    "value": "13",
    "name": "Deku Stick (1)"
  },
  {
    "value": "14",
    "name": "Large Magic Jar"
  },
  {
    "value": "15",
    "name": "Small Magic Jar"
  },
  {
    "value": "16",
    "name": "Link=Arrows, Zora=Heart, Goron=Magic"
  },
  {
    "value": "17",
    "name": "Small Key"
  },
  {
    "value": "18",
    "name": "Flexible"
  },
  {
    "value": "19",
    "name": "Orange Rupee"
  },
  {
    "value": "20",
    "name": "Purple Rupee"
  },
  {
    "value": "21",
    "name": "Recovery Hearts (3)"
  },
  {
    "value": "22",
    "name": "Hero-Shield"
  },
  {
    "value": "23",
    "name": "Deku Nuts (10)"
  },
  {
    "value": "24",
    "name": "Nothing"
  },
  {
    "value": "25",
    "name": "Bomb (None/Fake)"
  },
  {
    "value": "26",
    "name": "Big Fairy"
  },
  {
    "value": "27",
    "name": "Map"
  },
  {
    "value": "28",
    "name": "Compass"
  },
  {
    "value": "29",
    "name": "Mushroom Cloud"
  }
];
