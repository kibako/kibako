//
// NOTE: This file was generated using "scripts/actor_xml_converter.js"
// Do NOT modify this file directly.
//
export const chestData: Array<{name: string, value: string}> = [
  {
    "value": "0",
    "name": "[None]"
  },
  {
    "value": "1",
    "name": "Rupee (1)"
  },
  {
    "value": "2",
    "name": "Rupees (5)"
  },
  {
    "value": "3",
    "name": "Rupees (10)"
  },
  {
    "value": "4",
    "name": "Rupees (20)"
  },
  {
    "value": "5",
    "name": "Rupees (50)"
  },
  {
    "value": "6",
    "name": "Rupees (100)"
  },
  {
    "value": "7",
    "name": "Rupees (200)"
  },
  {
    "value": "8",
    "name": "Wallet (Adult)"
  },
  {
    "value": "9",
    "name": "Wallet (Giant)"
  },
  {
    "value": "10",
    "name": "Recovery Heart"
  },
  {
    "value": "11",
    "name": "GI_0B"
  },
  {
    "value": "12",
    "name": "Heart Piece"
  },
  {
    "value": "13",
    "name": "Heart Container"
  },
  {
    "value": "14",
    "name": "Small Magic Jar"
  },
  {
    "value": "15",
    "name": "Large Magic Jar"
  },
  {
    "value": "16",
    "name": "GI_10"
  },
  {
    "value": "17",
    "name": "Stray Fairy"
  },
  {
    "value": "18",
    "name": "GI_12"
  },
  {
    "value": "19",
    "name": "GI_13"
  },
  {
    "value": "20",
    "name": "Bombs (1)"
  },
  {
    "value": "21",
    "name": "Bombs (5)"
  },
  {
    "value": "22",
    "name": "Bombs (10)"
  },
  {
    "value": "23",
    "name": "Bombs (20)"
  },
  {
    "value": "24",
    "name": "Bombs (30)"
  },
  {
    "value": "25",
    "name": "Deku-Stick"
  },
  {
    "value": "26",
    "name": "Bombchus (10)"
  },
  {
    "value": "27",
    "name": "Bomb-Bag (20)"
  },
  {
    "value": "28",
    "name": "Bomb-Bag (30)"
  },
  {
    "value": "29",
    "name": "Bomb-Bag (40)"
  },
  {
    "value": "30",
    "name": "Arrows (10)"
  },
  {
    "value": "31",
    "name": "Arrows (30)"
  },
  {
    "value": "32",
    "name": "Arrows (40)"
  },
  {
    "value": "33",
    "name": "Arrows (50)"
  },
  {
    "value": "34",
    "name": "Quiver (30)"
  },
  {
    "value": "35",
    "name": "Quiver (40)"
  },
  {
    "value": "36",
    "name": "Quiver (50)"
  },
  {
    "value": "37",
    "name": "Fire-Arrows"
  },
  {
    "value": "38",
    "name": "Ice-Arrows"
  },
  {
    "value": "39",
    "name": "Light-Arrows"
  },
  {
    "value": "40",
    "name": "Deku-Nuts (1)"
  },
  {
    "value": "41",
    "name": "Deku-Nuts (5)"
  },
  {
    "value": "42",
    "name": "Deku-Nuts (10)"
  },
  {
    "value": "43",
    "name": "GI_2B"
  },
  {
    "value": "44",
    "name": "GI_2C"
  },
  {
    "value": "45",
    "name": "GI_2D"
  },
  {
    "value": "46",
    "name": "Bombchus (20)"
  },
  {
    "value": "47",
    "name": "GI_2F"
  },
  {
    "value": "48",
    "name": "GI_30"
  },
  {
    "value": "49",
    "name": "GI_31"
  },
  {
    "value": "50",
    "name": "Hero-Shield"
  },
  {
    "value": "51",
    "name": "Mirror-Shield"
  },
  {
    "value": "52",
    "name": "Powder-Keg"
  },
  {
    "value": "53",
    "name": "Magic Beans"
  },
  {
    "value": "54",
    "name": "Bombchu (1)"
  },
  {
    "value": "55",
    "name": "Kokiri-Sword"
  },
  {
    "value": "56",
    "name": "Razow-Sword"
  },
  {
    "value": "57",
    "name": "Gilded-Sword"
  },
  {
    "value": "58",
    "name": "Bombchus (5)"
  },
  {
    "value": "59",
    "name": "Great-Fairy Sword"
  },
  {
    "value": "60",
    "name": "Small Key"
  },
  {
    "value": "61",
    "name": "Boss-Key"
  },
  {
    "value": "62",
    "name": "Map"
  },
  {
    "value": "63",
    "name": "Compass"
  },
  {
    "value": "64",
    "name": "GI_40"
  },
  {
    "value": "65",
    "name": "Hookshor"
  },
  {
    "value": "66",
    "name": "Lens of Truth"
  },
  {
    "value": "67",
    "name": "Pictograph Box"
  },
  {
    "value": "68",
    "name": "GI_44"
  },
  {
    "value": "69",
    "name": "GI_45"
  },
  {
    "value": "70",
    "name": "GI_46"
  },
  {
    "value": "71",
    "name": "GI_47"
  },
  {
    "value": "72",
    "name": "GI_48"
  },
  {
    "value": "73",
    "name": "GI_49"
  },
  {
    "value": "74",
    "name": "GI_4A"
  },
  {
    "value": "75",
    "name": "GI_4B"
  },
  {
    "value": "76",
    "name": "Ocarina of time"
  },
  {
    "value": "77",
    "name": "GI_4D"
  },
  {
    "value": "78",
    "name": "GI_4E"
  },
  {
    "value": "79",
    "name": "GI_4F"
  },
  {
    "value": "80",
    "name": "Bomber's Notebook"
  },
  {
    "value": "81",
    "name": "GI_51"
  },
  {
    "value": "82",
    "name": "Golden-Skulltula Token"
  },
  {
    "value": "83",
    "name": "GI_53"
  },
  {
    "value": "84",
    "name": "GI_54"
  },
  {
    "value": "85",
    "name": "Remains (Odolwa)"
  },
  {
    "value": "86",
    "name": "Remains (Goht)"
  },
  {
    "value": "87",
    "name": "Remains (Gyorg)"
  },
  {
    "value": "88",
    "name": "Remains (Twinmold)"
  },
  {
    "value": "89",
    "name": "Red Potion Bottle"
  },
  {
    "value": "90",
    "name": "Bottle"
  },
  {
    "value": "91",
    "name": "Red Potion"
  },
  {
    "value": "92",
    "name": "Green Potion"
  },
  {
    "value": "93",
    "name": "Blue Potion"
  },
  {
    "value": "94",
    "name": "Fairy"
  },
  {
    "value": "95",
    "name": "Deku Princess"
  },
  {
    "value": "96",
    "name": "Milk-Bottle (Full)"
  },
  {
    "value": "97",
    "name": "Milk-Bottle (Half)"
  },
  {
    "value": "98",
    "name": "Fish"
  },
  {
    "value": "99",
    "name": "Bug"
  },
  {
    "value": "100",
    "name": "Blue-Fire"
  },
  {
    "value": "101",
    "name": "Poe"
  },
  {
    "value": "102",
    "name": "Big-Poe"
  },
  {
    "value": "103",
    "name": "Spring-Water (Cold)"
  },
  {
    "value": "104",
    "name": "Spring-Water (Hot)"
  },
  {
    "value": "105",
    "name": "Zora-Egg"
  },
  {
    "value": "106",
    "name": "Gold-Dust"
  },
  {
    "value": "107",
    "name": "Mushroom"
  },
  {
    "value": "108",
    "name": "GI_6C"
  },
  {
    "value": "109",
    "name": "GI_6D"
  },
  {
    "value": "110",
    "name": "Seahorse"
  },
  {
    "value": "111",
    "name": "Chateau-Romani Bottle"
  },
  {
    "value": "112",
    "name": "Hylian Loach"
  },
  {
    "value": "113",
    "name": "GI_71"
  },
  {
    "value": "114",
    "name": "GI_72"
  },
  {
    "value": "115",
    "name": "GI_73"
  },
  {
    "value": "116",
    "name": "GI_74"
  },
  {
    "value": "117",
    "name": "GI_75"
  },
  {
    "value": "118",
    "name": "Ice-Trap"
  },
  {
    "value": "119",
    "name": "GI_77"
  },
  {
    "value": "120",
    "name": "Mask (Deku)"
  },
  {
    "value": "121",
    "name": "Mask (Goron)"
  },
  {
    "value": "122",
    "name": "Mask (Zora)"
  },
  {
    "value": "123",
    "name": "Mask (Fierce-Deity)"
  },
  {
    "value": "124",
    "name": "Mask (Captain)"
  },
  {
    "value": "125",
    "name": "Mask (Giant)"
  },
  {
    "value": "126",
    "name": "Mask (All-Night)"
  },
  {
    "value": "127",
    "name": "Mask (Bunny)"
  },
  {
    "value": "128",
    "name": "Mask (Keaton)"
  },
  {
    "value": "129",
    "name": "Mask (Garo)"
  },
  {
    "value": "130",
    "name": "Mask (Romani)"
  },
  {
    "value": "131",
    "name": "Mask (Circus leader)"
  },
  {
    "value": "132",
    "name": "Mask (Postman)"
  },
  {
    "value": "133",
    "name": "Mask (Couple)"
  },
  {
    "value": "134",
    "name": "Mask (Great-Fairy)"
  },
  {
    "value": "135",
    "name": "Mask (Gibdo)"
  },
  {
    "value": "136",
    "name": "Mask (Don-Gero)"
  },
  {
    "value": "137",
    "name": "Mask (Kamaro)"
  },
  {
    "value": "138",
    "name": "Mask (Truth)"
  },
  {
    "value": "139",
    "name": "Mask (Stone)"
  },
  {
    "value": "140",
    "name": "Mask (Bremen)"
  },
  {
    "value": "141",
    "name": "Mask (Blast)"
  },
  {
    "value": "142",
    "name": "Mask (Scents)"
  },
  {
    "value": "143",
    "name": "Mask (Kafei)"
  },
  {
    "value": "144",
    "name": "GI_90"
  },
  {
    "value": "145",
    "name": "Chateau-Romani Milk"
  },
  {
    "value": "146",
    "name": "Regular Milk"
  },
  {
    "value": "147",
    "name": "Gold Dust (0x93)"
  },
  {
    "value": "148",
    "name": "Hylian Loach (0x94)"
  },
  {
    "value": "149",
    "name": "Seahorse (Caught)"
  },
  {
    "value": "150",
    "name": "Moon tear"
  },
  {
    "value": "151",
    "name": "Land-Deed (Land)"
  },
  {
    "value": "152",
    "name": "Land-Deed (Swamp)"
  },
  {
    "value": "153",
    "name": "Land-Deed (Mountain)"
  },
  {
    "value": "154",
    "name": "Land-Deed (Ocean)"
  },
  {
    "value": "155",
    "name": "Stolen Sword (Great-Fairy)"
  },
  {
    "value": "156",
    "name": "Stolen Sword (Kokiri)"
  },
  {
    "value": "157",
    "name": "Stolen Sword (Razor)"
  },
  {
    "value": "158",
    "name": "Stolen Sword (Gilded)"
  },
  {
    "value": "159",
    "name": "Stolen Shield (Hero)"
  },
  {
    "value": "160",
    "name": "Room-Key"
  },
  {
    "value": "161",
    "name": "Letter to Mama"
  },
  {
    "value": "162",
    "name": "GI_A2"
  },
  {
    "value": "163",
    "name": "GI_A3"
  },
  {
    "value": "164",
    "name": "GI_A4"
  },
  {
    "value": "165",
    "name": "GI_A5"
  },
  {
    "value": "166",
    "name": "GI_A6"
  },
  {
    "value": "167",
    "name": "GI_A7"
  },
  {
    "value": "168",
    "name": "GI_A8"
  },
  {
    "value": "169",
    "name": "Stolen Bottle"
  },
  {
    "value": "170",
    "name": "Letter to Kafei"
  },
  {
    "value": "171",
    "name": "Pendant of memories"
  },
  {
    "value": "172",
    "name": "GI_AC"
  },
  {
    "value": "173",
    "name": "GI_AD"
  },
  {
    "value": "174",
    "name": "GI_AE"
  },
  {
    "value": "175",
    "name": "GI_AF"
  },
  {
    "value": "176",
    "name": "GI_B0"
  },
  {
    "value": "177",
    "name": "GI_B1"
  },
  {
    "value": "178",
    "name": "GI_B2"
  },
  {
    "value": "179",
    "name": "GI_B3"
  },
  {
    "value": "180",
    "name": "Tingle-Map (Clock-Town)"
  },
  {
    "value": "181",
    "name": "Tingle-Map (Woodfall)"
  },
  {
    "value": "182",
    "name": "Tingle-Map (Snowhead)"
  },
  {
    "value": "183",
    "name": "Tingle-Map (Romani Ranch)"
  },
  {
    "value": "184",
    "name": "Tingle-Map (Great Bay)"
  },
  {
    "value": "185",
    "name": "Tingle-Map (Stone Tower)"
  }
];
