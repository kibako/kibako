/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export const ActorFlags = {
  INVISIBLE: (1 << 7),
};

export enum ActorCategory {
  SWITCH     = 0x00,
  BG         = 0x01,
  PLAYER     = 0x02,
  EXPLOSIVE  = 0x03,
  NPC        = 0x04,
  ENEMY      = 0x05,
  PROP       = 0x06,
  ITEMACTION = 0x07,
  MISC       = 0x08,
  BOSS       = 0x09,
  DOOR       = 0x0A,
  CHEST      = 0x0B,

  SIZE
};