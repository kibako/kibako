/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { actorTable } from "../../decomp/actorTable";
import { objectTableDecomp } from "../../decomp/objectTable";
import { RoomEntry } from "../rooms/roomList";
import { showNotification } from "../../../frontend/layout/Notifications";
import { Context } from "../../context";
import { getGameActorData } from "./actorData";

export function getMaxObjectCount() {
  return Context.isMM() ? 35 : 15;
}

export function fixObjectDependencies(room: RoomEntry, deleteUnused?: boolean): RoomEntry 
{
  room.objectDependencies = [...room.objectDependencies];

  const actorData = getGameActorData();
  const MAX_OBJECTS = getMaxObjectCount();

  //console.log(room.objectDependencies);
  //console.log(room.objectDependencies.map(d => objectTableDecomp.list[d]?.name).sort());

  const deps: string[] = [];

  for(const actor of room.actors)
  {
    const actEntry = actorTable.list[actor.id];
    if(!actEntry)continue;
    const param = actor.initValue;

    // static dependencies: depend purely on the actor itself
    const staticDeps = actorData[actEntry.name].objDeps;
    if(staticDeps && staticDeps.length) {
      deps.push(...staticDeps.filter(d => !d.startsWith("OBJECT_GAMEPLAY") && !d.startsWith("GAMEPLAY_")));
    }

    // add dynamic dependencies: some actors use object dependeing on their params.
    switch(actEntry.name)
    {
      case "ACTOR_EN_ITEM00":
        if(param & 0x0003)deps.push("OBJECT_GI_HEART");
      break;

      case "ACTOR_EN_SW": // Gold-Skulltula -> Gold-Skulltula Token
        if(param & 0x8000)deps.push("OBJECT_GI_SUTARU");
      break;

      case "ACTOR_OBJ_TSUBO": // Pot, has a dungeon and non-dungeon variant
        if(param & 0x0100)deps.push("OBJECT_TSUBO");
      break;
    }

    // @TODO: neighbor-room dependencies
  }

  if(deleteUnused) {
    room.objectDependencies = [];
  }

  // insert new dependencies
  for(const dep of deps) 
  {
    const depId = objectTableDecomp.enum[dep];
    if(depId && !room.objectDependencies.includes(depId)) 
    {
      // can't add more objects, show warning and ignore other objects
      if(room.objectDependencies.length >= MAX_OBJECTS) {
        showNotification('error', "Can't add "+dep+"\n Object-List is full");
        break;
      }

      console.log("Added new dep.: ", dep, depId);
      room.objectDependencies.push(depId);
    }
  }

  //console.log(uniqueDeps.sort());
  //console.log(uniqueDeps.map(d => objectTableDecomp.enum[d]));

  return room;
};