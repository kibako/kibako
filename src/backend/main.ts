/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { BinaryFile } from './utils/binaryFile';
import { readRomInfo } from './n64/rom';
import { ObjectTable, loadObjectTable } from './z64/actors/objectTable';
import { ActorOverlayTable, loadActorOverlayTable } from './z64/actors/actorOverlayTable';
import { RomInfo } from './n64/rom';
import { parseObjectTable } from './decomp/objectTable';

export let objectTable: ObjectTable;
export let actorOverlayTable: ActorOverlayTable;

export const loadRom = async (romBuffer: Uint8Array): Promise<RomInfo> => {
    const file = new BinaryFile(romBuffer);
    // already load global stuff
    const romInfo = readRomInfo(file);
    if(romInfo.id !== "ZL") { // @TODO: check if this is useful
        console.warn("ROM might not be compatible!");
        console.warn({romInfo});
    }

    objectTable = await loadObjectTable(file);
    actorOverlayTable = await loadActorOverlayTable(file);
    await parseObjectTable();

    return romInfo;
};
