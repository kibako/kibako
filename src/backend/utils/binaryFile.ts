/**
* @copyright 2018 - 2020 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Vector2, Vector3, Vector4, VectorRGBFloat, VectorRGBAFloat } from "backend/z64/types";
import { vec2, vec3 } from "gl-matrix";

/**
 * @TODO
 * Since this was ported over from an old JS project (ice-spear), 
 * refactoring + documentation would be nice.
 * Some stuff in here is completely unused or too complicated for this project.
 */

type FunctionRead = (offset: number, isLE?: boolean) => any | undefined;
type FunctionWrite = (offset: number, value: any, isLE?: boolean) => void;

export enum TypeSize {
    U8 = 1,
    S8 = 1,
    U16 = 2,
    S16 = 2,
    U32 = 4,
    S32 = 4,
    F32 = 4,
    U64 = 8,
    S64 = 8,
    F64 = 8,
};

export type Type = 'u8' | 's8' | 'u16' | 's16' | 'u32' | 's32' | 'u64' | 's64' | 'f32' | 'f64';

interface DataTypeEntry {
    read : FunctionRead;
    write: FunctionWrite;
    size : number;
};

export enum Endian {
    LITTLE = 0,
    BIG = 1,
};

export class BinaryFile
{
    buffer: Uint8Array;
    bufferView: DataView;
    endian: Endian = Endian.BIG;
    // If little endian is on, this set the word size to 16bit
    mixedEndian: boolean = false;
    isLE: boolean = true;

    offset = 0;
    maxOffset: number;
    offsetStack: number[] = [];

    types: Record<any, DataTypeEntry>;
    textDecoder = new TextDecoder('utf8');

    constructor(inputBuffer: Uint8Array)
    {
        this.buffer = inputBuffer;
        this.maxOffset = this.buffer.length;
        this.bufferView = new DataView(this.buffer.buffer);

        // data types
        this.types = {
            u8  : {
                read : this.bufferView.getUint8,
                write: this.bufferView.setUint8,
                size : TypeSize.U8
            },
            s8  : {
                read : this.bufferView.getInt8,
                write: this.bufferView.setInt8,
                size : TypeSize.S8
            },
            u16  : {
                read : this.bufferView.getUint16,
                write: this.bufferView.setUint16,
                size : TypeSize.U16
            },
            s16  : {
                read : this.bufferView.getInt16,
                write: this.bufferView.setInt16,
                size : TypeSize.S16
            },
            u32  : {
                read : this.bufferView.getUint32,
                write: (offset, value, isLE) => this.bufferView.setUint32(offset, value >>> 0, isLE),
                size : TypeSize.U32
            },
            s32  : {
                read : this.bufferView.getInt32,
                write: this.bufferView.setInt32,
                size : TypeSize.S32
            },
            f32  : {
                read : this.bufferView.getFloat32,
                write: this.bufferView.setFloat32,
                size : 4
            },
            f64  : {
                read : this.bufferView.getFloat64,
                write: this.bufferView.setFloat64,
                size : 8
            },
        };
    }

    setEndian(endian: Endian): BinaryFile
    {
        this.endian = endian
        this.isLE = this.endian === Endian.LITTLE;
        return this;
    }

    setMixedEndian(isMixed: boolean): BinaryFile {
        this.mixedEndian = isMixed;
        return this;
    }

    /**
     * Puts the current offset on the stack, but keeps it
     */
    posPush()
    {
        this.offsetStack.push(this.pos());
    }

    /**
     * Puts the current offset on the stack an jumps to the specified offset
     * @param offset offset to jump to
     */
    posPushAndSet(offset: number)
    {
        this.posPush();
        this.pos(offset);
    }

    /**
     * Pops the offset stack and jumps back to that position
     */
    posPop()
    {
        let offset = this.offsetStack.pop();
        if(offset != null)
            this.pos(offset);
    }

    /**
     * Convenience function to temporarily jump to an offset, read data, and jump back again.  
     * @param offset offset to temp. jump to
     * @param fn code to execute at that offset
     */
    readAt(offset: number, fn: (f: BinaryFile) => void)
    {
        this.posPushAndSet(offset);
        fn(this);
        this.posPop();
    }

    alignTo(bytes: number, fillByte?: number)
    {
        const targetPos = Math.ceil(this.pos() / bytes) * bytes;
        
        if(fillByte === undefined) {
            this.pos(targetPos);
        } else {
            for(let i=this.pos(); i<targetPos; ++i) {
                this.write('u8', fillByte);
            }
        }
        
        if(this.offset >= this.maxOffset)
            this.maxOffset = Math.max(0, this.offset);
    }

    skip(bytes: number)
    {
        this.pos(this.offset + bytes);
    }

    fill(byte: number, length: number)
    {
        for(let i=0; i<length; ++i) {
            this.write('u8', byte);
        }
    }

    pos(newPos?: number): number
    {
        if(newPos !== undefined) {
            this.offset = newPos;
            this.maxOffset = Math.max(this.maxOffset, this.offset);
        }
            
        return this.offset;
    }

    read<T extends number = number>(type: Type): T
    {
        if(this.offset >= this.buffer.length) {
            console.error("PEOF: " + this.offset);
            throw new Error("BinaryFile: EOF reached!");
        }

        let typeObj = this.types[type];
        if(!typeObj)return 0 as T;

        let val = typeObj.read.call(this.bufferView, this.offset, this.isLE);
        this.offset += typeObj.size;
        return val;
    }

    readAlign(type: Type, alignment: number) {
        const val = this.read(type);
        this.alignTo(alignment);
        return val;
    }

    readAndSkip(type: Type, skip: number) {
        const val = this.read(type);
        this.skip(skip);
        return val;
    }

    readArray(type: Type, length: number): number[]
    {
        const data = new Array<number>(length);
        for(let i=0; i<length; ++i) {
            data[i] = this.read(type);
        }
        return data;
    }

    readVector2(type: Type): Vector2
    {
        return [this.read(type), this.read(type)];
    }

    readVector3(type: Type): Vector3
    {
        return [this.read(type), this.read(type), this.read(type)];
    }

    readVector4(type: Type): Vector4
    {
        return [this.read(type), this.read(type), this.read(type), this.read(type)];
    }

    readRGB8(): VectorRGBFloat
    {
        return [
            this.read('u8') / 0xFF, 
            this.read('u8') / 0xFF, 
            this.read('u8') / 0xFF,
        ];
    }

    readRGBA8(): VectorRGBAFloat
    {
        return [
            this.read('u8') / 0xFF, 
            this.read('u8') / 0xFF, 
            this.read('u8') / 0xFF, 
            this.read('u8') / 0xFF, 
        ];
    }

    getDataView(posStart: number, posEnd: number)
    {
        return new DataView(
            this.buffer.buffer,
            posStart, 
            posEnd - posStart 
        );
    }

    readString(size = -1): string
    {
        let zeroEnding = false;

        if(size < 0)
        {
            let strBuff = this.buffer.slice(this.offset);
            size = strBuff.findIndex(val => val === 0);
            if(size < 0) {
                size = this.buffer.length - this.offset;
            }
            zeroEnding = true;
        }

        this.offset += size + (zeroEnding ? 1 : 0);
        return this.textDecoder.decode(this.buffer.slice(this.offset, this.offset + size));
    }

    readU32String(): string
    {
        const val = this.read('u32');
        return String.fromCharCode((val >> 24) & 0xFF) +
               String.fromCharCode((val >> 16) & 0xFF) +
               String.fromCharCode((val >>  8) & 0xFF) +
               String.fromCharCode((val >>  0) & 0xFF);
    }

    readU16String(): string
    {
        const val = this.read('u16');
        return String.fromCharCode((val >>  8) & 0xFF) +
               String.fromCharCode((val >>  0) & 0xFF);
    }

    write(type: string, value: any)
    {
        let typeObj = this.types[type];
        if(typeObj == null)
            return 0;

        if((this.offset + typeObj.size) >= this.buffer.length) {
            //this.resizeBuffer(this.buffer.length + BUFFER_SIZE_INCREMENT);
            throw new Error("BinaryFile buffer full!");
        }

        typeObj.write.call(this.bufferView, this.offset, value, this.isLE);

        this.offset += typeObj.size;
        if(this.offset > this.maxOffset)this.maxOffset = this.offset;
    }

    writeU8(value: any) {
        this.bufferView.setInt8(this.offset++, value);
    }

    writeVector2(type: Type, vec3: Vector2|vec2)
    {
        this.write(type, vec3[0]);
        this.write(type, vec3[1]);
    }

    writeVector3(type: Type, vec3: Vector3|vec3)
    {
        this.write(type, vec3[0]);
        this.write(type, vec3[1]);
        this.write(type, vec3[2]);
    }

    writeStackAllocator(offset: number, size: number) {
        this.write('u32', size); // buffer-size
        this.write('u32', offset); // buffer-pointer
        this.write('u32', offset); // current buffer-pointer (will move when filled)
        this.write('u32', offset + size); // buffer end-pointer
    };
    

    clone(): BinaryFile
    {
        const newFile = new BinaryFile(this.buffer);
        newFile.setEndian(this.endian);
        newFile.pos(this.pos());
        return newFile;
    }
};
