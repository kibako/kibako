/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Vector3 } from "backend/z64/types";

const DEFAULT_MIN_VAL = 9999999.0;
const DEFAULT_MAX_VAL = -9999999.0;

export interface AABB {
  min: Vector3;
  max: Vector3;
}

export function createAABB(): AABB {
  return {
    min: [DEFAULT_MIN_VAL, DEFAULT_MIN_VAL, DEFAULT_MIN_VAL],
    max: [DEFAULT_MAX_VAL, DEFAULT_MAX_VAL, DEFAULT_MAX_VAL],
  };
}

export function aabbConvertEmpty(a: AABB) {
  for(let i=0; i<3; ++i) { 
    if(a.min[i] >= DEFAULT_MIN_VAL)a.min[i] = -0.5;
    if(a.max[i] <= DEFAULT_MAX_VAL)a.max[i] = 0.5;
  }
}

export function aabbGetCenter(a: AABB): Vector3 {
  return [
    (a.max[0] + a.min[0]) * 0.5,
    (a.max[1] + a.min[1]) * 0.5,
    (a.max[2] + a.min[2]) * 0.5,
  ];
}

export function aabbGetExtend(a: AABB, factor: number = 1.0): Vector3 {
  return [
    (a.max[0] - a.min[0]) * factor,
    (a.max[1] - a.min[1]) * factor,
    (a.max[2] - a.min[2]) * factor,
  ];
}
