/**
* @copyright 2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { Vector3 } from "backend/z64/types";

export const vec3Add = (a: Vector3, b: Vector3): Vector3 => [
  a[0] + b[0],
  a[1] + b[1],
  a[2] + b[2],
];

export const vec3Diff = (a: Vector3, b: Vector3): Vector3 => [
    a[0] - b[0],
    a[1] - b[1],
    a[2] - b[2],
];

export const vec3Cross = (a: Vector3, b: Vector3): Vector3 => [
    a[1] * b[2]  - a[2] * b[1],
    a[2] * b[0]  - a[0] * b[2],
    a[0] * b[1]  - a[1] * b[0],
];

export const vec3Length = (a: Vector3): number => Math.sqrt(
    a[0]*a[0] + a[1]*a[1] + a[2]*a[2]
);

export const vec3Normalize = (a: Vector3): Vector3 => {
    let len = vec3Length(a);
    if(len === 0)return a;
    return [
        a[0] / len,
        a[1] / len,
        a[2] / len,
    ];
};

export const triangleNormal = (a: Vector3, b: Vector3, c: Vector3): Vector3 => 
    vec3Normalize(
        vec3Cross(
            vec3Diff(a, b),
            vec3Diff(a, c),
        )
    );

export const loopUInt = (val: number, max: number) => {
    max += 1;
    return val < 0 ? (max - (Math.abs(val)) % max) : (val % max);
}

export const intoToLetterAZ = (val: number) => 
{
    if(!Number.isInteger(val))return val + "";
    
    let result = '';
    for(let i=0; i<10; ++i) {
        if(val < 0)break;
        const remainder = val % 26;
        result = String.fromCharCode(65 + remainder) + result;
        val = Math.floor(val / 26) - 1;
    }
    return result;
};