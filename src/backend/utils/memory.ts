/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

export const memcpy = (dst: Uint8Array, src: Uint8Array, size: number, offsetDst: number = 0, offsetSrc: number = 0) => 
{
  if(size > dst.length || size > src.length) {
    console.warn("memcpy failed, size too big!");
    return;
  }

  dst.set(src.subarray(offsetSrc, offsetSrc + size), offsetDst);
};