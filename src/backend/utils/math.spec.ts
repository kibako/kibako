/**
* @copyright 2023 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

import { intoToLetterAZ, loopUInt } from './math';

describe('Utils - Math', () => 
{
  it('loopUInt', () => {
    expect(loopUInt(0, 0)).toEqual(0);

    expect(loopUInt(0, 5)).toEqual(0);
    expect(loopUInt(1, 5)).toEqual(1);
    expect(loopUInt(4, 5)).toEqual(4);
    expect(loopUInt(5, 5)).toEqual(5);
    expect(loopUInt(6, 5)).toEqual(0);

    expect(loopUInt(-1, 5)).toEqual(5);
    expect(loopUInt(-4, 5)).toEqual(2);
    expect(loopUInt(-5, 5)).toEqual(1);
    expect(loopUInt(-6, 5)).toEqual(6);
  });

  it('intoToLetterAZ', () => {
    expect(intoToLetterAZ(0)).toEqual('A');
    expect(intoToLetterAZ(25)).toEqual('Z');
    expect(intoToLetterAZ(26)).toEqual('AA');
    expect(intoToLetterAZ(51)).toEqual('AZ');
    expect(intoToLetterAZ(52)).toEqual('BA');
    expect(intoToLetterAZ(701)).toEqual('ZZ');
    expect(intoToLetterAZ(702)).toEqual('AAA');

    expect(intoToLetterAZ(Infinity)).toEqual('Infinity');
    expect(intoToLetterAZ(-Infinity)).toEqual('-Infinity');
    expect(intoToLetterAZ(NaN)).toEqual('NaN');
  });
});
