/**
 * @copyright 2020 - Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" file in the root directory
 */
type u16 = number;
//type s16 = number;
type u32 = number;
type s32 = number;
type u64 = BigInt;
type s64 = BigInt;
type f32 = number;
type f64 = number;

export const RAM_SIZE = (1024 * 1024 * 8);
export const ROM_SIZE = (1024 * 1024 * 64);

/**
 * Interface of all functions exported by the WASM module.
 */
export interface MipsiInstanceExports
{
  setLogLevel: (level: u32) => void;
  reset: () => void;
  clearCache: () => void;
  init: () => void;
  boot: () => void;
  setPc: (pc: u32) => void;
  getPc: () => u32;
  run: (limit: u32) => void;
  getCommandCount: () => u32;
  callFunction: (pc: u32, stackPointer: u32, limit: u32, argCount: u32, arg0: u32, arg1: u32, arg2: u32, arg3: u32) => void;
  mockFunction: (address: u32) => void;
  getArgFromStackU32: (idx: u32) => u32;
  getArgFromStackF32: (idx: u32) => f32;
  interrupt: (interruptType: u32, rcpType: u32) => void;
  render: () => void;
  getFramebufferAddress: () => u32;
  setGameControllerButton: (idx: u32, button: u16) => void;
  connectGameController: (idx: u32) => void;
  disconnectGameController: (idx: u32) => void;
  setGameControllerJoystick: (idx: u32, x: f32, y: f32) => void;
  cpuLogState: ()=> void;
  fpuLogState: ()=> void;
  printNextInstructions: (count: s32) => void;

  cpuGetRegisterU32: (reg: s32) => u32;
  cpuGetRegisterS32: (reg: s32) => s32;
  cpuGetRegisterU64: (reg: s32) => u64;
  cpuGetRegisterS64: (reg: s32) => s64;

  cpuSetRegisterU32: (reg: s32, value: u32) => void;
  cpuSetRegisterS32: (reg: s32, value: s32) => void;
  cpuSetRegisterU64: (reg: s32, value: u64) => void;
  cpuSetRegisterS64: (reg: s32, value: s64) => void;

  fpuGetRegisterF32: (reg: s32) => f32;
  fpuGetRegisterF64: (reg: s32) => f64;
  fpuGetRegisterS32: (reg: s32) => s32;
  fpuGetRegisterS64: (reg: s32) => s64;

  fpuSetRegisterF32: (reg: s32, value: f32) => void;
  fpuSetRegisterF64: (reg: s32, value: f64) => void;
  fpuSetRegisterS32: (reg: s32, value: s32) => void;
  fpuSetRegisterS64: (reg: s32, value: s64) => void;

  z64OverlayReallocate: (ramAddress: u32, size: u32, addressDiff: u32) => void;
  z64TextureUpscaleClamp: (sizeInX: u32, sizeInY: u32, sizeOutX: u32, sizeOutY: u32) => u32;
  z64TextureUpscaleWrapU: (sizeInX: u32, sizeInY: u32, sizeOutX: u32, sizeOutY: u32) => u32;
  z64TextureUpscaleWrapV: (sizeInX: u32, sizeInY: u32, sizeOutX: u32, sizeOutY: u32) => u32;
  z64TextureUpscaleWrapUV: (sizeInX: u32, sizeInY: u32, sizeOutX: u32, sizeOutY: u32) => u32;
  z64TextureGetUpscaleInputPtr: () => u32;

  gpuF3DEX2ParseDisplayList: (address: u32) => u32;
  gpuF3DEX2SetSegmentAddress: (segment: u32, address: u32) => void;
  gpuF3DEX2SyncSegmentAddress: (ramAddress: u32) => void;
  gpuF3DEX2Reset: () => void;
  gpuF3DEX2GetOutputPointer: () => u32;

  memory: WebAssembly.Memory;
};

/**
 * Interface of all functions imported by the WASM module.
 */
export interface MipsiInstanceImports
{
  hook_inject?: (pc: number, arg0: number, arg1: number, arg2: number, arg3: number) => void;
  log_call?: (pc: number) => void;
  print_char?: (c: number) => void;
  debugger_hook_step?: (pc: number) => void;
  debugger_hook_mem_write?: (address: number, value: number) => void;
  debugger_hook_stack_push?: (address: number) => void;
  debugger_hook_stack_pop?: () => void;
  debugger_hook_event?: (type: number) => void;

  gpuEmitTexture?: (address: u32, width: u32, height: u32, format: number, bpp: number) => void;
};

export interface MipsiInstance {
  exports: MipsiInstanceExports;
  memory: Uint8Array;
  calls: number[];
};

let wasmModule: WebAssembly.Module | undefined;

export async function createMipsiInstance(
  debugMode = false, 
  envImports: MipsiInstanceImports = {})
{
  debugMode = debugMode || !!process.env.DEBUG_MODE;
  const calls: number[] = [];

  if(!envImports.debugger_hook_step)envImports.debugger_hook_step = () => {};
  if(!envImports.debugger_hook_mem_write)envImports.debugger_hook_mem_write = () => {};
  if(!envImports.hook_inject)envImports.hook_inject = () => {};
  if(!envImports.debugger_hook_event)envImports.debugger_hook_event = () => {};
  if(!envImports.debugger_hook_stack_push)envImports.debugger_hook_stack_push  = () => {};
  if(!envImports.debugger_hook_stack_pop)envImports.debugger_hook_stack_pop  = () => {};
  if(!envImports.gpuEmitTexture)envImports.gpuEmitTexture = () => {};

  let outputBuffer = "";
  const print_char = (typeof window === 'undefined')
    ? (charCode: number) => process.stdout.write(String.fromCharCode(charCode)) 
    : (c: number) => {
      const char = String.fromCharCode(c);
      if (char === "\n") {
        console.log(outputBuffer);
        outputBuffer = "";
      } else {
        outputBuffer += char;
      }
    };

  const moduleName = debugMode ? "kibako64.debug.wasm" : "kibako64.wasm";

  if(!wasmModule) {
    wasmModule = await WebAssembly.compileStreaming(fetch("wasm/"+moduleName));
  }

  const instance = await WebAssembly.instantiate(wasmModule, { env: {
    print_char: envImports.print_char || print_char,
    log_call: (pc: number) => calls.push(pc >>> 0),
    hook_inject: envImports.hook_inject,

    debugger_hook_step: envImports.debugger_hook_step,
    debugger_hook_mem_write: envImports.debugger_hook_mem_write,
    debugger_hook_event: envImports.debugger_hook_event,
    debugger_hook_stack_push: envImports.debugger_hook_stack_push,
    debugger_hook_stack_pop: envImports.debugger_hook_stack_pop,
    gpuEmitTexture: envImports.gpuEmitTexture,
  }});

  const exports = instance.exports as unknown as MipsiInstanceExports;

  const memory = instance.exports.memory as any;
  const memoryBuffer = new Uint8Array(memory.buffer);

  console.log("Kibako64 Memory: total=%fmb, RAM=%fmb, ROM=%fmb noRamRom=%fmb (b:%d)", 
    memoryBuffer.byteLength / 1024 / 1024,
    RAM_SIZE / 1024 / 1024,
    ROM_SIZE / 1024 / 1024,
    (memoryBuffer.byteLength - RAM_SIZE - ROM_SIZE) / 1024 / 1024,
    (memoryBuffer.byteLength - RAM_SIZE - ROM_SIZE));

  memoryBuffer.fill(0, 0, RAM_SIZE);

  exports.setLogLevel(0b11);
  //exports.setLogLevel(0xFFFFFFFF);
  exports.init();

  return {exports, memory: memoryBuffer, calls};
}