/**
* @copyright 2018-2020 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*
* Note: This was ported over from my other project 'ice-spear'.
*/

export const YAZ0_HEADER_SIZE = 16;
const YAZ0_MAGIC = 0x59_61_7A_30; // "Yaz0"

interface Yaz0Header {
    magic: number,
    size: number,
    flags: number[],
};

export const createYaz0Header = (size: number, flags?: number[]) => ({
    magic: YAZ0_MAGIC, size,
    flags: flags ? flags : [0,0,0,0, 0,0,0,0]
});

export const getYaz0Header = (view: DataView): Yaz0Header | undefined => {
    const header: Yaz0Header = {
        magic: view.getUint32(0, false),
        size: view.getUint32(4, false),
        flags: [
            view.getUint8(8),  view.getUint8(9),  view.getUint8(10), view.getUint8(11),
            view.getUint8(12), view.getUint8(13), view.getUint8(14), view.getUint8(15),
        ],
    };

    if (header.magic !== YAZ0_MAGIC) {
        return undefined;
    }

    return header;
};

export const yaz0Decode = (yaz0Header: Yaz0Header, viewIn: DataView, viewOut: DataView)=> 
{
    let viewInPos = YAZ0_HEADER_SIZE;
    let viewOutPos = 0;
    let copyLength = 0;

    if(yaz0Header.size < 1)return; // empty or invalid file

    mainLoop:
    for(;;) 
    {
        let header = viewIn.getUint8(viewInPos++);

        for(let i=7; i>=0; --i)
        {
            if((header >> i) & 1)
            {
                viewOut.setUint8(viewOutPos++, viewIn.getUint8(viewInPos++));
            }else{
                const sizeMarker = viewIn.getUint8(viewInPos++);
                let copyOffset = (((sizeMarker & 0xF) << 8) | viewIn.getUint8(viewInPos++)) + 1;

                if((sizeMarker >> 4) === 0) // has 3 bytes
                {
                    copyLength = viewIn.getUint8(viewInPos++) + 0x12;
                }else{
                    copyLength = (sizeMarker >> 4) + 0x02;
                }

                const endPos = Math.min(viewOutPos + copyLength, yaz0Header.size);
                for(; viewOutPos<endPos; ++viewOutPos)
                {
                    viewOut.setUint8(viewOutPos, viewOut.getUint8(viewOutPos - copyOffset));
                }
            }

            if(viewOutPos >= yaz0Header.size)
                break mainLoop;
        }
    }
};
